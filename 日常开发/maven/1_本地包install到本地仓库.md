#### 本地包install到本地仓库

进入到jar包的目录，在当前目录打开cmd或者用git工具也行



**安装命令：**

`-DgroupId`：对应`pom`里的`groupId`

`-DartifactId`：对应`pom`里的`artifactId`

`-Dversion`：对应`pom`里的`version`

`-Dpackaging`：包类型

`-Dfile`：jar包路径地址



```xml
mvn install:install-file -DgroupId=com.xxx -DartifactId=api-xxx -Dversion=1.0.0 -Dpackaging=jar -Dfile=D:/jar/api-xxx-1.0.0.jar
```



**`pom`里面引用：**

```xml
<dependency>
    <groupId>com.xxx</groupId>
    <artifactId>api-xxx</artifactId>
    <version>api-xxx-1.0.0</version>
</dependency>
```

