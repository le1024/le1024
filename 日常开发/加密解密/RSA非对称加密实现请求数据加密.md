> RSA非对称加密实现请求数据加密

#### 需求：

在进行登录时，用户名和密码一般都是明文传输的，这样会造成密码泄露的风险，所以需要对请求数据进行加密传输



#### 实现方案：

选用`RSA`非对称加密实现请求数据加密，是因为非对称加密是同时需要公钥和私钥且必须是一对的。公钥可以对外暴露供多人使用，但私钥需要保持其私有性，不允许公开。



#### 实现示例：

##### 1.创建工程

`application.yml`

```yaml
server:
  port: 8080
  servlet:
    #配置项目访问名称,不配置默认为/
    context-path: /rsa
spring:
  thymeleaf:
    #是否开启缓存，开发时建议为false，默认为true
    cache: false
    #检查模板是否存在，默认为true
    check-template: true
    #检查模板位置是否存在，默认为true
    check-template-location: true
    #编码
    encoding: UTF-8
    #模板文件位置
    prefix: classpath:/templates/
    #content-type配置
    servlet:
      content-type: text/html
    #后缀
    suffix: .html
```

##### 2.pom

```xml
<dependencies>
    <dependency>
        <groupId>org.springframework.boot</groupId>
        <artifactId>spring-boot-starter-web</artifactId>
    </dependency>
    <dependency>
        <groupId>org.springframework.boot</groupId>
        <artifactId>spring-boot-starter-thymeleaf</artifactId>
    </dependency>
</dependencies>

<build>
    <finalName>rsa</finalName>
    <plugins>
        <plugin>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-maven-plugin</artifactId>
        </plugin>
    </plugins>
</build>
```

##### 3.创建加密工具类

`RSAUtil`

```java
public class RSAUtil {

    public static int KEY_SIZE = 1024;

    public static String ALGORITHM = "RSA";

    /**
     * 生成秘钥对
     */
    public static KeyPair getKeyPair() {
        try {
            KeyPairGenerator keyPairGenerator = KeyPairGenerator.getInstance(ALGORITHM);
            keyPairGenerator.initialize(KEY_SIZE);
            return keyPairGenerator.generateKeyPair();
        } catch (NoSuchAlgorithmException e1) {
            e1.printStackTrace();
        }
        return null;
    }

    /**
     * 获取公钥 base64编码
     */
    public static String getPublicKey(KeyPair keyPair) {
        PublicKey publicKey = keyPair.getPublic();
        byte[] bytes = publicKey.getEncoded();
        return byteToBase64(bytes);
    }

    /**
     * 获取私钥 base64编码
     */
    public static String getPrivateKey(KeyPair keyPair) {
        PrivateKey privateKey = keyPair.getPrivate();
        byte[] bytes = privateKey.getEncoded();
        return byteToBase64(bytes);
    }

    /**
     * 将Base64编码后的公钥转换成publicKey对象
     */
    public static PublicKey stringToPublicKey(String pubStr) throws NoSuchAlgorithmException, InvalidKeySpecException, IOException {
        byte[] keyBytes = base64ToByte(pubStr);
        X509EncodedKeySpec keySpec = new X509EncodedKeySpec(keyBytes);
        KeyFactory keyFactory = KeyFactory.getInstance(ALGORITHM);
        return keyFactory.generatePublic(keySpec);
    }

    /**
     * 将Base64编码后的私钥转换成privateKey对象
     */
    public static PrivateKey stringToPrivateKey(String priStr) throws NoSuchAlgorithmException, InvalidKeySpecException, IOException {
        byte[] keyBytes = base64ToByte(priStr);
        PKCS8EncodedKeySpec keySpec = new PKCS8EncodedKeySpec(keyBytes);
        KeyFactory keyFactory = KeyFactory.getInstance(ALGORITHM);
        return keyFactory.generatePrivate(keySpec);
    }

    /**
     * 公钥加密
     */
    public static byte[] publicEncrypt(byte[] content, PublicKey publicKey) throws Exception {
        Cipher cipher = Cipher.getInstance(ALGORITHM);
        cipher.init(Cipher.ENCRYPT_MODE, publicKey);
        return cipher.doFinal(content);
    }

    /**
     * 私钥解密
     */
    public static byte[] privateDecrypt(byte[] content, PrivateKey privateKey) throws Exception {
        Cipher cipher = Cipher.getInstance(ALGORITHM);
        cipher.init(Cipher.DECRYPT_MODE, privateKey);
        return cipher.doFinal(content);
    }

    /**
     * 字节数组转base64编码
     */
    public static String byteToBase64(byte[] bytes) {
        BASE64Encoder encoder =new BASE64Encoder();
        return encoder.encode(bytes);
    }

    /**
     * base64编码转字节数组
     */
    public static byte[] base64ToByte(String base64Str) throws IOException {
        BASE64Decoder decoder = new BASE64Decoder();
        return decoder.decodeBuffer(base64Str);
    }
}
```

`KeyManager`

```java
public class KeyManager {

    static{
        // 生成秘钥对
        // 可以根据别的方式自己的公钥私钥
        KeyPair keyPair = RSAUtil.getKeyPair();
        if (keyPair == null) {
            throw new ArithmeticException("get key error.");
        }
        // 获取私钥
        String privateKey = RSAUtil.getPrivateKey(keyPair);
        // 获取公钥
        String publicKey = RSAUtil.getPublicKey(keyPair);
        // 设置私钥
        KeyManager.setPrivateKey(privateKey);
        // 设置公钥
        KeyManager.setPublicKey(publicKey);
    }

    /**
     * 公钥
     */
    public static String publicKey;
    /**
     * 私钥
     */
    public static String privateKey;

    public static String getPublicKey() {
        return publicKey;
    }

    public static void setPublicKey(String publicKey) {
        KeyManager.publicKey = publicKey;
    }

    public static String getPrivateKey() {
        return privateKey;
    }

    public static void setPrivateKey(String privateKey) {
        KeyManager.privateKey = privateKey;
    }
}
```

##### 5.接口类

```java
@Slf4j
@RestController
public class LoginController {

    /**
     * 获取publicKey
     */
    @PostMapping("/publicKey")
    public String getPublicKey() {
        return KeyManager.getPublicKey();
    }

    /**
     * 登录
     */
    @PostMapping("/login")
    public String login(String username, String password) throws Exception {
        log.info("加密的username: " + username);
        log.info("加密的password: " + password);

        // 通过私钥获取到PrivateKey对象
        PrivateKey privateKey = RSAUtil.stringToPrivateKey(KeyManager.getPrivateKey());
        // 加密后的内容base64解码
        byte[] usernameByte =RSAUtil.base64ToByte(username);
        byte[] passwordByte =RSAUtil.base64ToByte(password);
        // 用私钥解密
        byte[] usernameDec = RSAUtil.privateDecrypt(usernameByte, privateKey);
        byte[] passwordDec = RSAUtil.privateDecrypt(passwordByte, privateKey);
        //解密后的原文

        username = new String(usernameDec);
        password = new String(passwordDec);
        log.info("解密的username：" + username);
        log.info("解密的password：" + password);

        if (Objects.equals("admin", username) && Objects.equals("123456", password)) {
            return "登录成功";
        }
        return "登录失败";
    }
}
```

#####  6.登录页面

`index.html`

```html
<html>
<body>
	<table>
		<tr>
			<td>用户名</td>
			<td><input type="text" name="username" id="username" /></td>
		</tr>
		<tr>
			<td>密码</td>
			<td><input type="password" name="password" id="password" /></td>
		</tr>
		<tr>
			<td><input type="button" value="登录" onclick="login()"></td>
		</tr>
	</table>
	<script src="https://cdn.bootcss.com/jquery/3.3.1/jquery.min.js"></script>
	<script src="rsa.js" type="text/javascript"></script>
	<script>
	//定义公钥
	let publicKey ;
	$(//获取公钥
		$.ajax({
            url:'publicKey',
            type:'post',
            success:function(res){
				publicKey = res;
            }
        })
	)
	//登录
    function login(){
    	const encrypt=new JSEncrypt();
    	encrypt.setPublicKey(publicKey);
		const username　=　encrypt.encrypt($('#username').val());
		const password　=　encrypt.encrypt($('#password').val());
		console.log("username：" + username);
		console.log("password：" + password);
    	$.ajax({
    		url:'login',
    		type:'post',
    		data:{'username': username,'password':password},
    		success:function(res){
    			console.log(res);
    		}
    	})
    }
    </script>
</body>
</html>
```

前端需要用引用`jsencrypt.js`，网上找到的js文件加密报错`Message too long for RSA`，下面给出本示例中使用的js文件：

🚀链接：https://www.aliyundrive.com/s/CuY8SuzS6cd



关于`jsencrypt.js`的使用可以参考：

https://www.npmjs.com/package/encryptlong



#### 测试

启动项目，访问：http://localhost:8080/rsa/

![image-20211222162155155](https://cdn.jsdelivr.net/gh/le1024/image1/le/image-20211222162155155.png)

![image-20211222162217949](https://cdn.jsdelivr.net/gh/le1024/image1/le/image-20211222162217949.png)

