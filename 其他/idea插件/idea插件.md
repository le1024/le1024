#### idea插件

------



>  **Atom Material Icons**：好看的文件夹及其他icon图标

<img src="https://cdn.jsdelivr.net/gh/le1024/image1/le/2208cc91e7bd46808270c8b0ab522bca.png" alt="img" style="zoom:50%;" />



> **Codota**：代码自动补全



> **RestfullToolkit**：Ctrl + / 弹出工具框，输入请求接口地址即可跳转到对应的方法类



> **spring-assistant-@valueToYml**：@value注解的字段，可以快速定位到yml的配置，通过Ctrl+鼠标点击@value的字段跳转



> **MybatisCodeHelperPro**：mybatis的辅助工具



> **Xcode-Dark Theme**：类似Mac的一款主题