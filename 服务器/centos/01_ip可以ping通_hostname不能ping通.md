#### 解决hostname无法ping通的问题

问题：

搭建kafka集群后，无法ping通集群服务中的服务器



前提：

已经为每个服务器设置好了hostname

```bash
#设置主机名
vim /etc/sysconfig/network

#hll1为你的主机名
hostname=hll1
```



解决：

编辑`/etc/hosts`

```bash
vim /etc/hosts
```

```bash
#将集群服务都配置上，集群中的每台机子都需要配置
192.168.171.132 hll1
192.168.171.133 hll2
192.168.171.134 hll3
```

