#### <font color="#FF5151">Linux磁盘分区挂载</font>

------



简单的记录下新磁盘(安装系统后未操作或者新扩容的)的分区格式化及挂载。



**通过虚拟机的来演示下磁盘分区、格式化、挂载、卸载**



##### 添加硬盘

![image-20220427151511211](https://fastly.jsdelivr.net/gh/le1024/image1/le/image-20220427151511211.png)



<img src="https://fastly.jsdelivr.net/gh/le1024/image1/le/image-20220427151708881.png" alt="image-20220427151708881" style="zoom:50%;" />

<img src="https://fastly.jsdelivr.net/gh/le1024/image1/le/image-20220427151853695.png" alt="image-20220427151853695" style="zoom:50%;" />

**之后一直下一步，然后完成新硬盘的添加**



##### 查看新硬盘

重启下服务器，然后用`lsblk -f`查看

![image-20220427152347884](https://fastly.jsdelivr.net/gh/le1024/image1/le/image-20220427152347884.png)

**sdb**就是刚刚新添加的硬盘，是一个新的硬盘，所以相关的属性信息都没有

`FSTYPE`：分区类型

`UUID`：标识分区的唯一标识符

`MOUNTPOINT`：分区挂载的目录



##### 开始分区

分区之前可以用`fdisk -l`

`/dev/sda`已有分区sda1 sda2 sda3

`/dev/sdb`，这个是新添加的没有进行分区过，这个`/dev/sdb`路径要记住，不用弄错了。

![image-20220427153924797](https://fastly.jsdelivr.net/gh/le1024/image1/le/image-20220427153924797.png)

**分区命令：fdisk /dev/sdb** 对新硬盘进行分区，`/dev/sdb`就是查询到的磁盘，不要写错了。如果写成**sda**就会把原先的硬盘是操作了。

输入命令：

```bash
fdisk /dev/sdb
```

`fdisk /dev/sdb`执行后就会进行硬盘的操作，通过输入相关命令进行操作

![image-20220427154429944](https://fastly.jsdelivr.net/gh/le1024/image1/le/image-20220427154429944.png)

> 提示：分区命令的操作，相关命令的执行结果都会存在内存中，直到通过w命令进行写入。所以在确定输入w命令之前请注意。

根据提示**Command (m from help)**，可以通过输入**m**获取帮助

![image-20220427154931404](https://fastly.jsdelivr.net/gh/le1024/image1/le/image-20220427154931404.png)

- m 显示命令帮助列表
- p 显示磁盘分区 同**fdisk -l**
- n 新增分区
- d 删除分区
- w 写入并退出 (执行这条命令一定要确认清楚再操作)
- q 不保存退出



**输入n，并回车**，创建新分区

![image-20220427155958335](https://fastly.jsdelivr.net/gh/le1024/image1/le/image-20220427155958335.png)

**输入p，并回车；或者直接回车，默认就是p，为了方便后面的操作也可以直接回车，使用默认的即可**

如果输入e扩展分区或者设置分区其他的配置，百度，我不会。。。

![image-20220427160649594](https://fastly.jsdelivr.net/gh/le1024/image1/le/image-20220427160649594.png)

当分区完成后

**输入w并回车，完成分区配置**

![image-20220427161019023](https://fastly.jsdelivr.net/gh/le1024/image1/le/image-20220427161019023.png)



##### 查看操作成功的分区

```bash
#两条命令都可以
lsblk -f
lsblk -l
```

![image-20220427161308001](https://fastly.jsdelivr.net/gh/le1024/image1/le/image-20220427161308001.png)

同第一次查询的结果对比，`sdb`已有一个`sdb1`分区，且已分配一个UUID，但挂载的还是空的。



##### 格式化分区

上一步已经成功硬盘分区了，但是还不能使用，没有挂载到具体的目录，在挂载前需要对硬盘进行格式化后才可以挂载。

**格式化命令：mkfs -t ext4 /dev/sdb1**

- mkfs格式化命令
- -t 格式化类型，不加此参数默认是ext2
- /dev/sdb1 分过区的硬盘分区，**注意不要格式化错误了**

![image-20220427163142288](https://fastly.jsdelivr.net/gh/le1024/image1/le/image-20220427163142288.png)

格式化成功就可以使用这款硬盘了



##### 挂载

就是将分区和一个目录关联起来

**挂载命令：mount 设备名称 挂载目录**

```bash
mount /dev/sdb1 /opt
```

**/opt** 可以是新建的一个目录

![image-20220427170028971](https://fastly.jsdelivr.net/gh/le1024/image1/le/image-20220427170028971.png)

**注意：目前只是临时挂载，系统重启后会失效**



**实现永久挂载**

编辑`/etc/fatab`，添加挂载信息，实现永久挂载

```bash
vim /etc/fstab
```

`/etc/fstab` 文件中记录了分区以及挂载点的一些情况

![image-20220427170336660](https://fastly.jsdelivr.net/gh/le1024/image1/le/image-20220427170336660.png)

添加：

```bash
/dev/sdb1 /opt ext4 defaults 0 0
```

`/dev/sdb1`就是格式化过的需要挂载的分区，也可以替换成UUID的格式

![image-20220427170552591](https://fastly.jsdelivr.net/gh/le1024/image1/le/image-20220427170552591.png)

保存并退出，接着输入

```bash
mount -a
```

- -a 就是auto，自动挂载

![image-20220427170719077](https://fastly.jsdelivr.net/gh/le1024/image1/le/image-20220427170719077.png)



##### 取消挂载

**卸载命令：umount 设备名称 | 挂载目录**

```shell
umount /dev/sdb1
umount /opt
```

**注意：不能在需要卸载的目录执行umount，不然会提示目标忙，无法卸载**

