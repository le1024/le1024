### <font color="#FF9224">Centos离线安装gcc环境</font>

------

#### 场景

安装redis或者nginx之类的软件，解压之后需要编译文件，就需要用到gcc这个编译软件，如果linux服务器没有gcc环境的话，就无法完成编译操作。

查看gcc：`gcc -v`

![image-20220427115511992](https://fastly.jsdelivr.net/gh/le1024/image1/le/image-20220427115511992.png)

如果没有则会提示：`gcc 未找到命令`



#### 安装

在有网络的情况下直接使用`yum`命令即可：

```bash
yum install gcc 
```

如果网络情况不允许，需要离线安装

> 下载gcc安装包

[gcc下载地址](https://mirrors.tuna.tsinghua.edu.cn/gnu/gcc/)

> 上传安装包至服务器并解压

```bash
tar -zxvf gcc.tar.gz
```

解压完成后会有个gcc文件夹，进入到gcc文件夹进行安装



> 安装gcc

```shell
cd gcc
#执行安装命令
rpm -Uvh *.rpm --nodeps --force
```

![image-20220427143029413](https://fastly.jsdelivr.net/gh/le1024/image1/le/image-20220427143029413.png)

然后等待安装完成即可