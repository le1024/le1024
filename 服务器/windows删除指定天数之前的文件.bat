@echo off
::删除指定路径下指定天数之前（以文件的最后修改日期为准）的文件。

rem 指定待删除文件的存放路径d:\bak 如果不是相应修改
set DEL_PATH=D:\data\local-mysql\backup\app-test\
rem 指定天数 如7天前
set DAYS=7
forfiles /p %DEL_PATH% /s /m *.nb3 /d -%DAYS% /c "cmd /c del /f @path\"

::pause