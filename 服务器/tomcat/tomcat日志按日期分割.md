### tomcat日志按日期分割

------



#### 安装cronolog

```bash
yum install cronolog
或者下载rpm文件
https://centos.pkgs.org/7/epel-x86_64/cronolog-1.6.2-14.el7.x86_64.rpm.html
```



#### 修改catalina.sh文件

```bash
vim ../tomcat/bin/catalina.sh
```

找到图中的配置：

![image-20220420162748199](https://cdn.jsdelivr.net/gh/le1024/image1/le/image-20220420162748199.png)

<font color="Orange">黄框的内容替换为：</font>

```bash
# cronolog填写你自己的位置，通过whereis cronolog查看具体位置
org.apache.catalina.startup.Bootstrap "$@" start 2>&1 | /usr/sbin/cronolog "$CATALINA_BASE"/logs/catalina.%Y-%m-%d.out >> /dev/null &
      
```

![image-20220420164248901](https://cdn.jsdelivr.net/gh/le1024/image1/le/image-20220420164248901.png)

