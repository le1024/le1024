> spring bean的自动装配

#### ByName

```xml
<bean id="cat" class="com.pojo.Cat">
<!--
	byName：会自动在容器上下文中查找，和自己对象set方法后面的值对应的beanid
	比如：setCat，如果上面的bean的id为cat就会报错无法找到对应的
-->
<bean id="people" class="com.pojo.People" autowire="byName">
    <property></property>
</bean>
```



#### ByType

```xml
<bean id="cat" class="com.pojo.Cat">
<!--
	byType：会自动在容器上下文中查找，和自己对象属性相同的bean
	比如：上面的bean的对象类型是Cat，People对象中也需要有个Cat才可以匹配上，且Cat需要是唯一的
-->
<bean id="people" class="com.pojo.People" autowire="byType">
    <property></property>
</bean>
```



