> spring bean创建的生命周期

spring bean的生命周期主要包含四个阶段：

1. 实例化bean
2. bean属性填充
3. 初始化bean
4. 销毁bean



**1.实例化bean**

当客户向容器请求一个尚未初始化的bean时，或者初始化bean的时候需要注入另外一个尚未初始化的依赖时，容器会先调用一个`doCreateBean()`方法进行实例化，实际就是通过反射的方式创建一个bean对象。



**2.bean填充属性**

bean的实例创建成功后，就会对这个bean对象进行`属性填充`，即注入这个bean依赖的其他bean对象。



**3.初始化bean**

bean对象属性填充完成后，就进行bean对象的初始化操作，分为以下几个步骤：

- 执行`aware`接口的方法。spring会检测该对象是否实现了`xxxAware`接口，通过aware类型的接口可以拿到spring容器的一些资源。比如：**EnvironmentAware**获取环境配置信息，**ResourceLoaderAware**获取classpath路径下的资源，**ServletContextAware**	全局存储信息
- 执行`BeanPostProcessor`的前置处理方法`postProcessBeforeInitialization`，对bean进行一些自定义的前置处理
- 判断bean是否实现了`InitializationBean`接口，如果实现了，将会通过`InitializingBean`的`afterPropertiesSet`初始化方法。`afterPropertiesSet`是为了拿到外界的某个对象的某个属性
- 执行用户自定义的初始化方法，如`init-method`，`@PostConstruct`
- 执行`BeanPostProcessor`的后置处理方法`postProcessAfterInitialization`



**4.销毁bean**

初始化完成后，bean就创建完成，就可以使用这个bean了。当不需要bean时，会进行销毁操作。

实现`DisposableBean`接口，在对象销毁前就会调用`destory()`方法，`@PreDestory`

