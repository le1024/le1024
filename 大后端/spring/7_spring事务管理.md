#### spring如何处理事务

spring支持编程式事务管理和声明式事务管理两种方式：



##### 编程式事务

使用`TransactionTemplate`来手动控制事务，针对业务代码块作处理

![img](https://s2.loli.net/2022/04/17/2WrXmOfJ6l9jnw5.png)



##### 声明式事务

是spring基于AOP机制实现的事务机制。不需要在业务代码块中任务管理。声明式事务只能针对方法级别。



#### spring事务的传播级别

spring对事务定义了不同的传播级别：`Propagation`

**`PROPAGATION_REQUIRED`**：默认的传播行为。如果当前没有事务，就创建一个新的事务。如果当前已有事务就加入当前事务。

**`PROPAGATION_SUPPORTS`**：如果当前存在事务，就加入该事务。如果当前不存在事务，就以非事务方式运行。

**`PROPAGATION_MANDATORY`**：如果当前存在事务，就加入该事务。如果当前不存在事务，就抛出异常。

**`PROPAGATION_REQUIRES_NEW`**：每次都会创建一个新的事务。

**`PROPAGATION_NOT_SUPPORTED`**：以非事务方式运行。如果当前存在事务，就将当前事务挂起。

**`PROPAGATION_NEVER`**：以非事务方式运行。如果当前存在事务，就抛出异常。

**`PARAGTION_NESTED`**：如果当前存在事务，则在嵌套事务内执行；如果当前没有事务，则按`REQUIRED`方式执行。



#### spring事务的隔离级别

**`ISOLATION_DEFAULT`**：使用数据库默认的隔离级别。

**`ISOLATION_READ_UNCOMMITTED`**：读未提交。允许事务在执行过程中，读取其他事务未提交的数据。

**`ISOLATION_READ_COMMITTED`**：读已提交。允许事务在执行过程中，读取其他事务已提交的数据。

**`ISOLATION_REPEATABLE_READ`**：可重复读。在同一个事务内，任意时刻查询的数据结果是一致的。

**`ISOLATION_SERIALIZABLE`**：所有事务依次执行。