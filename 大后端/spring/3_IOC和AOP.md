#### <strong style="color:green">IOC</strong>

ioc又名控制反转，是spring为了降低代码的耦合度，而设计的一种思想。通过<strong style="color:red">反射机制实现</strong>。

1. 通常服务的调用，直接通过new的方式实现，接着调用。而在**依赖注入**的模式下，创建被调用者的工作不需要由调用者来完成，是由spring进行控制，**获得依赖对象的过程被反转了**，因此称为控制反转。
2. 而**依赖注入**是在ioc的基础上完成的，由ioc在容器的运行期间，动态的把底层对象作为参数注入到上层类。

**IOC的两种方式**

1. xml配置文件方式注入

2. 注解方式注入

   需要开启注解扫描：

   ```xml
   <context:component-scan base-package="com.xxx.xxx">
   ```

   <strong style="color:red">@Component</strong>

   @Component衍生注解：@Repository ：dao层  @Service：service层  @Autowired：web层 
         

#### <strong style="color:green">AOP</strong>

面向切面编程。

**通过预编译方式和运行期动态代理，实现在不修改源码的情况下给程序动态统一添加功能的一种技术。**通过<strong style="color:red">动态代理实现</strong>

aop的主要功能：

统一日志处理，统一异常处理，多数据源配置，接口请求统计次数，记录接口入参出参等。