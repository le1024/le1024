> spring bean的作用域
>
> https://docs.spring.io/spring-framework/docs/5.2.0.RELEASE/spring-framework-reference/core.html#beans-factory-scopes

![image-20220406212358959](https://cdn.jsdelivr.net/gh/le1024/image1/le/image-20220406212358959.png)



**1.单例模式**

<strong style="color:red">singleton</strong>，spring默认的机制

```xml
<bean id="accountService" class="com.something.DefaultAccountService"/>

<!-- the following is equivalent, though redundant (singleton scope is the default) -->
<bean id="accountService" class="com.something.DefaultAccountService" scope="singleton"/>
```

一个应用只有一个对象的实例。它的作用范围就是整个引用。

生命周期：当应用加载，创建容器时，对象就被创建了。只要容器还在，对象就一直活着。当应用卸载，容器销毁时，对象也随之被销毁。



**2.原型模式**

<strong style="color:red">prototype</strong>，每次从容器中get的时候，都会产生一个新对象

```xml
<bean id="accountService" class="com.something.DefaultAccountService" scope="prototype"/>
```

每次访问时，都会创建新的对象实例。

生命周期：当使用对象的时候，创建新的对象实例。只要对象在使用中，就一直活着。当对象长时间不用时，就会被java的垃圾回收器回收了。



**3.Request, Session, Application, and WebSocket Scopes**

web开发中使用

<strong style="color:red">request</strong>：在一次http请求中，一个bean定义对应一个实例。作用域仅在基于web的spring applicationContext中有效。当请求处理结束后，request域的实例会被销毁。

<strong style="color:red">session</strong>：与request范围类似，确保每个session中有一个bean的实例，在session过期后，bean会随之失效。

<strong style="color:red">application</strong>：bean被定义在servletContext的生命周期中复用一个单例对象。

<strong style="color:red">websocket</strong>：bean被定义在websocket的生命周期中复用一个单例对象。

**4.global-session**

全局作用域

