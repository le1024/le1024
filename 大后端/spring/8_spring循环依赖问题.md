#### spring如何处理循环依赖的问题



循环依赖：即多个对象之间存在循环的引用关系，在初始化过程中，就会出现"先有鸡还是先有蛋"的问题。



##### @Lazy注解

构造方法造成的循环问题，在构造方法上添加`@Lazy`注解可解决。



##### 使用三级缓存

<img src="https://gitee.com/le1024/image1/raw/master/img/绘图1.png" style="zoom:50%;" />

`源码`

**DefaultSingletonBeanRegistry**

```java
protected Object getSingleton(String beanName, boolean allowEarlyReference) {
    	//singletonObjects就是单例池，先从单例池获取实例对象
		Object singletonObject = this.singletonObjects.get(beanName);
    	//判断实例对象是否为空且该实例是否循环依赖
		if (singletonObject == null && isSingletonCurrentlyInCreation(beanName)) {
			synchronized (this.singletonObjects) {
                //earlySingletonObjects就是二级缓存，单例池未获取到，从二级缓存中获取
				singletonObject = this.earlySingletonObjects.get(beanName);
				if (singletonObject == null && allowEarlyReference) {
                    //singletonFactories三级缓存
					ObjectFactory<?> singletonFactory = this.singletonFactories.get(beanName);
					if (singletonFactory != null) {
                        //这里可以理解为三级缓存在发现循环依赖时提起进行AOP，生成实例对象的动态代理
						singletonObject = singletonFactory.getObject();
						this.earlySingletonObjects.put(beanName, singletonObject);
						this.singletonFactories.remove(beanName);
					}
				}
			}
		}
		return singletonObject;
	}
```



**一级缓存，缓存最终的单例池对象**

```java
/** Cache of singleton objects: bean name to bean instance. */
private final Map<String, Object> singletonObjects = new ConcurrentHashMap<>(256);
```

**二级缓存，缓存初始化对象**

```java
/** Cache of early singleton objects: bean name to bean instance. */
private final Map<String, Object> earlySingletonObjects = new HashMap<>(16);
```

**三级缓存，缓存对象的ObjectFactory**

```java
/** Cache of singleton factories: bean name to ObjectFactory. */
private final Map<String, ObjectFactory<?>> singletonFactories = new HashMap<>(16);
```



对于对象之间的引用，二级缓存会保存new出来的不完整的对象，这样当单例池中找不到依赖的属性时，就可以先从二级缓存中获取到不完整对象，完成对象创建，在后续的依赖注入过程中，将单例池中对象的引用关系调整完成。



三级缓存：如果引用的对象配置了AOP，那么单例池中最终需要注入动态代理对象，而不是原对象。而生成动态代理是在对象初始化完成之后才开始的。于是spring增加了三级缓存，保存所有对象的动态代理的配置信息，在发现循环依赖时，将这个对象的动态代理信息提取出来，提前进行AOP生成动态代理对象。