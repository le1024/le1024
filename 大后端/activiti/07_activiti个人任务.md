> activiti个人任务



#### 1. 分配任务负责人

##### 1.1 固定分配

在进行业务流程建模时指定固定的任务负责人，如图：通过`Assignee`属性指定任务负责人

![image-20211204211244863](https://s2.loli.net/2022/04/20/hx4AnKWcrjOifuX.png)



##### 1.2 表达式分配

不用事先固定具体的任务负责人，通过表示式匹配的入参的方式去分配负责人

###### 1.2.1 UEL表达式

activiti使用uel表达式，同jsp中使用的el表达式一样，使用`${}`的方式取值，activiti支持两个uel表达式：UEL-value和UEL-method

1. **UEL-value定义**

   assignee0就是activiti中的一个流程变量

   ![image-20211204212112536](https://s2.loli.net/2022/04/20/A2ybrLFJS81vtXw.png)

   或者使用：user对象是一个流程变量，通过user.assignee0获取user对象的assignee0属性值

   ![image-20211204212227027](https://s2.loli.net/2022/04/20/ItgneSYp31lNV42.png)

2. **UEL-method方式**

   userBean 是 spring 容器中的一个 bean，表示调用该 bean 的 getUserId()方法

   ![image-20211204212446695](https://s2.loli.net/2022/04/20/Ufrv8bFwA1Cxpkn.png)

3. **UEL-value与UEL-method结合**

   比如：${UserService.getUserById(userId)}，UserService是spring容器的一个bean，getUserById是该bean的一个方法，userId是activiti的流程变量，userId作为参数传到UserService.getUserById方法中。

4. **其他方式**

   UEL表达式支持解析基础类型、bean、list、array、map等，也可作为条件判断，如：

   ${order.price > 100 && order.price < 250}



###### 1.2.2 编码配置负责人

**定义任务分配流程变量**

流程中的其他负责人流程变量，同样设置下即可

![image-20211204213254353](https://s2.loli.net/2022/04/20/ayJucQvUET5f9nA.png)

**设置流程变量-分配负责人**

```java
    /**
     * UEL方式 设置流程负责人
     */
    @Test
    public void assigneeUEL() {
        //获取processEngine
        ProcessEngine processEngine = ProcessEngines.getDefaultProcessEngine();
        //获取RuntimeService
        RuntimeService runtimeService = processEngine.getRuntimeService();
        /**
         * 设置流程变量：assignee0，就是我们在流程图设计的时候通过UEL表示设置的${assignee0}
         * 需要以map的方式传参数给activiti
         */
        Map<String, Object> assigneeMap = new HashMap<>();
        assigneeMap.put("assignee0", "张员工");
        assigneeMap.put("assignee1", "李经理");
        assigneeMap.put("assignee2", "王总经理");
        assigneeMap.put("assignee3", "赵财务");
        //启动流程实例，同时设置流程定义中的assignee的值
        runtimeService.startProcessInstanceByKey("evection", assigneeMap);

        System.out.println(processEngine.getName());
    }
```

执行完成后，就可以在`act_ru_variable`表中看到代码中配置的流程变量值

![image-20211204214554568](https://s2.loli.net/2022/04/20/stUVCnGa1R3dqIh.png)

**注意：使用表达式分配，就必须保证表达式能够正确执行成功，流程变量必须存在，不然activiti就会异常**



##### 1.3 监听器分配

可以使用监听器的方式来完成activiti的流程业务，通过监听器的方式指定负责人，在流程设计的时候就不用固执指定assignee或者UEL表达式去指定流程变量。

监听器是发生在对应的任务相关事件时执行自定义java逻辑或表达式。

<strong style="color:green">由于activiti bpmn visualizer没有监听器的配置，后面使用Camunda Modeler工具</strong>

任务相关事件：因软件问题，这里的事件可能不太一样

![image-20211204215552780](https://s2.loli.net/2022/04/20/Gz3X5uWYhPVM1kl.png)

事件选项包括：

```html
create：任务创建后触发
assignment：任务分配后触发
complete：任务完成后触发
delete：任务删除后触发
```

<strong style="color:green">重新设计流程，将之前UEL表达式分配方式的流程变量，不然流程变量不分配执行流程会报错，然后添加监听器的配置</strong>

![image-20211204221602068](https://s2.loli.net/2022/04/20/wg1nhjKElUbkOoS.png)

<strong style="color:red">建议，每次测试新的东西，为了方便查看，将数据表都删除，然后重新部署一份，再进行操作</strong>

**监听器代码：需要实现TaskListener接口，并重写nofity方法**

```java
public class MyTaskListener implements TaskListener {

    @Override
    public void notify(DelegateTask delegateTask) {
        //判断条件，根据我们在流程设计的内容和事件判断是否进行分配负责人
        if ("创建出差申请单".equals(delegateTask.getName())
            && "create".equals(delegateTask.getEventName())) {
            //分配负责人
            delegateTask.setAssignee("小张");
        }
    }
}
```

重新部署完成，执行启动流程实例，查看`act_run_task`任务表：负责人就是我们在监听器里面设置的

![image-20211204221828513](https://s2.loli.net/2022/04/20/fXtnW5gp1V8QJio.png)



**注意：使用监听器分配方式，按照监听器事件去执行监听器类的notify方法，方法如果不能正常执行也会影响任务的执行**



#### 2. 查询个人任务

##### 2.1 查询任务负责人的代办任务

```java
/**
 * 查询个人待执行任务
 */
@Test
public void findPersonalTaskList() {
    //获取流程引擎
    ProcessEngine processEngine = ProcessEngines.getDefaultProcessEngine();
    //流程定义key
    String processDefinitionKey = "evection";
    //任务负责人
    String assignee = "小张";
    //获取taskService
    TaskService taskService = processEngine.getTaskService();
    List<Task> taskList = taskService.createTaskQuery()
            .processDefinitionKey(processDefinitionKey)
            .includeProcessVariables()
            .taskAssignee(assignee)
            .list();

    taskList.forEach(task -> {
        System.out.println("----------------------------");
        System.out.println("流程实例id： " + task.getProcessInstanceId());
        System.out.println("任务id： " + task.getId());
        System.out.println("任务负责人： " + task.getAssignee());
        System.out.println("任务名称： " + task.getName());
    });
}
```



##### 2.2 查询BusinessKey

前面已经了解过关于BusinessKey的作用，BusinessKey就是为了关联业务系统和activiti业务流程，通常BusinessKey的值建议为业务表的主键ID

在启动流程实例的时候，可以添加BusinessKey

```java
/**
     * 查询BusinessKey
     */
    @Test
    public void findBusinessKey() {
        //获取流程引擎
        ProcessEngine processEngine = ProcessEngines.getDefaultProcessEngine();
        //获取TaskService
        TaskService taskService = processEngine.getTaskService();
        //获取RuntimeService
        RuntimeService runtimeService = processEngine.getRuntimeService();
        //查询流程任务对象
        Task task = taskService.createTaskQuery()
                .processDefinitionKey("evection")
                .taskAssignee("小张")
                .singleResult();
        //获取实例id
        String processInstanceId = task.getProcessInstanceId();
        //获取流程实例对象
        ProcessInstance processInstance = runtimeService
                .createProcessInstanceQuery()
                .processInstanceId(processInstanceId)
                .singleResult();
        //使用processInstance得到businessKey
        String businessKey = processInstance.getBusinessKey();
        System.out.println(businessKey);
    }
```



#### 3. 完成个人任务

在实际应用中，完成任务前需要校验任务的负责人是否具有该任务的办理权限，也就是根据是否是任务负责人进行查询是否有对应的任务

```java
    /**
     * 完成个人任务
     */
    @Test
    public void completePersonalTask() {
        //获取流程引擎
        ProcessEngine processEngine = ProcessEngines.getDefaultProcessEngine();
        //任务id，实际需要自己去查询出来，在act_ru_task表
        String taskId = "2505";
        //负责人
        String assignee = "小张";
        //获取TaskService
        TaskService taskService = processEngine.getTaskService();
        /**
         * 完成任务，需要判断该负责人是否可以完成当前任务
         * 校验方法：就是根据任务id和负责人去查询任务，如果查出到了就可以完成
         */
        Task task = taskService.createTaskQuery()
                .taskAssignee(assignee)
                .taskId(taskId)
                .singleResult();
        if (task != null) {
            taskService.complete(taskId);
            System.out.println(assignee + "完成任务:" + taskId);
        }
    }
```

```java
小张完成任务:2505
```

