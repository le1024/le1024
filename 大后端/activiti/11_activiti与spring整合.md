> activiti与spring整合

创建工程



#### pom

```xml
    <properties>
        <slf4j.version>1.6.6</slf4j.version>
        <log4j.version>1.2.12</log4j.version>
    </properties>
    <dependencies>
        <dependency>
            <groupId>org.activiti</groupId>
            <artifactId>activiti-engine</artifactId>
            <version>7.0.0.Beta1</version>
        </dependency>
        <dependency>
            <groupId>org.activiti</groupId>
            <artifactId>activiti-spring</artifactId>
            <version>7.0.0.Beta1</version>
        </dependency>
        <dependency>
            <groupId>org.activiti</groupId>
            <artifactId>activiti-bpmn-model</artifactId>
            <version>7.0.0.Beta1</version>
        </dependency>
        <dependency>
            <groupId>org.activiti</groupId>
            <artifactId>activiti-bpmn-converter</artifactId>
            <version>7.0.0.Beta1</version>
        </dependency>
        <dependency>
            <groupId>org.activiti</groupId>
            <artifactId>activiti-json-converter</artifactId>
            <version>7.0.0.Beta1</version>
        </dependency>
        <dependency>
            <groupId>org.activiti</groupId>
            <artifactId>activiti-bpmn-layout</artifactId>
            <version>7.0.0.Beta1</version>
        </dependency>
        <dependency>
            <groupId>org.activiti.cloud</groupId>
            <artifactId>activiti-cloud-services-api</artifactId>
            <version>7.0.0.Beta1</version>
        </dependency>
        <dependency>
            <groupId>aspectj</groupId>
            <artifactId>aspectjweaver</artifactId>
            <version>1.5.4</version>
        </dependency>
        <dependency>
            <groupId>mysql</groupId>
            <artifactId>mysql-connector-java</artifactId>
            <version>5.1.40</version>
        </dependency>
        <dependency>
            <groupId>junit</groupId>
            <artifactId>junit</artifactId>
            <version>4.12</version>
        </dependency>
        <dependency>
            <groupId>org.springframework</groupId>
            <artifactId>spring-test</artifactId>
            <version>5.0.7.RELEASE</version>
        </dependency>
        <!-- log start -->
        <dependency>
            <groupId>log4j</groupId>
            <artifactId>log4j</artifactId>
            <version>${log4j.version}</version>
        </dependency>
        <dependency>
            <groupId>org.slf4j</groupId>
            <artifactId>slf4j-api</artifactId>
            <version>${slf4j.version}</version>
        </dependency>
        <dependency>
            <groupId>org.slf4j</groupId>
            <artifactId>slf4j-log4j12</artifactId>
            <version>${slf4j.version}</version>
        </dependency>
        <dependency>
            <groupId>org.slf4j</groupId>
            <artifactId>slf4j-nop</artifactId>
            <version>${slf4j.version}</version>
        </dependency>
        <!-- log end -->
        <dependency>
            <groupId>org.mybatis</groupId>
            <artifactId>mybatis</artifactId>
            <version>3.4.5</version>
        </dependency>
        <dependency>
            <groupId>commons-dbcp</groupId>
            <artifactId>commons-dbcp</artifactId>
            <version>1.4</version>
        </dependency>
    </dependencies>
    <repositories>
        <repository>
            <id>alfresco</id>
            <name>Activiti Releases</name>
            <url>https://artifacts.alfresco.com/nexus/content/repositories/activiti-releases/</url>
            <releases>
                <enabled>true</enabled>
            </releases>
        </repository>
    </repositories>
```



#### 创建activiti-spring.xml

activiti中核心类就是ProcessEngine流程引擎，与spring整合后，就是通过spring来管理ProcessEngine，创建spring与activiti的整合配置文件：`activiti-spring.xml`，名称不固定

```xml
<beans xmlns="http://www.springframework.org/schema/beans"
       xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
       xmlns:tx="http://www.springframework.org/schema/tx"
       xmlns:aop="http://www.springframework.org/schema/aop"
       xsi:schemaLocation="http://www.springframework.org/schema/beans
        http://www.springframework.org/schema/beans/spring-beans.xsd
        http://www.springframework.org/schema/tx
        http://www.springframework.org/schema/tx/spring-tx.xsd
        http://www.springframework.org/schema/aop
        http://www.springframework.org/schema/aop/spring-aop.xsd">

    <!--数据源-->
    <bean id="dataSource" class="org.apache.commons.dbcp.BasicDataSource">
        <property name="driverClassName" value="com.mysql.jdbc.Driver" />
        <property name="url" value="jdbc:mysql://localhost:3306/activiti" />
        <property name="username" value="root" />
        <property name="password" value="root" />
        <property name="maxActive" value="3" />
        <property name="maxIdle" value="1" />
    </bean>

    <!--工作流引擎配置-->
    <bean id="processEngineConfiguration" class="org.activiti.spring.SpringProcessEngineConfiguration">
        <!--数据源-->
        <property name="dataSource" ref="dataSource" />
        <!--使用spring事务管理器-->
        <property name="transactionManager" ref="transactionManager" />
        <!--数据源策略-->
        <!--
        databaseSchemaUpdate取值：
        false：默认值。在activiti启动时，会对比数据库表中保存的版本，如果没有表或版本不匹配，将抛出异常。（生产环境常用）
        true：activiti会对数据库中所有表进行更新操作。如果表不存在，将会创建。（开发环境常用）
        create-drop：在activiti启动时创建表，在关闭时删除表，必须手动关闭引擎才会删除表。（单元测试用）
        drop-create：在activiti启动时删除旧表，然后创建新表，不需要关闭引擎。
        -->
        <property name="databaseSchemaUpdate" value="true" />
    </bean>

    <!--流程引擎-->
    <bean id="processEngine" class="org.activiti.spring.ProcessEngineFactoryBean">
        <property name="processEngineConfiguration" ref="processEngineConfiguration" />
    </bean>

    <!--资源服务service-->
    <bean id="repositoryService" factory-bean="processEngine" factory-method="getRepositoryService" />
    <!--流程运行service-->
    <bean id="runtimeService" factory-bean="processEngine" factory-method="getRuntimeService" />
    <!--任务管理service-->
    <bean id="taskService" factory-bean="processEngine" factory-method="getTaskService" />
    <!--历史任务service-->
    <bean id="historyService" factory-bean="processEngine" factory-method="getHistoryService" />

    <!--事务管理器-->
    <bean id="transactionManager" class="org.springframework.jdbc.datasource.DataSourceTransactionManager">
        <property name="dataSource" ref="dataSource" />
    </bean>

    <!-- 通知 -->
    <tx:advice id="txAdvice" transaction-manager="transactionManager">
        <tx:attributes>
            <!-- 传播行为 -->
            <tx:method name="save*" propagation="REQUIRED"/>
            <tx:method name="insert*" propagation="REQUIRED"/>
            <tx:method name="delete*" propagation="REQUIRED"/>
            <tx:method name="update*" propagation="REQUIRED"/>
            <tx:method name="find*" propagation="SUPPORTS" read-only="true"/>
            <tx:method name="get*" propagation="SUPPORTS" read-only="true"/>
        </tx:attributes>
    </tx:advice>
    <!-- 切面，根据具体项目修改切点配置-->
    <aop:config proxy-target-class="true">
        <aop:advisor advice-ref="txAdvice"
                     pointcut="execution(* com.hll.service.impl..*.*(..))"/>
    </aop:config>
</beans>
```



#### 测试代码

```java
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "classpath:activiti-spring.xml")
public class Test01 {

    @Autowired
    private RepositoryService repositoryService;

    /**
     * 流程部署
     */
    @Test
    public void deployment() {
        Deployment deployment = repositoryService.createDeployment()
                .name("activiti整合spring")
                .key("activiti-spring1")
                .addClasspathResource("bpmn/activiti-spring.bpmn20.xml")
                .deploy();

        System.out.println(deployment.getId());
        System.out.println(deployment.getName());
    }

    @Autowired
    private RuntimeService runtimeService;

    /**
     * 启动流程实例
     */
    @Test
    public void startProcess() {
        ProcessInstance processInstance = runtimeService.startProcessInstanceByKey("activiti-spring");
        System.out.println(processInstance.getProcessDefinitionKey());
        System.out.println(processInstance.getProcessInstanceId());
        System.out.println(processInstance.getName());

    }
}
```



#### 执行流程分析

1. 加载`activiti-spring.xml`文件。
2. 加载`SpringProcessEngineConfiguration`对象，这个对象需要依赖注入`DataSource`和`TransactionManager`对象。
3. 加载`ProcessEngineFactoryBean`工厂对象来创建`ProcessEngine`对象，而`ProcessEngineFactoryBean`工厂对象需要依赖注入`ProcessEngineConfiguration`对象。
4. `ProcessEngine`对象负责创建activiti中的业务service对象，比如：RepositoryService，RuntimeService，TaskService等，从而简化activiti的开发过程。
