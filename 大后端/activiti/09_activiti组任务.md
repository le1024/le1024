> activiti组任务

之前在流程定义中的任务节点设置的负责人assignee都是固定的负责人，在流程定义设计时将参与者与文件固定设置了，如需变更任务负责人需要修改流程定义，系统可扩展性差。

这里讨论的是组任务，不同于流程变量来控制任务负责人。



#### 1. 需求

针对这种情况，可以给任务设置多个候选人，从候选人中选择参与者来完成任务。

#### 2. 设置任务候选人

重新一套流程，简单点就行，在流程图中任务节点的配置中设置`Candidate Users`属性值，多个候选人以英文逗号分隔。

![image-20211206222034940](https://cdn.jsdelivr.net/gh/le1024/image1/le/image-20211206222034940.png)

注意：由于是Camunda Modeler工具创建流程图，记得把`camunda`修改为`activiti`，如下图：

![image-20211206222238772](https://cdn.jsdelivr.net/gh/le1024/image1/le/image-20211206222238772.png)



#### 3. 组任务

##### 3.1 查询组任务

指定候选人，查询该候选人的待办任务，候选人不能立即办理任务

```java
    /**
     * 根据候选人查询组任务
     */
    @Test
    public void findGroupTaskList() {
        //流程定义key
        String processDefinitionKey = "evection2";
        //任务候选人
        String candidateUser = "lisi"; //使用候选中的zhangsan wangwu都可以
        //获取流程引擎
        ProcessEngine processEngine = ProcessEngines.getDefaultProcessEngine();
        //获取taskService
        TaskService taskService = processEngine.getTaskService();
        //查询组任务
        List<Task> taskList = taskService.createTaskQuery()
                .processDefinitionKey(processDefinitionKey)
                .taskCandidateUser(candidateUser)
                .list();

        taskList.forEach(task -> {
            System.out.println("=======================");
            System.out.println("流程实例id：" + task.getProcessInstanceId());
            System.out.println("任务id：" + task.getId());
            System.out.println("任务名称：" + task.getName());
            System.out.println("任务负责人：" + task.getAssignee());
        });
    }
```

执行完成后，该任务的任务负责人是空的，因为开始流程设计里面的都是候选人，所以没法去完成任务，需要去拾取。

![image-20211206230510852](https://cdn.jsdelivr.net/gh/le1024/image1/le/image-20211206230510852.png)



##### 3.2 拾取任务

候选人拾取任务后该任务就变成了个人任务

```java
    /**
     * 拾取任务，变为个人任务
     */
    @Test
    public void claimTask() {
        //获取流程引擎
        ProcessEngine processEngine = ProcessEngines.getDefaultProcessEngine();
        //获取TaskService
        TaskService taskService = processEngine.getTaskService();
        //要拾取的任务id
        String taskId = "2505";
        //任务候选人
        String assignee = "lisi";
        /**
         * 拾取任务
         * 即使该用户不是任务的候选人也可以拾取任务，建议拾取时做校验，根据候选人查询
         */
        Task task = taskService.createTaskQuery()
                .taskId(taskId)
                .taskCandidateUser(assignee) //根据候选人查询
                .singleResult();
        if (task != null) {
            //当前候选人可以进行拾取
            taskService.claim(taskId, assignee);
            System.out.println(assignee+"拾取任务"+taskId+"成功");
        }
    }
```

![image-20211206230524462](https://cdn.jsdelivr.net/gh/le1024/image1/le/image-20211206230524462.png)

**说明：即使不是任务的候选人也可以进行拾取任务，所以需要根据任务候选人去查询任务再去进行拾取**

**组任务拾取之后，任务就会有负责人了，通过候选人再去查询将会查询不到任务**



##### 3.3 查询个人待办任务

查询方式同之前的个人任务查询

```java
    /**
     * 查询个人待办任务
     */
    @Test
    public void findPersonalTask() {
        //获取流程引擎
        ProcessEngine processEngine = ProcessEngines.getDefaultProcessEngine();
        //流程定义key
        String processDefinitionKey = "evection2";
        //任务负责人
        String assignee = "lisi";
        //创建TaskService
        TaskService taskService = processEngine.getTaskService();

        List<Task> taskList = taskService.createTaskQuery()
                .taskAssignee(assignee)
                .processDefinitionKey(processDefinitionKey)
                .list();

        for (Task task : taskList) {
            System.out.println("----------------------------");
            System.out.println("流程实例id：" + task.getProcessInstanceId());
            System.out.println("任务id：" + task.getId());
            System.out.println("任务负责人：" + task.getAssignee());
            System.out.println("任务名称：" + task.getName());
        }
    }
```



##### 3.4 办理个人任务

同之前的个人任务办理

```java
    /**
     * 办理个人任务
     */
    @Test
    public void completeTask() {
        //流程引擎
        ProcessEngine processEngine = ProcessEngines.getDefaultProcessEngine();
        //任务id,直接拿上一步查询到的taskId进行完成，不用再查一次
        String taskId = "2505";
        //TaskService
        TaskService taskService = processEngine.getTaskService();
        taskService.complete(taskId);
        System.out.println(taskId + "任务完成");
    }
```

**建议完成任务校验用户是不是该任务的负责人**



##### 3.5 归还组任务

如果拾取组任务之后，不想办理该任务，可以进行归还任务，归还之后就不是该任务的负责人了。

```java
    /**
     * 归还组任务
     */
    @Test
    public void returnToGroupTask() {
        //流程引擎
        ProcessEngine processEngine = ProcessEngines.getDefaultProcessEngine();
        //taskService
        TaskService taskService = processEngine.getTaskService();
        //当前待办任务id
        String taskId = "5002";
        //任务负责人
        String assignee = "lisi";
        //通过待办任务id和任务负责人去查询任务
        Task task = taskService.createTaskQuery()
                .taskId(taskId)
                .taskAssignee(assignee)
                .singleResult();
        if (task != null) {
            /**
             * 归还任务组就是将assignee值设为null即可，代表该任务没有负责人
             */
            taskService.setAssignee(taskId, null);
        }
    }
```



##### 3.6 任务交接

任务交接就是当前任务负责人将任务交给其他候选人办理该任务

```java
	    /**
     * 任务交接，将任务交给其他候选人办理该任务
     * 注意：被交接人不是候选人也可以，按业务需要不建议这样使用
     * 交接候选人建议查询一下是否是该任务的候选人再执行
     */
    @Test
    public void changeCandidateUser() {
        //流程引擎
        ProcessEngine processEngine = ProcessEngines.getDefaultProcessEngine();
        //TaskService
        TaskService taskService = processEngine.getTaskService();
        //当前任务
        String taskId = "5002";
        //任务负责人
        String assignee = "lisi";
        //候选人
        String candidateUser = "wangwu";

        Task task = taskService.createTaskQuery()
                .taskId(taskId)
                .taskAssignee(assignee)
                .singleResult();
        if (task != null) {
            //todo 需要校验一下candidateUser是不是该任务的候选人，用taskService.createTaskQuery()再去查询一次
            // 然后两个task比较一下后再操作
            taskService.setAssignee(taskId, candidateUser);
        }
    }
```



##### 3.7 数据库表操作

启动流程实例之后，查询当前任务执行表：

```sql
select * from act_ru_task;
```

记录了当前执行的任务，由于该任务是组任务，`assignee_`字段值是null，让拾取任务之后该字段才会有拾取用户的信息。

查询任务参与者：

```mysql
select * from act_ru_identitylink
```

记录了参与任务用户或组，当前任务如果设置了候选人，就会向该表插入候选人记录，有几个候选人就插入几条记录，与`act_ru_identitylink`对应的还有一张历史表`act_hi_identitylink`，向`act_ru_identitylink`插入记录的同时也会向`act_hi_identitylink`插入记录。

