**flowable入门**

****

&emsp;在这个初步教程中，将构建一个简单的例子，以展示如何创建一个Flowable流程引擎，介绍一些核心概念，并展示如何使用API。 截图时使用的是Eclipse，但实际上可以使用任何IDE。我们使用Maven获取Flowable依赖及管理构建，但是类似的任何其它方法也都可以使用（Gradle，Ivy，等等）。



我们将构建的例子是一个简单的*请假(holiday request)*流程：

- *员工(employee)*申请几天的假期
- *经理(manager)*批准或驳回申请
- 发送邮件通知结果给员工



#### 创建工程

&emsp;通过idea创建或者eclipse创建一个maven工程



#### 引入依赖

&emsp;需要添加两个依赖：

- Flowable流程引擎。使我们可以创建一个ProcessEngine流程引擎对象，并访问Flowable API。
- 数据库驱动依赖。生成flowable流程表

```xml
<dependencies>
  <!-- flowable依赖 -->
    <dependency>
        <groupId>org.flowable</groupId>
        <artifactId>flowable-engine</artifactId>
        <version>6.3.0</version>
    </dependency>
    <!-- mysql依赖-->
    <dependency>
        <groupId>mysql</groupId>
        <artifactId>mysql-connector-java</artifactId>
        <version>5.1.47</version>
    </dependency>
</dependencies>
```

可以直接使用main方法测试，也可以使用测试类（自行添加maven依赖）



#### 创建流程引擎

&emsp;首先要做的就是初始化`ProcessEngine`流程引擎实例。这是一个线程安全的对象，因此通常只需要在一个应用中初始化一次。*ProcessEngine*由`ProcessEngineConfiguration`实例创建。该实例可以配置与调整流程引擎的设置。通常使用一个配置xml文件创建`ProcessEngineConfiguration`，也可以通过编程方式创建它。

新建Test方法：

```java
@Test
    public void test01() {
        //创建了一个独立(standalone)配置对象
        //这里的'独立'指的是引擎是完全独立创建及使用的（而不是在Spring环境中使用，这时需要使用SpringProcessEngineConfiguration类代替）
        ProcessEngineConfiguration configuration = new StandaloneProcessEngineConfiguration()
                //配置数据库信息，运行需要先创建好数据库
                .setJdbcUrl("jdbc:mysql://localhost:3306/app-flowable?serverTimezone=UTC&nullCatalogMeansCurrent=true")
                .setJdbcUsername("root")
                .setJdbcPassword("root")
                .setJdbcDriver("com.mysql.jdbc.Driver")
                //设置了true,确保在JDBC参数连接的数据库中，数据库表结构不存在时，会创建相应的表结构
                .setDatabaseSchemaUpdate(ProcessEngineConfiguration.DB_SCHEMA_UPDATE_TRUE);

        //创建ProcessEngine对象
        ProcessEngine processEngine = configuration.buildProcessEngine();

    }
```

执行该程序，控制台不出错的话，就是操作完成了。查看下数据库，生成了34张`act`表。



如果控制台没有详细的日志的话，可以使用log4j作为slf4j的实现，在pom添加依赖：

```xml
<dependency>
  <groupId>org.slf4j</groupId>
  <artifactId>slf4j-api</artifactId>
  <version>1.7.21</version>
</dependency>
<dependency>
  <groupId>org.slf4j</groupId>
  <artifactId>slf4j-log4j12</artifactId>
  <version>1.7.21</version>
</dependency>
```

在*src/main/resources*文件夹下添加*log4j.properties*文件，并写入下列内容：

```properties
log4j.rootLogger=DEBUG, CA

log4j.appender.CA=org.apache.log4j.ConsoleAppender
log4j.appender.CA.layout=org.apache.log4j.PatternLayout
log4j.appender.CA.layout.ConversionPattern= %d{hh:mm:ss,SSS} [%t] %-5p %c %x - %m%n
```

重新运行应用。就可以看到关于引擎启动与创建数据库表结构的提示日志。



现在已经得到了一个启动可用的流程引擎。接下来为它提供一个流程！



#### 部署流程定义

&emsp;我们要构建的流程是一个非常简单的请假流程。Flowable引擎需要流程定义为BPMN 2.0格式，这是一个业界广泛接受的XML标准。 在Flowable术语中，我们将其称为一个**流程定义(process definition)**。一个*流程定义*可以启动多个**流程实例(process instance)**。*流程定义*可以看做是重复执行流程的蓝图。 在这个例子中，*流程定义*定义了请假的各个步骤，而一个*流程实例*对应某个雇员提出的一个请假申请。

&emsp;BPMN 2.0存储为XML，并包含可视化的部分：使用标准方式定义了每个步骤类型（人工任务，自动服务调用，等等）如何呈现，以及如何互相连接。这样BPMN 2.0标准使技术人员与业务人员能用双方都能理解的方式交流业务流程。

&emsp;我们要使用的流程定义为：

&emsp;来自官网文档图

![来自官网文档图](img.assets/getting.started.bpmn.process.png)

简单的说明一下这个流程：

- 假定启动流程需要提供一些信息，例如雇员名字、请假时长以及说明。当然，这些可以单独建模为流程中的第一步。但是如果将他们作为流程的"输入信息"，就能保证只有在实际请求时才会建立一个流程实例。否则将提交作为流程的第一步，用户可能在提交之前改变主意并取消，但流程实例已经创建了。在某些场景中，就可能影响重要的指标（例如提交了多少申请，但未完成），取决于业务目标。
- 图中左侧的圆圈叫做**启动事件(start event)**。这是一个流程实例的起点。
- 第一个矩形是一个**用户任务(user task)**。这是流程中用户操作的步骤。在这里例子中，后续经理需要批准或者驳回用户的申请。
- 带叉的菱形是一个**排他网关(exclusive gateway)**。后面还有其他的网关介绍。这里的排他网关取决于经理的操作，然后将流程实例转至批准或者驳回路径。
- 如果批准，则通过另一个**用户任务(user task)**，将经理的决定通知给申请人。当然也可以是别的通知方式（发邮件等）。
- 如果驳回，可以直接发邮件通知申请人。



一般来说，这样的*流程定义*使用可视化建模工具建立，如Flowable Designer(Eclipse)或Flowable Web Modeler(Web应用)。

但在这里我们直接撰写XML，以熟悉BPMN 2.0及其概念。后面再通过可视化建模工具创建流程。

与上面展示的流程图对应的BPMN 2.0 XML在下面显示。**请注意这只包含了“流程部分”**。如果使用图形化建模工具，**实际的XML文件还将包含“可视化部分”**，用于描述图形信息，如流程定义中各个元素的坐标（所有的图形化信息包含在XML的*BPMNDiagram*标签中，作为*definitions*标签的子元素）。



在*src/main/resources*文件夹下创建名为*holiday-request.bpmn20.xml*的文件，这个名字随意，这里当前需要设计的流程定义的，请假流程申请

将下面内容复制到创建的xml文件中

```xml
<?xml version="1.0" encoding="UTF-8"?>
<definitions xmlns="http://www.omg.org/spec/BPMN/20100524/MODEL"
             xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
             xmlns:xsd="http://www.w3.org/2001/XMLSchema"
             xmlns:bpmndi="http://www.omg.org/spec/BPMN/20100524/DI"
             xmlns:omgdc="http://www.omg.org/spec/DD/20100524/DC"
             xmlns:omgdi="http://www.omg.org/spec/DD/20100524/DI"
             xmlns:flowable="http://flowable.org/bpmn"
             typeLanguage="http://www.w3.org/2001/XMLSchema"
             expressionLanguage="http://www.w3.org/1999/XPath"
             targetNamespace="http://www.flowable.org/processdef">

    <process id="holidayRequest" name="Holiday Request" isExecutable="true">

        <startEvent id="startEvent"/>
        <sequenceFlow sourceRef="startEvent" targetRef="approveTask"/>

        <userTask id="approveTask" name="Approve or reject request"/>
        <sequenceFlow sourceRef="approveTask" targetRef="decision"/>

        <exclusiveGateway id="decision"/>
        <sequenceFlow sourceRef="decision" targetRef="externalSystemCall">
            <conditionExpression xsi:type="tFormalExpression">
                <![CDATA[
          ${approved}
        ]]>
            </conditionExpression>
        </sequenceFlow>
        <sequenceFlow  sourceRef="decision" targetRef="sendRejectionMail">
            <conditionExpression xsi:type="tFormalExpression">
                <![CDATA[
          ${!approved}
        ]]>
            </conditionExpression>
        </sequenceFlow>

        <serviceTask id="externalSystemCall" name="Enter holidays in external system"
                     flowable:class="org.flowable.CallExternalSystemDelegate"/>
        <sequenceFlow sourceRef="externalSystemCall" targetRef="holidayApprovedTask"/>

        <userTask id="holidayApprovedTask" name="Holiday approved"/>
        <sequenceFlow sourceRef="holidayApprovedTask" targetRef="approveEnd"/>

        <serviceTask id="sendRejectionMail" name="Send out rejection email"
                     flowable:class="org.flowable.SendRejectionMail"/>
        <sequenceFlow sourceRef="sendRejectionMail" targetRef="rejectEnd"/>

        <endEvent id="approveEnd"/>

        <endEvent id="rejectEnd"/>

    </process>

</definitions>
```

- 2-11行，在大多数流程定义中都是一样的。这是一种*样板文件*，需要与BPMN 2.0标准规范完全一致。

- 每一个步骤（在BPMN 2.0术语中称作**活动(activity)**）都有一个id属性，为其提供一个在XML文件中唯一的标识符。所有的活动都可以设置一个名字，以提高流程图的可读性。
- 活动之间通过**顺序流(sequence flow)**连接，在流程图中是一个有向箭头。在执行流程实例时，执行(execution)会从*启动事件*沿着*顺序流*流向下一个活动。
- 离开排他网关(带有X的菱形)的顺序流很特别：都以*表达式(execution)*的形式定义了*条件(condition)*，见25-32行。当流程实例的执行到达这个网关时，会计算*条件*，并使用第一个计算为true的顺序流。这就是排他的含义：只选择一个。当然如果有需要不同的策略，也可以使用其他类型的网格。
- 这里用作条件的表达式为*${approved}*，这是*${approved == true}*的简写。变量`approved`被称作**流程变量(process variable)**。流程变量是持久化的数据，与流程实例存储在一起并可以在流程实例的生命周期中使用。在这个例子中，我们需要在特定的地方（当经理用户任务提交时，或者以Flowable的术语来说，*完成(complete)*时）设置这个*流程变量*，因为这不是流程实例启动时就能获取的数据。

现在我们已经有了流程BPMN 2.0 XML文件，下来需要将它***部署(deploy)\***到引擎中。*部署*一个流程定义意味着：

- 流程引擎会将XML文件存储在数据库中，这样可以在需要的时候获取它。
- 流程定义转换为内部的、可执行的对象模型，这样使用它就可以启动*流程实例*。

将流程定义部署至Flowable引擎，需要使用*`RepositoryService`*，其可以从*`ProcessEngine`*对象获取。使用*`RepositoryService`*，可以通过XML文件的路径创建一个新的*部署(Deployment)*，并调用*`deploy()`*方法实际执行：

测试类中添加测试部署方式：

```java
	ProcessEngineConfiguration configuration = null;

    /**
     * 初始化封装下，不然每个测试方法都要写，很麻烦
     */
    @Before
    public void init() {
        configuration = new StandaloneProcessEngineConfiguration()
                .setJdbcUrl("jdbc:mysql://localhost:3306/app-flowable?serverTimezone=UTC&nullCatalogMeansCurrent=true")
                .setJdbcUsername("root")
                .setJdbcPassword("root")
                .setJdbcDriver("com.mysql.jdbc.Driver")
                //设置了true,确保在JDBC参数连接的数据库中，数据库表结构不存在时，会创建相应的表结构
                .setDatabaseSchemaUpdate(ProcessEngineConfiguration.DB_SCHEMA_UPDATE_TRUE);
    }

/**
     * 部署流程定义
     */
    @Test
    public void deployment() {
        //获取ProcessEngine对象
        ProcessEngine processEngine = configuration.buildProcessEngine();
        //获取RepositoryService对象，部署流程定义
        RepositoryService repositoryService = processEngine.getRepositoryService();
        //执行部署
        Deployment deployment = repositoryService.createDeployment()
                .addClasspathResource("holiday-request.bpmn20.xml")
            	//.key().name()
                .deploy();

        //打印部署结果
        System.out.println("deployment.getId() = " + deployment.getId());
        System.out.println("deployment.getKey() = " + deployment.getKey());
        System.out.println("deployment.getName() = " + deployment.getName());
    }
```

key，name自己根据业务定义

![image-20220726224055894](img.assets/image-20220726224055894.png)

查看下数据库：

`act_re_deployment`：流程定义部署表，没部署一次就会增加一条记录

这里的ID=1，就是我们代码部署生成的记录

![image-20220726224400893](img.assets/image-20220726224400893.png)



`act_re_procdef`：流程定义表，部署新的流程定义就会新增一条记录，表中`DELOYMENT_ID`就是`act_re_deployment`的主键ID

![image-20220726224536311](img.assets/image-20220726224536311.png)



`act_ge_bytearray`：流程资源表，流程部署的bpmn文件和png文件都会存在该表

![image-20220726224753074](img.assets/image-20220726224753074.png)



现在可以通过API查询验证流程定义已经部署在引擎中（并学习一些API）。通过*RepositoryService*创建的*ProcessDefinitionQuery*对象实现。

```java
@Test
    public void queryDeployment() {
        //获取ProcessEngine对象
        ProcessEngine processEngine = configuration.buildProcessEngine();
        //获取RepositoryService
        RepositoryService repositoryService = processEngine.getRepositoryService();
        //获取ProcessDefinition，实现查询流程定义，这个结果的信息是`act_re_procdef`表的数据
        ProcessDefinition processDefinition = repositoryService.createProcessDefinitionQuery()
                //这个id就是部署后的deployment.getId()，也就是`act_re_deployment`的主键id
                .deploymentId("1")
                .singleResult();
        //打印结果
        System.out.println("processDefinition.getDeploymentId() = " + processDefinition.getDeploymentId());
        System.out.println("processDefinition.getKey() = " + processDefinition.getKey());
    }
```

![image-20220726225459945](img.assets/image-20220726225459945.png)





#### 启动流程实例

现在已经在流程引擎中*部署*了流程定义，因此可以使用这个*流程定义*作为“蓝图”启动*流程实例*。

一个部署成功的流程定义可以启动多个流程实例，就好比请假申请单，是可以多个员工都可以去填写的。

要启动流程实例，通常需要提供一些初始化*流程变量*。一般来说，可以通过呈现给用户的表单，或者在流程由其他系统自动触发时通过REST API，来获取这些变量。这样才能达到一个比较全面的数据交互。

接下来，我们使用*`RuntimeService`*启动一个*流程实例*。收集的数据作为一个*java.util.Map*实例传递，其中的键就是之后用于获取变量的标识符。这个流程实例使用*key*启动。这个*key*就是BPMN 2.0 XML文件中设置的*id*属性，在这个例子里是*holidayRequest*，也就是`act_re_procdef`表中的`KEY_`字段

![image-20220726230025536](img.assets/image-20220726230025536.png)

![image-20220726230050732](img.assets/image-20220726230050732.png)

```java
/**
     * 启动流程实例
     */
    @Test
    public void startProcess() {
        //先获取ProcessEngine对象
        ProcessEngine processEngine = configuration.buildProcessEngine();
        //获取RuntimeService对象
        RuntimeService runtimeService = processEngine.getRuntimeService();

        //初始化流程变量，这些变量实际是从页面表单传过来的，员工填写请假申请单的一些相关表单数据
        Map<String, Object> variables = new HashMap<>();
        variables.put("employee", "zhangsan");
        variables.put("nrOfHolidays", "1");
        variables.put("description", "工作太累，想休息一下~");

        //启动流程实例
        //holidayRequest就是流程定义的ID，在xml或者`act_re_procdef`的`KEY_`字段都能查到
        ProcessInstance processInstance = runtimeService.startProcessInstanceByKey("holidayRequest", variables);

        System.out.println("processInstance.getDeploymentId() = " + processInstance.getDeploymentId());
        System.out.println("processInstance.getProcessDefinitionId() = " + processInstance.getProcessDefinitionId());
    }
```

执行成功后，观察数据库表：

`act_ru_task`：任务信息表，会新增一条任务记录

![image-20220727223920433](img.assets/image-20220727223920433.png)

`act_ru_variable`：流程实例的流程变量信息

可以看到代码中，map存放的变量数据

![image-20220727224033918](img.assets/image-20220727224033918.png)

`act_ru_execution`：流程执行的过程信息

![image-20220727224132919](img.assets/image-20220727224132919.png)

`act_hi_procinst`：流程实例历史信息

`act_hi_taskinst`：流程任务历史信息

`act_hi_actinst`：流程实例执行历史





#### 查询任务

在实际的应用中，会为用户提供界面化操作，让他们可以登录并查看任务列表。可以看到作为流程变量存储的流程实例数据，并决定后续如何操作。现在通过API的方式调用查询任务列表。