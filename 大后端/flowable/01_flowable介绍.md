**flowable简单介绍**

****

官网地址：https://tkjohn.github.io/flowable-userguide/#_introduction



#### flowable是什么

&emsp;Flowable是一个使用Java编写的轻量级业务流程引擎。Flowable流程引擎可用于部署BPMN 2.0流程定义（用于定义流程的行业XML标准）， 创建这些流程定义的流程实例，进行查询，访问运行中或历史的流程实例与相关数据，等等。



#### flowable与activiti

&emsp;flowable和activiti6是同一个团队开发的，activiti先，flowable后，flowable 算是 activiti6的升级版。