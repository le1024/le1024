> 什么是CSRF

CSRF（Cross-site request forgery）跨站请求伪造，也被称为"OneClick Attack"或者Session Riding。通过伪造用户请求访问受信任站点的非法请求访问。



客户端与服务进行交互的时候，由于http协议本身是无状态的协议，所以引入了cookie进行记录客户端身份。在cookie中会存放sessionId用来识别客户端身份。在跨域的情况下，sessionId可能被第三方恶意劫持，通过这个sessionId向服务端发起请求时，服务端会认为这个请求是合法的，可能会发生很多意想不到的事。



> Spring Security中的CSRF

从Spring Security4开始CSRF是默认开启的。默认会拦截请求，进行CSRF处理。CSRF为了保证不是其他第三方网站访问，要求访问时携带参数名为`_csrf`值为token(token在服务端产生)的内容，如果token和服务端的token匹配成功，则正常访问。



> 实现

1.`resources/templates`下新建`login.html`

```html
<!DOCTYPE html>
<html lang="en" xmlns:th="http://www.thymeleaf.org">
<head>
    <meta charset="UTF-8">
    <title>Title</title>
</head>
<body>
    <form action="/login" method="post">
        <!-- csrf -->
        <input type="hidden" name="_csrf" th:if="${_csrf}" th:value="${_csrf.getToken()}">
        用户名：<input type="text" name="username" />
        密码：<input type="password" name="password" />
        <input type="checkbox" name="remember-me" value="true">记住我
        <input type="submit" value="登录" />
    </form>
</body>
</html>
```



2.新建登录接口：

```java
@RequestMapping("/toLogin")
public String toLogin() {
    //这个跳转的是 templates里面的login.html
    return "login";
}
```

3.更新`SecurityConfig`配置：

```java
http.formLogin()
    //需要改成登录接口，不能是login.html了
    //.loginPage("/login.html")
    .loginPage("/toLogin")
    
http.authorizeRequests()
    //放行登录接口，不是之前的login.html
    //.antMatchers("/login.html", "/error.html").permitAll()
    .antMatchers("/toLogin").permitAll()
    
//注释
//http.csrf().disable();
```



4.测试

请求新的登录接口`http://localhost:8080/toLogin`

![image-20210831214907769](https://cdn.jsdelivr.net/gh/le1024/image1/le/image-20210831214907769.png)

输入帐号密码会成功登录，并访问页面，查看`network`请求参数，可以看到`_csrf`

![image-20210831215021915](https://cdn.jsdelivr.net/gh/le1024/image1/le/image-20210831215021915.png)





> 后面整理一下

[前后端分离实现CSRF]()

