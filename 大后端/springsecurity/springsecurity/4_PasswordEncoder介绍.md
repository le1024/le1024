> PasswordEncoder

Spring Security要求容器中必须要有`PasswordEncoder`实例。所以当自定义登录逻辑时要求必须给容器注入`PasswordEncoder`的bean对象。



- **PasswordEncoder说明**

  `encode(CharSequence rawPassword)`：参数为明文密码，通过此方法进行加密

  `matches(CharSequence rawPassword, String encodedPassword)`：比较密码是否一致，明文密码和加密密码

  `upgradeEncoding(String encodedPassword)`：对密码进行二次加密，很少用

```java
public interface PasswordEncoder {

	/**
	 * Encode the raw password. Generally, a good encoding algorithm applies a SHA-1 or
	 * greater hash combined with an 8-byte or greater randomly generated salt.
	 */
	String encode(CharSequence rawPassword);

	/**
	 * Verify the encoded password obtained from storage matches the submitted raw
	 * password after it too is encoded. Returns true if the passwords match, false if
	 * they do not. The stored password itself is never decoded.
	 * @param rawPassword the raw password to encode and match
	 * @param encodedPassword the encoded password from storage to compare with
	 * @return true if the raw password, after encoding, matches the encoded password from
	 * storage
	 */
	boolean matches(CharSequence rawPassword, String encodedPassword);

	/**
	 * Returns true if the encoded password should be encoded again for better security,
	 * else false. The default implementation always returns false.
	 * @param encodedPassword the encoded password to check
	 * @return true if the encoded password should be encoded again for better security,
	 * else false.
	 */
	default boolean upgradeEncoding(String encodedPassword) {
		return false;
	}

}
```



有多个实现接口，建议使用`BCryptPasswordEncoder`，单向加密

![image-20210825215432207](https://cdn.jsdelivr.net/gh/le1024/image1/le/image-20210825215432207.png)



- **BCryptPasswordEncoder**

  主要三个参数：

`strength`：密码强度，4~31，默认值-1，默认10强度

![image-20210825220506172](https://cdn.jsdelivr.net/gh/le1024/image1/le/image-20210825220506172.png)



`version`：加密版本，默认[BCryptVersion.$2A]()

![image-20210825220838758](https://cdn.jsdelivr.net/gh/le1024/image1/le/image-20210825220838758.png)



`random`：随机数，默认[null]()，框架会自己实现随机数

![image-20210825223745934](https://cdn.jsdelivr.net/gh/le1024/image1/le/image-20210825223745934.png)





- **加密方式**

通过`version`，`strength`，`random`获取盐，用盐和明文密码实现加密，这样加密的情况，即使是相同的密码，最后的加密的结果也是不一样的。

![image-20210825221556703](https://cdn.jsdelivr.net/gh/le1024/image1/le/image-20210825221556703.png)



- **实现密码匹配**

![image-20210825221854867](https://cdn.jsdelivr.net/gh/le1024/image1/le/image-20210825221854867.png)



> 代码测试

```java
public static void main(String[] args) {
    PasswordEncoder passwordEncoder = new BCryptPasswordEncoder();

    //明文密码
    String rawPassword = "123456";
    //加密密码
    String enPassword = passwordEncoder.encode(rawPassword);
    System.out.println(enPassword);

    //匹配密码
    boolean matches = passwordEncoder.matches(rawPassword, enPassword);
    System.out.println(matches);
}
```

