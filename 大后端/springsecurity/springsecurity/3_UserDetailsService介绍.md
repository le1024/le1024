因为集成`springsecurity`之后，默认用户名是user，密码也是直接打印在控制台的，这个是不符合正常项目开发的.

需要进行改造，和实际业务代码结合起来应用



> UserDetailsService

首先了解`UserDetailsService`接口，通过实现这个接口去加载我们项目中的自己配置的用户信息

用户不存在将会抛异常：`UsernameNotFoundException`

```java
public interface UserDetailsService {

	/**
	 * Locates the user based on the username. In the actual implementation, the search
	 * may possibly be case sensitive, or case insensitive depending on how the
	 * implementation instance is configured. In this case, the <code>UserDetails</code>
	 * object that comes back may have a username that is of a different case than what
	 * was actually requested..
	 * @param username the username identifying the user whose data is required.
	 * @return a fully populated user record (never <code>null</code>)
	 * @throws UsernameNotFoundException if the user could not be found or the user has no
	 * GrantedAuthority
	 */
    // 自定义需要实现这个
	UserDetails loadUserByUsername(String username) throws UsernameNotFoundException;

}
```

![image-20210825213646818](https://cdn.jsdelivr.net/gh/le1024/image1/le/image-20210825213646818.png)



> UserDetails

用户的相关信息

```java
public interface UserDetails extends Serializable {

	/** 
	 * 用户权限，不可返回null ！！！
	 * Returns the authorities granted to the user. Cannot return <code>null</code>.
	 * @return the authorities, sorted by natural key (never <code>null</code>)
	 */
	Collection<? extends GrantedAuthority> getAuthorities();

	/**
	 * 密码
	 * Returns the password used to authenticate the user.
	 * @return the password
	 */
	String getPassword();

	/** 
	 * 用户名
	 * Returns the username used to authenticate the user. Cannot return
	 * <code>null</code>.
	 * @return the username (never <code>null</code>)
	 */
	String getUsername();

	/** 
	 * 是否过期
	 * Indicates whether the user's account has expired. An expired account cannot be
	 * authenticated.
	 * @return <code>true</code> if the user's account is valid (ie non-expired),
	 * <code>false</code> if no longer valid (ie expired)
	 */
	boolean isAccountNonExpired();

	/** 
	 * 是否锁定
	 * Indicates whether the user is locked or unlocked. A locked user cannot be
	 * authenticated.
	 * @return <code>true</code> if the user is not locked, <code>false</code> otherwise
	 */
	boolean isAccountNonLocked();

	/** 
	 * 凭证(密码)是否过期
	 * Indicates whether the user's credentials (password) has expired. Expired
	 * credentials prevent authentication.
	 * @return <code>true</code> if the user's credentials are valid (ie non-expired),
	 * <code>false</code> if no longer valid (ie expired)
	 */
	boolean isCredentialsNonExpired();

	/**
	 * 用户是否启用
	 * Indicates whether the user is enabled or disabled. A disabled user cannot be
	 * authenticated.
	 * @return <code>true</code> if the user is enabled, <code>false</code> otherwise
	 */
	boolean isEnabled();

}
```



> UsernameNotFoundException

用户名查询不到异常。

在`loadUserByUsername`中通过自己实现的业务逻辑从数据库中查询用户，如果查询不到对应数据，应抛出`UsernameNotFoundException`