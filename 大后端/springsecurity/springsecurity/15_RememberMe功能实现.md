> Remember，记住我



**http.rememberMe()**

**`.userDetailsService(UserDetailsService)`：自定义登录逻辑，入参是实现`UserDetailsService`的Service**

![image-20210831180807686](https://cdn.jsdelivr.net/gh/le1024/image1/le/image-20210831180807686.png)



**`.tokenRepository(PersistentTokenRepository)`：指定登录数据存储位置，实现方式有两种：内存方式和JDBC方式，一般使用JDBC。**

![image-20210831174406554](https://cdn.jsdelivr.net/gh/le1024/image1/le/image-20210831174406554.png)



**`.rememberMeParameter`：前端记住我功能参数名，默认`remember-me`**

![image-20210831175809428](https://cdn.jsdelivr.net/gh/le1024/image1/le/image-20210831175809428.png)

![image-20210831175840228](https://cdn.jsdelivr.net/gh/le1024/image1/le/image-20210831175840228.png)



**`tokenValiditySeconds`：失效时间，默认2周**

点击`.tokenValiditySeconds`进入到`RememberMeConfigurer`类中，找到`tokenValiditySeconds`参数，点击进入到`tokenRememberMeServices.setTokenValiditySeconds(this.tokenValiditySeconds);`

![image-20210831180042221](https://cdn.jsdelivr.net/gh/le1024/image1/le/image-20210831180042221.png)

![image-20210831180603136](https://cdn.jsdelivr.net/gh/le1024/image1/le/image-20210831180603136.png)

点击`setTokenValiditySeconds`方法：

![image-20210831180636976](https://cdn.jsdelivr.net/gh/le1024/image1/le/image-20210831180636976.png)

点击`tokenValiditySeconds`，可以看到默认的过期时间是`TWO_WEEKS_S`

![image-20210831180648243](https://cdn.jsdelivr.net/gh/le1024/image1/le/image-20210831180648243.png)









> 功能实现

`application.yml`：

```yaml
spring:
  datasource:
    driver-class-name: com.mysql.jdbc.Driver
    url: jdbc:mysql://localhost:3306/springsecurity?useUnicode=true&characterEncoding=UTF-8&serverTimZone=Asia/Shanghai
    username: root
    password: root
```



`SecurityConfig`：

```java
@Autowired
private UserServiceImpl userService;
@Autowired
private DataSource dataSource;
@Autowired
private PersistentTokenRepository tokenRepository;

//记住我
http.rememberMe()
    //自定义参数，默认remember-me
    //.rememberMeParameter()
    //自动以失效时间，默认2周
    //.tokenValiditySeconds()
    //自定义登录逻辑
    .userDetailsService(userService)
    //指定登录数据存储位置
    .tokenRepository(tokenRepository);
```

```java
/*写在SecurityConfig类中*/
@Bean
public PersistentTokenRepository tokenRepository() {
    JdbcTokenRepositoryImpl jdbcTokenRepository = new JdbcTokenRepositoryImpl();
    // 设置数据源
    jdbcTokenRepository.setDataSource(dataSource);
    // 启动的时候创建表，第一次启动的时候开启，后面需要注释
    jdbcTokenRepository.setCreateTableOnStartup(true);
    return jdbcTokenRepository;
}
```

完成配置启动项目：会自动生成`persistent_logins`表

![image-20210831183005147](https://cdn.jsdelivr.net/gh/le1024/image1/le/image-20210831183005147.png)



`login.html`：

```html
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Title</title>
</head>
<body>
    <form action="/login" method="post">
        用户名：<input type="text" name="username" />
        密码：<input type="password" name="password" />
        <!-- remember-me：默认参数名 -->
        <input type="checkbox" name="remember-me" value="true">记住我
        <input type="submit" value="登录" />
    </form>
</body>
</html>
```



访问登录，选中`记住我`

![image-20210831183804259](https://cdn.jsdelivr.net/gh/le1024/image1/le/image-20210831183804259.png)

登录成功后，跳转到`success.html`，关闭浏览器，再去访问`success.html`会直接访问成功，说明`RememberMe`功能实现成功。