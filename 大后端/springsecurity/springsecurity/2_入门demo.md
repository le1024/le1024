> demo

新建一个springboot工程项目：

#### 1.添加依赖

创建springboot工程时，直接勾选web和security

```xml
<dependencies>
    <!--springsecurity-->
    <dependency>
        <groupId>org.springframework.boot</groupId>
        <artifactId>spring-boot-starter-security</artifactId>
    </dependency>
    <!--web-->
    <dependency>
        <groupId>org.springframework.boot</groupId>
        <artifactId>spring-boot-starter-web</artifactId>
    </dependency>
</dependencies>
```

#### 2.启动项目

集成`spring security`后，默认`spring security`就是生效的。

成功启动，可以控制台看到登录密码：

![image-20210825143056960](https://cdn.jsdelivr.net/gh/le1024/image1/le/image-20210825143056960.png)



#### 3.访问

`http://localhost:8080/login`，会进入到springsecurity默认的登录页.

默认用户名：user，密码：查看控制台

![image-20210825143602286](https://cdn.jsdelivr.net/gh/le1024/image1/le/image-20210825143602286.png)