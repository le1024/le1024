> 访问URL配置

`http.authorizeRequests`：授权，配置用户的访问的URL，实现类似于一个拦截器的功能。



示例：

```java
http.authorizeRequests()
    //放行登录页，失败页
    .antMatchers("/login.html", "/error.html").permitAll()
    //所有请求必须被认证
    .anyRequest().authenticated();
```



##### 1.anyRequest()

​	在之前写的示例中已经使用过`anyRequest()`，表示匹配所有的请求。一般情况下次方法都会使用，设置所有的请求内容都需要认证。

```java
//这个anyRequest需要写在http.authorizeRequests配置最后面
.anyRequest().authenticated();
```



##### 2.antMatcher()

源码：

```java
public C antMatchers(String... antPatterns) {
        Assert.state(!this.anyRequestConfigured, "Can't configure antMatchers after anyRequest");
        return this.chainRequestMatchers(AbstractRequestMatcherRegistry.RequestMatchers.antMatchers(antPatterns));
}
```

参数是不定项的，每个参数是一个ant表达式，用户匹配URL规则。

规则如下：

- `?`：匹配一个字符
- `*`：匹配0个或多个字符
- `*`：匹配0个或多个目录

在实际项目中放行所有静态资源，例：放行js文件

```java
.antMatchers("/js/**").permitAll()
```

```java
.antMatchers("/**/*.js").permitAll()
```



##### 3.regexMatchers()

###### 3.1 介绍

使用正则进行匹配。和`antMatchers()`主要区别就是参数。`antMatchers()`参数是ant表达式，`regexMatchers()`参数是正则表达式。

举例：.js结尾的文件都放行

```java
.regexMatchers(".*.[.]js").permitAll()
```

###### 3.2参数使用方式

源码中可得知：`antMatchers()`和`regexMatchers()`都有两个入参，`HttpMethod`表示请求方式

![image-20210827173434614](https://cdn.jsdelivr.net/gh/le1024/image1/le/image-20210827173434614.png)

![image-20210827173421257](https://cdn.jsdelivr.net/gh/le1024/image1/le/image-20210827173421257.png)



规定`/xxxx`请求只能通过`POST`请求去访问

```java
regexMatchers(HttpMethod.POST, "/xxxx").permitAll()
```

