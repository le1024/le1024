> access

授权控制的相关方法：`permiAll`、`denyAll`、`hasRole` `....`等都是基于`access表达式`实现的

相关源码：

![image-20210830211027946](https://cdn.jsdelivr.net/gh/le1024/image1/le/image-20210830211027946.png)

![image-20210830211052302](https://cdn.jsdelivr.net/gh/le1024/image1/le/image-20210830211052302.png)

![image-20210830211905342](https://cdn.jsdelivr.net/gh/le1024/image1/le/image-20210830211905342.png)



所以授权方式的代码也可以使用`access`的方式：

```java
.antMatchers("/login.html").access("permitAll()")
.antMatchers("/main.html").access("hasIpAddress('127.0.0.1')")
```



> 自定义access

根据当前用户的权限去访问



- 接口及实现类

```java
public interface MyService {


    /**
     * 自定义方法名
     * @param request 可获取到当前访问路径
     * @param authentication 可获取当前登录信息，登录权限等
     * @return
     */
    boolean hasPermission(HttpServletRequest request, Authentication authentication);
}
```

```java
@Service
public class MyServiceImpl implements MyService {
    @Override
    public boolean hasPermission(HttpServletRequest request, Authentication authentication) {
        //获取当前uri
        String uri = request.getRequestURI();

        //拿到当前登录信息
        Object principal = authentication.getPrincipal();
        if (principal instanceof UserDetails) {
            UserDetails userDetails = ((UserDetails) principal);
            //获取登录用户拥有的权限
            Collection<? extends GrantedAuthority> authorities = userDetails.getAuthorities();

            /**
             * authorities权限集合对象的泛型是GrantedAuthority,GrantedAuthority有个具体实现对象是SimpleGrantedAuthority
             * 所以我们判断authorities集合中有没有访问uri的权限，可以判断authorities是否包含uri，需要用SimpleGrantedAuthority对象去具体封装一下
             * new SimpleGrantedAuthority(uri)
             */

            return authorities.contains(new SimpleGrantedAuthority(uri));
        }

        return false;
    }
}
```



- 修改`SecurityConfig`

```java
//所有请求必须被认证,这个注释掉
//.anyRequest().authenticated();
//通过自定义的access实现用户权限
.anyRequest().access("@myServiceImpl.hasPermission(request, authentication)")
```



- 用户权限配置

```java
// 配置用户可访问 success.html
AuthorityUtils.commaSeparatedStringToAuthorityList("admin, normal, /success.html")
```



- 启动服务访问

登录成功后跳转到 `success.html`页面

![image-20210830214538400](https://cdn.jsdelivr.net/gh/le1024/image1/le/image-20210830214538400.png)

访问`main.html`会提示没有权限

![image-20210830214552970](https://cdn.jsdelivr.net/gh/le1024/image1/le/image-20210830214552970.png)

