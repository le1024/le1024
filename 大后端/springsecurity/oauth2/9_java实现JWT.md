> JAVA实现JWT

JWT针对很多语言有很多实现的工具库。java里面最常用的就是JJWT。

在官网可以看到：https://jwt.io/#debugger



依赖导入：

```xml
<!--jwt依赖-->
<dependency>
    <groupId>io.jsonwebtoken</groupId>
    <artifactId>jjwt</artifactId>
    <version>0.9.0</version>
</dependency>
```





#### [#生成Token](#)



```java
    @Test
    public void createToken() {
        JwtBuilder builder = Jwts.builder()
                //详情说明可以点进去看源码说明
                //唯一标识 {"jti":"1"}
                .setId("1")
                //签发用户 {"sub": "admin"}
                .setSubject("admin")
                //签发时间 {"iat":"2021-9-13 21:15:25"}
                .setIssuedAt(new Date())
                //秘钥
                .signWith(SignatureAlgorithm.HS256, "salt");

        String token = builder.compact();
        System.out.println(token);
        String[] str = token.split("\\.");
        //头部
        System.out.println(Base64Codec.BASE64.decodeToString(str[0]));
        //有效载荷
        System.out.println(Base64Codec.BASE64.decodeToString(str[1]));
        //签名
        System.out.println(str[2]);
    }
```



![image-20210913213018842](https://cdn.jsdelivr.net/gh/le1024/image1/le/image-20210913213018842.png)

[由于时间戳，每次生成的token值都是不一样的](#)



#### [#Token的验证解析](#)

```java
    @Test
    public void parseToken() {
        String token = "eyJhbGciOiJIUzI1NiJ9.eyJqdGkiOiIxIiwic3ViIjoiYWRtaW4iLCJpYXQiOjE2MzE1Mzk2ODl9.sxB284IJVbL8AoSWgZDVjnP_QICAH7ip_Q1wcN40pjc";

        //获取claims，有效荷载
        Claims claims = Jwts.parser()
                //秘钥
                .setSigningKey("salt")
                .parseClaimsJws(token)
                .getBody();

        System.out.println("jti:" + claims.getId());
        System.out.println("sub:" + claims.getSubject());
        System.out.println("iat:" + claims.getIssuedAt());
    }
```

![image-20210913214020504](https://cdn.jsdelivr.net/gh/le1024/image1/le/image-20210913214020504.png)



####  [#Token有效期](#)

设置`setExpiration`

```java
    @Test
    public void createTokenHasExp() {
        long now = System.currentTimeMillis();
        long exp = now + 60 * 1000; //过期时间

        JwtBuilder builder = Jwts.builder()
                //详情说明可以点进去看源码说明
                //唯一标识 {"jti":"1"}
                .setId("1")
                //签发用户 {"sub": "admin"}
                .setSubject("admin")
                //签发时间 {"iat":"2021-9-13 21:15:25"}
                .setIssuedAt(new Date())
                //秘钥
                .signWith(SignatureAlgorithm.HS256, "salt")
                //带有过期时间 {"exp": "xxxxx"}
                .setExpiration(new Date(exp));

        String token = builder.compact();
        System.out.println(token);
        String[] str = token.split("\\.");
        //头部
        System.out.println(Base64Codec.BASE64.decodeToString(str[0]));
        //有效载荷
        System.out.println(Base64Codec.BASE64.decodeToString(str[1]));
        //签名
        System.out.println(str[2]);
    }

```

解析可获得过期时间：

```java
claims.getExpiration()
```

如果已过有效期，再解析会报异常：

![image-20210913215403308](https://cdn.jsdelivr.net/gh/le1024/image1/le/image-20210913215403308.png)



#### [#自定义声明Token](#)

`claims`方法实现

```java
    @Test
    public void createTokenCustom() {
        JwtBuilder builder = Jwts.builder()
                //详情说明可以点进去看源码说明
                //唯一标识 {"jti":"1"}
                .setId("1")
                //签发用户 {"sub": "admin"}
                .setSubject("admin")
                //签发时间 {"iat":"2021-9-13 21:15:25"}
                .setIssuedAt(new Date())
                //秘钥
                .signWith(SignatureAlgorithm.HS256, "salt")
                //自定义
                .claim("city", "合肥")
                .claim("爱好", "大保健");

        String token = builder.compact();
        System.out.println(token);
        String[] str = token.split("\\.");
        //头部
        System.out.println(Base64Codec.BASE64.decodeToString(str[0]));
        //有效载荷
        System.out.println(Base64Codec.BASE64.decodeToString(str[1]));
        //签名
        System.out.println(str[2]);
    }
```

![image-20210913220138734](https://cdn.jsdelivr.net/gh/le1024/image1/le/image-20210913220138734.png)



解析：当做一个Map集合去获取值

![image-20210913220343042](https://cdn.jsdelivr.net/gh/le1024/image1/le/image-20210913220343042.png)

