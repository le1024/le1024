> SpringSecurity OAuth2整合单点登录(SSO)



**SSO**

单点登录，Single Sign On（SSO）指的是在多个应用系统中，只需登录一次，就可以访问其他相互信任的应用系统。



**[代码实现](#)**

需要两个服务，授权服务，客户端服务

#####  客户端服务

1.创建一个`springsecurity-sso-client`项目，POM引用同整合JWT的一致。

`application.yml`

```yaml
server:
  port: 8081 #客户端端口
  servlet:
    session:
      cookie:
        name: OAUTH2-CLIENT-SESSIONID01 #防止cookie冲突，冲突会导致登录验证不通过

#授权服务器地址
oauth2-server-url: http://localhost:8080
#与授权服务器对应的配置
security:
  oauth2:
    client:
      client-id: admin
      client-secret: 112233
      user-authorization-uri: ${oauth2-server-url}/oauth/authorize #获取授权码接口
      access-token-uri: ${oauth2-server-url}/oauth/token #获取令牌接口
    resource:
      jwt:
        key-uri: ${oauth2-server-url}/oauth/token_key #资源服务器jwt令牌端点

```

2.写个Controller，资源类

```java
@RestController
@RequestMapping("/user")
public class UserController {

    @GetMapping("/getCurrentUser")
    public Object getCurrentUser(Authentication authentication) {
        return authentication;
    }
}
```

3.**开放SSO**

主启动类，加上`@EnableOAuth2Sso`开放sso

```java
@SpringBootApplication
@EnableOAuth2Sso //启动sso
public class AppSpringsecuritySsoClientApplication {

    public static void main(String[] args) {
        SpringApplication.run(AppSpringsecuritySsoClientApplication.class, args);
    }

}
```



##### 授权服务端

基础框架同整合JWT一样，在此基础上只要改动`AuthorizationServerConfig`授权配置类即可

`.redirectUris("http://client")`：之前是随便重定向一个地址，获取授权码，现在<font color="green">重定向到客户端地址</font>

`.autoApprove(true)`：自动授权，不再弹出授权页面

完整如下：

```java
@Configuration
@EnableAuthorizationServer
public class AuthorizationServerConfig extends AuthorizationServerConfigurerAdapter {

    @Autowired
    private PasswordEncoder passwordEncoder;
    @Autowired
    private UserService userService;
    @Autowired
    private AuthenticationManager authenticationManager;

    @Autowired
    @Qualifier("jwtTokenStore")
    private TokenStore tokenStore;
    @Autowired
    private JwtAccessTokenConverter jwtAccessTokenConverter;
    @Autowired
    private TokenEnhancer tokenEnhancer;

    /**
     * 配置密码模式
     */
    @Override
    public void configure(AuthorizationServerEndpointsConfigurer endpoints) throws Exception {
        //实现扩展Token
        TokenEnhancerChain tokenEnhancerChain = new TokenEnhancerChain();
        List<TokenEnhancer> delegates = new ArrayList<>();
        //加上自定义扩展
        delegates.add(tokenEnhancer);
        //这里配上jwtToken转换，不然自定义的TokenEnhancer增强的还是OAuth2的token
        delegates.add(jwtAccessTokenConverter);
        tokenEnhancerChain.setTokenEnhancers(delegates);

        endpoints
                // 自定义登录逻辑
                .userDetailsService(userService)
                // 授权管理器
                .authenticationManager(authenticationManager)
                //令牌存储位置
                .tokenStore(tokenStore)
                .accessTokenConverter(jwtAccessTokenConverter)
                //完成jwt扩展配置
                .tokenEnhancer(tokenEnhancerChain);
    }


    @Override
    public void configure(ClientDetailsServiceConfigurer clients) throws Exception {
        //demo 用内存方式
        clients.inMemory()
                //客户端凭证：clientId和client密码
                //clientId
                .withClient("admin")
                //client密码
                .secret(passwordEncoder.encode("112233"))
                //重定向地址，现在重定向到客户端地址
                .redirectUris("http://localhost:8081/login")
                //自动授权，不需要手动去确认授权
                .autoApprove(true)
                //授权范围
                .scopes("all")
                //访问令牌失效时间，根据需要设置
                .accessTokenValiditySeconds(3600)
                //刷新令牌实现时间，根据需要设置
                .refreshTokenValiditySeconds(18400)
                //授权类型,可以配置多个
                //添加refresh_token，刷新令牌机制
                .authorizedGrantTypes("authorization_code", "password", "refresh_token");

        //如果有多个客户端，可以写多个继续配置其他客户端信息
        //clients.inMemory()....
    }

    @Override
    public void configure(AuthorizationServerSecurityConfigurer security) throws Exception {
        //access表达式，isAuthenticated：如果不是匿名访问的返回true
        security.tokenKeyAccess("isAuthenticated()");
    }
}
```



##### 测试

先启动授权服务，再启动客户端

访问资源地址：http://localhost:8081/user/getCurrentUser

会跳转到授权服务的8080端口登录地址

![image-20210916130352373](https://cdn.jsdelivr.net/gh/le1024/image1/le/image-20210916130352373.png)

输入用户名和密码，这个用户名和密码是认证服务器的密码不是客户端

登录成功，可成功返回资源地址返回的信息；即通过8080端授权，8081端获取信息

![image-20210916130634325](https://cdn.jsdelivr.net/gh/le1024/image1/le/image-20210916130634325.png)

