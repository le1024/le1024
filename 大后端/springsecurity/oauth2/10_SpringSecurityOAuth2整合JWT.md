> SpringSecurity OAuth2整合JWT

代码可在之前的密码模式的基础下修改，添加jwt的依赖



`SecurityConfig`，`ResourceServerConfig`配置类可不动

新增一个`JwtConfig`

```java
@Configuration
public class JwtConfig {

    @Bean
    public TokenStore jwtTokenStore() {
        return new JwtTokenStore(jwtAccessTokenConverter());
    }

    @Bean
    public JwtAccessTokenConverter jwtAccessTokenConverter() {
        JwtAccessTokenConverter jwtAccessTokenConverter = new JwtAccessTokenConverter();
        //你的秘钥
        jwtAccessTokenConverter.setSigningKey("secret");
        return jwtAccessTokenConverter;
    }
}
```

`AuthorizationServerConfig`修改如下：

```java
    @Autowired
    @Qualifier("jwtTokenStore")
    private TokenStore tokenStore;
    @Autowired
    private JwtAccessTokenConverter jwtAccessTokenConverter;

    /**
     * 配置密码模式
     */
    @Override
    public void configure(AuthorizationServerEndpointsConfigurer endpoints) throws Exception {
        endpoints
                // 自定义登录逻辑
                .userDetailsService(userService)
                // 授权管理器
                .authenticationManager(authenticationManager)
                //令牌存储位置
                .tokenStore(tokenStore)
                .accessTokenConverter(jwtAccessTokenConverter);
    }
```

启动项目测试，用postman访问`http://localhost:8080/oauth/token`获取token：

![image-20210914210437048](https://cdn.jsdelivr.net/gh/le1024/image1/le/image-20210914210437048.png)

可以复制token，去

[jwt网站是校验token]:https://jwt.io/

![image-20210914210558743](https://cdn.jsdelivr.net/gh/le1024/image1/le/image-20210914210558743.png)



通过token，去访问项目资源：

![image-20210914210653264](https://cdn.jsdelivr.net/gh/le1024/image1/le/image-20210914210653264.png)