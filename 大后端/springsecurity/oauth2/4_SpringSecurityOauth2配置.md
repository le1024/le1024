**Spring Security Oauth2**配置

------



#####  1.授权码模式

> 代码

`pom`

```xml
<properties>
        <java.version>1.8</java.version>
        <spring-cloud.version>Hoxton.SR12</spring-cloud.version>
    </properties>
    <dependencies>
        <dependency>
            <groupId>org.springframework.cloud</groupId>
            <artifactId>spring-cloud-starter-oauth2</artifactId>
        </dependency>
        <dependency>
            <groupId>org.springframework.cloud</groupId>
            <artifactId>spring-cloud-starter-security</artifactId>
        </dependency>
    </dependencies>

    <!--通过spring-cloud引入spring security-->
    <dependencyManagement>
        <dependencies>
            <dependency>
                <groupId>org.springframework.cloud</groupId>
                <artifactId>spring-cloud-dependencies</artifactId>
                <version>${spring-cloud.version}</version>
                <type>pom</type>
                <scope>import</scope>
            </dependency>
        </dependencies>
    </dependencyManagement>
```



`Spring Security配置类`

```java
@Configuration
@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter {

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.csrf()
                .disable()
                .authorizeRequests() //授权
                /**
                 * 允许/oauth/**，因为授权服务器的接口地址都是这个
                 */
                .antMatchers("/oauth/**", "/login/**", "/logout/**").permitAll() //放行接口
                .anyRequest().authenticated() //所有请求需要被认证
                .and()
                .formLogin().permitAll(); //放行登录
    }

    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }
}
```

`授权服务器配置类`

```java
@Configuration
@EnableAuthorizationServer
public class AuthorizationServerConfig extends AuthorizationServerConfigurerAdapter {

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Override
    public void configure(ClientDetailsServiceConfigurer clients) throws Exception {
        //demo 用内存方式
        clients.inMemory()
                //客户端凭证：clientId和client密码
                //clientId
                .withClient("admin")
                //client密码 不是自定义登录逻辑里面的
                .secret(passwordEncoder.encode("123456"))
                //重定向地址，这个地址可以不用管，可自己任意配置，主要是获取授权码
                .redirectUris("http://www.baidu.com")
                //授权范围
                .scopes("all")
                //授权类型
                .authorizedGrantTypes("authorization_code");

        //可以使用jdbc的方式，凭证、密码、授权都可以在数据库配置，然后根据这些条件去查询是否可以颁发授权码
    }
}
```

`资源服务器配置类`

```java
@Configuration
@EnableResourceServer
public class ResourceServerConfig extends ResourceServerConfigurerAdapter {

    @Override
    public void configure(HttpSecurity http) throws Exception {
        http.authorizeRequests()
                .anyRequest().authenticated() //所有的都需要认证
                .and()
                //有令牌的时候放行/user/**
                .requestMatchers().antMatchers("/user/**");
    }
}
```

`UserService`

```java
@Service
public class UserService implements UserDetailsService {
    @Autowired
    private PasswordEncoder passwordEncoder;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        // 这个User是自己创建的User对象
        return new User("admin", passwordEncoder.encode("123456"),
                AuthorityUtils.commaSeparatedStringToAuthorityList("admin"));
    }
}
```

`User对象`

```java
@AllArgsConstructor
@NoArgsConstructor
public class User implements UserDetails {

    private String username;

    private String password;

    private List<GrantedAuthority> authorities;

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return authorities;
    }

    @Override
    public String getPassword() {
        return password;
    }

    @Override
    public String getUsername() {
        return username;
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return true;
    }
}
```

再写一个简单的资源，获取AccessToken之后可以访问

```java
@RestController
@RequestMapping("/user")
public class UserController {

    @RequestMapping("/getCurrnetUser")
    public Object getCurrentUser(Authentication authentication) {
        return authentication.getPrincipal();
    }
}
```





> 测试

测试的时候，更新了代码没有生效的话，把maven clean一下，然后再maven install，之后重启访问                      



1. 获取授权码

   1.1 访问地址：后面的参数值是在`授权服务器配置类`里配置的

   http://localhost:8080/oauth/authorize?response_type=code&client_id=admin&redirect_uri=http://www.baidu.com&scope=all

   同Spring Security配置一样，访问开始是需要登录的，账户和密码是`UserService`里面的

   ![image-20210902233706533](https://cdn.jsdelivr.net/gh/le1024/image1/le/image-20210902233706533.png)

   1.2 登录成功后，进入授权页面，`Approve`同意授权，`Deny`拒绝，点击`Authorize`授权

   ![，](https://cdn.jsdelivr.net/gh/le1024/image1/le/image-20210902233814775.png)

   1.3 会跳转到配置的uri，`code`就是授权码：

   ![image-20210902233937697](https://cdn.jsdelivr.net/gh/le1024/image1/le/image-20210902233937697.png)

   1.4 用postman模拟一个post请求，通过`授权码`去获取`令牌`，请求地址：`/oauth/token`

   这里的账户和密码是`AuthorizationServerConfig`里的
   
   ![image-20210902234345659](https://cdn.jsdelivr.net/gh/le1024/image1/le/image-20210902234345659.png)



1.5 继续填写其他需要的参数，`grant_type`、`client_id`、`redirect_uri`、`scope`和授权服务器配置类配置参数一致

`code`：授权之后跳转的uri后面带的code

**授权码code只能使用一次，请求失败需要重新获取授权码，之前的授权码会失效！！！！**

![image-20210902234650652](https://cdn.jsdelivr.net/gh/le1024/image1/le/image-20210902234650652.png)



1.6 发送请求：成功获取访问令牌`AccessToken`，令牌类型是：`bearer`

![image-20210902235347462](https://cdn.jsdelivr.net/gh/le1024/image1/le/image-20210902235347462.png)



1.7 通过令牌去访问资源（上面代码创建的Controller）

![image-20210902235735931](https://cdn.jsdelivr.net/gh/le1024/image1/le/image-20210902235735931.png)





![image-20210902235812327](https://cdn.jsdelivr.net/gh/le1024/image1/le/image-20210902235812327.png)



------



##### 2.密码模式

> 代码

在**授权码模式**代码基础上更改一部分就可。

`SecurityConfig`

添加`AuthenticationManager`授权管理器

```java
@Bean
@Override
protected AuthenticationManager authenticationManager() throws Exception {
    return super.authenticationManager();
}
```



`AuthorizationServerConfig`

```java
@Autowired
private UserService userService;
@Autowired
private AuthenticationManager authenticationManager;

/**
 * 配置密码模式
 */
@Override
public void configure(AuthorizationServerEndpointsConfigurer endpoints) throws Exception {
    endpoints
        // 自定义登录逻辑
        .userDetailsService(userService)
        // 授权管理器
        .authenticationManager(authenticationManager);
}
```

然后授权类型，再配置上`password`

```java
//授权类型,可以配置多个
.authorizedGrantTypes("authorization_code", "password");
```



> 测试

`grant_type`改为`password`，其余参数为`scope`、`username`、`password`

![image-20210903215150144](https://cdn.jsdelivr.net/gh/le1024/image1/le/image-20210903215150144.png)

