**Spring Security Oauth2**

------



##### 1.授权服务器

授权相关的功能都在这个授权服务器中，**主要有四个端点，也就是接口请求的地址**：

- `Authorize Endpoint`：授权端点，进行授权

  接口：`/oauth/authorize`

- `Token Endpoint`：令牌端点，经过授权拿到对应的Token

  接口：`/oauth/token`，POST请求

- `Introspection Endpoint`：校验端点，校验Token的合法性

  接口：`/oauth/introspect`

- `Revocation Endpoint`：撤销端点，撤销授权

  接口：`/oauth/revoke`



![image-20210901223504845](https://cdn.jsdelivr.net/gh/le1024/image1/le/image-20210901223504845.png)



##### 2.Spring Security Oauth2架构

![img](https://cdn.jsdelivr.net/gh/le1024/image1/le/751560-20190312201914220-1038764382.png)

流程：

1. 用户访问，此时没有token 。`OAuth2RestTemaplte`会报错，这个报错信息会被`OAuth2ClientContextFilter`捕获并重定向到认证服务器
2. 认证服务器通过`Authorization Endpoint`进行授权，并通过`AuthorizationServerTokenServices`生成授权码返回给客户端
3. 客户端拿到授权码去认证服务器通过`Token Endpoint`调用`AuthorizationServerTokenServices`生成`Token`并返回给客户端
4. 客户端拿到`Token`去资源服务器访问资源，一般会通过`OAuth2AuthenticationManager`调用`ResourceServerTokenServices`进行校验，校验通过可以获取资源