> 自定义Oauth2授权页面



这个是默认的Oauth2的授权页面，实际项目中需要自定义一个比较好看的

![image-20210904113100812](https://cdn.jsdelivr.net/gh/le1024/image1/le/image-20210904113100812.png)

> 前后端不分离，springboot + thymeleaf

##### #前后端不分离，springboot + thymeleaf

页面：页面样式来源于网络

**登录页`custom_login.html`**

```html
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:th="http://www.thymeleaf.org">
<head>
  <meta charset="UTF-8">
  <title>登录</title>
</head>

<style>
  .login-container {
    margin: 50px;
    width: 100%;
  }

  .form-container {
    margin: 0px auto;
    width: 50%;
    text-align: center;
    box-shadow: 1px 1px 10px #888888;
    height: 300px;
    padding: 5px;
  }

  input {
    margin-top: 10px;
    width: 350px;
    height: 30px;
    border-radius: 3px;
    border: 1px #E9686B solid;
    padding-left: 2px;

  }


  .btn {
    width: 350px;
    height: 35px;
    line-height: 35px;
    cursor: pointer;
    margin-top: 20px;
    border-radius: 3px;
    background-color: #E9686B;
    color: white;
    border: none;
    font-size: 15px;
  }

  .title{
    margin-top: 5px;
    font-size: 18px;
    color: #E9686B;
  }
</style>
<body>
<div class="login-container">
  <div class="form-container">
    <p class="title">用户登录</p>
    <form name="loginForm" method="post" action="/login">
      <input type="text" name="username" placeholder="用户名"/>
      <br>
      <input type="password" name="password" placeholder="密码"/>
      <br>
      <button type="submit" class="btn">登 &nbsp;&nbsp; 录</button>
    </form>
    <p style="color: red" th:if="${param.error}">用户名或密码错误</p>
  </div>
</div>
</body>
</html>
```

**授权页`custom_grant.html`**

```html
<!DOCTYPE html>
<html lang="en" xmlns:th="http://www.thymeleaf.org">
<head>
    <meta charset="UTF-8">
    <title>授权</title>
</head>
<style>

    html{
        padding: 0px;
        margin: 0px;
    }

    .title {
        background-color: #E9686B;
        height: 50px;
        padding-left: 20%;
        padding-right: 20%;
        color: white;
        line-height: 50px;
        font-size: 18px;
    }
    .title-left{
        float: right;
    }
    .title-right{
        float: left;
    }
    .title-left a{
        color: white;
    }
    .container{
        clear: both;
        text-align: center;
    }
    .btn {
        width: 350px;
        height: 35px;
        line-height: 35px;
        cursor: pointer;
        margin-top: 20px;
        border-radius: 3px;
        background-color: #E9686B;
        color: white;
        border: none;
        font-size: 15px;
    }
</style>
<body style="margin: 0px">
<div class="title">
    <div class="title-right">OAUTH-BOOT 授权</div>
    <div class="title-left">
        <a href="#help">帮助</a>
    </div>
</div>
<div class="container">
    <h3 th:text="${clientId}+' 请求授权，该应用将获取你的以下信息'"></h3>
    <p>昵称，头像和性别</p>
    授权后表明你已同意 <a  href="#boot" style="color: #E9686B">OAUTH-BOOT 服务协议</a>
    <form method="post" action="/oauth/authorize">

        <input type="hidden" name="user_oauth_approval" value="true">
        <input type="hidden" name="_csrf" th:if="${_csrf}" th:value="${_csrf.getToken()}"/>

        <div th:each="item:${scopes}">
            <input type="radio" th:name="'scope.'+${item}" value="true" hidden="hidden" checked="checked"/>
        </div>

        <button class="btn" type="submit"> 同意/授权</button>

    </form>
</div>
</body>
</html>
```

后端：

**配置类`SecurityConfig`**

```java
/**
 * Spring Security配置类
 * @author helele
 * @date 2021/9/2 22:07
 */
@Configuration
@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter {

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.requestMatchers()
                .antMatchers("/oauth/**", "/custom/login", "/login")
                .and()
                .authorizeRequests()
                .antMatchers("/oauth/**","/custom/login", "/login")
                .permitAll()
                .anyRequest()
                .authenticated();

        http.formLogin()
                .loginPage("/custom/login")
                .loginProcessingUrl("/login");

        http.csrf().disable();
    }

    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }


}
```

**授权服务器配置类`AuthorizationServerConfig`**

```java
/**
 * 授权服务器配置类
 * @author helele
 * @date 2021/9/2 22:19
 */
@Configuration
@EnableAuthorizationServer
public class AuthorizationServerConfig extends AuthorizationServerConfigurerAdapter {

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Override
    public void configure(ClientDetailsServiceConfigurer clients) throws Exception {
        //demo 用内存方式
        clients.inMemory()
                //客户端凭证：clientId和client密码
                //clientId
                .withClient("admin")
                //client密码
                .secret(passwordEncoder.encode("112233"))
                //重定向地址，这个地址可以不用管，可自己任意配置，主要是获取授权码
                .redirectUris("http://www.baidu.com")
                //授权范围
                .scopes("all")
                //授权类型,可以配置多个
                .authorizedGrantTypes("authorization_code", "password");

        //可以使用jdbc的方式，凭证、密码、授权都可以在数据库配置，然后根据这些条件去查询是否可以颁发授权码
    }
}
```

**资源服务器配置类`ResourceServerConfig`**

```java
/**
 * 资源服务器配置类
 * @author helele
 * @date 2021/9/2 22:30
 */
@Configuration
@EnableResourceServer
public class ResourceServerConfig extends ResourceServerConfigurerAdapter {

    @Override
    public void configure(HttpSecurity http) throws Exception {
        http.authorizeRequests()
                .anyRequest().authenticated() //所有的都需要认证
                .and()
                //有令牌的时候放行/user/**
                .requestMatchers().antMatchers("/user/**");
    }
}
```

**登录接口`CustomLoginController`**

```java
@Controller
public class CustomLoginController {

    @GetMapping("/custom/login")
    public String login(Model model) {
        model.addAttribute("loginProcessUrl", "/oauth/authorize");
        return "custom_login";
    }
}
```

**登录接口`CustomAuthController`**

```java
@Controller
@SessionAttributes("authorizationRequest")
public class CustomAuthController {


    @RequestMapping("/oauth/confirm_access")
    public ModelAndView confirmAccess(Map<String, Object> model, HttpServletRequest request) {
        AuthorizationRequest authorizationRequest = (AuthorizationRequest) model.get("authorizationRequest");

        ModelAndView view = new ModelAndView();
        view.setViewName("custom_grant");
        view.addObject("clientId", authorizationRequest.getClientId());
        view.addObject("scopes",authorizationRequest.getScope());
        return view;
    }
}
```

**用户对象类`User`**

```java
@AllArgsConstructor
@NoArgsConstructor
public class User implements UserDetails {

    private String username;

    private String password;

    private List<GrantedAuthority> authorities;

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return authorities;
    }

    @Override
    public String getPassword() {
        return password;
    }

    @Override
    public String getUsername() {
        return username;
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return true;
    }
}
```

**实现类`UserService`**

```java
@Service
public class UserService implements UserDetailsService {
    @Autowired
    private PasswordEncoder passwordEncoder;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        // 这个User是自己创建的User对象
        return new User("admin", passwordEncoder.encode("123456"),
                AuthorityUtils.commaSeparatedStringToAuthorityList("admin"));
    }
}
```



- **测试**

访问：http://localhost:8080/oauth/authorize?response_type=code&client_id=admin&redirect_uri=http://www.baidu.com&scope=all

![image-20210904172950651](https://cdn.jsdelivr.net/gh/le1024/image1/le/image-20210904172950651.png)

登录后可看到授权页面

![image-20210904173050740](https://cdn.jsdelivr.net/gh/le1024/image1/le/image-20210904173050740.png)

后面的步骤就和之前的一样了





##### #pom

```xml
<properties>
        <java.version>1.8</java.version>
        <spring-cloud.version>Hoxton.SR12</spring-cloud.version>
    </properties>
    <dependencies>
        <dependency>
            <groupId>org.springframework.cloud</groupId>
            <artifactId>spring-cloud-starter-oauth2</artifactId>
        </dependency>
        <dependency>
            <groupId>org.springframework.cloud</groupId>
            <artifactId>spring-cloud-starter-security</artifactId>
        </dependency>
        <!--thymeleaf springsecurity5依赖-->
        <dependency>
            <groupId>org.thymeleaf.extras</groupId>
            <artifactId>thymeleaf-extras-springsecurity5</artifactId>
            <version>3.0.4.RELEASE</version>
        </dependency>

        <!--thymeleaf依赖-->
        <dependency>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-starter-thymeleaf</artifactId>
            <version>2.1.5.RELEASE</version>
        </dependency>
    </dependencies>

    <!--通过spring-cloud引入spring security-->
    <dependencyManagement>
        <dependencies>
            <dependency>
                <groupId>org.springframework.cloud</groupId>
                <artifactId>spring-cloud-dependencies</artifactId>
                <version>${spring-cloud.version}</version>
                <type>pom</type>
                <scope>import</scope>
            </dependency>
        </dependencies>
    </dependencyManagement>
```

