> 解析JWT的Token

前面通过Token获取资源时，认证方式是通过Basic Auth，现在更改为在请求头中加入**Authorization**



`Authorization`的参数格式为：`Bearer`+空格+token

![image-20210914221920762](https://cdn.jsdelivr.net/gh/le1024/image1/le/image-20210914221920762.png)



将资源请求里修改下，从Authorization获取到token并解析：

```java
@RestController
@RequestMapping("/user")
public class UserController {

    @RequestMapping("/getCurrent")
    public Object getCurrentUser(Authentication authentication, HttpServletRequest request) {
        //获取到请求头里面的authorization
        String authorization = request.getHeader("Authorization");
        //截取出token,去除Bearer和空格
        String token = authorization.substring(authorization.indexOf("Bearer") + 7);
        return Jwts.parser()
                //JwtConfig类中配置秘钥
                .setSigningKey("secret".getBytes(StandardCharsets.UTF_8))
                .parseClaimsJws(token)
                .getBody();
    }
}
```



启动项目测试：

正常获取Token

![image-20210914222144600](https://cdn.jsdelivr.net/gh/le1024/image1/le/image-20210914222144600.png)

复制Token，去请求资源服务：

![image-20210914222223197](https://cdn.jsdelivr.net/gh/le1024/image1/le/image-20210914222223197.png)

![image-20210914222246661](https://cdn.jsdelivr.net/gh/le1024/image1/le/image-20210914222246661.png)

可正常解析Token值，注意Authorization参数值：Bearer后面有空格