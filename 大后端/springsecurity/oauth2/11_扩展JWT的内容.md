> 扩展JWT内容

**`TokenEnhancer`**

通过这个类可以实现对JWT Token的增强策略，添加一些额外的信息



在`AuthorizationServerConfig`配置类中，通过`endpoints.tokenEnhancer`来实现

```java
endpoints.tokenEnhancer(tokenEnhancerChain);
```

![image-20210914215521035](https://cdn.jsdelivr.net/gh/le1024/image1/le/image-20210914215521035.png)

`tokenEnhancer(TokenEnhancer tokenEnhancer)`：通过接收**TokenEnhancer**参数，由于**TokenEnhancer**只能进行一个单一的配置，所以一般都是通过一个增强链**TokenEnhancerChain**实现多配置

![image-20210914215948627](https://cdn.jsdelivr.net/gh/le1024/image1/le/image-20210914215948627.png)





首先自定义一个类实现`TokenEnhancer`，实现扩展信息配置

```java
public class MyTokenEnhancer implements TokenEnhancer {

    @Override
    public OAuth2AccessToken enhance(OAuth2AccessToken accessToken, OAuth2Authentication authentication) {
        // setAdditionalInformation()接收的是map类型，定义一个map进行扩展参数信息
        Map<String, Object> map = new HashMap<>();
        //添加附加信息
        map.put("enhancer", "enhancer data");
        //注意，这一步也只是增强了OAuth2的Token，在AuthorizationServerConfig类中还是需要转成jwt的token，从而增强jwttoken
        ((DefaultOAuth2AccessToken) accessToken).setAdditionalInformation(map);
        return accessToken;
    }
}
```



`jwtConfig`添加`MyTokenEnhancer`注入

```java
    /**
     * @Bean 引入自定义的TokenEnhancer，方便引用
     * @return
     */
    @Bean
    public TokenEnhancer tokenEnhancer() {
        return new MyTokenEnhancer();
    }
```



`AuthorizationServerConfig`

```java
@Autowired
@Qualifier("jwtTokenStore")
private TokenStore tokenStore;
@Autowired
private JwtAccessTokenConverter jwtAccessTokenConverter;
@Autowired
private TokenEnhancer tokenEnhancer;

/**
     * 配置密码模式
     */
@Override
public void configure(AuthorizationServerEndpointsConfigurer endpoints) throws Exception {
    //实现扩展Token
    TokenEnhancerChain tokenEnhancerChain = new TokenEnhancerChain();
    List<TokenEnhancer> delegates = new ArrayList<>();
    //加上自定义扩展
    delegates.add(tokenEnhancer);
    //这里配上jwtToken转换，不然自定义的TokenEnhancer增强的还是OAuth2的token
    delegates.add(jwtAccessTokenConverter);
    tokenEnhancerChain.setTokenEnhancers(delegates);

    endpoints
        // 自定义登录逻辑
        .userDetailsService(userService)
        // 授权管理器
        .authenticationManager(authenticationManager)
        //令牌存储位置
        .tokenStore(tokenStore)
        .accessTokenConverter(jwtAccessTokenConverter)
        //完成jwt扩展配置
        .tokenEnhancer(tokenEnhancerChain);
}
```



启动测试：

![image-20210914220328827](https://cdn.jsdelivr.net/gh/le1024/image1/le/image-20210914220328827.png)

复制Token去JWT官网去验证，可以看到扩展信息：

![image-20210914220420185](https://cdn.jsdelivr.net/gh/le1024/image1/le/image-20210914220420185.png)

