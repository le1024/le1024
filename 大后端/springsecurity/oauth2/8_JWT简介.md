> JWT

#### 1.什么是JWT

JSON Web令牌(JWT)是一个开放标准([RFC 7519](https://tools.ietf.org/html/rfc7519))，它定义了一种以JSON对象的形式在各方之间安全地传输信息的紧凑和独立的方式。可以验证和信任此信息，因为它是数字签名的。可以使用一个秘密对JWT进行签名(使用**HMAC**算法)或使用**RSA**或**ECDSA**.

官网：https://jwt.io



JWT令牌优点：

1. JWT基于JSON，方便解析
2. 可以在令牌中自定义丰富的内容易扩展
3. 通过非对称加密算法及数字签名技术，JWT防止篡改，安全性高
4. 资源服务使用JWT可以不依赖认证服务即可完成授权

缺点：

1. JWT令牌较长，占用存储空间较大



#### 2.JWT组成

https://jwt.io/introduction

一个JWT实际上是一个字符串，以其紧凑的形式由点分隔的三个部分组成(`.`)分别为

- 头部
- 有效载荷
- 签名

因此，JWT通常如下所示：

`xxxxx.yyyyy.zzzzz`



##### 2.1 头部

头部用于描述关于该JWT的最基本的信息，由两部分组成：令牌类型（即JWT）和使用的签名算法（如HMAC SHA 256或RSA）

例如：

```json
{
  "alg": "HS256",
  "typ": "JWT"
}
```

- alg：签名的算法
- typ：类型

上面的JSON通过**Base64Url**编码后成为JWT的第一部分。



##### 2.2 有效载荷

第二部分是载荷，存放有效信息的。由三部分组成：注册声明，公共声明，私有声明

- 标准中注册的声明（建议但不强制使用）

```html
iss：jwt签名者
sub：jwt所面向的用户
aud：接收jwt的一方
exp：jwt的过期时间，过期时间必须大于签发时间
nbf：定义在什么时间之前，该jwt是不可用的
iat：jwt的签发时间
jti：jwt的唯一身份标识，主要用于一次性token，从而回避重放攻击
```

- 公共的声明

公共的声明可以添加任何的信息，一般添加用户的相关信息或者其他业务需要的必须信息，但不建议添加敏感信息，因为该部分可被解密

- 私有的声明

私有声明是提供者和消费者所共同定义的声明，一般不建议存放敏感信息，因为base64是对称解密的，意味着该部分信息可以归类为明文信息



有效载荷可以是：

```json
{
  "sub": "1234567890",
  "name": "John Doe",
  "admin": true
}
```

其中`sub`是标准的声明，`name`是自定义的声明（公共或私有的）

上面的JSON通过**Base64Url**编码后成为JWT的第二部分。



##### 2.3 签名

JWT的第三部分是一个签证信息，由三部分组成：

- header(base64编码后的)
- payload(base64编码后的)
- **secret(盐，要保密)**



将base64编码后的header和base64编码后的payload使用`.`连接组成字符串，通过header中声明的加密方式进行加盐secret组和加密，构成jwt的第三部分：

第三部分生成格式如下：

```java
HMACSHA256(
  base64UrlEncode(header) + "." +
  base64UrlEncode(payload),
  secret)
```

最终完整的JWT如图：

红色代码：header的加密

紫色代码：payload的加密

蓝色代码：header的加密和payload的加密通过`.`连接，加`secret`加密后的

![Encoded JWT](https://cdn.jsdelivr.net/gh/le1024/image1/le/encoded-jwt3.png)

注意：`secret`是保存在服务端的，`jwt`的签发生成也是在服务端的，`secret`是用来进行`jwt`的签发和`jwt`的验证，所以，`secret`就是你服务端的私钥，在任何场景下都不应该泄露出去。一旦客户端得知这个`secret`，那就意味着客户端可以自我签发`jwt`了。