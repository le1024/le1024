> 集成Redis存储token

将生成的Token存储在redis里面，不放在内存



示例是基于密码模式进行测试



##### #环境代码配置

引入依赖：

```xml
<!--引入redis-->
<dependency>
    <groupId>org.springframework.boot</groupId>
    <artifactId>spring-boot-starter-data-redis</artifactId>
</dependency>
<!--对象池依赖-->
<dependency>
    <groupId>org.apache.commons</groupId>
    <artifactId>commons-pool2</artifactId>
</dependency>
```



`yml`

```yaml
redis:
    host: localhost
    port: 8888
    password: 123456
```



`RedisConfig`

```java
@Configuration
public class RedisConfig {

    @Autowired
    private RedisConnectionFactory redisConnectionFactory;

    @Bean
    public TokenStore redisTokenStore() {
        return new RedisTokenStore(redisConnectionFactory);
    }
}
```



**授权服务器配置类**`AuthorizationServerConfig`

```java
@Autowired
private TokenStore tokenStore;

/**
 * 配置密码模式
 */
@Override
public void configure(AuthorizationServerEndpointsConfigurer endpoints) throws Exception {
    endpoints
        //令牌存储位置
        .tokenStore(tokenStore);
}
```



##### #启动测试

![image-20210911175619776](https://cdn.jsdelivr.net/gh/le1024/image1/le/image-20210911175619776.png)

打开redis连接工具可以看到已经存储的token，还有其他信息，红框标记的token值是一样的

![image-20210911175658748](https://cdn.jsdelivr.net/gh/le1024/image1/le/image-20210911175658748.png)