#### 接入微信公众号

接入微信公众平台开发，开发者需要按照如下步骤完成：

1. 填写服务器配置
2. 验证服务器地址的有效性
3. 依据接口文档实现业务逻辑



#### 填写服务器配置

登录到微信公众号平台：https://mp.weixin.qq.com

![image-20220406151455905](https://s2.loli.net/2022/04/20/5iDcatbpsVBnoES.png)

**服务器地址**：填写你自己开发服务器的接口地址，需要全路径的，必须以http://或https://开头，分别支持80端口和443端口。

可使用`花生壳`或者`ngrok`等内网穿透工具搭配本地服务进行开发，服务器地址就填写内网穿透工具提供的域名，然后加上你本地的服务地址

**令牌token**：任意填写，用作生成签名

**消息加密密钥和解密方式**：随机生成即可。具体的查看：https://developers.weixin.qq.com/doc/oplatform/Third-party_Platforms/2.0/api/Before_Develop/Message_encryption_and_decryption.html



最后，设置成启用。



#### 验证消息来自微信服务器

开发者提交信息后，微信服务器将发送GET请求到填写的服务器地址URL上，GET请求携带参数如下表所示：

| 参数      | 描述                                                         |
| :-------- | :----------------------------------------------------------- |
| signature | 微信加密签名，signature结合了开发者填写的token参数和请求中的timestamp参数、nonce参数。 |
| timestamp | 时间戳                                                       |
| nonce     | 随机数                                                       |
| echostr   | 随机字符串                                                   |

开发者通过检验signature对请求进行校验（下面有校验方式）。若确认此次GET请求来自微信服务器，请原样返回echostr参数内容，则接入生效，成为开发者成功，否则接入失败。加密/校验流程如下：

1）将token、timestamp、nonce三个参数进行字典序排序 

2）将三个参数字符串拼接成一个字符串进行sha1加密 

3）开发者获得加密后的字符串可与signature对比，标识该请求来源于微信

`代码`

```java

	@Value("${wx.token}")
    private String token; //在服务器配置项配置的token，保持一致
/**
     * 确认来自微信的验证
     * @param request
     * @param response
     * @return
     */
    @GetMapping(value = "/msg", produces = "text/plain;charset=utf-8")
    public String wxMsgConfirm(HttpServletRequest request, HttpServletResponse response){
        String signature = request.getParameter("signature");
        String timestamp = request.getParameter("timestamp");
        String nonce = request.getParameter("nonce");
        String echostr = request.getParameter("echostr");
        if (SignUtil.checkSignature(token, signature, timestamp, nonce)) {
            return echostr;
        } else {
            return null;
        }
    }
```

```java
/**
     * 监听来自微信的消息
     * @param request
     * @param response
     */
    @PostMapping(value = "/msg")
    public void wxMsgHandler(HttpServletRequest request, HttpServletResponse response){
        try {
            // 将请求、响应的编码均设置为UTF-8（防止中文乱码）
            request.setCharacterEncoding("UTF-8");
            response.setCharacterEncoding("UTF-8");
            InputStream inputStream = request.getInputStream();
            String msgXml = IOUtils.toString(inputStream, "UTF-8");

            // 调用核心业务类接收消息、处理消息
            String respMessage = processMessage(request, msgXml);

            // 响应消息
            PrintWriter out = response.getWriter();
            out.print(respMessage);
            out.close();
        } catch (Exception e) {
            System.err.println(e.getMessage());
        }
    }

public String processMessage(HttpServletRequest request, String msgXml) {
        WechatMsg resqMsg = new WechatMsg(msgXml);
        handleMessage(request, resqMsg);
        if(resqMsg.getMsgType() != null)
            return resqMsg.createResponseMsg();
        else
            return "";
    }

    private void handleMessage(HttpServletRequest request, WechatMsg reqMsg){
        String type = reqMsg.getMsgType();
        String eventType = reqMsg.getEvent();
        String fromUserName = reqMsg.getFromUserName();

        //关注公众号事件
        if("event".equals(type) && "subscribe".equals(eventType)){
            //...
        }
    }
```

```java
@Data
public class WechatMsg {
    /**
     * 发送者
     */
    private String fromUserName;

    /**
     * 接收者
     */
    private String toUserName;

    /**
     * 消息类型
     */
    private String msgType;

    /**
     * 事件（消息类型为事件）
     */
    private String event;

    /**
     * 事件Key（消息类型为事件）
     */
    private String eventKey;

    /**
     * 消息内容（消息类型为文本）
     */
    private String content;



    public WechatMsg(String fromUserName, String toUserName) {
        this.fromUserName = fromUserName;
        this.toUserName = toUserName;
    }

    /**
     * 同构XML构造方法
     *
     * @param msgXml 消息体
     */
    public WechatMsg(String msgXml) {
        try {
            HashMap<String, String> map = new HashMap<String, String>();
            DocumentBuilderFactory factory = DocumentBuilderFactory
                    .newInstance();
            DocumentBuilder builder = factory.newDocumentBuilder();
            Document doc = builder.parse(new InputSource(new StringReader(msgXml)));

            Element root = doc.getDocumentElement();
            NodeList nodes = root.getChildNodes();
            if (nodes != null) {
                for (int i = 0; i < nodes.getLength(); i++) {
                    Node node = nodes.item(i);
                    map.put(node.getNodeName(), node.getTextContent());
                }
            }
            this.fromUserName = map.get("FromUserName");
            this.toUserName = map.get("ToUserName");
            this.msgType = map.get("MsgType");
            if("event".equals(this.msgType)) {
                this.event = map.get("Event");
                this.eventKey = map.get("EventKey");
            } else if("text".equals(this.msgType)) {
                this.content = map.get("Content");
            } else {
                // 未知消息类型
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    /**
     * 回复消息
     * @return 消息XML
     */
    public String createResponseMsg() {
        //用户发送文本信息回复
        if("text".equals(this.msgType)) {
            String msg = "<xml>" +
                    "<ToUserName><![CDATA[" + this.fromUserName + "]]></ToUserName>" +
                    "<FromUserName><![CDATA[" + this.toUserName + "]]></FromUserName>" +
                    "<CreateTime>" + (new Date()).getTime() + "</CreateTime>" +
                    "<MsgType><![CDATA[" + this.msgType + "]]></MsgType>" +
                    "<Content><![CDATA[" + "您好！" + "]]></Content>" +
                    "</xml>";
            return msg;
        } 
        //关注事件回复
        else if("event".equals(this.msgType) && "subscribe".equals(this.event)){
            String msg = "<xml>" +
                    "<ToUserName><![CDATA[" + this.fromUserName + "]]></ToUserName>" +
                    "<FromUserName><![CDATA[" + this.toUserName + "]]></FromUserName>" +
                    "<CreateTime>" + (new Date()).getTime() + "</CreateTime>" +
                    "<MsgType><![CDATA[" + this.msgType + "]]></MsgType>" +
                    "<Content><![CDATA[" + "感谢您的关注！" + "]]></Content>" +
                    "</xml>";
            return msg;
        } else {
            return null;
        }
    }
}
```

/msg的接口校验成功后，即成为微信公众号的开发者，用户每次向公众号发送消息、或者产生自定义菜单、或产生微信支付订单等情况时，开发者填写的服务器配置URL将得到微信服务器推送过来的消息和事件，开发者可以依据自身业务逻辑进行响应，可参考微信文档开发：

**接收普通消息**

https://developers.weixin.qq.com/doc/offiaccount/Message_Management/Receiving_standard_messages.html

**接收事件推送**

https://developers.weixin.qq.com/doc/offiaccount/Message_Management/Receiving_event_pushes.html



> 可申请微信测试公众号测试开发，测试账号申请地址：https://mp.weixin.qq.com/debug/cgi-bin/sandboxinfo?action=showinfo&t=sandbox/index