> Zookeeper集群搭建

#### 下载zookeeper

https://archive.apache.org/dist/zookeeper/



#### 服务器准备

需要三台服务器：

- 192.168.171.132
- 192.168.171.133
- 192.168.171.134

三台服务器都需要安装jdk



#### 解压zookeeper

将下载好的zookeeper上传到服务器。可以只上传到一个服务器上面，然后解压：

```shell
tar -zxvf zookeeper-3.4.10.tar.gz
```

将解压的文件通过scp传输到另外两个服务器上：可以等zk的配置都配置完成后再复制过去

```shell
scp -r /opt/zookeeper-3.4.10/ root@192.168.171.133:/opt
scp -r /opt/zookeeper-3.4.10/ root@192.168.171.134:/opt
```



#### 修改配置文件

将默认的`zoo_sample.cfg`文件复制一份为`zoo.cfg`文件

```shell
cp zoo_sample.cfg zoo.cfg

vim zoo.cfg
```

![image-20211224164237541](https://cdn.jsdelivr.net/gh/le1024/image1/le/image-20211224164237541.png)

修改dataDir：在指定位置创建好目录

添加server.A=B:C:D，A对应后面创建的`myid`文件，B是集群的ip地址，C:D是端口配置。



#### 创建myid

根据上面配置的server.A=B:C:D

```shell
server.0=192.168.171.132:2888:3888
server.1=192.168.171.133:2888:3888
server.2=192.168.171.134:2888:3888
```

需要去`/opt/zookeeper-3.4.10/data`下创建一个`myid`文件，根据当前服务器的ip，在`myid`文件里面根据配置设置对应的值，比如：192.168.171.132服务器`myid`值为`0`

![image-20211224165119850](https://cdn.jsdelivr.net/gh/le1024/image1/le/image-20211224165119850.png)



#### 配置环境变量

为了能够在任意目录启动zookeeper集群，我们需要配置环境变量。

vim进入到`/etc/profile`目录，添加配置信息：

```shell
#set zk environment
export ZK_HOME=/opt/zookeeper-3.4.10
export PATH=$PATH:$ZK_HOME/bin
```

使配置环境生效

```shell
source /etc/profile
```



<strong style="color:green">可以使用上面提供的scp命令，将当前修改好的zk传到另外两个服务器。然后再进行对应的修改。</strong>

或者自行上传重新配置一遍。



**另外两个服务器的`myid`要修改成对应的值**



#### 启动zookeeper

```shell
# 启动
zkServer.sh start
# 停止
zkServer.sh stop
# 重启
zkServer.sh restart
# 查询状态
zkServer.sh status
```



启动时候需要修改内存的话，打开`zkServer.sh`：

在启动模块，加上vm参数，自行配置

![image-20211224165911603](https://cdn.jsdelivr.net/gh/le1024/image1/le/image-20211224165911603.png)



查看三台服务器状态：

![image-20211224200546255](https://cdn.jsdelivr.net/gh/le1024/image1/le/image-20211224200546255.png)

![image-20211224200622008](https://cdn.jsdelivr.net/gh/le1024/image1/le/image-20211224200622008.png)

![image-20211224200645432](https://cdn.jsdelivr.net/gh/le1024/image1/le/image-20211224200645432.png)