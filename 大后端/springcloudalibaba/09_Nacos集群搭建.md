> Nacos集群搭建



需要服务：

- 1个nginx服务，本次版本-1.20.1

- 3个nacos服务，本次版本-1.1.4

- 1个mysql服务，本次版本-5.7.32

nginx和mysql请事先准备好



#### mysql数据库配置

在`../nacos/conf`路径下有`nacos-mysql.sql`，将其导入到mysql数据库中

![image-20211026225236124](https://cdn.jsdelivr.net/gh/le1024/image1/le/image-20211026225236124.png)



在`../nacos/conf`路径的`applicatin.properties`文件中配置数据库连接信息

先cp一份`application.properties`文件，防止文件修改坏了，无法使用nacos

```bash
cp application.properties application.properties.bak
```

![image-20211026225501185](https://cdn.jsdelivr.net/gh/le1024/image1/le/image-20211026225501185.png)

```bash
vim application.properties
```

添加数据库配置，数据库信息请以自己实际环境为准，mysql8的url可能需要别的参数，后面再试

```properties
spring.datasource.platform=mysql
 
db.num=1
db.url.0=jdbc:mysql://127.0.0.1:3306/nacos_config?characterEncoding=utf8&connectTimeout=1000&socketTimeout=3000&autoReconnect=true
db.user=root
db.password=123456
```

启动服务，然后在nacos控制台新建一个配置文件，在数据库`config_info`表中可以看到新增的信息



#### 集群配置cluster.conf

复制一份`cluster.conf`

![image-20211026232208025](https://cdn.jsdelivr.net/gh/le1024/image1/le/image-20211026232208025.png)

编辑`cluster.conf`，添加三个节点，配置集群8848/8849/8850

ip为服务器ip，`hostname -i`命令查询到的ip地址，不可写127.0.0.1，如果`hostname -i`查询结果是127.0.0.1，需要去设置ip地址，百度搜索`vim /etc/hosts`添加ip地址

![image-20211028151347628](https://cdn.jsdelivr.net/gh/le1024/image1/le/image-20211028151347628.png)



#### startup.sh启动集群服务

通过`startup.sh`默认是启动端口8848的，在集群模式下，会有多端口服务，通过传递不同的端口号启动不同的nacos实例。

**./startup.sh -p port**，port就是`cluster.conf`里面配置的端口号

打开`startup.sh`文件，找到需要修改的位置1：

![image-20211026233019058](https://cdn.jsdelivr.net/gh/le1024/image1/le/image-20211026233019058.png)

`:m:f:s`意思是，在执行`startup.sh`命令时，可以带上`-m -f -s`这样的参数，比如：以单机模式启动nacos

```bash
./startup.sh -m standalone
```

同理，我们需要传递端口号：

```bash
./startup.sh -p 8848
```

修改如下：

![image-20211026233336677](https://cdn.jsdelivr.net/gh/le1024/image1/le/image-20211026233336677.png)



修改位置2：

![image-20211026233439252](https://cdn.jsdelivr.net/gh/le1024/image1/le/image-20211026233439252.png)

改成如下，添加启动端口：`-Dserver.port=${PORT}`

![image-20211026233629987](https://cdn.jsdelivr.net/gh/le1024/image1/le/image-20211026233629987.png)

之后就可以通过：端口自己定义

```bash
./startup.sh -p 8848
./startup.sh -p 8849
./startup.sh -p 8850
```

![image-20211027153837435](https://cdn.jsdelivr.net/gh/le1024/image1/le/image-20211027153837435.png)



启动nacos集群服务，不同的端口都可以访问到nacos服务，且都能看到数据库里面创建的配置文件(自己建的配置文件)，说明配置nacos多服务成功，数据库为同一个mysql服务

![image-20211026234255375](https://cdn.jsdelivr.net/gh/le1024/image1/le/image-20211026234255375.png)

![image-20211026234318552](https://cdn.jsdelivr.net/gh/le1024/image1/le/image-20211026234318552.png)

![image-20211026234339674](https://cdn.jsdelivr.net/gh/le1024/image1/le/image-20211026234339674.png)



#### Nginx负载均衡配置

通过Nginx来转发请求到上面配置的三个nacos服务

复制一份`nginx.conf`再进行修改

```bash
cp nginx.conf nginx.conf.bak
```

```bash
vim nginx.conf
```

`upstream `配置上nacos的三个服务8848/8849/8850

```bash
upstream nacos_cluster {
        server 192.168.171.132:8848;
        server 192.168.171.132:8849;
        server 192.168.171.132:8850;
    }

    server {
        listen       80;
        server_name  localhost;

        #charset koi8-r;

        #access_log  logs/host.access.log  main;

        location / {
            #root   html;
            #index  index.html index.htm;
            proxy_pass http://nacos_cluster;
        }
省略....
```

![image-20211027153934380](https://cdn.jsdelivr.net/gh/le1024/image1/le/image-20211027153934380.png)



#### 实现nginx访问nacos

启动三个nacos服务，启动nginx

**nacos启动**

```bash
./startup.sh -p 8848
./startup.sh -p 8849
./startup.sh -p 8850
```

**nginx启动**

```bash
./nginx
```

**访问nginx**

http://192.168.171.132/nacos/#/login

![image-20211027154212795](https://cdn.jsdelivr.net/gh/le1024/image1/le/image-20211027154212795.png)

![image-20211027154234417](https://cdn.jsdelivr.net/gh/le1024/image1/le/image-20211027154234417.png)

可以测试新建配置文件，再观察数据库`config_info`表，然后每个nacos服务都可以看到该文件

![image-20211027162506589](https://cdn.jsdelivr.net/gh/le1024/image1/le/image-20211027162506589.png)

![image-20211027162535985](https://cdn.jsdelivr.net/gh/le1024/image1/le/image-20211027162535985.png)



查看节点列表，后面有重新配置过，换了个端口，所以这个截图的端口跟上面的不一样，原理都是一样的

![image-20211028151642903](https://cdn.jsdelivr.net/gh/le1024/image1/le/image-20211028151642903.png)

到此，1个Nginx+3个nacos注册中心+1个mysql服务搭建成功



#### 服务注册进集群

在之前搭建的服务`cloudalibaba-provider-payment9002`进行修改

修改yaml就行了，之前服务配置的nacos地址是单机服务地址：`localhost:8848`，改为nginx地址即可

```yaml
spring:
  application:
    name: nacos-payment-provider
  cloud:
    nacos:
      discovery:
        #server-addr: localhost:8848 #配置nacos地址
        #换成nginx的地址及端口，做集群访问
        server-addr: 192.168.171.132:8888
```

启动服务`cloudalibaba-provider-payment9002`，查看nacos服务列表

![image-20211028151854109](https://cdn.jsdelivr.net/gh/le1024/image1/le/image-20211028151854109.png)

