> Seata介绍

http://seata.io/zh-cn/

**Seata是一款开源的分布式事务解决方案，致力于在微服务架构下提供高性能和简单易用的分布式事务服务**



**Seata中的重要概念：一ID + 三组件**

<strong style="color:black">Transaction ID XID</strong>

全局唯一事务ID

<strong style="color:black">TC (Transaction Coordinator) - 事务协调者</strong>

维护全局和分支事务的状态，驱动全局事务提交或回滚。

<strong style="color:black">TM (Transaction Manager) - 事务管理器</strong>

定义全局事务范围：开始全局事务、提交或回滚全局事务。

<strong style="color:black">RM (Resource Manager) - 资源管理器</strong>

管理分支事务处理的资源，与TC交谈以注册分支事务和报告分支事务的状态，并驱动分支事务提交或回滚。



Seata事务处理过程：

1. TM向TC申请开启一个全局事务，全局事务创建成功并生成一个全局唯一的XID
2. XID在微服务调用链路的上下文传播
3. RM向TC注册分支事务，将其纳入XID对应的全局事务的管辖
4. TM向TC发起针对XID的全局事务提交或回滚
5. TC调度XID下管辖的全部分支事务完成提交或回滚请求

![img](https://cdn.jsdelivr.net/gh/le1024/image1/le/TB1rDpkJAvoK1RjSZPfXXXPKFXa-794-478.png)