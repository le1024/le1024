> Nacos服务注册

一定要看哦**~**

https://spring-cloud-alibaba-group.github.io/github-pages/greenwich/spring-cloud-alibaba.html#_spring_cloud_alibaba_nacos_config



#### 基于Nacos的服务提供者

------

**payment9001服务**

- **新建Module**`cloudalibaba-provider-payment9001`

- **POM**

  父工程pom添加springcloud alibaba

  ```xml
  <dependency>
      <groupId>com.alibaba.cloud</groupId>
      <artifactId>spring-cloud-alibaba-dependencies</artifactId>
      <version>2.1.0.RELEASE</version>
      <type>pom</type>
      <scope>import</scope>
  </dependency>
  ```

  本模块pom

  ```xml
  <dependencies>
          <!--SpringCloud ailibaba nacos -->
          <dependency>
              <groupId>com.alibaba.cloud</groupId>
              <artifactId>spring-cloud-starter-alibaba-nacos-discovery</artifactId>
          </dependency>
          <!-- SpringBoot整合Web组件 -->
          <dependency>
              <groupId>org.springframework.boot</groupId>
              <artifactId>spring-boot-starter-web</artifactId>
          </dependency>
          <dependency>
              <groupId>org.springframework.boot</groupId>
              <artifactId>spring-boot-starter-actuator</artifactId>
          </dependency>
          <!--日常通用jar包配置-->
          <dependency>
              <groupId>org.springframework.boot</groupId>
              <artifactId>spring-boot-devtools</artifactId>
              <scope>runtime</scope>
              <optional>true</optional>
          </dependency>
          <dependency>
              <groupId>org.projectlombok</groupId>
              <artifactId>lombok</artifactId>
              <optional>true</optional>
          </dependency>
          <dependency>
              <groupId>org.springframework.boot</groupId>
              <artifactId>spring-boot-starter-test</artifactId>
              <scope>test</scope>
          </dependency>
      </dependencies>
  ```

  

- **yml**

  ```yaml
  server:
    port: 9001
  
  spring:
    application:
      name: nacos-payment-provider
    cloud:
      nacos:
        discovery:
          server-addr: localhost:8848 #配置nacos地址
  
  management:
    endpoints:
      web:
        exposure:
          include: "*"
  ```

  

- **主启动类**

  ```java
  @SpringBootApplication
  @EnableDiscoveryClient
  public class PaymentMain9001 {
  
      public static void main(String[] args) {
          SpringApplication.run(PaymentMain9001.class, args);
      }
  }
  ```

  

- **业务类**

  ```java
  @RestController
  public class PaymentController {
  
      @Value("${server.port}")
      private String serverPort;
  
      @GetMapping("/payment/nacos/{id}")
      public String getPayment(@PathVariable("id") Integer id) {
          return "nacos registry, server port:" + serverPort + "\t id:" + id;
      }
  }
  ```

  

- **测试**

  Nacos启动，payment9001服务启动

  在nacos控制台可以看到payment9001服务成功注册进nacos，服务名为`nacos-payment-provider`

  ![image-20211020230506516](https://cdn.jsdelivr.net/gh/le1024/image1/le/image-20211020230506516.png)

  访问payment9001服务：http://localhost:9001/payment/nacos/1

  ![image-20211020230720235](https://cdn.jsdelivr.net/gh/le1024/image1/le/image-20211020230720235.png)



**payment9002服务**

先参考payment9001服务再搭建一个payment9002服务，为了后面演示nacos的负载均衡

服务搭建完成，启动，再去通过nacos控制台观察：

![image-20211020232011058](https://cdn.jsdelivr.net/gh/le1024/image1/le/image-20211020232011058.png)

![image-20211020232029910](https://cdn.jsdelivr.net/gh/le1024/image1/le/image-20211020232029910.png)





#### 基于Nacos的服务消费者

------

**order83服务**

- **新建Module**`cloudalibaba-consumer-nacos-order83`

- **POM**

  ```xml
  <dependencies>
          <!--SpringCloud ailibaba nacos -->
          <dependency>
              <groupId>com.alibaba.cloud</groupId>
              <artifactId>spring-cloud-starter-alibaba-nacos-discovery</artifactId>
          </dependency>
          <!-- 引入自己定义的api通用包，可以使用Payment支付Entity -->
          <dependency>
              <groupId>app.springcloud</groupId>
              <artifactId>cloud-api-commons</artifactId>
              <version>${project.version}</version>
          </dependency>
          <!-- SpringBoot整合Web组件 -->
          <dependency>
              <groupId>org.springframework.boot</groupId>
              <artifactId>spring-boot-starter-web</artifactId>
          </dependency>
          <dependency>
              <groupId>org.springframework.boot</groupId>
              <artifactId>spring-boot-starter-actuator</artifactId>
          </dependency>
          <!--日常通用jar包配置-->
          <dependency>
              <groupId>org.springframework.boot</groupId>
              <artifactId>spring-boot-devtools</artifactId>
              <scope>runtime</scope>
              <optional>true</optional>
          </dependency>
          <dependency>
              <groupId>org.projectlombok</groupId>
              <artifactId>lombok</artifactId>
              <optional>true</optional>
          </dependency>
          <dependency>
              <groupId>org.springframework.boot</groupId>
              <artifactId>spring-boot-starter-test</artifactId>
              <scope>test</scope>
          </dependency>
      </dependencies>
  ```

  

- **yml**

  ```yaml
  server:
    port: 83
  
  spring:
    application:
      name: nacos-order-consumer
    cloud:
      nacos:
        discovery:
          server-addr: localhost:8848
  
  #消费者要去访问的微服务名称
  service-url:
    nacos-user-service: http://nacos-payment-provider
  
  ```

  

- **主启动类**

  ```java
  @SpringBootApplication
  @EnableDiscoveryClient
  public class OrderNacosMain83 {
  
      public static void main(String[] args) {
          SpringApplication.run(OrderNacosMain83.class, args);
      }
  }
  ```

  

- **业务类**

  ```java
  @Configuration
  public class ApplicationContextBean {
  
      @Bean
      @LoadBalanced
      public RestTemplate restTemplate() {
          return new RestTemplate();
      }
  }
  ```

  

  ```java
  @RestController
  public class OrderNacosController {
  
      @Autowired
      private RestTemplate restTemplate;
  
      @Value("${service-url.nacos-user-service}")
      private String serverURL;
  
      @GetMapping("/consumer/payment/nacos/{id}")
      public String paymentInfo(@PathVariable("id") Integer id) {
          return restTemplate.getForObject(serverURL + "/payment/nacos/" + id, String.class);
      }
  }
  ```

  

- **测试**

  启动payment9001/9002，nacosOrder83服务

  在nacos控制台可以看到服务注册成功

  ![image-20211021153222842](https://cdn.jsdelivr.net/gh/le1024/image1/le/image-20211021153222842.png)

  访问地址：http://localhost:83/consumer/payment/nacos/1

  **83访问9001/9002，轮询负载OK**

