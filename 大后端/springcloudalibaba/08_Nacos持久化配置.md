> Nacos持久化配置

**Nacos默认自带的是嵌入式数据库derby，无法观察数据存储基本情况，并且集群环境无法保证数据一致性**



<strong style="color:red">目前nacos仅支持mysql数据库，mysql版本：5.6.6+</strong>

请先安装好mysql数据库

<strong style="color:green">本次是在nacos 2.0.3版本下操作的</strong>

<strong style="color:orange">以下均为配置持久化环境示例和相关文件说明，实际需要请自行修改相关文件</strong>

<strong style="color:orange">不同的nacos版本，配置文件可能会有不同的地方(默认值之类信息)，需要注意！！！</strong>

<strong style="color:orange">主要讲解Nacos持久化配置，实际环境在linux环境中使用，不用windows，配置都差不多</strong>



1.在nacos文件中找到sql脚本：`..\nacos\conf\nacos-mysql.sql`

![image-20211026163926516](https://cdn.jsdelivr.net/gh/le1024/image1/le/image-20211026163926516.png)

2.创建好数据库，比如：`nacos-config`

3.运行脚本，会创建如下12张表

![image-20211026164214697](https://cdn.jsdelivr.net/gh/le1024/image1/le/image-20211026164214697.png)

4.在nacos文件中找到配置文件：`..\nacos\conf\application.properties`

在配置文件中添加mysql连接配置

![image-20211026164952894](https://cdn.jsdelivr.net/gh/le1024/image1/le/image-20211026164952894.png)

```properties
### mysql
spring.datasource.platform=mysql

db.num=1
db.url.0=mysql://127.0.0.1:3306/nacos_config?characterEncoding=utf8&connectTimeout=1000&socketTimeout=3000&autoReconnect=true&useUnicode=true&useSSL=false&serverTimezone=UTC
db.user.0=nacos
db.password.0=nacos
```





完成配置，进入bin目录，运行`startup.cmd`，默认为集群模式(打开startup文件可以看到)，直接启动就可

![image-20211026172622154](https://cdn.jsdelivr.net/gh/le1024/image1/le/image-20211026172622154.png)

![image-20211026172016900](https://cdn.jsdelivr.net/gh/le1024/image1/le/image-20211026172016900.png)



启动后会报如下异常，启动失败

![image-20211026173036235](https://cdn.jsdelivr.net/gh/le1024/image1/le/image-20211026173036235.png)

是因为集群方式启动，没有获取到集群配置的网络信息，在nacos的conf路径下找到`cluster.conf.example`文件，将后缀`.example`删除，建议复制一份更改名字为`cluster.conf`

![image-20211026173219142](https://cdn.jsdelivr.net/gh/le1024/image1/le/image-20211026173219142.png)

![image-20211026173334163](https://cdn.jsdelivr.net/gh/le1024/image1/le/image-20211026173334163.png)



再次启动nacos，启动过程可能会有点久，耐心等待。。。

```bash

2021-10-26 17:34:19,551 INFO Nacos is starting...

2021-10-26 17:34:20,555 INFO Nacos is starting...

2021-10-26 17:34:21,560 INFO Nacos is starting...

2021-10-26 17:34:22,564 INFO Nacos is starting...

2021-10-26 17:34:23,568 INFO Nacos is starting...

2021-10-26 17:34:24,573 INFO Nacos is starting...

2021-10-26 17:34:27,027 INFO Nacos is starting...

2021-10-26 17:34:28,042 INFO Nacos is starting...

2021-10-26 17:34:29,056 INFO Nacos is starting...

2021-10-26 17:34:30,069 INFO Nacos is starting...

2021-10-26 17:34:31,085 INFO Nacos is starting...

2021-10-26 17:34:31,547 INFO Nacos started successfully in cluster mode. use external storage
```



登录系统，新建配置文件

![image-20211026174455449](https://cdn.jsdelivr.net/gh/le1024/image1/le/image-20211026174455449.png)

在mysql中可以看到新建的配置文件

![image-20211026174434739](https://cdn.jsdelivr.net/gh/le1024/image1/le/image-20211026174434739.png)



<strong style="color:orange">不同的nacos版本，配置文件可能会有不同的地方，需要注意！！！</strong>

