> Sentinel流控规则



![image-20211029144226061](https://cdn.jsdelivr.net/gh/le1024/image1/le/image-20211029144226061.png)

![image-20211029144236727](https://cdn.jsdelivr.net/gh/le1024/image1/le/image-20211029144236727.png)

https://sentinelguard.io/zh-cn/docs/flow-control.html



#### 基于QPS和并发数的流量控制

****

**QPS流量控制**

当QPS超过某个阈值的时候，则采取措施进行流量控制。流量控制有3种，对应`FlowRule`中的`controlBehavior`字段：

- 快速失败方式：`RuleConstant.CONTROL_BEHAVIOR_DEFAULT`
- Warm Up方式：`RuleConstant.CONTROL_BEHAVIOR_WARM_UP`
- 排队等待方式：`RuleConstant.CONTROL_BEHAVIOR_RATE_LIMITER`

**并发线程数流量控制**

线程数限流用于保护业务线程数不被耗尽



#### 基于调用关系的流量控制

****

- **直接**

- **关联**

  根据调用链路入口限流，`NodeSelectorSlot` 中记录了资源之间的调用链路，这些资源通过调用关系，相互之间构成一棵调用树。

- **链路**

  具有关系的资源流量控制，当两个资源之间具有资源争抢或者依赖关系的时候，这两个资源便具有了关联。



具体说明如下：

![image-20211029150836454](https://cdn.jsdelivr.net/gh/le1024/image1/le/image-20211029150836454.png)



列出部分流控模式使用demo：

操作如下：可以在`簇点链路`菜单列表下，点击`流控`按钮即可新增流控规则

![image-20211029150709468](https://cdn.jsdelivr.net/gh/le1024/image1/le/image-20211029150709468.png)

![image-20211029150934796](https://cdn.jsdelivr.net/gh/le1024/image1/le/image-20211029150934796.png)

或者在`流控规则`菜单下操作也可

![image-20211029151043997](https://cdn.jsdelivr.net/gh/le1024/image1/le/image-20211029151043997.png)



#### 流控模式--直接

****

##### QPS直接失败

配置规则为1秒钟内查询1次就是OK，若超过次数1，就直接-快速失败，报默认错误

![image-20211029151150618](https://cdn.jsdelivr.net/gh/le1024/image1/le/image-20211029151150618.png)

![image-20211029151159021](https://cdn.jsdelivr.net/gh/le1024/image1/le/image-20211029151159021.png)



配置完成后，请求：http://localhost:8401/testA，点击慢点，一秒内的点击次数没有超过1次，服务是正常响应的

![image-20211029151347935](https://cdn.jsdelivr.net/gh/le1024/image1/le/image-20211029151347935.png)

当我们不断的点击，一秒内多次点击请求，直接提示异常：`Blocked by Sentinel (flow limiting)`

![image-20211029151423725](https://cdn.jsdelivr.net/gh/le1024/image1/le/image-20211029151423725.png)

##### 线程数直接失败

将阈值类型更改为线程数

![image-20211029155132116](https://cdn.jsdelivr.net/gh/le1024/image1/le/image-20211029155132116.png)

为了可以达到实际效果，`testA`接口需要改下：

<strong style="color:red">原因是：QPS的规则是请求直接被挡在了服务外面，没有进去，而线程是进入到了服务里面，实际中的服务是会耗时的，所以用sleep让线程睡会，来模拟实际效果</strong>

```java
@GetMapping("/testA")
public String testA() throws Exception {
    TimeUnit.SECONDS.sleep(1);
    return "-----testA";
}
```

不断的去请求接口：http://localhost:8401/testA

![image-20211029155854727](https://cdn.jsdelivr.net/gh/le1024/image1/le/image-20211029155854727.png)

##### Warm up

https://github.com/alibaba/Sentinel/wiki/%E6%B5%81%E9%87%8F%E6%8E%A7%E5%88%B6

预热/冷启动方式。当系统处于长期低请求情况下，当流量突然激增时，直接把系统拉倒高请求可能会瞬间把系统压垮。通过冷启动的方式，让流量缓慢的增加，在一定时间内逐渐增加到阈值上限，给系统一个预热的期间，避免系统压垮。

<strong style="color:red">默认 coldFactor 为 3，即请求QPS从(threshold / 3) 开始，经多少预热时长才逐渐升至设定的 QPS 阈值。</strong>

配置如下：

阀值为10，预热时长设置5秒。
系统初始化的阀值为10 / 3 约等于3,即阀值刚开始为3；然后过了5秒后阀值才慢慢升高恢复到10

![image-20211101161552603](https://cdn.jsdelivr.net/gh/le1024/image1/le/image-20211101161552603.png)

测试访问接口，不断请求接口，开始会提示异常，后面会慢慢变的ok。

该功能实际可用的场景为：<strong style="color:green">秒杀系统，在秒杀开启的瞬间，会有很多流量进来，通过warmup把系统保护起来，再慢慢的把流量放进来，把阈值增长到设置的阈值</strong>



##### 匀速排队

严格按照控制请求通过的间隔时间，也就是让请求以均匀的速度通过。这种方式主要是应对间隔性突发流量，如消息队列。

如在某一秒内有大量的请求到来，而接下来的几秒处于空闲状态，希望系统能够在接下来的空闲期间逐渐处理请求，而不是在第一时间直接拒绝多余的请求。

`/testB`每秒1次请求，超过的话就排队等待，等待的超时时间为20000毫秒

![image-20211101173357585](https://cdn.jsdelivr.net/gh/le1024/image1/le/image-20211101173357585.png)

```java
@GetMapping("/testB")
public String testB() {
    log.info(Thread.currentThread().getName() + "\t...testB");
    return "-----testB";
}
```



现在通过**postman**测试多次请求`testB`

1.先创建一个集合文件夹

![image-20211029162129248](https://cdn.jsdelivr.net/gh/le1024/image1/le/image-20211029162129248.png)

将测试接口保存到创建的集合文件夹中

![image-20211029162201458](https://cdn.jsdelivr.net/gh/le1024/image1/le/image-20211029162201458.png)

![image-20211029162219513](https://cdn.jsdelivr.net/gh/le1024/image1/le/image-20211029162219513.png)

![image-20211029162439870](https://cdn.jsdelivr.net/gh/le1024/image1/le/image-20211029162439870.png)

![image-20211029162728082](https://cdn.jsdelivr.net/gh/le1024/image1/le/image-20211029162728082.png)



在请求`testB`过程中

![image-20211029162828198](https://cdn.jsdelivr.net/gh/le1024/image1/le/image-20211029162828198.png)

查看控制台：请求都是依次排队进来，一秒一个

![image-20211101173619539](https://cdn.jsdelivr.net/gh/le1024/image1/le/image-20211101173619539.png)





#### 流控模式-关联

****

微服务之间的一些接口是相互影响的，为了减少服务影响，当关联的资源达到阈值时，就限流自己，即A资源关联B资源，B资源达到指定阈值，A资源进行限流

打开sentinel控制台，配置流控规则：

![image-20211029161325083](https://cdn.jsdelivr.net/gh/le1024/image1/le/image-20211029161325083.png)

设置效果为：
当关联资源`/testB`的qps阀值超过1时，就限流`/testA`的Rest访问地址，当关联资源到阈值后限制配置好的资源名

用jmeter或者postman来测试模拟密集访问`testB`接口

先确认`testA``testB`服务请求是正常的，请求没有`Blocked by Sentinel (flow limiting)`提示

![image-20211029161734649](https://cdn.jsdelivr.net/gh/le1024/image1/le/image-20211029161734649.png)

![image-20211029161635849](https://cdn.jsdelivr.net/gh/le1024/image1/le/image-20211029161635849.png)



现在通过**postman**测试多次请求`testB`

1.先创建一个集合文件夹

![image-20211029162129248](https://s2.loli.net/2022/04/20/DNO7J4cPHjoghZn.png)

将测试接口保存到创建的集合文件夹中

![image-20211029162201458](https://s2.loli.net/2022/04/20/oh8fGUvEHgxRl5W.png)

![image-20211029162219513](https://s2.loli.net/2022/04/20/WIOPZHRGMDyU1sk.png)

![image-20211029162439870](https://s2.loli.net/2022/04/20/Gj14qRutrBTvhPN.png)

<img src="https://s2.loli.net/2022/04/20/nBgYWMXlv2TAObw.png" alt="image-20211029162728082" style="zoom: 33%;" />



在请求`testB`过程中

![image-20211029162828198](https://s2.loli.net/2022/04/20/3KjSTYZtyHw1nsv.png)

再去请求`testA`

![image-20211029162852910](https://cdn.jsdelivr.net/gh/le1024/image1/le/image-20211029162852910.png)

**大批量的请求testB，导致testA失效**



#### 流控模式-链路

教程来自：https://blog.csdn.net/qq_31155349/article/details/108478190

链路流控模式指的是，当从某个接口过来的资源达到限流条件时，开启限流。

```html
从1.6.3 版本开始，Sentinel Web filter默认收敛所有URL的入口context，因此链路限流不生效。
1.7.0 版本开始（对应SCA的2.1.1.RELEASE)，官方在CommonFilter 引入了WEB_CONTEXT_UNIFY 参数，用于控制是否收敛context。将其配置为 false 即可根据不同的URL 进行链路限流。
SCA 2.1.1.RELEASE之后的版本,可以通过配置spring.cloud.sentinel.web-context-unify=false即可关闭收敛
我们当前使用的版本是SpringCloud Alibaba 2.1.0.RELEASE，无法实现链路限流。
目前官方还未发布SCA 2.1.2.RELEASE，所以我们只能使用2.1.1.RELEASE，需要写代码的形式实现
```

上面的一段话从网上摘抄的，意思就是：**默认配置链路规则是不会生效的**

<strong style="color:red">推荐做法仍然是关闭官方的CommonFilter实例化，自己手动实例化CommonFilter，设置WEB_CONTEXT_UNIFY=false</strong>

<strong style="color:red">将SpringCloud Alibaba的版本调整2.1.1RELEASE</strong>



**配置文件中关闭sentinel官方的CommonFilter**

```yaml
spring:
    cloud:
        sentinel:
            filter:
                enabled: false
```

**添加配置类，自己构建CommonFilter实例**

```java
@Configuration
public class FilterContextConfig {

    @Bean
    public FilterRegistrationBean sentinelFilterRegistration() {
        FilterRegistrationBean registrationBean = new FilterRegistrationBean();
        registrationBean.setFilter(new CommonFilter());
        registrationBean.addUrlPatterns("/*");
        // 入口资源关闭聚合
        registrationBean.addInitParameter(CommonFilter.WEB_CONTEXT_UNIFY, "false");
        registrationBean.setName("sentinelFilter");
        registrationBean.setOrder(1);

        return registrationBean;
    }
}
```

**创建资源类**

```java
@Service
public class FlowLimitService {

    @SentinelResource(value = "testC") //将此方法标注为sentinel的资源，value=资源名
    public String testC() {
        return "testC";
    }
}
```

**接口调用类**

```java
@RestController
public class FlowLimitController {

    @Autowired
    private FlowLimitService flowLimitService;

    @GetMapping("/testA")
    public String testA() throws Exception {
        //return "-----testA";
        return flowLimitService.testC();
    }

    @GetMapping("/testB")
    public String testB() {
        //return "-----testB";
        return flowLimitService.testC();
    }
}
```

**Sentinel控制台配置规则**

![image-20211029174659028](https://cdn.jsdelivr.net/gh/le1024/image1/le/image-20211029174659028.png)



启动服务后，分别对`testA`，`testB`进行访问：

资源A正常

![image-20211029174758702](https://cdn.jsdelivr.net/gh/le1024/image1/le/image-20211029174758702.png)

资源B访问失败，被限流

![image-20211029174826554](https://cdn.jsdelivr.net/gh/le1024/image1/le/image-20211029174826554.png)

