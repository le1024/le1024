> Nacos对比各个注册中心

![image-20211021154154965](https://cdn.jsdelivr.net/gh/le1024/image1/le/image-20211021154154965.png)



<strong style="color:red">Nacos 支持AP和CP模式的切换</strong>



**何时选择使用何种模式**

如果不需要存储服务级别信息且服务实例是通过nacos-client注册， 并能够保持心跳上报，那么就可以选择AP模式。当前主流的服务如springcloud和dubbo服务，都适用于AP模式，AP模式为了服务的可用性而减弱了一致性，因此AP模式下只支持注册临时实例。

如果需要在服务级别编辑或者存储配置信息，那么CP是必须，K8S服务和DNS服务适用于CP模式。CP模式支持注册持久实例，此时则是以 Raft 协议为集群运行模式，该模式下注册实例之前必须先注册服务，如果服务不存在，则会返回错误。