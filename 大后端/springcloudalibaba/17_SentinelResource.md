> SentinelResource

注解`@SentinelResource`同Hystrix的`@HystrixCommand`作用类似。

实现服务降级的兜底方法

https://sentinelguard.io/zh-cn/docs/annotation-support.html



#### 按资源名称限流

**代码：**

```java
@RestController
public class RateLimitController {

    @GetMapping("/byResource")
    //value值为资源名，建议跟url一致，方便使用理解
    //blockHandler值为兜底方法
    @SentinelResource(value = "byResource", blockHandler = "handleException")
    public String byResource() {
        return "byResource,按资源名称进行限流";
    }

    public String handleException(BlockException exception) {
        return exception.getClass().getCanonicalName() + "\t 服务不可用";
    }
}
```

**配置：**

![image-20211103152741715](https://cdn.jsdelivr.net/gh/le1024/image1/le/image-20211103152741715.png)



![image-20211103152649890](https://cdn.jsdelivr.net/gh/le1024/image1/le/image-20211103152649890.png)

**测试：**

正常访问：http://localhost:8401/byResource

![image-20211103152909446](https://cdn.jsdelivr.net/gh/le1024/image1/le/image-20211103152909446.png)

多次频繁访问，请求量超过阈值，通过兜底方法返回提示信息，实现服务降级

![image-20211103152947753](https://cdn.jsdelivr.net/gh/le1024/image1/le/image-20211103152947753.png)

**注意！！！**

![image-20211103154907712](https://gitee.com/le1024/image1/raw/master/img/image-20211103154907712.png)

如果将上面的`handleException`方法改成`private`，是不会生效的，访问系统会直接提示异常，而不是返回自定的信息

![image-20211103155027242](https://cdn.jsdelivr.net/gh/le1024/image1/le/image-20211103155027242.png)



#### 按访问URL限流

@SentinelResource未配置兜底方法，限流后会返回sentinel默认的处理信息

**代码：**

```java
@GetMapping("/byUrl")
@SentinelResource(value = "byUrl")
public String byUrl() {
    return "byUrl,按访问Url进行限流";
}
```

**配置：**

![image-20211103154406106](https://cdn.jsdelivr.net/gh/le1024/image1/le/image-20211103154406106.png)

**测试：**

http://localhost:8401/byUrl，返回默认的处理信息

![image-20211103154451228](https://cdn.jsdelivr.net/gh/le1024/image1/le/image-20211103154451228.png)

#### 自定义限流处理逻辑

每个方法都有一个自己的兜底方法，代码膨胀，且处理方法和业务逻辑耦合在一起，不直观；同Hystrix一样，同样可以实现自定的限流逻辑处理器。

正如官网的说明：

- `blockHandler` / `blockHandlerClass`: `blockHandler` 对应处理 `BlockException` 的函数名称，可选项。blockHandler 函数访问范围需要是 `public`，返回类型需要与原方法相匹配，参数类型需要和原方法相匹配并且最后加一个额外的参数，类型为 `BlockException`。blockHandler 函数默认需要和原方法在同一个类中。若希望使用其他类的函数，则可以指定 `blockHandlerClass` 为对应的类的 `Class` 对象，注意对应的函数必需为 **static** 函数，否则无法解析。
- `fallback`：fallback 函数名称，可选项，用于在抛出异常的时候提供 fallback 处理逻辑。fallback 函数可以针对所有类型的异常（除了 `exceptionsToIgnore` 里面排除掉的异常类型）进行处理。fallback 函数签名和位置要求：
  - 返回值类型必须与原函数返回值类型一致；
  - 方法参数列表需要和原函数一致，或者可以额外多一个 `Throwable` 类型的参数用于接收对应的异常。
  - fallback 函数默认需要和原方法在同一个类中。若希望使用其他类的函数，则可以指定 `fallbackClass` 为对应的类的 `Class` 对象，注意对应的函数必需为 **static** 函数，否则无法解析。

**创建一个自己的BlockHandler**

```java
public class CustomerBlockHandler {

    //必需是static的，不然无法解析
    public static String handleException1(BlockException e) {
        return "返回自定义限流处理信息-----1";
    }

    //必需是static的，不然无法解析
    public static String handleException2(BlockException e) {
        return "返回自定义限流处理信息-----2";
    }
}
```

**接口类方法**

```java
@GetMapping("/customBlockHandler")
    @SentinelResource(value = "customBlockHandler", //资源名
            blockHandlerClass = CustomerBlockHandler.class, //自定义Handler类
            blockHandler = "handlerException1" //自定义handler类中的具体的方法名，然后对应的方法就是兜底方法
    )
    public String customBlockHandler() {
        return "customBlockHandler,按自定义限流处理";
    }
```

![image-20211103161326634](https://cdn.jsdelivr.net/gh/le1024/image1/le/image-20211103161326634.png)



**配置**

注意配置的是资源名，@SentinelResource的value值

![image-20211103160546052](https://cdn.jsdelivr.net/gh/le1024/image1/le/image-20211103160546052.png)



**测试**

一直请求：http://localhost:8401/customBlockHandler

![image-20211103161026831](https://cdn.jsdelivr.net/gh/le1024/image1/le/image-20211103161026831.png)

