> Nacos安装

官方下载：https://github.com/alibaba/nacos/releases



#### windows安装

前提环境：**java8+maven**

直接解压已下载好的nacos，进入到`bin`目录

![image-20211020221616560](https://cdn.jsdelivr.net/gh/le1024/image1/le/image-20211020221616560.png)

1.直接双击`startup.cmd`运行

2.当前`bin`目录打开cmd，以命令行的方式启动

standalone代表着单机模式运行，非集群模式

```bash
startup.cmd  -m standalone
```

![image-20211020221937116](https://cdn.jsdelivr.net/gh/le1024/image1/le/image-20211020221937116.png)

1.1.4版本的nacos默认启动就是单机模式的，直接用`startup.cmd`启动就行

![image-20211026172327939](https://cdn.jsdelivr.net/gh/le1024/image1/le/image-20211026172327939.png)



如需集群方式启动，将`set MODE=cluster`即可



浏览器输入地址：http://localhost:8848/nacos/

默认用户名密码：`nacos/nacos`

![image-20211020222356882](https://cdn.jsdelivr.net/gh/le1024/image1/le/image-20211020222356882.png)



#### Linux安装

安装系统版本：centos

https://github.com/alibaba/nacos/releases/tag/1.1.4，将下载好的nacos上传至服务器，目录随意

![image-20211026222711148](https://cdn.jsdelivr.net/gh/le1024/image1/le/image-20211026222711148.png)

解压文件

```bash
tar -zxvf nacos-server-1.1.4.tar.gz
```

![image-20211026222820658](https://cdn.jsdelivr.net/gh/le1024/image1/le/image-20211026222820658.png)

进入`bin`目录

启动nacos，默认为集群`cluster`方式，启动加参数`-m standalone`单机启动服务

```bash
./startup.sh -m standalone
```

![image-20211026224516949](https://cdn.jsdelivr.net/gh/le1024/image1/le/image-20211026224516949.png)

如果集群方式启动，本地测试服务器内存大小不够的话，可以修改`startup.sh`，减小启动内存

![image-20211026224744931](https://cdn.jsdelivr.net/gh/le1024/image1/le/image-20211026224744931.png)

