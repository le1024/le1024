> Nacos作为服务配置中心



#### Nacos作为服务配置中心-基础配置

**1. 创建Module**`cloudalibaba-config-nacos-client3377`

**2. POM依赖**

```xml
<dependencies>
        <!--nacos-config-->
        <dependency>
            <groupId>com.alibaba.cloud</groupId>
            <artifactId>spring-cloud-starter-alibaba-nacos-config</artifactId>
        </dependency>
        <!--nacos-discovery-->
        <dependency>
            <groupId>com.alibaba.cloud</groupId>
            <artifactId>spring-cloud-starter-alibaba-nacos-discovery</artifactId>
        </dependency>
        <!--web + actuator-->
        <dependency>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-starter-web</artifactId>
        </dependency>
        <dependency>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-starter-actuator</artifactId>
        </dependency>
        <!--一般基础配置-->
        <dependency>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-devtools</artifactId>
            <scope>runtime</scope>
            <optional>true</optional>
        </dependency>
        <dependency>
            <groupId>org.projectlombok</groupId>
            <artifactId>lombok</artifactId>
            <optional>true</optional>
        </dependency>
        <dependency>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-starter-test</artifactId>
            <scope>test</scope>
        </dependency>
    </dependencies>
```



**3. 配置YML文件**

需要配置`bootstrap.yml`和`application.yml`

Nacos同Springcloud一样，在项目初始化的时候，要保证先从配置中心拉取配置，拉取配置后才能保证项目的正常启动。

SpringBoot中配置文件的加载顺序是存在优先级的，<font color="red">bootstrap优先级高于application</font>

`bootstrap.yml`

```yaml
server:
  port: 3377
spring:
  application:
    name: nacos-config-client
  cloud:
    nacos:
      discovery:
        server-addr: localhost:8848 #nacos服务注册中心地址
      config:
        server-addr: localhost:8848 #nacos服务配置中心地址
        file-extension: yaml #指定配置文件格式

```

`application.yml`

```yaml
spring:
  profiles:
    active: dev #表示开发环境
```



**4. 主启动类**

```java
@SpringBootApplication
@EnableDiscoveryClient
public class NacosConfigClientMain3377 {

    public static void main(String[] args) {
        SpringApplication.run(NacosConfigClientMain3377.class, args);
    }
}
```



**5. 业务类**

```java
@RestController
@RefreshScope //在控制器类加入@RefreshScope注解使当前类下的配置支持Nacos的动态刷新功能。
public class ConfigClientController {

    @Value("${config.info}")
    private String configInfo;

    @GetMapping("/config/info")
    public String getConfigInfo() {
        return configInfo;
    }
}
```



**6. Nacos中添加配置信息**

详细配置说明参考官网：https://nacos.io/zh-cn/docs/quick-start-spring-cloud.html

先看一下Nacos添加配置文件的界面

![image-20211021211754383](https://cdn.jsdelivr.net/gh/le1024/image1/le/image-20211021211754383.png)

Nacos中的dataid的组成格式及与SpringBoot配置文件中的匹配规则为：

<strong style="color:green">${prefix}-${spring.profiles.active}.${spring.cloud.nacos.config.file-extension}</strong>

上述就是dataId的完整格式

- `prefix` 默认为 `spring.application.name` 的值，也可以通过配置项 `spring.cloud.nacos.config.prefix`来配置
- `spring.profiles.active` 即为当前环境对应的 profile，**当 `spring.profiles.active` 为空时**，**dataId 的拼接格式变成 `${prefix}.${file-extension}`**，不建议为空
- `file-exetension` 为配置内容的数据格式，可以通过配置项 `spring.cloud.nacos.config.file-extension` 来配置。目前只支持 `properties` 和 `yaml` 类型

![image-20211021212404200](https://cdn.jsdelivr.net/gh/le1024/image1/le/image-20211021212404200.png)

点击界面右侧➕创建配置文件：

![image-20211021212737041](https://cdn.jsdelivr.net/gh/le1024/image1/le/image-20211021212737041.png)

点击发布即可

![image-20211021212757601](https://cdn.jsdelivr.net/gh/le1024/image1/le/image-20211021212757601.png)

![image-20211021213101564](https://cdn.jsdelivr.net/gh/le1024/image1/le/image-20211021213101564.png)



**7. 测试**

启动工程

![image-20211021213223772](https://cdn.jsdelivr.net/gh/le1024/image1/le/image-20211021213223772.png)

请求地址：http://localhost:3377/config/info

成功获取到nacos里面创建的配置文件信息

![image-20211021213254393](https://cdn.jsdelivr.net/gh/le1024/image1/le/image-20211021213254393.png)

**8. Nacos的动态刷新**

在Nacos配置列表选择配置文件，点击编辑：修改version=2，再次发布

![image-20211021213442902](https://cdn.jsdelivr.net/gh/le1024/image1/le/image-20211021213442902.png)

再次请求：http://localhost:3377/config/info

version值立刻刷新了，而不是像Springcloud的config，需要发起一个POST请求来完成刷新

![image-20211021213522959](https://cdn.jsdelivr.net/gh/le1024/image1/le/image-20211021213522959.png)

#### Nacos作为服务配置中心-分类配置

在项目的实际开发中，是需要有多环境多项目的管理，dev开发环境，pro生产环境，test测试环境等等等，nacos可以帮助我们很友好的实现这些功能。



**1. Namespace+Group+Data ID三者关系？为什么这么设计？**

![image-20211022092208049](https://cdn.jsdelivr.net/gh/le1024/image1/le/image-20211022092208049.png)

类似于java里面的package和类名

最外层的`namespace`可以区分部署环境，`group`和`dataID`逻辑上区分两个目标对象

在Nacos中，三者的默认值：

<strong style="color:blue">NameSpace=public，Group=DEFAULT_GROUP，Cluster是DEFAULT</strong>

Nacos默认命令空间是public，主要用来实现隔离。比如三个环境：开发、测试、生产环境，可以创建三个namespace来实现隔离

Group默认就是DEFAULT_GROUP，group可以把不同的微服务划分到同一个分组里面去

Service就是微服务：一个Service可以包含多个Cluster(集群)，Nacos默认Cluster是DEFAULT，Cluster是对指定微服务的一个虚拟划分

Instance就是微服务的实例



**2. 三种方案实现加载配置**

**DataID方案**

指定spring.profile.active和配置文件的DataID来使不同环境下读取不同的配置

默认空间+默认分组+新建dev和test两个DataID

新建dev配置DataID：

![image-20211022102907520](https://cdn.jsdelivr.net/gh/le1024/image1/le/image-20211022102907520.png)



新建test配置DataID：

![image-20211022103116192](https://cdn.jsdelivr.net/gh/le1024/image1/le/image-20211022103116192.png)



创建完成，通过`spring.profile.active`属性就能进行多环境下配置文件的读取，`spring.profile.active`就对应上面创建的`dev`和`test`值

启动服务测试，当前配置为`test`环境

![image-20211022103815632](https://cdn.jsdelivr.net/gh/le1024/image1/le/image-20211022103815632.png)

<strong style="color:red">配置是什么就加载什么</strong>



**Group方案**

通过Group实现环境区分，创建新的Group

![image-20211022111645274](https://cdn.jsdelivr.net/gh/le1024/image1/le/image-20211022111645274.png)

![image-20211022111830737](https://cdn.jsdelivr.net/gh/le1024/image1/le/image-20211022111830737.png)



`bootstrap.yml`添加新属性：`group`来绑定nacos创建的分组

```yaml
spring:
  application:
    name: nacos-config-client
  cloud:
    nacos:
      discovery:
        server-addr: localhost:8848 #nacos服务注册中心地址
      config:
        server-addr: localhost:8848 #nacos服务配置中心地址
        file-extension: yaml #指定配置文件格式
        group: TEST_GROUP #绑定nacos创建的分组，默认是DEFAULT_GROUP
```

如果修改了DataId的值，`application.yml`文件的属性`spring.profiles.active`的值也需要修改，当前演示只添加了新的分组group，没有更改DataId

![image-20211022113530194](https://cdn.jsdelivr.net/gh/le1024/image1/le/image-20211022113530194.png)

启动服务测试：

![image-20211022113640065](https://cdn.jsdelivr.net/gh/le1024/image1/le/image-20211022113640065.png)

<strong style="color:red">配置是什么就加载什么</strong>



**Namespace方案**

新建dev/test的Namespace

![image-20211022161221917](https://cdn.jsdelivr.net/gh/le1024/image1/le/image-20211022161221917.png)

![image-20211022161302399](https://cdn.jsdelivr.net/gh/le1024/image1/le/image-20211022161302399.png)

在namespace为`dev`下创建配置文件：

![image-20211022161737292](https://cdn.jsdelivr.net/gh/le1024/image1/le/image-20211022161737292.png)

![image-20211022161807423](https://cdn.jsdelivr.net/gh/le1024/image1/le/image-20211022161807423.png)

修改配置文件`bootstrap.yml`，主要是新增`namespace`配置，对应的值就是：**命名空间ID**

![image-20211022161957530](https://cdn.jsdelivr.net/gh/le1024/image1/le/image-20211022161957530.png)

```yaml
server:
  port: 3377
spring:
  application:
    name: nacos-config-client
  cloud:
    nacos:
      discovery:
        server-addr: localhost:8848 #nacos服务注册中心地址
      config:
        server-addr: localhost:8848 #nacos服务配置中心地址
        file-extension: yaml #指定配置文件格式
        #group: TEST_GROUP #绑定nacos创建的分组，默认是DEFAULT_GROUP
        namespace: b847cf13-ced1-492d-9ad9-b083bd6cad08 #绑定nacos创建的命名空间ID
```

重新加载服务后，访问：

![image-20211022162752361](https://cdn.jsdelivr.net/gh/le1024/image1/le/image-20211022162752361.png)



只配置`namespace`进行演示，`group`、`dataId`就不配置了，可以自行根据规则，在不同的命名空间下创建不同的分组，然后再创建不同的配置文件，只要遵循nacos的规则就行，之后在配置文件`bootstrap.yml`和`application.yml`配置上对应的值即可。



**注意：yml文件配置信息组成的dataId，分组，命令空间在nacos控制台要事先创建好，不然会无法找到对应的配置文件！！！**



**再写一遍**：<strong style="color:red">配置是什么就加载什么！！！</strong>

