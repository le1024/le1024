> Sentinel熔断降级

官方文档：https://sentinelguard.io/zh-cn/docs/circuit-breaking.html

#### 熔断降级说明

Sentinel的降级规则即熔断降级，同Hystrix的服务熔断。Sentinel熔断降级会在调用链路中某个资源出现不稳定状态时(如调用超时或异常比例升高)，对这个资源的调用进行限制，让请求快速失败，避免影响到其他的资源到导致服务级联错误。

当资源被降级后，在接下来的降级时间窗口内，对该资源自动熔断（默认抛出DegradeException）

Sentinel 提供以下几种熔断策略：

![image-20211101211021140](https://cdn.jsdelivr.net/gh/le1024/image1/le/image-20211101211021140.png)

**RT (平均响应时间，秒级)**

平均响应时间超过阈值(毫秒值)且在时间窗口内通过的请求≥5，满足这两个条件后触发降级。窗口期过后关闭断路器。

RT最大4900（更大的通过 -Dcsp.sentinel.statistic.max.rt=xxx生效）



**异常比例（秒级）**

QPS ≥ 5且异常比例(秒级统计)超过阈值时，触发降级；时间窗口结束后，关闭降级



**异常数（分钟级）**

异常数（分钟统计）超过阈值时触发降级；时间窗口结束后，关闭降级



<strong style="color:orange">sentinel断路器是没有半开状态的</strong>



#### RT

平均响应时间(DEGRADE_GRAGE_RT)：当1秒内持续进入5个请求，对应时刻的平均响应时间(秒级)均超过阈值(count，以ms为单位)，那么在接下来的时间窗口(DegrandeRule中的timeWindow，以s为单位)之内，对这个方法的调用都会自动熔断(抛出DegrandeException)。Sentinel默认统计的RT上限是4900ms，超过此阈值都会算作4900ms，需要更改的话通过启动配置` -Dcsp.sentinel.statistic.max.rt=xxx`配置

![image-20211101222531841](https://cdn.jsdelivr.net/gh/le1024/image1/le/image-20211101222531841.png)

<strong style="color:blue">注：1秒内5个请求，是sentinel默认的，查看源码，DegradeRule</strong>



**代码：**

```java
@GetMapping("/testD")
public String testD() {
    try {
        TimeUnit.SECONDS.sleep(1);
    } catch (InterruptedException e) {
        e.printStackTrace();
    }
    log.info("测试RT。。。。");
    return "-----testD";
}
```

**降级配置：**

当前为旧版本1.7.0，高版本有更多的配置

![image-20211102094607194](https://cdn.jsdelivr.net/gh/le1024/image1/le/image-20211102094607194.png)

RT设为200ms，表示希望在200ms内处理完任务，如果超过200ms还未处理完，在时间窗口1s内，断路器打开，服务不可用

**Jmeter压测：**

![image-20211102100834716](https://cdn.jsdelivr.net/gh/le1024/image1/le/image-20211102100834716.png)

![image-20211102100847563](https://cdn.jsdelivr.net/gh/le1024/image1/le/image-20211102100847563.png)



结合降级配置，1秒打进10个线程(大于5个了)去调用testD，希望在200ms内完成任务，如果200ms未完成，则在时间窗口1s后打开断路器，微服务不可用

启动jmeter后，再去请求接口：http://localhost:8401/testD，由于高访问量，超过了预设的配置，服务不可用

![image-20211102101444825](https://cdn.jsdelivr.net/gh/le1024/image1/le/image-20211102101444825.png)

关闭jmeter，再去重新访问：http://localhost:8401/testD，服务恢复正常



#### 异常比例

当资源的每秒请求量 ≥ 5，并且每秒异常数占通过量的比值超过阈值之后，服务进入熔断状态，即在时间窗口期内，对这个方法返回不可用。<strong style="color:red">异常比率的阈值范围是0.0 ~ 1.0，代表0% ~ 100%</strong>

![image-20211102205616550](https://cdn.jsdelivr.net/gh/le1024/image1/le/image-20211102205616550.png)

**代码：**

```java
@GetMapping("/testE")
public String testE() {
    log.info("测试异常比例");
    int count = 1/0;
    return "-----testE";
}
```

**配置：**

![image-20211102210314821](https://cdn.jsdelivr.net/gh/le1024/image1/le/image-20211102210314821.png)

设置比例0.2，意思是超过20%的异常就进行熔断

**Jmeter压测：**

![image-20211102210549525](https://cdn.jsdelivr.net/gh/le1024/image1/le/image-20211102210549525.png)

现在测试，代码中的异常存在，现在肯定是100%的错误率，所以当我们去访问：http://localhost:8401/testE

![image-20211102210750649](https://cdn.jsdelivr.net/gh/le1024/image1/le/image-20211102210750649.png)

#### 异常数

当资源近1分钟的异常数超过阈值后会进入熔断状态。注意由于统计时间窗口是分钟级别的，若`timeWindow`小于60s，则结束熔断状态后仍然可能再次进入熔断状态

<strong style="color:red">时间窗口一定要大于60s</strong>

![image-20211102211003605](https://cdn.jsdelivr.net/gh/le1024/image1/le/image-20211102211003605.png)



**代码：**

```java
@GetMapping("/testF")
public String testE() {
    log.info("测试异常数");
    int count = 1/0;
    return "-----testF";
}
```

**配置：**

![image-20211102211532788](https://cdn.jsdelivr.net/gh/le1024/image1/le/image-20211102211532788.png)

<strong style="color:red">异常数是按分钟统计的</strong>

**测试：**

访问：http://localhost:8401/testF

根据配置，前五次都是直接报异常信息的

![image-20211102211634658](https://cdn.jsdelivr.net/gh/le1024/image1/le/image-20211102211634658.png)

过了五次后，服务进入熔断状态

![image-20211102211719936](https://cdn.jsdelivr.net/gh/le1024/image1/le/image-20211102211719936.png)