> Nacos集群说明

https://nacos.io/zh-cn/docs/cluster-mode-quick-start.html



生产模式需要使用集群模式，前面的单机模式不可实际中使用

集群模式简易架构图：

![image-20211026151859251](https://cdn.jsdelivr.net/gh/le1024/image1/le/image-20211026151859251.png)





**Nacos支持三种部署模式**

- 单机模式 - 用于测试和单机试用
- 集群模式 - 用于生产环境，确保高可用
- 多集群模式 - 用于多数据中心场景



**数据库配置**

默认nacos使用嵌入式数据库`derby`实现数据的存储，如果启动多个默认配置的nacos节点，数据存储的一致性就会有问题。

<strong style="color:red">Nacos可以使用集中式存储的方式来支持集群化部署，目前只支持mysql的存储</strong>

- 1.安装数据库，版本要求：5.6.5+

- 2.初始化mysql数据库，数据库初始化文件：nacos-mysql.sql

  在nacos解压文件中找到sql脚本，运行创建nacos数据库

  ![image-20211026155201798](https://cdn.jsdelivr.net/gh/le1024/image1/le/image-20211026155201798.png)

  ![image-20211026160023892](https://cdn.jsdelivr.net/gh/le1024/image1/le/image-20211026160023892.png)

- 3.修改`conf/application.properties`文件，增加支持mysql数据源配置(目前仅支持mysql)，添加mysql数据源url、用户名和密码

  ```properties
  spring.datasource.platform=mysql
  
  db.num=1
  db.url.0=mysql://127.0.0.1:3306/nacos_config?characterEncoding=utf8&connectTimeout=1000&socketTimeout=3000&autoReconnect=true&useUnicode=true&useSSL=false&serverTimezone=UTC
  db.user.0=nacos
  db.password.0=nacos
  ```




<strong style="color:orange">详细的配置看[08_Nacos持久化配置](./08_Nacos持久化配置)</strong>

