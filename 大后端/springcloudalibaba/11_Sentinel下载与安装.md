> Sentinel下载与安装

[下载地址：Sentinel](https://github.com/alibaba/Sentinel/releases)

Sentinel 分为两个部分:

- 核心库（Java 客户端）不依赖任何框架/库，能够运行于所有 Java 运行时环境，同时对 Dubbo / Spring Cloud 等框架也有较好的支持。
- 控制台（Dashboard）基于 Spring Boot 开发，打包后可以直接运行，不需要额外的 Tomcat 等应用容器。



准备工作

- java环境ok
- 8080端口未占用，sentinel默认8080端口
- 下载sentinel-dashboard-1.7.0.jar（版本随意）



运行sentinel

```bash
java -jar sentinel-dashboard-1.7.0.jar
```

访问：http://localhost:8080，默认帐号密码：sentinel/sentinel

![image-20211029102059067](https://cdn.jsdelivr.net/gh/le1024/image1/le/image-20211029102059067.png)

![image-20211029102132595](https://cdn.jsdelivr.net/gh/le1024/image1/le/image-20211029102132595.png)

只启动sentinel，主控制台页面是空的，不会有其他的信息，<strong style="color:red">因为sentinel采用的是懒加载机制，当有微服务请求之后才会出现效果</strong>

