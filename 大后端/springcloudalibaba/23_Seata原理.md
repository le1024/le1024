> Seata原理



再次过一下seata的三大组件：**TC TM RM**

Seata分布式事务流程：

1.TM开启分布式事务，TM向TC注册全局事务记录；

2.按业务场景，编排数据库、服务等事务资源，RM向TC汇报资源准备状态；

3.TM结束分布式事务，事务一阶段结束，TM通知TC提交或回滚分布式事务；

4.TC汇总事务信息，决定分布式事务是提交还是回滚；

5.TC通知当前业务的所有RM进行提交或回滚事务，事务二阶段结束。

可以简单的画图理解为：

![image-20211106135651339](https://cdn.jsdelivr.net/gh/le1024/image1/le/image-20211106135651339.png)



**默认是AT事务模式**

详细的说明可以看官网说明：http://seata.io/zh-cn/docs/overview/what-is-seata.html

**整体机制**

两阶段提交协议的演变：

- 一阶段：业务数据和回滚日志记录在同一个本地事务中提交，释放本地锁和连接资源。
- 二阶段：
  - 提交异步化，非常快速地完成。
  - 回滚通过一阶段的回滚日志进行反向补偿。



一阶段：

1.实际业务中，会有业务的SQL需要执行，比如update1(下面用这个名字来说明)：`update tb set age = 2 where id = 1`，更新age=2,，seata会拦截这一段SQL(update1)并解析update1，找到update1要更新的业务数据，即select1：`select age from tb where id = 1`，找到age，假如age=1，seata会将这个select1保存为“Before Image”即查询前镜像。

2.执行业务SQL update1，此时数据会被更新

3.业务SQL update1更新完成后，seata同样会个操作select2：`select age from tb where id = 1`，此时age为更新后的2，将其保存为"after image"查询后镜像

4.前后镜像数据及业务sql均会以回滚日志记录的方式存到`undo_log`表中

以上操作均在一个数据库事务内完成，保证了操作的原子性。



二阶段提交：

如果业务正常完成，业务SQL执行完毕，select1和select2的前后镜像数据及行锁都会被清理，删除`undo_log`表数据



二阶段回滚：

1.事务回滚的话，seata需要回滚一阶段已经执行的业务sql update1，还原原业务数据

2.通过XID和Branch Id查询相应的`undo_logs`数据，找到前镜像数据selece1进行还原，还原前需要拿当前数据库数据同后镜像数据select2进行校验，如果相同，则说明数据未改变过，执行select1回滚数据。（如果是新增update，事务回滚就对应delete，如果是update，就根据前镜像还原）

3.如果不同，说明数据有改动，不可通过前镜像还原数据。官网说需要根据配置策略来做处理，详细的说明在另外的文档中介绍，这个文档暂时没看到，不知道是不是指的是TCC模式





通过业务来实现效果：

以Debug模式启动之前创建的三个服务，并且在账户微服务服务打上一个断点：

![image-20211106151611976](https://cdn.jsdelivr.net/gh/le1024/image1/le/image-20211106151611976.png)



可以先看下三个微服务数据库的`undo_log`表都是空的，Seata数据库的三个表`branch_table`、`global_table`、`lock_table`也是空的

![image-20211106152015747](https://cdn.jsdelivr.net/gh/le1024/image1/le/image-20211106152015747.png)

![image-20211106152024837](https://cdn.jsdelivr.net/gh/le1024/image1/le/image-20211106152024837.png)



请求：http://localhost:2001/order/create?userId=1&productId=1&count=10&money=100，会进入debug模式

然后再去观察数据库：seata库中的表都有数据了

全局事务表：有全局事务ID，`transaction-service-group`也是我们在配置文件配置的名称

![image-20211106152541586](https://cdn.jsdelivr.net/gh/le1024/image1/le/image-20211106152541586.png)

分支事务表：单独的分支id，统一的全局事务ID

![image-20211106152703271](https://cdn.jsdelivr.net/gh/le1024/image1/le/image-20211106152703271.png)

锁表：事务的锁表

![image-20211106152856678](https://cdn.jsdelivr.net/gh/le1024/image1/le/image-20211106152856678.png)

再查看账户数据库对应的`undo_log`表：

![image-20211106152950588](https://cdn.jsdelivr.net/gh/le1024/image1/le/image-20211106152950588.png)

rollback_info字段值：是一段json，可以复制去网上json解析一下

![image-20211106153108386](https://cdn.jsdelivr.net/gh/le1024/image1/le/image-20211106153108386.png)

可以看到全局事务ID，相应的前镜像数据和后镜像数据，还可以找到字段一一对应的数据

![image-20211106153431592](https://cdn.jsdelivr.net/gh/le1024/image1/le/image-20211106153431592.png)

另外两个服务数据库的`undo_log`一样就不弄了。



**可以看出，该流程中，全局事务ID贯穿整个流程，完成一系列的数据关联。**

走完debug，再去查看数据库表：表的数据都是被清理

