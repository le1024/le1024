> Sentinel持久化

之前在sentinel控制台配置的一系列规则，在服务重启之后，都会清除，需要重新配置，所以需要对配置的规则进行持久化。

https://github.com/alibaba/Sentinel/wiki/%E5%9C%A8%E7%94%9F%E4%BA%A7%E7%8E%AF%E5%A2%83%E4%B8%AD%E4%BD%BF%E7%94%A8-Sentinel#Push%E6%A8%A1%E5%BC%8F

官方有三种方式实现规则持久化，`原始方式`、`pull模式`、`push模式`，推荐使用`push模式`

| 推送模式                                                     | 说明                                                         | 优点                         | 缺点                                                         |
| ------------------------------------------------------------ | ------------------------------------------------------------ | ---------------------------- | ------------------------------------------------------------ |
| [原始模式](https://github.com/alibaba/Sentinel/wiki/在生产环境中使用-Sentinel#原始模式) | API 将规则推送至客户端并直接更新到内存中，扩展写数据源（[`WritableDataSource`](https://github.com/alibaba/Sentinel/wiki/动态规则扩展)） | 简单，无任何依赖             | 不保证一致性；规则保存在内存中，重启即消失。严重不建议用于生产环境 |
| [Pull 模式](https://github.com/alibaba/Sentinel/wiki/在生产环境中使用-Sentinel#Pull模式) | 扩展写数据源（[`WritableDataSource`](https://github.com/alibaba/Sentinel/wiki/动态规则扩展)）， 客户端主动向某个规则管理中心定期轮询拉取规则，这个规则中心可以是 RDBMS、文件 等 | 简单，无任何依赖；规则持久化 | 不保证一致性；实时性不保证，拉取过于频繁也可能会有性能问题。 |
| **[Push 模式](https://github.com/alibaba/Sentinel/wiki/在生产环境中使用-Sentinel#Push模式)** | 扩展读数据源（[`ReadableDataSource`](https://github.com/alibaba/Sentinel/wiki/动态规则扩展)），规则中心统一推送，客户端通过注册监听器的方式时刻监听变化，比如使用 Nacos、Zookeeper 等配置中心。这种方式有更好的实时性和一致性保证。**生产环境下一般采用 push 模式的数据源。** | 规则持久化；一致性；快速     | 引入第三方依赖                                               |



#### 实现方式，基于nacos

搭配nacos实现，将限流配置规则持久化进nacos，当刷新某个rest地址时，sentinel就能获取到持久化在nacos里的规则，只要nacos不删除，sentinel的规则就持续有效。



##### 实现步骤

修改`cloudalibaba-sentinel-service8401`服务（该服务的完整创建代码在[12_Sentinel实时监控](./12_Sentinel实时监控)）

**pom**

添加依赖

```xml
<!--SpringCloud ailibaba sentinel-datasource-nacos -->
<dependency>
    <groupId>com.alibaba.csp</groupId>
    <artifactId>sentinel-datasource-nacos</artifactId>
</dependency>
```

**Yml**

配置nacos数据源

```yaml
 spring:
  cloud:
    sentinel:
      datasource:
        ds1:
          nacos:
            server-addr: localhost:8848 #nacos地址
            dataId: ${spring.application.name} #nacos控制台创建的配置文件dataId
            groupId: DEFAULT_GROUP
            data-type: json
            rule-type: flow
```

**nacos添加sentinel规则配置**

![image-20211104160625066](https://cdn.jsdelivr.net/gh/le1024/image1/le/image-20211104160625066.png)

![image-20211104162552084](https://cdn.jsdelivr.net/gh/le1024/image1/le/image-20211104162552084.png)

```json
[
    {
        "resource": "/testA",
        "limitApp": "default",
        "grade": 1,
        "count": 1,
        "strategy": 0,
        "controlBehavior": 0,
        "clusterMode": false
    }
]
```

resource：资源名称；
limitApp：来源应用，默认写default；
grade：阈值类型，0表示线程数，1表示QPS；
count：单机阈值；
strategy：流控模式，0表示直接，1表示关联，2表示链路；
controlBehavior：流控效果，0表示快速失败，1表示Warm Up，2表示排队等待；
clusterMode：是否集群。

<font color="red">以上参数仅针对sentinel1.7.0版本</font>，对应在sentinel的配置为：

![image-20211104162452385](https://cdn.jsdelivr.net/gh/le1024/image1/le/image-20211104162452385.png)

**启动8401服务**

启动完成后，刷新sentinel控制台，会发现多了一条流控规则，这个并不是我们手动在sentinel创建的

![image-20211104162756218](https://cdn.jsdelivr.net/gh/le1024/image1/le/image-20211104162756218.png)

访问：http://localhost:8401/testA，配置生效

![image-20211104162911748](https://cdn.jsdelivr.net/gh/le1024/image1/le/image-20211104162911748.png)

**停止8401服务**

刷新sentinel控制台，流控规则会自动清空

![image-20211104163219775](https://cdn.jsdelivr.net/gh/le1024/image1/le/image-20211104163219775.png)

重新启动8401，待启动完成稍等直接多次访问：http://localhost:8401/testA，并没有对sentinel做任何配置，会发现限流生效，再刷新sentinel控制台，规则又再次出现

![image-20211104162911748](https://s2.loli.net/2022/04/20/Knltz1QH4JjVCRq.png)

![image-20211104162756218](https://s2.loli.net/2022/04/20/j8dAeXVP7y5OpZt.png)



**总结，通过nacos实现的sentinel规则配置持久化完成**





#### 实现方式，基于zookeeper



#### 实现方式，基于redis

