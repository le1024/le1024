> Sentinel热点参数限流

官网：https://sentinelguard.io/zh-cn/docs/parameter-flow-control.html



#### 热点参数限流介绍

何为热点？热点即经常访问的数据。很多时候我们希望统计某个热点数据中访问频次最高的 Top K 数据，并对其访问进行限制。比如：

- 商品 ID 为参数，统计一段时间内最常购买的商品 ID 并进行限制
- 用户 ID 为参数，针对一段时间内频繁访问的用户 ID 进行限制

热点参数限流会统计传入参数中的热点参数，并根据配置的限流阈值与模式，对包含热点参数的资源调用进行限流。热点参数限流可以看做是一种特殊的流量控制，仅对包含热点参数的资源调用生效。



<strong style="color:red">@SentinelResource</strong>

实现热点参数限流需要通过`@SentinelResource`注解，这个注解同Hystrix的`@HystrixCommand`是类似的，可以实现一个兜底的方法返回自定提示，而不是默认的`Blocked by Sentinel (flow limiting)`



![image-20211102215726086](https://cdn.jsdelivr.net/gh/le1024/image1/le/image-20211102215726086.png)

![image-20211102215455210](https://cdn.jsdelivr.net/gh/le1024/image1/le/image-20211102215455210.png)

<strong style="color:blue">热点规则仅支持QPS模式</strong>

<strong style="color:blue">资源名就是@SentinelResource注解中的value值</strong>

<strong style="color:blue">参数索引，对应方法里面的参数；比如0代表p1，1代表p2，如果有更多参数，依此类推</strong>

<strong style="color:black">单机阈值和窗口时长同之前一样，每秒的QPS请求量及超过窗口时长服务熔断</strong>



#### 基本配置

**代码：**

```java
@GetMapping("/testHotkey")
// value值是在控制台设置热点的资源名，blockHandler是兜底方法
@SentinelResource(value = "testHotkey", blockHandler = "deal_testHotkey")
public String testHotkey(@RequestParam(value = "p1", required = false) String p1,
                         @RequestParam(value = "p2", required = false) String p2) {

    return "----- testHotkey";
}

public String deal_testHotkey(String p1, String p2, BlockException e) {
    return "----- deal_testHotkey o(╥﹏╥)o";
}
```

**配置：**

![image-20211103105906757](https://cdn.jsdelivr.net/gh/le1024/image1/le/image-20211103105906757.png)

**测试：**

首先访问：http://localhost:8401/testHotkey，一直不断的去访问，1秒的请求量超过了阈值都是正常返回

![image-20211103110549921](https://cdn.jsdelivr.net/gh/le1024/image1/le/image-20211103110549921.png)

根据设置的规则，访问：http://localhost:8401/testHotkey?p1=1，不断请求，当1秒的请求量超过阈值，服务降级，返回`deal_testHotkey`方法的提示

![image-20211103110715378](https://cdn.jsdelivr.net/gh/le1024/image1/le/image-20211103110715378.png)

当请求量正常，返回的也是正常的提示

![image-20211103110745395](https://cdn.jsdelivr.net/gh/le1024/image1/le/image-20211103110745395.png)

访问：http://localhost:8401/testHotkey?p2=2，也是正常的，因为热点规则只针对p1参数进行了限制



根据规则也可以设置第二个参数，然后进行测试

<font color="red">也可以针对同一个资源设置多个规则</font>

![image-20211103111047828](https://cdn.jsdelivr.net/gh/le1024/image1/le/image-20211103111047828.png)



#### 额外配置

就是更细粒度的划分规则，比如上面的配置，针对参数p1 1秒的qps请求设置为1，当p1=xxx时进行请求且超出阈值时都会限流。可以配置p1等于某个特殊值时，再单独的配置一个阈值。

假如p1=5时，阈值可以达到100，在一秒多次请求时，是不会满足配置规则，从而不会进行限流。其他条件的，依旧被通用的规则限制

**配置：**

填写完`参数值` `限流阈值`后，点击添加按钮新增额外规则：p1=5，阈值为100

**设置的参数类型要和接口参数类型对应上哦**

![image-20211103113921712](https://cdn.jsdelivr.net/gh/le1024/image1/le/image-20211103113921712.png)

**测试：**

❌http://localhost:8401/testHotkey?p1=1，超出阈值服务降级

![image-20211103113522090](https://cdn.jsdelivr.net/gh/le1024/image1/le/image-20211103113522090.png)

✅http://localhost:8401/testHotkey?p1=5，一直请求，服务正常，**当前请求p1=5，且请求量没有超过阈值100**

![image-20211103113848099](https://cdn.jsdelivr.net/gh/le1024/image1/le/image-20211103113848099.png)



记个知识点：

<strong style="color:red">@SentinelResource
处理的是Sentinel控制台配置的违规情况，有blockHandler方法配置的兜底处理；</strong>

<strong style="color:red">RuntimeException
int age = 10/0,这个是java运行时报出的运行时异常RunTimeException，@SentinelResource不管</strong>

<strong style="color:blue">
 @SentinelResource主管配置出错，运行出错该走异常走异常</strong>

