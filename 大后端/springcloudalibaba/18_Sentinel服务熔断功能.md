> Sentinel服务熔断功能

sentinel整合ribbon+OpenFeign



### 创建生产者服务工程

**服务1：**`cloudalibaba-provider-payment9003`

**pom**

```xml
<dependencies>
        <!--SpringCloud ailibaba nacos -->
        <dependency>
            <groupId>com.alibaba.cloud</groupId>
            <artifactId>spring-cloud-starter-alibaba-nacos-discovery</artifactId>
        </dependency>
        <dependency><!-- 引入自己定义的api通用包，可以使用Payment支付Entity -->
            <groupId>app.springcloud</groupId>
            <artifactId>cloud-api-commons</artifactId>
            <version>${project.version}</version>
        </dependency>
        <!-- SpringBoot整合Web组件 -->
        <dependency>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-starter-web</artifactId>
        </dependency>
        <dependency>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-starter-actuator</artifactId>
        </dependency>
        <!--日常通用jar包配置-->
        <dependency>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-devtools</artifactId>
            <scope>runtime</scope>
            <optional>true</optional>
        </dependency>
        <dependency>
            <groupId>org.projectlombok</groupId>
            <artifactId>lombok</artifactId>
            <optional>true</optional>
        </dependency>
        <dependency>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-starter-test</artifactId>
            <scope>test</scope>
        </dependency>
    </dependencies>
```

**yml**

```yaml
server:
  port: 9003

spring:
  application:
    name: nacos-payment-provider
  cloud:
    nacos:
      discovery:
        server-addr: localhost:8848 #nacos地址

management:
  endpoints:
    web:
      exposure:
        include: "*"
```

**主启动类**

```java
@SpringBootApplication
@EnableDiscoveryClient
public class PaymentMain9003 {

    public static void main(String[] args) {
        SpringApplication.run(PaymentMain9003.class, args);
    }
}
```

**业务类controller层**

```java
@RestController
public class PaymentController {

    @Value("${server.port}")
    private String serverPort;

    //模拟数据
    public static HashMap<Integer, Payment> hashMap = new HashMap<>();

    static {
        hashMap.put(1, new Payment(1L, "1"));
        hashMap.put(2, new Payment(2L, "2"));
        hashMap.put(3, new Payment(3L, "3"));
    }

    @GetMapping("/paymentInfo/{id}")
    public CommonResult<Payment> paymentInfo(@PathVariable("id") int id) {
        Payment payment = hashMap.get(id);
        return new CommonResult<Payment>(200, "get data from :" + serverPort, payment);
    }
}
```



**服务2：**`cloudalibaba-provider-payment9004`

同9003服务一样，改个配置端口为9004即可



### 创建消费者服务工程

**服务：**`cloudalibaba-consumer-nacos-order84`

**pom**

```xml
    <dependencies>
        <!--SpringCloud ailibaba nacos -->
        <dependency>
            <groupId>com.alibaba.cloud</groupId>
            <artifactId>spring-cloud-starter-alibaba-nacos-discovery</artifactId>
        </dependency>
        <!--SpringCloud ailibaba sentinel -->
        <dependency>
            <groupId>com.alibaba.cloud</groupId>
            <artifactId>spring-cloud-starter-alibaba-sentinel</artifactId>
        </dependency>
        <!-- 引入自己定义的api通用包，可以使用Payment支付Entity -->
        <dependency>
            <groupId>app.springcloud</groupId>
            <artifactId>cloud-api-commons</artifactId>
            <version>${project.version}</version>
        </dependency>
        <!-- SpringBoot整合Web组件 -->
        <dependency>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-starter-web</artifactId>
        </dependency>
        <dependency>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-starter-actuator</artifactId>
        </dependency>
        <!--日常通用jar包配置-->
        <dependency>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-devtools</artifactId>
            <scope>runtime</scope>
            <optional>true</optional>
        </dependency>
        <dependency>
            <groupId>org.projectlombok</groupId>
            <artifactId>lombok</artifactId>
            <optional>true</optional>
        </dependency>
        <dependency>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-starter-test</artifactId>
            <scope>test</scope>
        </dependency>
    </dependencies>
```

**yml**

```yaml
server:
  port: 84

spring:
  application:
    name: nacos-order-consumer
  cloud:
    nacos:
      discovery:
        server-addr: localhost:8848 #nacos配置地址
    sentinel:
      transport:
        port: 8719
        dashboard: localhost:8080 #sentinel dashboard地址

#消费者将要去访问的微服务名称(注册成功进nacos的微服务提供者)
service-url:
  nacos-user-service: http://nacos-payment-provider
```

**启动类**

```java
@SpringBootApplication
@EnableDiscoveryClient
public class OrderNacosMain84 {

    public static void main(String[] args) {
        SpringApplication.run(OrderNacosMain84.class, args);
    }
}
```

**配置类**

```java
@Configuration
public class ApplicationContextConfig {

    @Bean
    @LoadBalanced //负载均衡
    public RestTemplate getRestTemplate() {
        return new RestTemplate();
    }
}
```

**业务类controller**

```java
@RestController
@Slf4j
public class CircleBreakerController {


    @Value("${service-url.nacos-user-service}")
    private String serviceUrl;

    @Autowired
    private RestTemplate restTemplate;

    @GetMapping("/consumer/fallback/{id}")
    public CommonResult<Payment> fallback(@PathVariable("id") int id) {
        CommonResult<Payment> result = restTemplate.getForObject(serviceUrl + "/paymentInfo/" + id, CommonResult.class);
        if (id == 4) {
            throw new IllegalArgumentException("IllegalArgumentException,非法参数异常....");
        } else {
            assert result != null;
            if (result.getData() == null) {
                throw new NullPointerException("NullPointerException,空指针异常,无对应的数据....");
            }
        }

        return result;
    }
}
```



### 整合Ribbon

#### 默认无配置

按上述创建完三个工程后，启动服务

请求：http://localhost:84/consumer/fallback/1，多次请求，服务是以负载均衡的方式返回数据的

![image-20211103221412639](https://cdn.jsdelivr.net/gh/le1024/image1/le/image-20211103221412639.png)

![image-20211103221428253](https://cdn.jsdelivr.net/gh/le1024/image1/le/image-20211103221428253.png)

请求：http://localhost:84/consumer/fallback/4，根据接口的逻辑，直接抛异常

![image-20211103221449122](https://cdn.jsdelivr.net/gh/le1024/image1/le/image-20211103221449122.png)

请求：http://localhost:84/consumer/fallback/5，根据接口的逻辑，直接抛异常

![image-20211103221528619](https://cdn.jsdelivr.net/gh/le1024/image1/le/image-20211103221528619.png)





#### 配置fallback

`fallback`：fallback 函数名称，可选项，用于在抛出异常的时候提供 fallback 处理逻辑。fallback 函数可以针对所有类型的异常（除了 `exceptionsToIgnore` 里面排除掉的异常类型）进行处理。fallback 函数签名和位置要求：

- 返回值类型必须与原函数返回值类型一致；
- 方法参数列表需要和原函数一致，或者可以额外多一个 `Throwable` 类型的参数用于接收对应的异常。
- fallback 函数默认需要和原方法在同一个类中。若希望使用其他类的函数，则可以指定 `fallbackClass` 为对应的类的 `Class` 对象，注意对应的函数必需为 static 函数，否则无法解析。

**修改order84服务controller**

为之前的方法通过`@SentinelResource`的`fallback`属性添加兜底方法，<strong style="color:red">fallback负责业务异常，blockhandler负责sentinel配置的规则异常</strong>

```java
    @GetMapping("/consumer/fallback/{id}")
	//配置兜底方法：
    @SentinelResource(value = "fallback", fallback = "handleFallback") //fallback负责业务异常，之前的blockHandler负责sentinel配置规则违规
    public CommonResult<Payment> fallback(@PathVariable("id") int id) {
        CommonResult<Payment> result = restTemplate.getForObject(serviceUrl + "/paymentInfo/" + id, CommonResult.class);
        if (id == 4) {
            throw new IllegalArgumentException("IllegalArgumentException,非法参数异常....");
        } else {
            assert result != null;
            if (result.getData() == null) {
                throw new NullPointerException("NullPointerException,空指针异常,无对应的数据....");
            }
        }

        return result;
    }

    /**
     * 方法必需是public
     * 方法名为fallback配置的名字
     */
    public CommonResult handleFallback(@PathVariable("id") int id, Throwable e) {
        return new CommonResult(500, "fallback兜底方法，exception内容：" + e.getMessage());
    }
```

<strong style="color:black">该测试sentinel无需配置规则</strong>

重启服务后，再次访问：

http://localhost:84/consumer/fallback/4

![image-20211103222444366](https://cdn.jsdelivr.net/gh/le1024/image1/le/image-20211103222444366.png)

http://localhost:84/consumer/fallback/5

![image-20211103222432714](https://cdn.jsdelivr.net/gh/le1024/image1/le/image-20211103222432714.png)

<strong style="color:black">返回的信息，均为fallback配置的兜底方法信息</strong>



#### 配置blockHandler

- `blockHandler` / `blockHandlerClass`: `blockHandler` 对应处理 `BlockException` 的函数名称，可选项。blockHandler 函数访问范围需要是 `public`，返回类型需要与原方法相匹配，参数类型需要和原方法相匹配并且最后加一个额外的参数，类型为 `BlockException`。blockHandler 函数默认需要和原方法在同一个类中。若希望使用其他类的函数，则可以指定 `blockHandlerClass` 为对应的类的 `Class` 对象，注意对应的函数必需为 static 函数，否则无法解析。

blockHanlder是针对sentinel配置违规的，<strong style="color:black">需要针对资源在sentinel配置规则</strong>

**代码**

```java
    @GetMapping("/consumer/fallback/{id}")
    //配置兜底方法：fallback负责业务异常，之前的blockHandler负责sentinel配置规则违规
    //@SentinelResource(value = "fallback", fallback = "handleFallback")
    @SentinelResource(value = "fallback", blockHandler = "blockHandler")
    public CommonResult<Payment> fallback(@PathVariable("id") int id) {
        CommonResult<Payment> result = restTemplate.getForObject(serviceUrl + "/paymentInfo/" + id, CommonResult.class);
        if (id == 4) {
            throw new IllegalArgumentException("IllegalArgumentException,非法参数异常....");
        } else {
            assert result != null;
            if (result.getData() == null) {
                throw new NullPointerException("NullPointerException,空指针异常,无对应的数据....");
            }
        }

        return result;
    }

/**
     * 方法必需是public
     * blockHanlder配置的名称
     */
    public CommonResult blockHandler(@PathVariable("id") int id, BlockException e) {
        return new CommonResult(500, "blockHandler兜底方法，exception内容：" + e.getMessage());
    }
```

**配置**

![image-20211103223549851](https://cdn.jsdelivr.net/gh/le1024/image1/le/image-20211103223549851.png)

**测试**

根据配置规则，由于异常数是按分钟统计的，所以在一分钟内，超过有2个异常请求，就会服务降级

先请求正常的：http://localhost:84/consumer/fallback/1

![image-20211103224043915](https://cdn.jsdelivr.net/gh/le1024/image1/le/image-20211103224043915.png)

接着多次请求：http://localhost:84/consumer/fallback/4或者http://localhost:84/consumer/fallback/5，前两次：服务抛业务异常

![image-20211103224129958](https://cdn.jsdelivr.net/gh/le1024/image1/le/image-20211103224129958.png)

超过阈值后就会走兜底方法，服务降级：

![image-20211103224215073](https://cdn.jsdelivr.net/gh/le1024/image1/le/image-20211103224215073.png)



#### 配置blockHandler和fallback

**代码**

将`fallback`和`blockHandler`都配置上

```java
    @GetMapping("/consumer/fallback/{id}")
    //配置兜底方法：fallback负责业务异常，之前的blockHandler负责sentinel配置规则违规
    //@SentinelResource(value = "fallback", fallback = "handleFallback")
    //@SentinelResource(value = "fallback", blockHandler = "blockHandler")
    @SentinelResource(value = "fallback", fallback = "handleFallback", blockHandler = "blockHandler")
    public CommonResult<Payment> fallback(@PathVariable("id") int id) {
        CommonResult<Payment> result = restTemplate.getForObject(serviceUrl + "/paymentInfo/" + id, CommonResult.class);
        if (id == 4) {
            throw new IllegalArgumentException("IllegalArgumentException,非法参数异常....");
        } else {
            assert result != null;
            if (result.getData() == null) {
                throw new NullPointerException("NullPointerException,空指针异常,无对应的数据....");
            }
        }

        return result;
    }
```

**配置**

配置规则随意，主要是测试功能`fallback`和`blockHandler`

![image-20211104092618743](https://cdn.jsdelivr.net/gh/le1024/image1/le/image-20211104092618743.png)

**测试**

1.访问正常的：http://localhost:84/consumer/fallback/1

一秒一个，未超过设置的阈值

![image-20211104092945945](https://cdn.jsdelivr.net/gh/le1024/image1/le/image-20211104092945945.png)

一秒多个请求，超过阈值，<strong style="color:black">BlockHandler实现异常处理</strong>

![image-20211104093011153](https://cdn.jsdelivr.net/gh/le1024/image1/le/image-20211104093011153.png)

2.访问异常的：http://localhost:84/consumer/fallback/4

一秒一个，未超过阈值，<strong style="color:black">fallback实现异常处理</strong>

![image-20211104093234855](https://cdn.jsdelivr.net/gh/le1024/image1/le/image-20211104093234855.png)

一秒多个请求，超过阈值，<strong style="color:black">BlockHandler实现异常处理</strong>

![image-20211104093356909](https://cdn.jsdelivr.net/gh/le1024/image1/le/image-20211104093356909.png)



<strong style="color:red">总结一下：fallback处理的是java异常，BlockHandler处理的是sentinel配置违规异常</strong>



#### exceptionsToIgnore

异常忽略

**代码**

注解`@SentinelResource`添加一个`exceptionToIgnore`属性，该属性的值为异常的名称，表示配置该异常后，fallback的兜底方法失效，不再起作用

```java
@SentinelResource(value = "fallback",
            fallback = "handleFallback",
            blockHandler = "blockHandler",
            exceptionsToIgnore = {IllegalArgumentException.class}
    )
```

**测试**

重启服务，访问：http://localhost:84/consumer/fallback/4，页面直接报异常信息，而不是fallback提供的自定义信息

![image-20211104094320083](https://cdn.jsdelivr.net/gh/le1024/image1/le/image-20211104094320083.png)



### 整合Feign

还是用上面的服务测试，使用84服务通过Feign调用9003服务

修改84服务

**pom**

84工程引入`openFeign`

```xml
<!--SpringCloud openfeign -->
<dependency>
    <groupId>org.springframework.cloud</groupId>
    <artifactId>spring-cloud-starter-openfeign</artifactId>
</dependency>
```

**yml**

激活sentinel对feign的支持

```yaml
# 激活Sentinel对Feign的支持
feign:
  sentinel:
    enabled: true
```

**主启动类**

添加`@EnableFeignClients `启用feign功能

**业务类**

带有`@FeignClient`注解的业务接口

```java
//调用nacos-payment-provider微服务接口
//PaymentFallbackService实现兜底
@FeignClient(value = "nacos-payment-provider", fallback = PaymentFallbackService.class)
public interface PaymentService {

    @GetMapping("/paymentInfo/{id}")
    public CommonResult<Payment> paymentInfo(@PathVariable("id") Long id);
}
```

业务接口实现类，即兜底方法统一类

```java
@Component
public class PaymentFallbackService implements PaymentService{

    @Override
    public CommonResult<Payment> paymentInfo(Long id) {
        return new CommonResult<>(500, "服务降级兜底方法，获取数据失败", new Payment(id, null));
    }
}
```

controller类

```java
    //===========OpenFeign方式================
    @Resource
    private PaymentService paymentService;

    @GetMapping("/consumer/openfeign/{id}")
    @SentinelResource(value = "openfeign",
            fallback = "feignFallback",
            blockHandler = "feignBlockHandler")
    public CommonResult<Payment> paymentInfo(@PathVariable("id") Long id) {
        if (id == 4) {
            throw new IllegalArgumentException("IllegalArgumentException, 非法参数异常....");
        }
        return paymentService.paymentInfo(id);
    }

    public CommonResult feignFallback(@PathVariable("id") Long id, Throwable e) {
        return new CommonResult(500, "openfeign fallback兜底方法，exception内容：" + e.getMessage());
    }

    public CommonResult feignBlockHandler(@PathVariable("id") Long id, BlockException e) {
        return new CommonResult(500, "openfeign blockHandler兜底方法，exception内容：" + e.getMessage());
    }
```

**测试**

`fallback`和`blockHandler`测试和上面的一样，不测了

正常访问：http://localhost:84/consumer/openfeign/1，然后把9003服务停掉再次访问：

![image-20211104115659202](https://cdn.jsdelivr.net/gh/le1024/image1/le/image-20211104115659202.png)

此时走的就是`@FeignClient`注解的`fallback`属性的配置方法。

**Sentinel的配置可以和Feign的兜底方法配合使用**

