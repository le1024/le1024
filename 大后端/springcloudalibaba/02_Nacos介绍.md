> Nacos介绍

官网：

https://nacos.io/zh-cn/docs/quick-start.html

https://spring-cloud-alibaba-group.github.io/github-pages/greenwich/spring-cloud-alibaba.html#_spring_cloud_alibaba_nacos_discovery



#### 是什么

一个更易于构建云原生应用的动态服务发现、配置管理和服务管理平台。

Nacos: Dynamic Naming and Configuration Service

Nacos就是注册中心 + 配置中心的组合，**Nacos=Eureka + Config + Bus**



#### 能干嘛

- 替代Eureka做服务注册中心
- 替代Config做服务配置中心



#### 注册中心比较

简洁版

![image-20211020221211608](https://cdn.jsdelivr.net/gh/le1024/image1/le/image-20211020221211608.png)