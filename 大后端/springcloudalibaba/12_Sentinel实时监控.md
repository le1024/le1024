> Sentinel实时监控



**1. 创建工程cloudalibaba-sentinel-service8401**

**2. Pom依赖**

添加`sentinel依赖`

```xml
<!--SpringCloud ailibaba sentinel-datasource-nacos 后续做持久化用到-->
<dependency>
    <groupId>com.alibaba.csp</groupId>
    <artifactId>sentinel-datasource-nacos</artifactId>
</dependency>
<!--SpringCloud ailibaba sentinel -->
<dependency>
    <groupId>com.alibaba.cloud</groupId>
    <artifactId>spring-cloud-starter-alibaba-sentinel</artifactId>
</dependency>
```

添加`nacos依赖`

```xml
<!--SpringCloud ailibaba nacos -->
<dependency>
    <groupId>com.alibaba.cloud</groupId>
    <artifactId>spring-cloud-starter-alibaba-nacos-discovery</artifactId>
</dependency>
```

完整依赖：

```xml
<dependencies>
    <!--SpringCloud ailibaba nacos -->
    <dependency>
        <groupId>com.alibaba.cloud</groupId>
        <artifactId>spring-cloud-starter-alibaba-nacos-discovery</artifactId>
    </dependency>
    <!--SpringCloud ailibaba sentinel-datasource-nacos 后续做持久化用到-->
    <dependency>
        <groupId>com.alibaba.csp</groupId>
        <artifactId>sentinel-datasource-nacos</artifactId>
    </dependency>
    <!--SpringCloud ailibaba sentinel -->
    <dependency>
        <groupId>com.alibaba.cloud</groupId>
        <artifactId>spring-cloud-starter-alibaba-sentinel</artifactId>
    </dependency>
    <!--openfeign-->
    <dependency>
        <groupId>org.springframework.cloud</groupId>
        <artifactId>spring-cloud-starter-openfeign</artifactId>
    </dependency>
    <!-- SpringBoot整合Web组件+actuator -->
    <dependency>
        <groupId>org.springframework.boot</groupId>
        <artifactId>spring-boot-starter-web</artifactId>
    </dependency>
    <dependency>
        <groupId>org.springframework.boot</groupId>
        <artifactId>spring-boot-starter-actuator</artifactId>
    </dependency>
    <!--日常通用jar包配置-->
    <dependency>
        <groupId>org.springframework.boot</groupId>
        <artifactId>spring-boot-devtools</artifactId>
        <scope>runtime</scope>
        <optional>true</optional>
    </dependency>
    <dependency>
        <groupId>cn.hutool</groupId>
        <artifactId>hutool-all</artifactId>
        <version>4.6.3</version>
    </dependency>
    <dependency>
        <groupId>org.projectlombok</groupId>
        <artifactId>lombok</artifactId>
        <optional>true</optional>
    </dependency>
    <dependency>
        <groupId>org.springframework.boot</groupId>
        <artifactId>spring-boot-starter-test</artifactId>
        <scope>test</scope>
    </dependency>

</dependencies>
```



**3. yml配置**

```yaml
server:
  port: 8401

spring:
  application:
    name: cloudalibaba-sentinel-service
  cloud:
    nacos:
      discovery:
        #nacos服务地址
        server-addr: localhost:8848
    sentinel:
      transport:
        #sentinel dashboard地址
        dashboard: localhost:8080
        #默认端口8719，如果被占用，自动从8719开始+1依次扫描，直至找到未占用的端口
        port: 8719
management:
  endpoints:
    web:
      exposure:
        include: "*"

```



**4. 主启动类**

```java
@SpringBootApplication
@EnableDiscoveryClient
public class SentinelServiceMain8401 {

    public static void main(String[] args) {
        SpringApplication.run(SentinelServiceMain8401.class, args);
    }
}
```



**5. 业务类接口**

```java
@RestController
public class FlowLimitController {

    @GetMapping("/testA")
    public String testA() {
        return "-----testA";
    }

    @GetMapping("/testB")
    public String testB() {
        return "-----testB";
    }
}
```



**6. 测试**

- 启动nacos
- 启动sentinel
- 启动cloudalibaba-sentinel-service8401服务

查看nacos控制台，微服务注册成功

![image-20211029125056236](https://cdn.jsdelivr.net/gh/le1024/image1/le/image-20211029125056236.png)

查看sentinel控制台，初始为空的界面，<strong style="color:red">sentinel采用懒加载</strong>

![image-20211029125130275](https://cdn.jsdelivr.net/gh/le1024/image1/le/image-20211029125130275.png)

进行一次接口请求，http://localhost:8401/testA

![image-20211029125206207](https://cdn.jsdelivr.net/gh/le1024/image1/le/image-20211029125206207.png)

再次刷新sentinel控制台

![image-20211029125254139](https://cdn.jsdelivr.net/gh/le1024/image1/le/image-20211029125254139.png)

可以多次刷新接口，点开实时监控菜单，可以实时看到我们请求的变化

![image-20211029125418054](https://cdn.jsdelivr.net/gh/le1024/image1/le/image-20211029125418054.png)

两个接口都请求

![image-20211029125525532](https://cdn.jsdelivr.net/gh/le1024/image1/le/image-20211029125525532.png)





**可以得出结论：sentinel完成了对微服务8401的监控**