> Seata下载与安装

下载地址：

https://github.com/seata/seata/releases



下载完成后，解压至指定目录，版本可随意下载，稳定的版本就行

![](https://cdn.jsdelivr.net/gh/le1024/image1/le/image-20211104215039312.png)



<strong style="color:rgb(60,179,113)">修改file.conf文件</strong>

****

文件在conf路径下

1.备份原始`file.conf.example`文件，修改配置不在源文件上修改，防止配错方便恢复文件。将`file.conf.example`复制一份名为`file.conf`的文件，默认有的`file.conf`可以不需要。（当前版本seata-1.1.0，实际根据版本来看配置文件）

2.自定义事务组名称

打开`file.conf`文件，在32行的位置，找到`service`模块
![image-20211104220540656](https://cdn.jsdelivr.net/gh/le1024/image1/le/image-20211104220540656.png)

更改`vgroup_mapping.my_test_tx_group`属性值，该值随意，建议格式为`xxx_tx_group`

```yaml
vgroup_mapping.my_test_tx_group = "hll_tx_group"
```

3.事务日志存储模块和数据库连接信息

找到`store`模块

![image-20211104220905940](https://cdn.jsdelivr.net/gh/le1024/image1/le/image-20211104220905940.png)

更改`mode`属性值，将其配置为`db`，即数据库模式

```yaml
mode = "db"
```

更改`db`模块，配置数据库连接，后面需要建立好数据库

```yaml
  db {
    ## the implement of javax.sql.DataSource, such as DruidDataSource(druid)/BasicDataSource(dbcp) etc.
    datasource = "dbcp"
    ## mysql/oracle/h2/oceanbase etc.
    dbType = "mysql"
    driverClassName = "com.mysql.jdbc.Driver"
    url = "jdbc:mysql://127.0.0.1:3306/seata"
    user = "root"
    password = "root"
    minConn = 1
    maxConn = 10
    globalTable = "global_table"
    branchTable = "branch_table"
    lockTable = "lock_table"
    queryLimit = 100
  }
```



<strong style="color:rgb(60,179,113)">创建数据库seata</strong>

****

1.下载建库脚本，低版本的脚本直接就在`conf`路径下就有了，高版本的需要单独去下载，本次为1.1.0版本

https://github.com/seata/seata/blob/1.1.0/script/server/db/mysql.sql

访问之后，可以选择不同的分支去下载复制，跟版本对应，sql语句这里就不贴了

![image-20211104221849250](https://cdn.jsdelivr.net/gh/le1024/image1/le/image-20211104221849250.png)

复制建表语句运行，如下：共三张表 	`branch_table`、`global_table`、`lock_table`

<img src="https://cdn.jsdelivr.net/gh/le1024/image1/le/image-20211104222031173.png" alt="image-20211104222031173" style="zoom:50%;float:left;" />



<strong style="color:rgb(60,179,113)">修改registry.conf</strong>

****

文件在`conf`路径

1.复制一份源文件，修改在新文件操作

2.该文件是配置注册中心的，支持多种注册中心，打开文件可以看到

`file 、nacos 、eureka、redis、zk、consul、etcd3、sofa`

这里主要以`nacos`为主，配置`nacos`就行

![image-20211104222647050](https://cdn.jsdelivr.net/gh/le1024/image1/le/image-20211104222647050.png)



<strong style="color:rgb(60,179,113)">启动Nacos和Seata</strong>

****

先启动Nacos

```bash
startup.cmd -m standalone
```

再启动Seata，进入到bin目录

```bash
seata-server.bat
```

![image-20211104223821673](https://cdn.jsdelivr.net/gh/le1024/image1/le/image-20211104223821673.png)

这样的就是成功了