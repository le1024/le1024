> keepalived.conf详解

```javascript
#全局配置
global_defs {
	notification_email { #邮件报警
		acassen@firewall.loc #设置报警邮件地址，可以设置多个，每行一个。
		failover@firewall.loc #需开启本机的sendmail服务
		sysadmin@firewall.loc
	}
	notification_email_from	Alexandre.Cassen@firewall.loc #设置邮件的发送地址
	smtp_server 192.168.200.1 #设置SMTP server地址
	smtp_connect_timeout 30 #设置连接SMTP server的超时时间
	router_id LVS_DEVEL #此处注意router_id为负载均衡标识，在局域网内应该是唯一的。
}

#检测脚本配置,检测nginx是否还活着
vrrp_script chk_http_port {
	script "/usr/local/src/nginx_check.sh" #脚本位置
	interval 2 #检测脚本执行的间隔	
	weight 2 #权重
}

#虚拟ip配置
vrrp_instance VI_1 {
	state MASTER #主服务用MASTER  备份的用BACKUP
	interface ens33 #网卡, ifconfig命令可以查看
	virtual_router_id 51 #主备机的virtual_router_id必须相同
	priority 100 #主备机的优先级，主机值较大，备机值较小
	advert_int 1 #MASTER 与BACKUP 负载均衡器之间同步检查的时间间隔，单位为秒。
	authentication { #设置验证类型和密码
		auth_type PASS #设置验证类型，只要有PASS和AH
		auth_pass 1111 #设置验证密码。在同一个vrrp_instance下，MASTER和BACKUP必须使用相同的密码才能正常通信
	}
	virtual_ipaddress {
		192.168.171.130 #虚拟ip地址,可以有多个地址，每个地址占一行
	}
} 
```



注意：

```bash
route_id  XXX #MASTER和BACKUP不同

virtual_router_id 51 #同一个实例下，MASTER和BACKUP相同

priority 100 #优先级，同一个实例下，MASTER高于BACKUP
```

