> upstream配置说明

```properties
upstream myserver {
	xxxxx
}
```

四种配置方式

#### 1.weight

权重方式

默认方式加权轮询，weight不指定时，各服务器weight相同，每个请求按时间顺序依次分配到不同的服务器。

配置weight时，weight和访问比率成正比。如果后端服务down掉，会自动剔除。

```properties
upstream myserver {
	server 192.168.1.110:8080 weight=1;
    server 192.168.1.111:8080 weight=2;
}
```



#### 2.ip_hash

每个请求按访问ip的hash结果分配，这样每个访客固定访问一个后端服务器，可以解决session不能跨服务器的问题。如果后端服务器down掉，需要手动down掉。

```properties
upstream myserver {
	ip_hash;
	server 192.168.1.110:8080;
    server 192.168.1.111:8080;
}
```



#### 3.fair

按后端服务器的响应时间来分配请求，响应时间短的优先分配

```properties
upstream myserver {
	server 192.168.1.110:8080;
    server 192.168.1.111:8080;
    fair;
}
```



#### 4.url_hash

按访问url的hash结果来分配请求，使每个url定向到同一个后端服务器，后端服务器为缓存服务器时比较有效。

upstream加入hash，hash_method是使用的hash算法

```properties
upstream myserver {
	server 192.168.1.110:8080;
    server 192.168.1.111:8080;
    hash $request_uri;
    hash_method crc32;
}
```



**负载均衡upstream配置实例说明**

```properties
upstream myserver {
	ip_hash;
	server 192.168.1.110:8080;
    server 192.168.1.111:8080 weight=1;
    server 192.168.1.112:8080 weight=1 down;
    server 192.168.1.113:8080 weight=1 backup;
    server 192.168.1.114:8080 weight=1 max_fails=3 fail_timeout=30s;
}
```

- down：表示当前的sever不参与负载
- weight：默认1.weight越大，负载的权重就越大
- backup：其他所有非backup机器down或者忙时，请求backup机器。所以这台机器的压力最轻。
- max_fails：设置最大失败次数，最大进行3次尝试。默认1
- fail_timeout：超时时间，单位秒。默认10s

注意：

当upstream中只有一个sever时，max_fails 和 fail_timeout 参数可能不会起作用

weight\backup和ip_hash不能一起使用