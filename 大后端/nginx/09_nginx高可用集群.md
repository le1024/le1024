> nginx-高可用

#### 1.nginx高可用

单机nginx请求分发到多tomcat，如果nginx宕机会导致整体服务不可用

![image-20210928172315882](https://cdn.jsdelivr.net/gh/le1024/image1/le/image-20210928172315882.png)

所以为了避免出现这种问题，将nginx配置多个，实现高可用

![image-20210929161332191](https://cdn.jsdelivr.net/gh/le1024/image1/le/image-20210929161332191.png)

为了实现这种效果，需要**`keepalived`**，通过`keepalived`来监听到哪个nginx服务器宕机了，通过`keepalived`设置一个虚拟ip是访问nginx，当主服务的nginx宕机，`keepalived`会将虚拟ip绑定到备份服务器的nginx，保证服务可用。



#### 2.准备工作

**（1）需要装备两台服务器**

**（2）安装nginx**

**（3）安装keepalived**

下载地址：https://www.keepalived.org/download.html

```bash
tar -zxvf keepalived-2.2.4.tar.gz
```

执行命令：

```bash
./configure --prefix=/usr/local/keepalived
```

![image-20210929140133671](https://cdn.jsdelivr.net/gh/le1024/image1/le/image-20210929140133671.png)

根据报错提示安装依赖，如果没有报错可忽略：

```bash
yum -y install libnl libnl-devel
```

编译：

```bash
make && make install
```

完成之后，`keepalived`就会安装在`/usr/local/keepalived`目录下

配置文件`keepalived.conf`在`/usr/local/keepalived/etc/keepalived/`目录下



**（4）主机nginx服务器配置**

编辑`keepalived.conf`

```
vim /usr/local/keepalived/etc/keepalived/keepalived.conf
```

里面的配置，根据实际的服务器信息写

```properties
global_defs {
	notification_email {
		acassen@firewall.loc
		failover@firewall.loc
		sysadmin@firewall.loc
	}
	notification_email_from	Alexandre.Cassen@firewall.loc
	smtp_server 192.168.200.1
	smtp_connect_timeout 30
	router_id LVS_DEVEL
}

#检测脚本配置,检测nginx是否还活着
vrrp_script chk_http_port {
	script "/usr/local/src/nginx_check.sh" #脚本位置
	
	interval 2
	
	weight 2
}

vrrp_instance VI_1 {
	state MASTER #主服务用MASTER  备份的用BACKUP
	interface ens33 #网卡, ifconfig命令可以查看
	virtual_router_id 51 #主备机的virtual_router_id必须相同
	priority 100 #主备机的优先级，主机值较大，备机值较小
	advert_int 1
	authentication {
		auth_type PASS
		auth_pass 1111
	}
	virtual_ipaddress {
		192.168.171.130 #虚拟ip地址，自己配置
	}
} 
```

编辑检测脚本`nginx_chekc.sh`，并放入指定位置：`/usr/local/src`

```properties
#!/bin/bash
A=`ps -C nginx -no-header | wc -l`
if [ $A -eq 0 ];then
	/usr/local/nginx/sbin/nginx
	sleep 2
	if [ $A -eq 0 ];then
		killall keepalived
	fi
fi
```



**（5）备机nginx服务器配置**

编辑`keepalived.conf`

```
vim /usr/local/keepalived/etc/keepalived/keepalived.conf
```

里面的配置，根据实际的服务器信息写

```properties
global_defs {
	notification_email {
		acassen@firewall.loc
	}
	notification_email_from	Alexandre.Cassen@firewall.loc
	smtp_server 192.168.200.1
	smtp_connect_timeout 30
	router_id LVS_DEVEL
}

#检测脚本配置,检测nginx是否还活着
vrrp_script chk_http_port {
	script "/usr/local/src/nginx_check.sh" #脚本位置
	
	interval 2
	
	weight 2
}

vrrp_instance VI_1 {
	state BACKUP #主服务用MASTER  备份的用BACKUP
	interface ens33 #网卡, ifconfig命令可以查看
	virtual_router_id 51 #主备机的virtual_router_id必须相同
	priority 90 #主备机的优先级，主机值较大，备机值较小
	advert_int 1
	authentication {
		auth_type PASS
		auth_pass 1111
	}
	virtual_ipaddress {
		192.168.171.130 #虚拟ip地址，自己配置
	}
} 
```

编辑检测脚本`nginx_chekc.sh`，并放入指定位置：`/usr/local/src`

```properties
#!/bin/bash
A=`ps -C nginx -no-header | wc -l`
if [ $A -eq 0 ];then
	/usr/local/nginx/sbin/nginx
	sleep 2
	if [ $A -eq 0 ];then
		killall keepalived
	fi
fi
```



**（6）启动keepalived**

```bash
systemctl start keepalived
```

启动会报错，`keepalived默认会读取/etc/keepalived/keepalived.conf配置文件`，将配置文件位置更改下

etc目录下新建keepalived文件夹

```bash
cd /etc
mkdir keepalived

cp /usr/local/keepalived/etc/keepalived/keepalived.conf /etc/keepalived/
```

重新启动keepalived

或者在安装的时候，直接安装到etc下

```bash
./configure --prefix=/etc/keepalived
```

查看启动状态：

```bash
ps -ef|grep keepalived

或者

systemctl status keepalived
```



#### 3.测试

**（1）两台服务器的nginx和keepalived都正常开启**

浏览器输入`keepalived.conf`配置的虚拟ip：`192.168.171.130`

![image-20210929160246802](https://cdn.jsdelivr.net/gh/le1024/image1/le/image-20210929160246802.png)

输入`ip addr`：可以在网卡里看到虚拟ip

![image-20210929160426006](https://cdn.jsdelivr.net/gh/le1024/image1/le/image-20210929160426006.png)



**（2）停止主服务器的nginx和keepalived**

停止keepalived

```bash
systemctl stop keepalived
```

停止nginx

```bash
cd /usr/local/nginx/sbin

./nginx -s stop
```

然后再去访问：`192.168.171.130`，也是可以正常访问的
