**nginx上传请求413问题**

****

编辑`nginx.conf`，添加

```properties
client_max_body_size 20M;
```

可设置到`http`、`server`、`location`中：

设置到http{}内，控制全局nginx所有请求报文大小
设置到server{}内，控制该server的所有请求报文大小
设置到location{}内，控制满足该路由规则的请求报文大小



springboot还需要设置：

```yaml
spring:
  servlet:
    multipart:
      max-file-size: 20MB
      max-request-size: 20MB
```

