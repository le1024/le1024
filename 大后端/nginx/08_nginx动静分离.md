> nginx-动静分离

#### 1.什么是动静分离

简单来说，就是把动态资源和静态资源请求分开，不能单纯的理解为把动态页面和静态页面物理分离。

可以理解成，nginx处理静态页面，tomcat处理动态页面。

动静分离实现的方式大致可分为两种：

- 纯粹把静态资源放在独立的服务器上
- 动态和静态一起发布，通过nginx区分开



#### 2.准备工作

##### 2.1上传测试文件

服务器新建文件地址：`/home/data/image`和`/home/data/html`，路径随意建立

分别上传一张图片和一个静态HTML



##### 2.2修改配置文件

```bash
vim /usr/local/nginx/conf/nginx.conf
```

```properties
location /html/  {
    root /home/data/;
	index index.html index.htm;
}

location /image/ {
    root /home/data/;
    autoindex on;
}
```

**root**配置的含义是，路径结果为root路径+location路径，比如：`/home/data/html/`

**autoindex**：以文件目录的形式浏览



##### 2.3 测试

访问图片前，先看下`autoindex`的效果如下：

![image-20210928165942935](https://cdn.jsdelivr.net/gh/le1024/image1/le/image-20210928165942935.png)



访问图片：http://106.13.187.36/image/a.png

![image-20210928170044524](https://cdn.jsdelivr.net/gh/le1024/image1/le/image-20210928170044524.png)

访问HTML：http://106.13.187.36/html/a.html

![image-20210928170032576](https://cdn.jsdelivr.net/gh/le1024/image1/le/image-20210928170032576.png)

