> nginx基本操作命令

使用nginx命令的前提条件，需要进入到nginx的目录

`/usr/local/nginx/sbin`



#### 查看nginx版本

```bash
./nginx -v
```



#### 启动nginx

```bash
./nginx
```



#### 停止nginx

```shell
./nginx -s stop
```



#### 重加载nginx

修改`nginx.conf`之后，重加载nginx，使修改生效

```bash
./nginx -s reload
```

