> nginx安装

http://nginx.org/en/download.html



上传安装包到服务器。



- 解压

  ```bash
  tar -zxvf nginx-1.20.1.tar.gz
  ```

  ![image-20210923215555229](https://fastly.jsdelivr.net/gh/le1024/image1/le/image-20210923215555229.png)



- 编译安装

  ```bash
  ./configure
  
  ## 如果后期考虑配置https的话,请用这个编译方式
  ./configure --prefix=/usr/local/nginx --with-http_stub_status_module --with-http_ssl_module
  ```
```bash
make && make install
```

如果`make`报错的话：

  **make:  No rule to make target `build', needed by `default'. Stop.**

安装一下依赖库：

```bash
  yum install -y gcc pcre pcre-devel openssl openssl-devel gd gd-devel
```

如果没有网络环境，离线安装：

[Centos离线安装gcc环境](https://blog.csdn.net/sinat_33151213/article/details/125079385)

 然后重新`./configure`，`make && make install`

  安装成功后：

  ![image-20210923220319240](https://fastly.jsdelivr.net/gh/le1024/image1/le/image-20210923220319240.png)



- 启动

  ```bash
  cd /usr/local/nginx
  ```

  ![image-20210923220504940](https://fastly.jsdelivr.net/gh/le1024/image1/le/image-20210923220504940.png)

  ```bash
  cd sbin
  
  #这个就是启动命令
  ./nginx
  ```

  ![image-20210923220548678](https://fastly.jsdelivr.net/gh/le1024/image1/le/image-20210923220548678.png)

  默认是80端口，确认端口已开启：

  ```bash
  #端口未开启的话，可使用下面的命令
  firewall-cmd --zone=public --add-port=80/tcp --permanent
  #使端口开启生效
  firewall-cmd --reload
  
  #或者直接关闭防火墙
  systemctl stop firewalld
  ```
  
  
  
  ![image-20210923220826890](https://fastly.jsdelivr.net/gh/le1024/image1/le/image-20210923220826890.png)

