> nginx配置文件

`/usr/local/nginx/conf/nginx.conf`



### nginx.conf组成

**nginx.conf配置文件可分为三部分**：

 

#### 1.全局块

从配置文件开始到events模块之间的内容，主要会设置一些影响nginx服务器整体运行的配置指令

```properties
#user  nobody;
worker_processes  1; #处理nginx的并发数量值，该值越大处理并发数量越多，受服务器配置影响

#error_log  logs/error.log;
#error_log  logs/error.log  notice;
#error_log  logs/error.log  info;

#pid        logs/nginx.pid;
```





#### 2.events模块

events模块涉及的指令主要影响nginx服务器与用户的网络连接

```properties
events {
    worker_connections  1024; #表示nginx支持的最大连接数
}
```





#### 3.http块

nginx服务器配置最频繁的部分，代理、缓存和日志定义等绝大多数功能和第三方模块配置都在这里

http块也分为：`http全局块`，`server块`

##### 3.1http全局块

http全局块配置的指令包括文件引入，MIME-TYPE定义、日志定义、连接超时时间、单链接请求数上限等

```properties
    include       mime.types;
    default_type  application/octet-stream;

    #log_format  main  '$remote_addr - $remote_user [$time_local] "$request" '
    #                  '$status $body_bytes_sent "$http_referer" '
    #                  '"$http_user_agent" "$http_x_forwarded_for"';

    #access_log  logs/access.log  main;

    sendfile        on;
    #tcp_nopush     on;

    #keepalive_timeout  0;
    keepalive_timeout  65;
```



##### 3.2server块

```properties
    server {
        listen       80;
        server_name  localhost;

        #charset koi8-r;

        #access_log  logs/host.access.log  main;

        location / {
            root   html;
            index  index.html index.htm;
        }

        #error_page  404              /404.html;

        # redirect server error pages to the static page /50x.html
        #
        error_page   500 502 503 504  /50x.html;
        location = /50x.html {
            root   html;
        }

        # proxy the PHP scripts to Apache listening on 127.0.0.1:80
        #
        #location ~ \.php$ {
        #    proxy_pass   http://127.0.0.1;
        #}

        # pass the PHP scripts to FastCGI server listening on 127.0.0.1:9000
        #
        #location ~ \.php$ {
        #    root           html;
        #    fastcgi_pass   127.0.0.1:9000;
        #    fastcgi_index  index.php;
        #    fastcgi_param  SCRIPT_FILENAME  /scripts$fastcgi_script_name;
        #    include        fastcgi_params;
        #}

        # deny access to .htaccess files, if Apache's document root
        # concurs with nginx's one
        #
        #location ~ /\.ht {
        #    deny  all;
        #}
    }
```

