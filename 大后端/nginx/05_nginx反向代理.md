> nginx-反向代理

### 实例一：通过服务器80端口去访问8080端口



编辑`nginx.conf`文件

```bash
cd /usr/local/nginx/conf

vim nginx.conf
```

在`server`模块中：

```properties
# 修改为本机ip
server_name 106.13.187.36

location / {
	root html;
	# 添加proxy_pass
	proxy_pass http://106.13.187.36:8080;
	index index.html idnex.htm;
}
```

保存，退出



重新加载配置文件

```bash
./nginx -s reload
```



可以成功放到到8080端口的tomcat服务

![image-20210926202901965](https://cdn.jsdelivr.net/gh/le1024/image1/le/image-20210926202901965.png)





### 实例二：通过不同路径去访问不同tomcat

默认请求用`80`端口，可以改成别的，修改`listen`即可

访问http://127.0.0.1/a/ 跳转到http://127.0.0.1:8080

访问http://127.0.0.1/b/ 跳转到http://127.0.0.1:8081

需要先准备两个tomcat，8080和8081



编辑`nginx.conf`文件

将`location`配置通过正则表达式去匹配访问路径

```properties
location ~ /a/ {
	proxy_pass http://106.13.187.36:8080;
}

location ~ /b/ {
	proxy_pass http://106.13.187.36:8081;
}
```

保存配置，重加载配置文件:

```bash
./nginx -s reload
```



访问：

![image-20210926205452464](https://cdn.jsdelivr.net/gh/le1024/image1/le/image-20210926205452464.png)

![image-20210926205507385](https://cdn.jsdelivr.net/gh/le1024/image1/le/image-20210926205507385.png)



### location指令说明

```properties
location [ = | ~ | ~* | ^~] uri {

}
```

1. `= `：用于不含正则表达式的uri前，需要请求字符串与uri严格匹配
2. `~ `：用于表示uri包含正则表达式，区分大小写
3. `~*` ：用于表示uri包含正则表达式，不区分大小写
4. `^~ `：表示普通字符匹配，如果该选项匹配，只匹配该选项，不匹配别的选项，一般用来匹配目录
5. `/`：通用匹配



**location 匹配的优先级(与location在配置文件中的顺序无关)**

`=`精确匹配会第一个被处理。

`^~`普通匹配。按照最长匹配原则找到最满足的匹配项。

正则匹配。

