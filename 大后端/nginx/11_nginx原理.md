> nginx原理

#### 1.master和worker

nginx启动之后，会有一个主进程master process和一系列工作进程worker process

master接收信号后将任务分配给worker进行执行，worker可有多个

![image-20210929170249567](https://cdn.jsdelivr.net/gh/le1024/image1/le/image-20210929170249567.png)

![image-20210929172000317](https://cdn.jsdelivr.net/gh/le1024/image1/le/image-20210929172000317.png)



#### 2.worker工作方式

客户端发送一个请求给master之后，worker获取任务的机制不是直接分配也不是轮询方式，而是一种争抢的机制，“抢”到任务后再执行任务。

![image-20210929173704164](https://cdn.jsdelivr.net/gh/le1024/image1/le/image-20210929173704164.png)



#### 3.机制优点

- 可以使用 nginx –s reload 热部署，利用 nginx 进行热部署操作

- 每个 woker 是独立的进程，若其中一个woker出现问题，其他继续进行争抢，实现请求过程，不会造成服务中断



#### 4.worker数量的设置

nginx同redis一样，采用io多路复用机制，每个worker都是一个独立的进程，每个进程里只有一个主线程，通过异步非阻塞的方式来处理请求。

每个worker进程可以把一个CPU的性能发挥到极致，所以`worker的数量和服务器的CPU数相等是最为适宜的`，设少了会浪费性能，设多了会造成CPU频繁切换带来的损耗。

在`nginx.conf`配置

```bash
worker_processes 2; #根据服务器配置
```





#### 5.连接数worker_connection

Q1：发送请求，占用了worker的几个连接数

```html
2或者4个

情况1：访问静态资源，发起一个请求，有一个连接，当请求成功返回资源又会产生一个连接数，总共2个
情况2：当连上tomcat去访问数据库后，有一个连接，tomcat返回资源又会产生一个连接，加上情况1，共4个
```



Q2：一个nginx能建立的最大连接数

```javascript
最大连接数：
worker_connections * worker_process

最大并发数：
worker_connections * worker_process

静态访问最大并发数：
worker_connections * worker_process / 2

作为反向代理，最大并发数：
worker_connections * worker_process / 4
```

浏览器的每次访问要占两个连接

作为反向代理服务器，每个并发会建立与客户端的连接与后端服务的连接，会占用两个连接

