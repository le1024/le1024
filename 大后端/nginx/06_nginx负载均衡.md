> nginx-负载均衡

准备两个tomcat8080，tomcat8081，两个tomcat服务下都有/a/a.html文件

访问http://127.0.0.1/a/a.html，请求平均到8080、8081服务



默认请求用`80`端口，可以改成别的，修改`listen`即可



修改nginx配置，`nginx.conf`

通过**`upstream`**配置实现，添加在`http`模块下

```bash
vim /usr/local/nginx/conf/nginx.conf
```

```properties
#myserver是自定义的服务名称
upstream myserver {
	#将两个tomcat的服务配好
	server 106.13.187.36:8080;
	server 106.13.187.36:8081;
}
```

```properties
server {
        listen       80;
        server_name  106.13.187.36;

        #charset koi8-r;

        #access_log  logs/host.access.log  main;

        location / {
        		#proxy_pass配置上upstream定义的服务名称
                proxy_pass http://myserver;
                root html;
                index index.html index.htm;
        }
//后面的配置省略        
.....
```



访问http://106.13.187.36/a/a.html，会以轮询的方式在浏览器显示a.html的内容



------

**问题1：**访问一直都是第一个配置的服务内容，没有出现轮询的效果

浏览器会自动发送 http://xxxxx/favicon.ico 请求，占一个名额，只需要在nginx里配置 location /favicon.ico { return 200; } 就好了

```properties
location /favicon.ico {
	return 200;
}
```

![image-20210927165252973](https://cdn.jsdelivr.net/gh/le1024/image1/le/image-20210927165252973.png)

