#!/bin/bash
A=`ps -C nginx -no-header | wc -l`
if [ $A -eq 0 ];then
	/usr/local/nginx/sbin/nginx
	sleep 2
	if [ $A -eq 0 ];then
		killall keepalived
	fi
fi
