> Gateway常用的Route Predicate介绍

启动网关服务之后，在控制台可以看到如下信息：

![image-20211014232034862](https://cdn.jsdelivr.net/gh/le1024/image1/le/image-20211014232034862.png)



https://cloud.spring.io/spring-cloud-static/spring-cloud-gateway/2.2.1.RELEASE/reference/html/#gateway-request-predicates-factories



#### Route Predicate Factories是什么

Spring Cloud Gateway将路由匹配作为Spring WebFlux HandlerMapping基础架构的一部分。
Spring Cloud Gateway包括许多内置的Route Predicate工厂。所有这些Predicate都与HTTP请求的不同属性匹配。多个Route Predicate工厂可以进行组合

Spring Cloud Gateway 创建 Route 对象时， 使用 RoutePredicateFactory 创建 Predicate 对象，Predicate 对象可以赋值给 Route。 Spring Cloud Gateway 包含许多内置的Route Predicate Factories。

所有这些谓词都匹配HTTP请求的不同属性。多种谓词工厂可以组合，并通过逻辑and。



#### 常用的Route Predicate

![image-20211014232531470](https://cdn.jsdelivr.net/gh/le1024/image1/le/image-20211014232531470.png)

**这个可以理解为，SQL查询的where条件，多个条件用and连接，只有条件都匹配了才会去进行路由**



##### After Route Predicate

匹配指定日期时间之后发生的请求

```properties
....
- id: payment_route2         #路由id，没有固定规则但要求唯一，建议配合服务名
	#uri: http://localhost:8001 #匹配后提供服务的路由地址
	uri: lb://cloud-payment-service #路由地址配置成微服务名称
		predicates:
			- Path=/payment/lb/**    #断言，路径相匹配的进行路由
			- After=2021-10-15T09:23:03.685+08:00[Asia/Shanghai]
```

网关访问请求http://gateway:port/payment/lb，需要在时间`2021-10-15 09.24.04.685`之后才能访问成功



##### Before Route Predicate

匹配指定日期时间之前发生的请求

```properties
....
- id: payment_route2         #路由id，没有固定规则但要求唯一，建议配合服务名
	#uri: http://localhost:8001 #匹配后提供服务的路由地址
	uri: lb://cloud-payment-service #路由地址配置成微服务名称
		predicates:
			- Path=/payment/lb/**    #断言，路径相匹配的进行路由
			- Before=2021-10-16T09:23:03.685+08:00[Asia/Shanghai]
```

网关访问请求http://gateway:port/payment/lb，需要在时间`2021-10-16 09.24.04.685`之前才能访问成功



##### Between Route Predicate

匹配指定两个日期时间之间的请求

```properties
....
- id: payment_route2         #路由id，没有固定规则但要求唯一，建议配合服务名
	#uri: http://localhost:8001 #匹配后提供服务的路由地址
	uri: lb://cloud-payment-service #路由地址配置成微服务名称
		predicates:
			- Path=/payment/lb/**    #断言，路径相匹配的进行路由
			- Between=2021-10-15T09:23:03.685+08:00[Asia/Shanghai],2021-10-16T09:23:03.685+08:00[Asia/Shanghai]
```

网关访问请求http://gateway:port/payment/lb，需要在时间`2021-10-15 09.24.04.685`和`2021-10-16 09.24.04.685`之间才能访问成功



##### Cookie Route Predicate

Cookie Route Predicate需要两个参数，一个是 Cookie name ,一个是正则表达式。
路由规则会通过获取对应的 Cookie name 值和正则表达式去匹配，如果匹配上就会执行路由，如果没有匹配上则不执行

```properties
....
- id: payment_route2         #路由id，没有固定规则但要求唯一，建议配合服务名
	#uri: http://localhost:8001 #匹配后提供服务的路由地址
	uri: lb://cloud-payment-service #路由地址配置成微服务名称
		predicates:
			- Path=/payment/lb/**    #断言，路径相匹配的进行路由
			- Cookie=username,hll
```

```bash
curl http://localhost:9527/payment/lb --cookie "username=hll"
```

携带cookie才能访问成功



##### Header Route Predicate

需要两个参数：一个是属性名称和一个正则表达式

与具有给定名称的标头匹配，其值与正则表达式匹配

```properties
....
- id: payment_route2         #路由id，没有固定规则但要求唯一，建议配合服务名
	#uri: http://localhost:8001 #匹配后提供服务的路由地址
	uri: lb://cloud-payment-service #路由地址配置成微服务名称
		predicates:
			- Path=/payment/lb/**    #断言，路径相匹配的进行路由
			#- Cookie=username,hll   #断言可以多个，为了测试方便，当前demo只用当前的测试
			- Header=X-Request-Id, \d+ #请求头要有X-Request-Id属性并且值为整数的正则表达式
```

```bash
curl http://localhost:9527/payment/lb -H "X-Request-Id:123"
```



##### Host Route Predicate

Host Route Predicate 接收一组参数，一组匹配的域名列表，这个模板是一个 ant 分隔的模板，用.号作为分隔符。
它通过参数中的主机地址作为匹配规则。

```properties
....
- id: payment_route2         #路由id，没有固定规则但要求唯一，建议配合服务名
	#uri: http://localhost:8001 #匹配后提供服务的路由地址
	uri: lb://cloud-payment-service #路由地址配置成微服务名称
		predicates:
			- Path=/payment/lb/**    #断言，路径相匹配的进行路由
			#- Cookie=username,hll   #断言可以多个，为了测试方便，当前demo只用当前的测试
			#- Header=X-Request-Id, \d+ #请求头要有X-Request-Id属性并且值为整数的正则表达式
			- Host=**.baidu.com
```

```bash
curl http://localhost:9588/paymentInfo -H "Host: news.baidu.com" 
```





##### Method Route Predicate

采用一个或多个参数：要匹配的 HTTP 方法

```properties
....
- id: payment_route2         #路由id，没有固定规则但要求唯一，建议配合服务名
	#uri: http://localhost:8001 #匹配后提供服务的路由地址
	uri: lb://cloud-payment-service #路由地址配置成微服务名称
		predicates:
			- Path=/payment/lb/**    #断言，路径相匹配的进行路由
			#- Cookie=username,hll   #断言可以多个，为了测试方便，当前demo只用当前的测试
			#- Header=X-Request-Id, \d+ #请求头要有X-Request-Id属性并且值为整数的正则表达式
			#- Host=**.baidu.com
			- Method=GET
```

```bash
✅curl http://localhost:9527/payment/lb
```

```bash
❎curl -X POST http://localhost:9527/payment/lb
```





##### Path Route Predicate

匹配请求路径，接口地址



##### Query Route Predicate

支持传入两个参数，一个是属性名，一个为属性值，属性值可以是正则表达式。

```properties
....
- id: payment_route2         #路由id，没有固定规则但要求唯一，建议配合服务名
	#uri: http://localhost:8001 #匹配后提供服务的路由地址
	uri: lb://cloud-payment-service #路由地址配置成微服务名称
		predicates:
			- Path=/payment/lb/**    #断言，路径相匹配的进行路由
			#- Cookie=username,hll   #断言可以多个，为了测试方便，当前demo只用当前的测试
			#- Header=X-Request-Id, \d+ #请求头要有X-Request-Id属性并且值为整数的正则表达式
			#- Host=**.baidu.com
			#- Method=GET
			- Query=username, \d+ # 要有参数名username并且值还要是整数才能路由
```

```bash
✅http://localhost:9527/payment/lb?username=31
 
❎http://localhost:9527/payment/lb?username=-31
```



