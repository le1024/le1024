> Gateway过滤器Filter

#### Gateway Filter是什么

路由过滤器允许以某种方式修改传入的HTTP请求或传出的HTTP响应。路由过滤器的范围是指定的路由。Spring Cloud Gateway 包括许多内置的 GatewayFilter 工厂。

https://cloud.spring.io/spring-cloud-static/spring-cloud-gateway/2.2.1.RELEASE/reference/html/#gatewayfilter-factories



#### 自定义全局GlobalFilter

需要实现两个接口：`GlobalFilter`，`Ordered`

```java
@Component
@Slf4j
public class MyGatewayFilter implements GlobalFilter, Ordered {

    @Override
    public Mono<Void> filter(ServerWebExchange exchange, GatewayFilterChain chain) {
        log.info("执行自定义全局过滤器");
        /**
         * 类似HttpServletRequest
         */
        String username = exchange.getRequest().getQueryParams().getFirst("username");
        if (username == null) {
            log.info("用户名为null，请求失败");
            exchange.getResponse().setStatusCode(HttpStatus.NOT_ACCEPTABLE);
            return exchange.getResponse().setComplete();
        }

        return chain.filter(exchange);
    }

    /**
     * 值越小越先执行
     * @return
     */
    @Override
    public int getOrder() {
        return 0;
    }
}
```

启动Eureka7001，payment8001/8002，gateway9527

访问：http://localhost:9527/payment/lb，无法访问成功

![image-20211015113152259](https://cdn.jsdelivr.net/gh/le1024/image1/le/image-20211015113152259.png)

请求地址传入参数进行访问：http://localhost:9527/payment/lb?username=hll

参数名需要和过滤器的匹配上

![image-20211015113228377](https://cdn.jsdelivr.net/gh/le1024/image1/le/image-20211015113228377.png)