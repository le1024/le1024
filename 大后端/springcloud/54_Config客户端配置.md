> Config客户端配置

**实现客户端3355访问服务端3344通过GitHub/Gitee获取配置信息**



#### 创建客户端服务

1. **创建工程**：`cloud-config-client-3355`

2. **pom**

   ```xml
   <dependencies>
           <dependency>
               <groupId>org.springframework.cloud</groupId>
               <artifactId>spring-cloud-starter-config</artifactId>
           </dependency>
           <dependency>
               <groupId>org.springframework.cloud</groupId>
               <artifactId>spring-cloud-starter-netflix-eureka-client</artifactId>
           </dependency>
           <dependency>
               <groupId>org.springframework.boot</groupId>
               <artifactId>spring-boot-starter-web</artifactId>
           </dependency>
           <dependency>
               <groupId>org.springframework.boot</groupId>
               <artifactId>spring-boot-starter-actuator</artifactId>
           </dependency>
   
           <dependency>
               <groupId>org.springframework.boot</groupId>
               <artifactId>spring-boot-devtools</artifactId>
               <scope>runtime</scope>
               <optional>true</optional>
           </dependency>
           <dependency>
               <groupId>org.projectlombok</groupId>
               <artifactId>lombok</artifactId>
               <optional>true</optional>
           </dependency>
           <dependency>
               <groupId>org.springframework.boot</groupId>
               <artifactId>spring-boot-starter-test</artifactId>
               <scope>test</scope>
           </dependency>
       </dependencies>
   ```

3. <strong style="color:red">bootstrap.yml</strong>

   `bootstrap.yml`和`application.yml`都是配置文件的，`application.yml`是用户级的资源配置项，`applicatin.yml`是系统级的，<font color="red">优先级更高</font>

   `Bootstrap`属性有高优先级，默认情况下，它们不会被本地配置覆盖。 `Bootstrap context`和`Application Context`有着不同的约定，所以新增了一个`bootstrap.yml`文件，保证`Bootstrap Context`和`Application Context`配置的分离。

   ```yaml
   server:
     port: 3355
   
   spring:
     application:
       name: cloud-config-client
     cloud:
     #config客户端配置
       config:
         label: master #分支
         name: config  #配置文件名称
         profile: dev  #读取后缀名称 label,name,profile综合：读取master分支上config-dev.yml的配置文件被读取http://config3344.com:3344/master/config-dev.yml
         uri: http://config3344.com:3344 #config服务端地址
   
   #注册到Eureka
   eureka:
     client:
       register-with-eureka: true
       fetch-registry: true
       service-url:
         defaultZone: http://eureka7001.com:7001/eureka/
   ```

4. **主启动类**

   ```java
   @SpringBootApplication
   @EnableEurekaClient
   public class ConfigClientMain3355 {
   
       public static void main(String[] args) {
           SpringApplication.run(ConfigClientMain3355.class, args);
       }
   }
   ```

5. **业务类，创建个接口**

   ```java
   @RestController
   public class ConfigClientController {
   
       //config.info这个是在gitee仓库配置文件有这么一个配置信息
       @Value("${config.info}")
       private String configInfo;
   
       @GetMapping("/configInfo")
       public String configInfo() {
           return configInfo;
       }
   }
   ```

#### 服务测试

启动服务：

![image-20211015172815472](https://cdn.jsdelivr.net/gh/le1024/image1/le/image-20211015172815472.png)

访问客户端服务接口：http://localhost:3355/configInfo

可以正确通过config服务端读取到gitee仓库的配置文件信息

![image-20211015172906695](https://cdn.jsdelivr.net/gh/le1024/image1/le/image-20211015172906695.png)



#### 动态刷新问题

当git仓库的文件发生变化时，config服务端重新访问就可立即响应，但是config客户端返回的依旧是旧的信息

比如：我们将`master/config-dev.yml`的里面`version`值改为2

![image-20211015173259668](https://cdn.jsdelivr.net/gh/le1024/image1/le/image-20211015173259668.png)

重新访问config服务端：

![image-20211015173348069](https://cdn.jsdelivr.net/gh/le1024/image1/le/image-20211015173348069.png)

重新访问config客户端：

![image-20211015173425101](https://cdn.jsdelivr.net/gh/le1024/image1/le/image-20211015173425101.png)

此时，**当我们把config客户端重启之后可以解决问题**，但当改动的频率越来越多，这样的操作是不现实的，

我们需要实现[config客户端动态刷新](./55_Config动态刷新)

