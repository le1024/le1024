> Eureka集群原理

![image-20211003210626087](https://cdn.jsdelivr.net/gh/le1024/image1/le/image-20211003210626087.png)

问题：微服务RPC远程服务调用最核心的是什么 
       高可用，试想你的注册中心只有一个only one， 它出故障了那就呵呵(￣▽￣)"了，会导致整个为服务环境不可用

​		解决办法：搭建Eureka注册中心集群 ，实现负载均衡+故障容错



Eureka集群之间的关系：

<strong style="color: rgb(255, 111, 22)">相互注册，相互守望</strong>

