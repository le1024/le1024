> actuator微服务信息完善

##### 主机名称：服务名称修改

注册中心的注册的服务默认是带有主机名称的

![image-20211004172256214](https://cdn.jsdelivr.net/gh/le1024/image1/le/image-20211004172256214.png)

修改8001/8002工程的yaml，让微服务不含有主机名称

```yaml
#eureka配置
eureka:
  client:
    #表示是否将自己注册进EurekaServer，默认true
    register-with-eureka: true
    #是否从EurekaServer抓取已有的注册信息，默认为true。单节点无所谓，集群必须设置为true才能配合ribbon使用负载均衡
    fetch-registry: true
    service-url:
      #defaultZone: http://localhost:7001/eureka
      #EurekaServer地址，集群配置，多个地址以英文逗号隔开
      defaultZone: http://eureka7001.com:7001/eureka,http://eureka7002.com:7002/eureka
  instance:
  	#自定义微服务主机名称
    instance-id: payment8001
```

重新访问：

![image-20211004172631626](https://cdn.jsdelivr.net/gh/le1024/image1/le/image-20211004172631626.png)



##### 访问信息带有ip信息提示

![image-20211004172752177](https://cdn.jsdelivr.net/gh/le1024/image1/le/image-20211004172752177.png)

修改8001/8002工程的yaml

```yaml
#eureka配置
eureka:
  client:
    #表示是否将自己注册进EurekaServer，默认true
    register-with-eureka: true
    #是否从EurekaServer抓取已有的注册信息，默认为true。单节点无所谓，集群必须设置为true才能配合ribbon使用负载均衡
    fetch-registry: true
    service-url:
      #defaultZone: http://localhost:7001/eureka
      #EurekaServer地址，集群配置，多个地址以英文逗号隔开
      defaultZone: http://eureka7001.com:7001/eureka,http://eureka7002.com:7002/eureka
  instance:
    #微服务主机名称
    instance-id: payment8001
    #访问路径可以显示IP地址
    prefer-ip-address: true
```

![image-20211004172931418](https://cdn.jsdelivr.net/gh/le1024/image1/le/image-20211004172931418.png)