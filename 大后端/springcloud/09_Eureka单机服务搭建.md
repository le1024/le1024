> Eureka单机服务搭建



#### EurekaServer端服务注册中心

基于父工程，右键新建`module`，选择`Maven`，创建消费者订单模块**`cloud-eureka-server7001`**

- **修改pom.xml**

  Eureka新老版本对比：

  ```xml
  以前的老版本（当前使用2018）
  <dependency>
          <groupId>org.springframework.cloud</groupId>
          <artifactId>spring-cloud-starter-eureka</artifactId>
  </dependency>
   
  现在新版本（当前使用2020.2）
  <dependency>
      <groupId>org.springframework.cloud</groupId>
      <artifactId>spring-cloud-starter-netflix-eureka-server</artifactId>
  </dependency>
  ```

  ````xml
  <?xml version="1.0" encoding="UTF-8"?>
  <project xmlns="http://maven.apache.org/POM/4.0.0"
           xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
           xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
      <parent>
          <artifactId>cloud</artifactId>
          <groupId>app.springcloud</groupId>
          <version>1.0-SNAPSHOT</version>
      </parent>
      <modelVersion>4.0.0</modelVersion>
  
      <artifactId>cloud-eureka-server7001</artifactId>
  
      <dependencies>
          <!--eureka-server-->
          <dependency>
              <groupId>org.springframework.cloud</groupId>
              <artifactId>spring-cloud-starter-netflix-eureka-server</artifactId>
          </dependency>
          <!-- 引入自己定义的api通用包，可以使用Payment支付Entity -->
          <dependency>
              <groupId>app.springcloud</groupId>
              <artifactId>cloud-api-commons</artifactId>
              <version>${project.version}</version>
          </dependency>
          <!--boot web actuator-->
          <dependency>
              <groupId>org.springframework.boot</groupId>
              <artifactId>spring-boot-starter-web</artifactId>
          </dependency>
          <dependency>
              <groupId>org.springframework.boot</groupId>
              <artifactId>spring-boot-starter-actuator</artifactId>
          </dependency>
          <!--一般通用配置-->
          <dependency>
              <groupId>org.springframework.boot</groupId>
              <artifactId>spring-boot-devtools</artifactId>
              <scope>runtime</scope>
              <optional>true</optional>
          </dependency>
          <dependency>
              <groupId>org.projectlombok</groupId>
              <artifactId>lombok</artifactId>
          </dependency>
          <dependency>
              <groupId>org.springframework.boot</groupId>
              <artifactId>spring-boot-starter-test</artifactId>
              <scope>test</scope>
          </dependency>
          <dependency>
              <groupId>junit</groupId>
              <artifactId>junit</artifactId>
          </dependency>
      </dependencies>
  
  </project>
  ````

- **编写application.yml**

  ```yaml
  server:
    port: 7001
  
  #eureka配置
  eureka:
    instance:
      hostname: localhost #eureka服务端的实例名称
    client:
      register-with-eureka: false #false表示不向注册中心注册自己，当前服务不需要注册，只关注业务服务就行
      fetch-registry: false #false表示自己就是注册中心，职责就是维护服务实例，并不需要去检索服务
      service-url:
        #设置与Eureka Server交互的地址查询服务和注册服务都需要依赖这个地址。
        defaultZone: http://${eureka.instance.hostname}:${server.port}/eureka/
  ```

- **主启动类**

  **`@EnableEurekaServer`**

  ```java
  @SpringBootApplication
  @EnableEurekaServer #表示为Eureka的sever
  public class EurekaMain7001 {
  
      public static void main(String[] args) {
          SpringApplication.run(EurekaMain7001.class, args);
      }
  }
  ```

- 测试

  访问：http://localhost:7001，如果看到下面的页面就是表示启动Eureka成功了

  ![image-20211003174934512](https://cdn.jsdelivr.net/gh/le1024/image1/le/image-20211003174934512.png)





#### EurekaClient端(cloud-provider-payment8001)注册进EurekaServer

修改`cloud-provider-payment8001`工程

- **`pom.xml`添加`EurekaClient依赖`**

  ```xml
  <!--eureka-client-->
  <dependency>
      <groupId>org.springframework.cloud</groupId>
      <artifactId>spring-cloud-starter-netflix-eureka-client</artifactId>
  </dependency>
  ```

- **`application.yml`添加Eureka配置**

  ```yaml
  #eureka配置
  eureka:
    client:
      #表示是否将自己注册进EurekaServer，默认true
      register-with-eureka: true
      #是否从EurekaServer抓取已有的注册信息，默认为true。单节点无所谓，集群必须设置为true才能配合ribbon使用负载均衡
      fetch-registry: true
      service-url:
        #EurekaServer地址
        defaultZone: http://localhost:7001/eureka
  ```

- **主启动类添加`@EnableEurekaClient`**

  ```java
  @SpringBootApplication
  @EnableEurekaClient
  public class PaymentMain8001 {
  
      public static void main(String[] args) {
          SpringApplication.run(PaymentMain8001.class, args);
      }
  }
  ```

- **启动测试**

  访问注册中心：http://localhost:7001

  ![image-20211003202724462](https://cdn.jsdelivr.net/gh/le1024/image1/le/image-20211003202724462.png)



#### EurekaClient端(cloud-consumer-order80)注册进EurekaServer

修改`cloud-consumer-order80`工程

- **修改`pom.xml`添加`EurekaClient`依赖**

  ```xml
  <!--eureka-client-->
  <dependency>
      <groupId>org.springframework.cloud</groupId>
      <artifactId>spring-cloud-starter-netflix-eureka-client</artifactId>
  </dependency>
  ```

- **`application.yml`添加EurekaClient**

  ```yaml
  #EurekaClient配置
  eureka:
    client:
      #表示将自己注册进EurekaServer，默认为true
      register-with-eureka: true
      #是否从EurekaServer抓取已有的注册信息，默认为true。单节点无所谓。，集群必须设置true才能配合ribbon使用负载均衡
      fetch-registry: true
      service-url:
        #EurekaServer地址
        defaultZone: http://localhost:7001/eureka
  ```

- **主启动类添加`@EnableEurekaClient`**

  ```java
  @SpringBootApplication
  @EnableEurekaClient
  public class AppMain80 {
  
      public static void main(String[] args) {
          SpringApplication.run(AppMain80.class, args);
      }
  }
  ```

- **启动测试**

  访问注册中心：http://localhost:7001

  ![image-20211003204852843](https://cdn.jsdelivr.net/gh/le1024/image1/le/image-20211003204852843.png)