> Sleuth服务跟踪

https://github.com/spring-cloud/spring-cloud-sleuth

https://docs.spring.io/spring-cloud-sleuth/docs/current-SNAPSHOT/reference/html/



#### Sleuth是什么

Spring Cloud Sleuth提供了一套完整的服务跟踪的解决方案，在分布式系统中提供追踪解决方案并且兼容支持了zipkin



#### 搭建链路监控

##### Zipkin

Zipkin是一个分布式跟踪系统。它有助于收集解决服务体系结构中的延迟问题所需的时间数据。功能包括收集和查找这些数据。

SpringCloud从F版起已不需要自己构建Zipkin Server了，只需调用jar包即可。

官网：https://zipkin.io/

下载地址：https://repo1.maven.org/maven2/io/zipkin/zipkin-server/2.23.4/zipkin-server-2.23.4-exec.jar



运行zipkin服务：

```bash
java -jar zipkin-server-2.23.4-exec.jar
```

![image-20211020151843603](https://cdn.jsdelivr.net/gh/le1024/image1/le/image-20211020151843603.png)

访问zipkin服务：

```html
http://localhost:9411/zipkin/
```

![image-20211020153011367](https://cdn.jsdelivr.net/gh/le1024/image1/le/image-20211020153011367.png)



服务链路：

![image-20211020162235732](https://cdn.jsdelivr.net/gh/le1024/image1/le/image-20211020162235732.png)

![image-20211020162049810](https://cdn.jsdelivr.net/gh/le1024/image1/le/image-20211020162049810.png)

Trace:类似于树结构的Span集合，表示一条调用链路，存在唯一标识

span:表示调用链路来源，通俗的理解span就是一次请求信息



##### 服务提供者

在`cloud-provider-payment8001`服务上进行修改

添加依赖：

```xml
<!--包含了sleuth+zipkin-->
<dependency>
    <groupId>org.springframework.cloud</groupId>
    <artifactId>spring-cloud-starter-zipkin</artifactId>
</dependency>

```

yml配置

```yaml
spring:
  application:
    name: cloud-payment-service
  zipkin:
    base-url: http://localhost:9411
  sleuth:
    sampler:
      probability: 1 #采样率值介于0到1之间，1表示全部采集,一般配置0.5
....
```

controller添加新接口：

```java
@GetMapping("/payment/zipkin")
public String paymentZipkin() {
    return "paymentZipkin server fall back";
}
```





##### 服务消费者

在`cloud-consumer-order80`服务上进行修改

添加依赖

```xml
<!--包含了sleuth+zipkin-->
        <dependency>
            <groupId>org.springframework.cloud</groupId>
            <artifactId>spring-cloud-starter-zipkin</artifactId>
        </dependency>
```

yml配置

```yaml
spring:
  application:
    name: cloud-order-service
  zipkin:
    base-url: http://localhost:9411
  sleuth:
    sampler:
      probability: 1 #采样率值介于0到1之间，1表示全部采集,一般配置0.5
```

controller新接口：

```java
@GetMapping("/consumer/payment/zipkin")
public String paymentZipkin() {
    return restTemplate.getForObject(PaymentSrv_URL + "/payment/zipkin", String.class);
}
```



##### 启动服务

依次启动eureka7001，payment8001，order80

访问80服务接口：http://localhost/consumer/payment/zipkin，多访问几次

![image-20211020165722815](https://cdn.jsdelivr.net/gh/le1024/image1/le/image-20211020165722815.png)

打开Zipkin界面：可以查看到启动的微服务

![image-20211020165835149](https://cdn.jsdelivr.net/gh/le1024/image1/le/image-20211020165835149.png)

点击服务查询，可以看到服务请求

![image-20211020165905628](https://cdn.jsdelivr.net/gh/le1024/image1/le/image-20211020165905628.png)

在依赖模块可以看请求的链路：

![image-20211020170018620](https://cdn.jsdelivr.net/gh/le1024/image1/le/image-20211020170018620.png)

