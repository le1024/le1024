> SpringCloud Bus消息总线

继续上一知识，通过**Spring Cloud Bus 配合 Spring Cloud Config 使用可以实现配置的动态刷新**。



#### SpringCloud Bus是什么

SpringCloud Bus可以配合SpringCloud Config实现配置的动态刷新。

SpringCloud Bus是用来将分布式系统的节点与轻量级消息系统链接器起来的框架。它整合了java的事件处理机制和消息中间件的功能。

SpringCloud Bus目前支持RabbitMQ和Kafka。



#### SpringCloud Bus能干嘛

SpringCloud Bus能管理和传播分布式系统间的消息，就像一个分布式执行器，可用于广播状态更改、事件推送等，也可以当做微服务间的通信通道。



#### 为什么称为总线

**什么是消息总线**

在微服务架构的系统中，通常会使用<font color="red">轻量级的消息代理</font>来构建一个<font color="red">共用的消息主题</font>，并让微服务中的所有微服务实例连接上来。由于<font color="red">该主题中产生的消息会被所有实例监听和消费，所以称它为消息总线</font>。在总线上的各个实例，都可以方便地广播一些需要让其他连接在该主题上的实例都知道的消息。



**基本原理**

<strong style="color:red">Config客户端实例都监听MQ中同一个topic（默认是SpringcloudBus）</strong>。当一个服务刷新数据的时候，它会把这个消息放入到Topic中，这样其他监听同一个Topic的微服务就能得到通知，然后去自动更新自身的配置。

