> 服务发现Discovery

对于注册进eureka里面的微服务，可以通过服务发现来获得该服务的信息



以`cloud-provider-payment8001`服务为例：

修改cloud-provider-payment8001的Controller

```java
@Resource
private DiscoveryClient discoveryClient;

@GetMapping("/payment/discovery")
public Object discovery() {
    //获取微服务列表
    List<String> services = discoveryClient.getServices();
    for (String service : services) {
        log.info(service);
    }

    //获取具体微服务实例信息
    List<ServiceInstance> instances = discoveryClient.getInstances("CLOUD-PAYMENT-SERVICE");
    for (ServiceInstance instance : instances) {
        //微服务具体信息
        log.info(instance.getInstanceId());
        log.info(instance.getHost());
        log.info(String.valueOf(instance.getPort()));
        log.info(String.valueOf(instance.getUri()));
    }

    return this.discoveryClient;
}
```

修改主启动类：添加**`@EnableDiscoveryClient`** //服务发现

```java
@SpringBootApplication
@EnableEurekaClient
@EnableDiscoveryClient //服务发现
public class PaymentMain8001 {

    public static void main(String[] args) {
        SpringApplication.run(PaymentMain8001.class, args);
    }
}
```

测试，先启动EurekaServer，再启动8001服务，访问：http://localhost:8001/payment/discovery

![image-20211004203306522](https://gitee.com/le1024/image1/raw/master/img/image-20211004203306522.png)

![image-20211004203336103](https://cdn.jsdelivr.net/gh/le1024/image1/le/image-20211004203336103.png)