> SpringCloud Stream消息驱动之消费者



#### 消费者服务模块

##### 创建Module`cloud-stream-rabbitmq-consumer8802`

##### pom

```xml
<dependencies>
        <dependency>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-starter-web</artifactId>
        </dependency>
        <dependency>
            <groupId>org.springframework.cloud</groupId>
            <artifactId>spring-cloud-starter-netflix-eureka-client</artifactId>
        </dependency>
        <dependency>
            <groupId>org.springframework.cloud</groupId>
            <artifactId>spring-cloud-starter-stream-rabbit</artifactId>
        </dependency>
        <dependency>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-starter-actuator</artifactId>
        </dependency>
        <!--基础配置-->
        <dependency>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-devtools</artifactId>
            <scope>runtime</scope>
            <optional>true</optional>
        </dependency>
        <dependency>
            <groupId>org.projectlombok</groupId>
            <artifactId>lombok</artifactId>
            <optional>true</optional>
        </dependency>
        <dependency>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-starter-test</artifactId>
            <scope>test</scope>
        </dependency>
    </dependencies>
```



##### yml

```yaml
server:
  port: 8802

spring:
  application:
    name: cloud-stream-consumer
  cloud:
    stream:
      binders: # 在此处配置上rabbitMQ的服务信息
        defaultRabbit: #表示定义名称，与bindings.input.binder对应
          type: rabbit #消息组件类型
          environment:
            spring:
              rabbitmq: #rabbitMQ环境配置
                host: localhost
                port: 5672
                username: guest
                password: guest
      bindings: #服务的整合处理
        input:
          destination: studyExchange #使用的Exchange名称
          content-type: application/json #消息类型
          binder: defaultRabbit #设置要绑定的消息服务的具体设置

eureka:
  instance:
    instance-id: receive-8802.com #显示的主机名称
    prefer-ip-address: true #显示ip
    lease-renewal-interval-in-seconds: 2 #设置心跳间隔时间
    lease-expiration-duration-in-seconds: 5 #超过5秒，剔除服务
  client:
    service-url:
      defaultZone: http://eureka7001.com:7001/eureka/

```



##### 业务类

`StreamMQMain8802`

```java
@SpringBootApplication
public class StreamMQMain8802 {

    public static void main(String[] args) {
        SpringApplication.run(StreamMQMain8802.class, args);
    }
}
```

`ReceiveMessageListener`

```java
package com.springcloud.service;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.stream.annotation.EnableBinding;
import org.springframework.cloud.stream.annotation.StreamListener;
import org.springframework.cloud.stream.messaging.Sink;
import org.springframework.messaging.Message;
import org.springframework.stereotype.Component;

/**
 * @author helele
 * @date 2021/10/19 17:31
 */
@Component
@EnableBinding(Sink.class)
public class ReceiveMessageListener {

    @Value("${server.port}")
    private String serverPort;

    @StreamListener(Sink.INPUT)
    public void input(Message<String> message) {
        System.out.println("消费者1号，------>接收到的消息：" + message.getPayload() + "\t port：" + serverPort);
    }
}
```



##### 测试

- 启动rabbitMQ
- 启动eureka7001
- 启动Stream8801
- 启动Stream8802

在rabbitMQ控制台，可以在Exchange模块看到已经绑定上了消费者

![image-20211019175552502](https://cdn.jsdelivr.net/gh/le1024/image1/le/image-20211019175552502.png)

请求地址：http://localhost:8801/sendMessage

![image-20211019175259786](https://cdn.jsdelivr.net/gh/le1024/image1/le/image-20211019175259786.png)

![image-20211019175319637](https://cdn.jsdelivr.net/gh/le1024/image1/le/image-20211019175319637.png)

<strong style="color:red">通过Stream实现了mq消息的发送与接收，且编码没有直接对mq进行操作</strong>

