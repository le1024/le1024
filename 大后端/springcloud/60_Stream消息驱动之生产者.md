> SpringCloud Stream消息驱动之生产者

#### 准备工作

需要有rabbitMQ环境



#### 生产者服务模块

##### 创建Module`cloud-stream-rabbitmq-provider8801`



##### pom

引入pom：

```xml
    <dependencies>
        <dependency>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-starter-web</artifactId>
        </dependency>
        <dependency>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-starter-actuator</artifactId>
        </dependency>
        <dependency>
            <groupId>org.springframework.cloud</groupId>
            <artifactId>spring-cloud-starter-netflix-eureka-client</artifactId>
        </dependency>
        <dependency>
            <groupId>org.springframework.cloud</groupId>
            <artifactId>spring-cloud-starter-stream-rabbit</artifactId>
        </dependency>
        <!--基础配置-->
        <dependency>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-devtools</artifactId>
            <scope>runtime</scope>
            <optional>true</optional>
        </dependency>
        <dependency>
            <groupId>org.projectlombok</groupId>
            <artifactId>lombok</artifactId>
            <optional>true</optional>
        </dependency>
        <dependency>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-starter-test</artifactId>
            <scope>test</scope>
        </dependency>
    </dependencies>
```



##### yml

```yaml
server:
  port: 8801

spring:
  application:
    name: cloud-stream-provider
  cloud:
    stream:
      binders: #在此处配置需要绑定的rabbitMQ信息
        defaultRabbit: #表示定义的名称，用于binding整合
          type: rabbit #消息组件类型
          environment: #设置rabbitMQ的相关的环境配置
            spring:
              rabbitmq:
                host: localhost
                port: 5672
                username: guest
                password: guest
      bindings: #服务的整合处理
        output:
          destination: studyExchange #定义Exchange名字
          content-type: application/json  #设置消息类型
          binder: defaultRabbit #设置要绑定的消息服务的具体设置
eureka:
  client:
    service-url:
      defaultZone: http://eureka7001.com:7001/eureka/
  instance:
    instance-id: send-8801.com #显示的主机名称
    prefer-ip-address: true #访问路径变为ip地址
    lease-renewal-interval-in-seconds: 2
    lease-expiration-duration-in-seconds: 5 #设置心跳的时间间隔（默认30秒）

```



##### 业务类

`主启动类`

```java
@SpringBootApplication
public class StreamMQMain8801 {

    public static void main(String[] args) {
        SpringApplication.run(StreamMQMain8801.class, args);
    }
}
```



`IMessageProvider`

```java
public interface IMessageProvider {

    public String send();
}
```

`MessageProviderImpl`

```java
package com.springcloud.service.imp;

import com.springcloud.service.IMessageProvider;
import org.springframework.cloud.stream.annotation.EnableBinding;
import org.springframework.cloud.stream.annotation.Output;
import org.springframework.cloud.stream.messaging.Source;
import org.springframework.integration.support.MessageBuilder;
import org.springframework.messaging.MessageChannel;

import javax.annotation.Resource;
import java.util.UUID;

/**
 * @author helele
 * @date 2021/10/19 15:56
 */
@EnableBinding(Source.class) //可以理解为是一个消息的发送管道的定义
public class MessageProviderImpl implements IMessageProvider {

    @Resource
    private MessageChannel output; //消息的发送管道

    @Override
    public String send() {
        String serial = UUID.randomUUID().toString();
        //创建并发送消息
        this.output.send(MessageBuilder.withPayload(serial).build());
        return serial;
    }
}

```



`MessageController`

```java
@RestController
public class MessageController {

    @Resource
    private IMessageProvider messageProvider;

    @GetMapping("/sendMessage")
    public String send() {
        return messageProvider.send();
    }
}
```



##### 测试

- 启动rabbitMQ
- 启动eureka7001
- 启动stream8801

![image-20211019163959410](https://cdn.jsdelivr.net/gh/le1024/image1/le/image-20211019163959410.png)



访问接口：http://localhost:8801/sendMessage，一直刷新，多次请求

![image-20211019165336702](https://cdn.jsdelivr.net/gh/le1024/image1/le/image-20211019165336702.png)

这里的波峰就能看出来我们请求的频率

![image-20211019165430641](https://cdn.jsdelivr.net/gh/le1024/image1/le/image-20211019165430641.png)

