> OpenFeign服务调用

<strong style="color:red">Feign自带负载均衡配置项</strong>



实现方式：<strong style="color:orange">微服务调用接口+@FeignClient</strong>



- **新建OpenFeign客户端服务**：`cloud-consumer-feign-order80`

- **修改pom**

  ```xml
      <dependencies>
          <!--openfeign-->
          <dependency>
              <groupId>org.springframework.cloud</groupId>
              <artifactId>spring-cloud-starter-openfeign</artifactId>
          </dependency>
          <!--eureka client-->
          <dependency>
              <groupId>org.springframework.cloud</groupId>
              <artifactId>spring-cloud-starter-netflix-eureka-client</artifactId>
          </dependency>
          <!-- 引入自己定义的api通用包，可以使用Payment支付Entity -->
          <dependency>
              <groupId>app.springcloud</groupId>
              <artifactId>cloud-api-commons</artifactId>
              <version>${project.version}</version>
          </dependency>
          <!--web-->
          <dependency>
              <groupId>org.springframework.boot</groupId>
              <artifactId>spring-boot-starter-web</artifactId>
          </dependency>
          <dependency>
              <groupId>org.springframework.boot</groupId>
              <artifactId>spring-boot-starter-actuator</artifactId>
          </dependency>
          <!--一般基础通用配置-->
          <dependency>
              <groupId>org.springframework.boot</groupId>
              <artifactId>spring-boot-devtools</artifactId>
              <scope>runtime</scope>
              <optional>true</optional>
          </dependency>
          <dependency>
              <groupId>org.projectlombok</groupId>
              <artifactId>lombok</artifactId>
              <optional>true</optional>
          </dependency>
          <dependency>
              <groupId>org.springframework.boot</groupId>
              <artifactId>spring-boot-starter-test</artifactId>
              <scope>test</scope>
          </dependency>
      </dependencies>
  ```

  

- **修改application.yml**

  ```properties
  server:
    port: 80
  
  #eureka配置
  eureka:
    client:
      register-with-eureka: false #客户端不注册到Eureka中心
      service-url:
        defaultZone: http://eureka7001.com:7001/eureka/,http://eureka7002.com:7002/eureka/
  ```

  

- **主启动类**

  添加注解启动Feign：<font color="red">@EnableFeignClients</font>

  ```java
  @SpringBootApplication
  @EnableFeignClients
  public class OrderFeignMain80 {
  
      public static void main(String[] args) {
          SpringApplication.run(OrderFeignMain80.class, args);
      }
  }
  ```

  

- **业务类**

  新建`PaymentFeignService`接口并新增注解`@FeignClient`

  ```java
  @Component
  @FeignClient(value = "CLOUD-PAYMENT-SERVICE") //告诉Feign调用哪个微服务集群
  public interface PaymentFeignService {
  
      @GetMapping("/payment/get/{id}")
      CommonResult<Payment> getPaymentById(@PathVariable("id") Long id);
  }
  ```

  新建`OrderFeignController`

  ```java
  @RestController
  public class OrderFeignController {
  
      @Autowired
      private PaymentFeignService paymentFeignService;
  
      @GetMapping("/consumer/payment/get/{id}")
      public CommonResult<Payment> getPaymentById(@PathVariable("id") Long id) {
          return paymentFeignService.getPaymentById(id);
      }
  }
  ```

- 测试

  启动Eureka集群：7001/7002

  启动payment服务集群：8001/8002

  启动Feign服务：80

  访问：http://localhost/consumer/payment/get/1

  ![image-20211008224604055](https://cdn.jsdelivr.net/gh/le1024/image1/le/image-20211008224604055.png)



**小总结：**

![image-20211008224041122](https://cdn.jsdelivr.net/gh/le1024/image1/le/image-20211008224041122.png)

