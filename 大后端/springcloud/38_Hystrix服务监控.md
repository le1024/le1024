> Hystrix服务监控

除了隔离依赖服务的调用之外，Hystrix还提供了<strong style="color:red">准实时的调用监控(Hystrix Dashboard)</strong>，Hystrix会持续记录所有通过Hystrix发起的请求的执行信息，并以统计报表和图形的形式展示给用户，包括每秒执行多少成功请求多少失败请求等。

Netflix通过hystrix-metrics-event-stream项目实现了对以上指标的监控。Spring Cloud也提供了Hystrix Dashboard的整合，对监控内容转化成可视化界面。



#### 创建Hystrix Dashboard服务

- **新建**`cloud-consumer-hystrix-dashboard9001`

- **pom**

  ```xml
  <dependencies>
          <dependency>
              <groupId>org.springframework.cloud</groupId>
              <artifactId>spring-cloud-starter-netflix-hystrix-dashboard</artifactId>
          </dependency>
          <dependency>
              <groupId>org.springframework.boot</groupId>
              <artifactId>spring-boot-starter-actuator</artifactId>
          </dependency>
  
          <dependency>
              <groupId>org.springframework.boot</groupId>
              <artifactId>spring-boot-devtools</artifactId>
              <scope>runtime</scope>
              <optional>true</optional>
          </dependency>
          <dependency>
              <groupId>org.projectlombok</groupId>
              <artifactId>lombok</artifactId>
              <optional>true</optional>
          </dependency>
          <dependency>
              <groupId>org.springframework.boot</groupId>
              <artifactId>spring-boot-starter-test</artifactId>
              <scope>test</scope>
          </dependency>
      </dependencies>
  ```

- **yml**

  ```properties
  server:
    port: 9001
  ```

- **启动类**

  多了个新注解：`@EnableHystrixDashboard`，开启Hystrix监控

  ```java
  @SpringBootApplication
  @EnableHystrixDashboard
  public class HystrixDashboardMain9001 {
  
      public static void main(String[] args) {
          SpringApplication.run(HystrixDashboardMain9001.class, args);
      }
  }
  ```

- **被监控的服务需要提供监控依赖**

  8001、8002、8003等Provider微服务

  ```xml
  <dependency>
      <groupId>org.springframework.boot</groupId>
      <artifactId>spring-boot-starter-web</artifactId>
  </dependency>
  <dependency>
      <groupId>org.springframework.boot</groupId>
      <artifactId>spring-boot-starter-actuator</artifactId>
  </dependency>
  ```

- **启动9001服务**

  访问：http://localhost:9001/hystrix，看到下面的界面就表示配置完成

  ![image-20211013175120884](https://cdn.jsdelivr.net/gh/le1024/image1/le/image-20211013175120884.png)



#### 服务监控演示

- **修改**`cloud-provider-hystrix-payment8001`

  启动类，添加服务配置

  ```java
  /**
   *此配置是为了服务监控而配置，与服务容错本身无关，springcloud升级后的坑
   *ServletRegistrationBean因为springboot的默认路径不是"/hystrix.stream"，
   *只要在自己的项目里配置上下面的servlet就可以了
   */
  @Bean
  public ServletRegistrationBean getServlet() {
      HystrixMetricsStreamServlet streamServlet = new HystrixMetricsStreamServlet();
      ServletRegistrationBean registrationBean = new ServletRegistrationBean(streamServlet);
      registrationBean.setLoadOnStartup(1);
      registrationBean.addUrlMappings("/hystrix.stream");
      registrationBean.setName("HystrixMetricsStreamServlet");
      return registrationBean;
  }
  ```

- **启动服务**

  启动Eureka7001(或者更多集群)，启动9001，启动8001

- **配置9001监控8001**

  填写监控地址：http://localhost:8001/hystrix.stream

  ![image-20211013175608815](https://gitee.com/le1024/image1/raw/master/img/image-20211013175608815.png)

  `Delay`：该参数用来控制服务器上轮询监控信息的延迟时间，默认为2000毫秒，可以通过配置该属性来降低客户端的网络和CPU消耗。

  `Title`：该参数对应了头部标题Hystrix Stream之后的内容，默认会使用具体监控实例的URL，可以通过配置该信息来展示更合适的标题。 

- **测试**

  1. 先请求8001服务接口：http://localhost:8001/payment/circuit/1，http://localhost:8001/payment/circuit/-1，确认服务正常

     ![image-20211013205646184](https://cdn.jsdelivr.net/gh/le1024/image1/le/image-20211013205646184.png)

     ![image-20211013205704447](https://cdn.jsdelivr.net/gh/le1024/image1/le/image-20211013205704447.png)

  2. 点击`Monitor Stream`

     ![image-20211013204907801](https://cdn.jsdelivr.net/gh/le1024/image1/le/image-20211013204907801.png)

     可以看到如下监控界面：

     ![image-20211013205740602](https://cdn.jsdelivr.net/gh/le1024/image1/le/image-20211013205740602.png)

  3. 连续访问http://localhost:8001/payment/circuit/1，再回到监控界面查看界面变化

     ![image-20211013210052219](https://cdn.jsdelivr.net/gh/le1024/image1/le/image-20211013210052219.png)

  4. 连续访问http://localhost:8001/payment/circuit/-1，再回到监控界面查询界面变化

     ![image-20211013210601896](https://cdn.jsdelivr.net/gh/le1024/image1/le/image-20211013210601896.png)

     ![image-20211013210443336](https://cdn.jsdelivr.net/gh/le1024/image1/le/image-20211013210443336.png)

     ​	

     

#### 监控界面说明

1. **7色对应说明**

   不同的颜色数量对应不同的请求数量

   ![image-20211013210741441](https://cdn.jsdelivr.net/gh/le1024/image1/le/image-20211013210741441.png)

   ![image-20211013210754939](https://cdn.jsdelivr.net/gh/le1024/image1/le/image-20211013210754939.png)

2. **实心圆说明**

   有两种含义，通过颜色的变化代表了实例的健康程度，健康程度从

   <font color="green">绿色</font><<font color="yellow">黄色</font><<font color="orange">橙色</font><<font color="red">红色 </font> 递减

   除了颜色之外，实心圆的大小也会随着请求流量发生变化，流量越大实心圆越大。所以通过实心圆的展示，可以在大量的实例中快速发现`故障实例和高并发实例`

   ![image-20211013211103628](https://cdn.jsdelivr.net/gh/le1024/image1/le/image-20211013211103628.png)

3. **曲线**

   用来记录2分钟内流量的相对变化，可以通过他来观察到流量的上升和下降趋势

   ![image-20211013211522513](https://cdn.jsdelivr.net/gh/le1024/image1/le/image-20211013211522513.png)

4. **整图说明**

   ![image-20211013212327380](https://cdn.jsdelivr.net/gh/le1024/image1/le/image-20211013212327380.png)

   ![image-20211013212359230](https://cdn.jsdelivr.net/gh/le1024/image1/le/image-20211013212359230.png)

