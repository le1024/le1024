> Eureka集群搭建

参考**`cloud-eureka-server7001`**新建**`cloud-eureka-server7002`**、**`cloud-eureka-server7003`**工程。

只创建一个也行。随意。依赖引用等保持一样，只有yaml后面需要进行修改



前期工作：修改映射的配置(现在有多个服务，不能用一个localhost去明确表示是哪一个服务了)

找到C:\Windows\System32\drivers\etc路径下的hosts文件

```properties
127.0.0.1  eureka7001.com
127.0.0.1  eureka7002.com
127.0.0.1  eureka7003.com
```

![image-20211003212906824](https://cdn.jsdelivr.net/gh/le1024/image1/le/image-20211003212906824.png)



**`cloud-eureka-server7001`**工程修改：

- **修改yaml**

  ```yaml
  #EurekaServer配置
  eureka:
    instance:
      hostname: eureka7001.com #eureka服务端的实例名称
    client:
      register-with-eureka: false #false表示不向注册中心注册自己，当前服务不需要注册，只关注业务服务就行
      fetch-registry: false #false表示自己就是注册中心，职责就是维护服务实例，并不需要去检索服务
      service-url:
        #设置与Eureka Server交互的地址查询服务和注册服务都需要依赖这个地址。
        #相互注册，相互守望，这里配置另外的EurekaSever地址，多个服务地址用英文逗号隔开
        defaultZone: http://eureka7002.com:7002/eureka/,http://eureka7003.com:7003/eureka/
  ```

  

**`cloud-eureka-server7002`**工程修改：

- **修改yaml**

  ```yaml
  server:
    port: 7002
  
  #EurekaServer配置
  eureka:
    instance:
      hostname: eureka7002.com
    client:
      #false表示不向注册中心注册自己，当前服务不需要注册，只关注业务服务就行
      register-with-eureka: false
      #false表示自己就是注册中心，职责就是维护服务实例，并不需要去检索服务
      fetch-registry: false
      service-url:
      #相互注册，相互守望，这里配置另外的EurekaSever地址，多个服务地址用英文逗号隔开
        defaultZone: http://eureka7001.com:7001/eureka/,http://eureka7003.com:7003/eureka/
  
  ```



**`cloud-eureka-server7003`**工程修改：

- **修改yaml**

  ```yaml
  server:
    port: 7003
  
  #EurekaServer
  eureka:
    instance:
      hostname: eureka7003.com
    client:
      register-with-eureka: false
      fetch-registry: false
      service-url:
      #相互注册，相互守望，这里配置另外的EurekaSever地址，多个服务地址用英文逗号隔开
        defaultZone: http://eureka7001.com:7001/eureka/,http://eureka7002.com:7002/eureka/
  ```

  



分别启动：**`cloud-eureka-server7001`**、**`cloud-eureka-server7002`**、**`cloud-eureka-server7003`**

可以通过http://localhost:7001或者http://eureka7001.com:7001访问，7002/7003也是一样

![image-20211003215009052](https://cdn.jsdelivr.net/gh/le1024/image1/le/image-20211003215009052.png)

![image-20211003215047458](https://cdn.jsdelivr.net/gh/le1024/image1/le/image-20211003215047458.png)

![image-20211003215136710](https://cdn.jsdelivr.net/gh/le1024/image1/le/image-20211003215136710.png)

