> Gateway服务网关

https://cloud.spring.io/spring-cloud-static/spring-cloud-gateway/2.2.1.RELEASE/reference/html/



#### Gateway是什么

SpringCloud Gateway是在Spring生态系统之上构建的API网关服务，基于Spring 5，SpringBoot 2和Project Reactor等技术。

SpringCloud Gateway旨在提供一种简单而有效的方式来对API进行路由，以及提供一些强大的过滤器功能，例如：熔断、限流、重试等。



SpringCloud Gateway 作为 Spring Cloud 生态系统中的网关，目标是替代 Zuul，在Spring Cloud 2.0以上版本中，没有对新版本的Zuul 2.0以上最新高性能版本进行集成，仍然还是使用的Zuul 1.x非Reactor模式的老版本。

而为了提升网关的性能，SpringCloud Gateway是基于WebFlux框架实现的，而WebFlux框架底层则使用了高性能的Reactor模式通信框架Netty。



<strong style="color:blue">SpringCloud Gateway 使用的Webflux中的reactor-netty响应式编程组件，底层使用了Netty通讯框架。</strong>



#### Gateway可以做什么

- [x] 反向代理
- [x] 鉴权
- [x] 流量控制
- [x] 熔断
- [x] 日志监控
- [x] .....



#### 为什么选择Gateway

Zuul1已经进入到了维护阶段，且Zuul2并未成熟，Gateway作为springcloud团队研发的，在集成方面有天然的优势，且Gateway是基于<font color="red">异步非阻塞模型</font>进行开发的，性能方面是优于Zuul的。



#### Gateway和Zuul的区别

在SpringCloud Finchley 正式版之前，Spring Cloud 推荐的网关是 Netflix 提供的Zuul

1. Zuul1.x是基于阻塞I/O的API Gateway
2. Zuul1.x<font color="red">基于Servlet2.5使用阻塞架构</font>，不支持任何长连接(如websocket)，zuul的设计模式和nginx较像，每次I/O操作都是从工作线程中选择一个执行，请求线程被阻塞到工作线程完成，但是差别是nginx是c++实现，zuul是用java实现，jvm本身会有第一次加载慢的情况，使得zuul的性能相对较差。
3. Spring Cloud Gateway 建立 在 Spring Framework 5、 Project Reactor 和 Spring Boot 2 之上， 使用非阻塞 API。
4. Spring Cloud Gateway 还支持 WebSocket， 并且与Spring紧密集成拥有更好的开发体验。



#### Gateway特性

基于Spring Framework 5, Project Reactor 和 Spring Boot 2.0 进行构建；

- [x] 动态路由：能够匹配任何请求属性
- [x] 可以对路由指定Predicate（断言）和Filter（过滤器）
- [x] 集成Hystrix的断路器功能
- [x] 集成SpringCloud服务发现功能
- [x] 请求限流功能
- [x] 支持路径重写