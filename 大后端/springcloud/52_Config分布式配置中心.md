> Config分布式配置中心

https://cloud.spring.io/spring-cloud-static/spring-cloud-config/2.2.1.RELEASE/reference/html/



分布式系统面临的配置问题

当单体应用中的业务拆分成多个子服务，每个服务的粒度相对较小，因此系统中会出现大量的服务。由于每个服务有自己的的配置信息，随着配置的增多，一套集中式的、动态的配置管理设施必不可少。

SpringCloud就提供了ConfigServer来解决这个问题



#### SpringCloud Config是什么

SpringCloud Config为微服务架构中的微服务提供集中化的外部配置支持配置服务器为各个不同微服务应用的所有环境提供了一个中心化的外部配置。

SpringCloud Config分为<font color="red">服务端和客户端</font>

服务端也称为<font color="red">分布式配置中心，他是一个独立的微服务应用</font>，用来连接配置服务器并为客户端提供获取配置信息，加密解密信息等访问接口

客户端则是通过指定的配置中心来管理应用资源，以及与业务相关的配置内容，并在启动的时候从配置中心获取和加载配置信息，配置服务器默认采用Git来存储配置信息，这样有助于对环境配置的版本管理，并且可以通过Git客户端工具来便捷的访问和管理配置内容。



#### SpringCloud Config可以做什么

- [x] 集中管理配置文件
- [x] 不同环境不同配置，动态化的配置更新，分环境部署：dev/test/prod/beta/release
- [x] 运行期间动态配置，不用在每个服务部署机器上编写配置文件，直接从配置中心统一拉取配置信息
- [x] 当配置发生更新，不用重新启动服务即可感知配置的变化并应用新的配置
- [x] 将配置信息以rest接口的方式暴露



**SpringCloud Config默认使用Git来存储配置文件，同时也推荐使用Git**

