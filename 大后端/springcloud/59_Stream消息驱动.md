> SpringCloud Stream消息驱动

https://spring.io/projects/spring-cloud-stream#overview

https://cloud.spring.io/spring-cloud-static/spring-cloud-stream/3.0.1.RELEASE/reference/html/



#### Stream消息驱动是什么

**什么是SpringCloud Stream**

官方定义SpringCloud Stream是一个构建消息驱动微服务的框架。

应用程序通过inputs和outputs来与SpringCloud Stream中的`binder对象`交互。通过我们配置来`binding`，而SpringCloud Stream的`binder对象`负责与消息中间件交互。

通过使用Spring Integration来连接消息代理中间件以实现消息时间驱动。SpringCloud Stream 为一些供应商的消息中间件产品提供了个性化的自动化配置实现，引用了发布-订阅、消费者、分区的三个核心概念。



<strong style="color:orange">目前SpringCloud Stream仅支持RabbitMQ和Kafka</strong>

<strong style="color:red">屏蔽底层消息中间件的差异，降低切换成本，统一消息的编程模型。</strong>



#### 设计思想

**为什么使用SpringCloud Stream**

在stream中有个重要的东西：<font color="red">Binder</font>，绑定器

Binder可以生成Binding，Binding用来绑定消息容器的生产者和消费者，它有两种类型，INPUT和OUTPUT，<font color="red">INPUT对应于消费者</font>，<font color="red">OUTPUT对应于生产者</font>

在没有绑定器这个概念的情况下，我们的SpringBoot应用要直接与消息中间件进行信息交互的时候，由于各消息中间件构建的初衷不同，它们的实现细节上会有较大的差异性，通过定义绑定器作为中间层，完美地实现了<font color="red">应用程序与消息中间件细节之间的隔离</font>。Stream对消息中间件的进一步封装，可以做到代码层面对中间件的无感知，甚至于动态的切换中间件(rabbitmq切换为kafka)，使得微服务开发的高度解耦，服务可以关注更多自己的业务流程

<strong style="color:red">通过定义绑定器Binder作为中间件，实现了应用程序与消息中间件细节之间的隔离</strong>



**Stream中的消息通信方式遵循了发布-订阅模式**

Topic主题进行广播，在rabbitMQ中对应Exchange，在Kafka中对应Topic



#### 标准流程

![image-20211018224428309](https://cdn.jsdelivr.net/gh/le1024/image1/le/image-20211018224428309.png)

![image-20211018224453645](https://cdn.jsdelivr.net/gh/le1024/image1/le/image-20211018224453645.png)

**Binder**：很方便的连接中间件，屏蔽差异

**Channel**：通道，是Queue的一种抽象，在消息通讯系统中就是实现存储和转发的媒介，通过channel对队列进行配置

**Source和Sink**：简单的可理解为参照对象就是SpringCloud Stream自身，从Stream发出消息就是输出，接受消息就是输入



#### API和常用注解

![image-20211018224827859](https://cdn.jsdelivr.net/gh/le1024/image1/le/image-20211018224827859.png)

