> Zuul查看路由信息

需要有

```xml
<dependency>
    <groupId>org.springframework.boot</groupId>
    <artifactId>spring-boot-starter-actuator</artifactId>
</dependency>
```



开启查看路由端点：

```yaml
# 开启查看路由的端点
management:
  endpoints:
    web:
      exposure:
        include: 'routes'
```



访问：http://localhost:9527/actuator/routes或http://myzuul.com:9527/actuator/routes

![image-20211014113035493](https://cdn.jsdelivr.net/gh/le1024/image1/le/image-20211014113035493.png)

