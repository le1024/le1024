> Ribbon介绍



##### Ribbon是什么

SpringCloud Ribbon是基于Netfix Ribbon实现的一套<strong style="color:red">客户端 负载均衡工具</strong>

注意是<strong style="color:red">客户端</strong>，配置在消费者服务的

简单来说，Ribbon是Netflix发布的开源项目，主要功能是提供<strong style="color:red">客户端的软件负载均衡算法和服务调用。</strong>Ribbon客户端组件提供了一系列完善的配置项如连接超时，重试等。就是在配置文件中列出Load Balancer(简称LB)后面所有的机器，Ribbon会自动的帮助你基于某种规则（如简单轮询，随机连接等）去连接这些机器。我们很容易使用Ribbon实现自定义的负载均衡算法。



##### 官网资料

https://github.com/Netflix/ribbon/wiki/Getting-Started

Ribbon目前也进入维护模式

SpringCloud推出了LoadBalancer来替代Ribbon，但目前还未正式开始，Ribbon目前在许多方面还是大规模的使用中



##### Ribbon可以做什么

<strong style="color:red">实现负载均衡+RestTemplate调用</strong>



###### LB负载均衡(Load Balance)是什么

简单来说，就是将用户的请求平摊的分配到多个服务上，从而达到系统的HA（高可用）。

常见的负载均衡软件有Nginx，LVS，硬件F5等



###### Ribbon负载均衡和Nginx负载均衡的区别

Ribbon是本地负载均衡，在调用微服务接口时候，会在注册中心上获得注册信息服务列表之后缓存到JVM本地，从而在本地实现RPC远程服务调用技术

Nginx是服务器负载均衡，客户端所有的请求都会交给Nginx，然后由nginx实现转发请求。即负载均衡是有服务端实现的。



###### 集中式LB和进程内LB

集中式LB：在服务的消费方和提供方之间使用独立的LB设施（可以是硬件，如F5，也可以是软件：nginx），由该设施负责把访问请求通过某种策略转发至服务的提供方



进程内LB：将LB逻辑集成到消费方，消费方从服务注册中心获知哪些地址可用，然后自己再从这些地址中选择出一个合适的服务器。

Ribbon就属于进程内LB，他只是一个类库，集成于消费者方进程，消费方通过他来获取到服务提供方的地址



##### 依赖引入

```xml
<dependency>
    <groupId>org.springframework.cloud</groupId>
    <artifactId>spring-cloud-starter-netflix-ribbon</artifactId>
</dependency>
```

由于spring-cloud-starter-netflix-eureka-client自带了spring-cloud-starter-ribbon引用，所以不再次引入ribbon也可以

![image-20211007190625962](https://cdn.jsdelivr.net/gh/le1024/image1/le/image-20211007190625962.png)