> Zuul路由配置

<font color="red">路由功能负责将外部请求转发到具体的服务实例上去，是实现统一访问入口的基础</font>



- **新建**`cloud-zuul-gateway9527`

- **pom**

  ```xml
  <dependencies>
          <dependency>
              <groupId>org.springframework.cloud</groupId>
              <artifactId>spring-cloud-starter-netflix-eureka-client</artifactId>
          </dependency>
          <dependency>
              <groupId>org.springframework.cloud</groupId>
              <artifactId>spring-cloud-starter-netflix-zuul</artifactId>
          </dependency>
  
          <dependency>
              <groupId>org.springframework.boot</groupId>
              <artifactId>spring-boot-starter-actuator</artifactId>
          </dependency>
          <dependency>
              <groupId>org.springframework.boot</groupId>
              <artifactId>spring-boot-devtools</artifactId>
              <scope>runtime</scope>
              <optional>true</optional>
          </dependency>
          <dependency>
              <groupId>org.projectlombok</groupId>
              <artifactId>lombok</artifactId>
              <optional>true</optional>
          </dependency>
          <dependency>
              <groupId>org.springframework.boot</groupId>
              <artifactId>spring-boot-starter-test</artifactId>
              <scope>test</scope>
          </dependency>
      </dependencies>
  ```

- **yml**

  ```properties
  server:
    port: 9527
  spring:
    application:
      name: cloud-zuul-gateway
  
  eureka:
    client:
      service-url:
        defaultZone: http://eureka7001.com:7001/eureka/
    instance:
      instance-id: gateway-9527.com
      prefer-ip-address: true
  
  ```

- **hosts文件修改**

  ```properties
  127.0.0.1	myzuul.com
  ```

- **主启动类**

  ```java
  @SpringBootApplication
  @EnableZuulProxy
  public class ZuulMain9527 {
  
      public static void main(String[] args) {
          SpringApplication.run(ZuulMain9527.class, args);
      }
  }
  ```

- **启动**

  启动Eureka7001(启动集群也行)，启动payment8001，启动zuul9527

  可以看到网关服务注册成功

  ![image-20211013223638341](https://cdn.jsdelivr.net/gh/le1024/image1/le/image-20211013223638341.png)

- **测试**

  先直接访问8001服务：http://localhost:8001/payment/get/1

  ![image-20211013223726851](https://cdn.jsdelivr.net/gh/le1024/image1/le/image-20211013223726851.png)

  通过网关服务去访问8001服务接口，

  访问格式：<font color="red">zuul映射配置</font>+<font color="blue">注册中心注册后对外暴露的服务名称</font>+<font color="green">rest调用地址</font>

  http://myzuul.com:9527/cloud-payment-service/payment/get/1

  ![image-20211013223928349](https://cdn.jsdelivr.net/gh/le1024/image1/le/image-20211013223928349.png)

  

