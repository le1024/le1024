> Hystrix服务熔断配置

修改`cloud-provider-hystrix-payment8001`



- 修改`PaymentService`

  添加服务熔断方法，关于熔断的参数配置好，在`HystrixCommandProperties`类中可以找到

  ```java
      /*=======服务熔断=======*/
      /*=======相关的配置都可以在HystrixCommandProperties类中找到=======*/
      @HystrixCommand(fallbackMethod = "paymentCircuitBreaker_fallback", commandProperties = {
              //是否开启断路器
              @HystrixProperty(name = "circuitBreaker.enabled", value = "true"),
              //请求次数
              @HystrixProperty(name = "circuitBreaker.requestVolumeThreshold", value = "10"),
              //时间窗口期，即规定的时间内的操作结果
              @HystrixProperty(name = "circuitBreaker.sleepWindowInMilliseconds", value = "10000"),
              //失败率达到多少后跳闸
              @HystrixProperty(name = "circuitBreaker.errorThresholdPercentage", value = "60")
      })
      public String paymentCircuitBreaker(Integer id) {
          if (id < 0) {
              throw  new RuntimeException("*******id 不能为负数");
          }
  
          String serialNumber = IdUtil.fastSimpleUUID();
          return Thread.currentThread().getName() + "\t" + "调用成功，流水号: " + serialNumber;
      }
  
      /**
       * 熔断的兜底方法
       * 这个跟服务降级是一样的
       * @param id
       * @return
       */
      public String paymentCircuitBreaker_fallback(Integer id) {
          return "id 不能负数，请稍后再试，/(ㄒoㄒ)/~~   id: " +id;
      }
  ```

- `PaymentController`

  ```java
      @GetMapping("/payment/circuit/{id}")
      public String paymentCircuitBreaker(@PathVariable("id") Integer id)
      {
          String result = paymentService.paymentCircuitBreaker(id);
          log.info("****result: "+result);
          return result;
      }
  ```

- 启动服务开始测试

  Eureka7001服务，Hystrix8001服务

  首先正常访问接口，传不同的参数值，正数和负数

  http://localhost:8001/payment/circuit/1

  ![image-20211012222908412](https://cdn.jsdelivr.net/gh/le1024/image1/le/image-20211012222908412.png)

  http://localhost:8001/payment/circuit/-1

  ![image-20211012222929622](https://cdn.jsdelivr.net/gh/le1024/image1/le/image-20211012222929622.png)

  现在，重点测试，，一直多次请求访问错误的，即http://localhost:8001/payment/circuit/-1，使我们的请求错误频率要达到在上面**服务熔断代码@HystrixCommand**里面配置的规则

  在我们多次进行错误访问后，再去请求正确的：http://localhost:8001/payment/circuit/1，会发现正确的也提示错误了，无法正常返回结果

  ![image-20211012224038951](https://cdn.jsdelivr.net/gh/le1024/image1/le/image-20211012224038951.png)

  这时，我们一直访问正确的，慢慢的，就会访问成功了，也就是<strong style="color:red">请求链路恢复</strong>

  ![image-20211012224143862](https://cdn.jsdelivr.net/gh/le1024/image1/le/image-20211012224143862.png)

