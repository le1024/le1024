> SpringCloud

官网：https://spring.io/projects/spring-cloud#learn

Spring Cloud是一系列框架的有序集合。它利用Spring Boot的开发便利性巧妙地简化了分布式系统基础设施的开发，如服务发现注册、配置中心、消息总线、负载均衡、断路器、数据监控等，都可以用Spring Boot的开发风格做到一键启动和部署。



SpringCloud和SpringBoot版本选择：

建议springboot版本选择2.0之后的

https://spring.io/projects/spring-cloud/#overview

![image-20210929215537891](https://cdn.jsdelivr.net/gh/le1024/image1/le/image-20210929215537891.png)



具体版本对应可访问查看：https://start.spring.io/actuator/info

![image-20210929220402662](https://cdn.jsdelivr.net/gh/le1024/image1/le/image-20210929220402662.png)



**本次SpringCloud学习版本环境**

| 服务                    | 版本              |
| ----------------------- | ----------------- |
| **springcloud**         | **Hoxton.SR1**    |
| **springboot**          | **2.2.2RELEASE**  |
| **springcloud alibaba** | **2.1.0.RELEASE** |
| **java**                | **jdk8**          |
| **mave**                | **3.5及以上**     |
| **mysql**               | **5.7及以上**     |



**组件说明：**

![image-20210929225125393](https://cdn.jsdelivr.net/gh/le1024/image1/le/image-20210929225125393.png)

