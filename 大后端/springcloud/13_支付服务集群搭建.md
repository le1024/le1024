> 支付模块服务集群搭建

参考**`cloud-provider-payment8001`**新建**`cloud-provider-payment8002`**，里面的代码，配置都可以复制一份，然后在此基础上进行修改即可。





- 修改**`cloud-provider-payment8002`**yaml

  ```yaml
  #端口改成8002，其余配置不用修改，和8001工程一致
  server:
    port: 8002
  ```

- 修改8001、8002工程的Controller

  ```java
  @Value("${server.port}")
  private String serverPort;
  
  @PostMapping(value = "/payment/create")
  public CommonResult create(@RequestBody Payment payment) {
      int result = paymentService.create(payment);
  
      if (result > 0) {
          return new CommonResult(200, "创建成功，服务端口：" + serverPort, result);
      }
      return new CommonResult(500, "创建失败");
  
  }
  
  @GetMapping("/payment/get/{id}")
  public CommonResult getById(@PathVariable("id") Long id) {
      Payment payment = paymentService.getById(id);
      if (payment != null) {
          return new CommonResult(200, "查询成功，服务端口：" + serverPort, payment);
      }
      return new CommonResult(500, "查询失败,没有对应id记录:" + id);
  
  }
  ```

- 修改80工程

  请求服务地址需要更新成集群方式的

  ```java
      //单机请求，使用集群之后，这里不能写死
      //public static final String PaymentSrv_URL = "http://localhost:8001";
  
      //集群方式，通过在Eureka上注册的微服务名称调用
      //还需要配置RestTemplate负载均衡的能力@LoadBalanced，不然会找不到具体的服务，报UnknownHostException异常
      public static final String PaymentSrv_URL = "http://CLOUD-PAYMENT-SERVICE";
  ```

  `RestTemplate`通过负载均衡的能力

  ```java
  @Configuration
  public class ApplicationContextConfig {
  
      @Bean
      @LoadBalanced //使用@LoadBalanced注解赋予RestTemplate负载均衡的能力
      public RestTemplate restTemplate() {
          return new RestTemplate();
      }
  }
  ```

  

<hr />



测试：

先启动EurekaServer，7001/7002服务 

再启动支付服务，8001/8002服务

再启动订单服务，80服务



**访问注册中心服务7001和7002，确认服务正常**

![image-20211004170705212](https://cdn.jsdelivr.net/gh/le1024/image1/le/image-20211004170705212.png)

**确认8001,8002服务也是正常的，然后通过80服务访问**http://localhost/consumer/payment/get/1

服务返回的端口是变化的，说明支付服务的集群功能搭建完成，且简单实现了负载均衡的功能，默认为轮询的方式

![image-20211004170856699](https://cdn.jsdelivr.net/gh/le1024/image1/le/image-20211004170856699.png)

![image-20211004170917337](https://cdn.jsdelivr.net/gh/le1024/image1/le/image-20211004170917337.png)



**负载均衡Ribbon和服务注册Eureka整合后Consumer可以直接调用服务而不用再关心地址和端口号，且该服务还有负载功能了。**

