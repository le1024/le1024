> Hystrix工作流程

https://github.com/Netflix/Hystrix/wiki/How-it-Works

![image-20211013171529895](https://cdn.jsdelivr.net/gh/le1024/image1/le/image-20211013171529895.png)

![img](https://cdn.jsdelivr.net/gh/le1024/image1/le/528977-20190426084523578-1057721007.png)

<hr>

1. 每次调用创建一个新的HystrixCommand,把依赖调用封装在run()方法中.
2. 执行execute()/queue做同步或异步调用.
3. 当前调用是否已被缓存，是则直接返回结果，否则进入步骤 4
4. 判断熔断器(circuit-breaker)是否打开,如果打开跳到步骤 8,进行降级策略,如果关闭进入步骤 5
5. 判断线程池/队列/信号量是否跑满，如果跑满进入降级步骤8,否则继续后续步骤 6
6. 调用HystrixCommand的run方法.运行依赖逻辑
   - 6.1. 调用是否出现异常，否：继续，是进入步骤8，
   - 6.2. 调用是否超时，否：返回调用结果，是进入步骤8
7. 搜集5、6步骤所有的运行状态(成功, 失败, 拒绝,超时)上报给熔断器，用于统计从而判断熔断器状
8. getFallback()降级逻辑.四种触发getFallback调用情况（图中步骤8的箭头来源）：
   返回执行成功结果

tips：如果我们没有为命令实现降级逻辑或者在降级处理逻辑中抛出了异常， Hystrix 依然会返回一个 Observable 对象， 但是它不会发射任何结果数据， 而是通过 onError 方法通知命令立即中断请求，并通过onError()方法将引起命令失败的异常发送给调用者。