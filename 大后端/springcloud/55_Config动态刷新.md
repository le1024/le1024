> Config动态刷新

还是需要手动操作的，不算完全的自动化刷新



1. **首先config3355客户端需要引入依赖**

   ```xml
   <dependency>
       <groupId>org.springframework.boot</groupId>
       <artifactId>spring-boot-starter-actuator</artifactId>
   </dependency>
   ```

2. **修改yml，暴露监控端口**

   ```yaml
   # 暴露监控端点
   management:
     endpoints:
       web:
         exposure:
           include: "*"
   ```

3. **controller添加新注解：@RefreshScope**

   ```java
   @RefreshScope
   ```

4. **启动eureka7001，config3344，config3355**

5. **修改git配置文件完成后，还需要手动发送一个POST请求，不然config3355是不会正确获取到更新后的配置**

6. **发送POST请求，向客户端发送**

   ```java
   curl -X POST "http://localhost:3355/actuator/refresh"
   ```

7. **再次请求config3355，访问成功**



<strong style="color:green">每次更新配置文件后，虽然不需要重启config客户端服务，但是需要发送一个POST请求来完成配置更新，如果服务过多就需要多次发送</strong>

<strong style="color:orange">后面通过SpringCloud Bus实现配置的动态刷新</strong>

