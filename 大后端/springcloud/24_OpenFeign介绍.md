> OpenFeign介绍



#### OpenFeign是什么

官网解释：
https://cloud.spring.io/spring-cloud-static/Hoxton.SR1/reference/htmlsingle/#spring-cloud-openfeign

https://github.com/spring-cloud/spring-cloud-openfeign

Feign是一个声明式的WebService客户端。使用Feign能让编写WebService客户更加简单。他的使用方法是

<strong style="color:red">定义一个服务接口然后在上面添加注解</strong>

<strong style="color:red">Feign是用在消费端的</strong>

Feign也支持可拔插式的编码器和解码器。Spring Cloud对Feign进行了封装，使其支持了Spring MVC标准注解和HttpMessageConverters。Feign可以与Eureka和Ribbon组合使用以支持负载均衡



#### OpenFeign可以做什么

Feign旨在使编写java http客户端变得更加容易

前面在使用Ribbon+RestTemplate时，利用RestTemplate对http请求的封装处理，形成了一套模板化的调用方法。但是实际开发中，由于对服务依赖的调用可能不止一处，往往一个接口被多出调用，通常都会针对每个微服务自行封装一些客户端类来包装这些依赖服务的调用。

所以，Feign在此基础上做了进一步封装，来定义和实现依赖服务接口的定义。在Feign的实现下，只需要创建一个接口并使用注解的方式来配置它（以前是DAO接口上面标注一个@Mapper注解，现在是一个微服务接口上面标注一个Feign注解即可），即可完成对服务提供方的接口绑定，简化使用SpringCloud的Ribbon，自动封装服务调用客户端的开发量



<strong style="color:blue">Feign集成了Ribbon</strong> 

所以Feign自带负载均衡配置项

利用Ribbon维护了Payment服务列表信息，并且通过轮询实现了客户端的负载均衡。而与Ribbon不同的是，<strong style="color:red">通过Feign只需要定义服务绑定接口且以声明式的方法</strong>，优雅而又简单的实现了服务调用



#### Feign和OpenFeign区别

**Feign：**

Feign是Spring Cloud组件中的一个轻量级RESTful的HTTP服务客户端

Feign内置了Ribbon，用来做客户端负载均衡，去调用服务注册中心的服务。Feign的使用方式是：使用Feign的注解定义接口，调用这个接口，就可以调用服务注册中心的服务

```xml
<dependency>
    <groupId>org.springframework.cloud</groupId>
    <artifactId>spring-cloud-starter-feign</artifactId>
</dependency>
```



**OpenFeign**：

OpenFeign是Spring Cloud 在Feign的基础上支持了SpringMVC的注解，如@RequesMapping等等

OpenFeign的@FeignClient可以解析SpringMVC的@RequestMapping注解下的接口，并通过动态代理的方式产生实现类，实现类中做负载均衡并调用其他服务。

```xml
<dependency>
    <groupId>org.springframework.cloud</groupId>
    <artifactId>spring-cloud-starter-openfeign</artifactId>
</dependency>
```

