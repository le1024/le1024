> Ribbon核心组件IRule

#### IRule

IRule：根据特定算法中从服务列表中选取一个要访问的服务

Ribbon默认的规则是轮询

![image-20211007204428789](https://cdn.jsdelivr.net/gh/le1024/image1/le/image-20211007204428789.png)



>  **com.netflix.loadbalancer.RoundRobinRule**

轮询



>  **com.netflix.loadbalancer.RandomRule**

随机



>  **com.netflix.loadbalancer.RetryRule**

先按照RoundRobinRule的策略获取服务，如果获取服务失败则在指定时间内会进行重试，获取可用的服务



> **WeightedResponseTimeRule**

对RoundRobinRule的扩展，响应速度越快的实例选择权重越大，越容易被选择



> **BestAvailableRule**

会先过滤掉由于多次访问故障而处于断路器跳闸状态的服务，然后选择一个并发量最小的服务



> **AvailabilityFilteringRule**

先过滤掉故障实例，再选择并发较小的实例



> **ZoneAvoidanceRule**

默认规则,复合判断server所在区域的性能和server的可用性选择服务器



#### 替换Ribbon规则

修改**`cloud-consumer-order80`**

注意：

<strong style="color:red">官方文档明确给出了警告：
这个自定义配置类不能放在@ComponentScan所扫描的当前包下以及子包下，
否则我们自定义的这个配置类就会被所有的Ribbon客户端所共享，达不到特殊化定制的目的了。</strong>

顾名思义就是不能放在跟主启动类同一个包下



- 新建package，`com.myrule`

- 新建规则类，`MySelfRule`

  ```java
  @Configuration
  public class MySelfRule {
  
      @Bean
      public IRule mySelfRule() {
          return new RandomRule(); //定义为随机
      }
  }
  ```

- 主启动类添加注解`@RibbonClient`

  ```java
  @RibbonClient(name = "CLOUD-PAYMENT-SERVICE",configuration=MySelfRule.class)
  ```

- 测试访问，返回信息不再是轮询了，而是随机的方式返回

  启动Eureka7001就行，不用集群，再启动生产者8001/8002服务，再启动消费者80服务

  ![image-20211007210032035](https://cdn.jsdelivr.net/gh/le1024/image1/le/image-20211007210032035.png)

  

