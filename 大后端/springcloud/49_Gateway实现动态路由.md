> Gateway实现动态路由

前面实现的路由访问，在`yml`里面是写死的配置，实际中服务可能会有多个，

![image-20211014223809366](https://cdn.jsdelivr.net/gh/le1024/image1/le/image-20211014223809366.png)



默认情况下Gateway会根据注册中心注册的服务列表，
以注册中心上<strong style="color:red">微服务名</strong>为路径创建<strong style="color:red">动态路由进行转发，从而实现动态路由的功能</strong>

![image-20211014225551901](https://cdn.jsdelivr.net/gh/le1024/image1/le/image-20211014225551901.png)

```yaml
server:
  port: 9527

spring:
  application:
    name: cloud-gateway
  #网关配置
  cloud:
    gateway:
      discovery:
        locator:
          enabled: true              #开启从注册中心动态创建路由的功能，利用微服务名称进行路由
      routes:
        - id: payment_route          #路由id，没有固定规则但要求唯一，建议配合服务名
          #uri: http://localhost:8001 #匹配后提供服务的路由地址
          uri: lb://cloud-payment-service #路由地址配置成微服务名称
          predicates:
            - Path=/payment/get/**   #断言，路径相匹配的进行路由
        - id: payment_route2         #路由id，没有固定规则但要求唯一，建议配合服务名
          #uri: http://localhost:8001 #匹配后提供服务的路由地址
          uri: lb://cloud-payment-service #路由地址配置成微服务名称
          predicates:
            - Path=/payment/lb/**    #断言，路径相匹配的进行路由


#网关需要注册到服务中心
eureka:
  client:
    register-with-eureka: true
    fetch-registry: true
    service-url:
      defaultZone: http://eureka7001.com:7001/eureka/
  instance:
    hostname: cloud-gateway-service

```



**需要注意的是uri的协议为lb，表示启用Gateway的负载均衡功能。**



启动Eureka7001，payment8001/8002，gateway9527测试：

访问：http://localhost:9527/payment/lb

结果：8001/8002两个端口切换