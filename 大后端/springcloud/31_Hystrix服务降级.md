> Hystrix服务降级

Hystrix服务降级，需要配置<strong style="color:green">@HystrixCommand</strong>注解



#### `hystrix8001`生产端配置服务降级：

设置自身调用超时时间峰值，峰值内可以正常运行，超过了需要规定时间，则需要有兜底的方法处理，作服务降级fallback

- **修改业务类**

  修改service里原先的`paymentInfo_TimeOut`方法

  ```java
      /**
       * 超时访问，演示服务降级
       * @param id
       * @return
       */
      @HystrixCommand(
              //配置兜底方法，一旦当前服务方法调用失败，就会调用paymentInfo_TimeOutHandler
              fallbackMethod = "paymentInfo_TimeOutHandler",
              commandProperties = {
                      //配置调用的时间峰值，当前配置为3秒
                      @HystrixProperty(name="execution.isolation.thread.timeoutInMilliseconds", value = "3000")
              }
      )
      public String paymentInfo_TimeOut(Integer id) {
          try {
              //线程暂停了5秒，超过了上面配置的3秒，所以肯定会出异常，直接调用兜底方法
              TimeUnit.SECONDS.sleep(5);
          } catch (InterruptedException e) {
              e.printStackTrace();
          }
          return "线程池:"+Thread.currentThread().getName()+"paymentInfo_TimeOut,id: "+id+"\t"+"O(∩_∩)O，耗费3秒";
      }
  
      /**
       * paymentInfo_TimeOut方法失败的兜底方案
       * 通过这个方法返回友好的提示信息
       * 注意一点：该方法需要通主方法的入参要对应上
       * @return
       */
      public String paymentInfo_TimeOutHandler(Integer id) {
          return "服务请求失败，请稍后再试~~~";
      }
  ```

  

- **主启动类添加新注解**

  <strong style="color:green">@EnableCircuitBreaker</strong>

- **测试**

  启动Eureka7001，Hystrix8001

  访问：http://localhost:8001/payment/hystrix/timeout/1

  ![image-20211011215958374](https://cdn.jsdelivr.net/gh/le1024/image1/le/image-20211011215958374.png)

------



#### `hystrix80`消费端配置服务降级：

同生产端配置一样，添加兜底方法

- **修改yml**

  ```properties
  feign:
    hystrix:
      enabled: true
  ```

  

- **主启动类添加注解**

  同生产的的不一样，是一个新注解：<strong style="color:green">@EnableHystrix</strong>

- **业务类**

  跟生产的一样

  ```java
      @GetMapping("/consumer/payment/hystrix/timeout/{id}")
      @HystrixCommand(fallbackMethod = "paymentTimeoutHandler", commandProperties = {
              @HystrixProperty(name="execution.isolation.thread.timeoutInMilliseconds", value="1500")
      })
      public String paymentInfo_Timeout(@PathVariable("id") Integer id) {
  
          //int i = 1/0; //异常也会进行fallback
          return paymentHystrixService.paymentInfo_Timeout(id);
      }
  
      public String paymentTimeoutHandler(@PathVariable("id") Integer id)
      {
          return "我是消费者80,对方支付系统繁忙请10秒钟后再试或者自己运行出错请检查自己,o(╥﹏╥)o";
      }
  ```

- **测试**

  访问：http://localhost/consumer/payment/hystrix/timeout/1

  ![image-20211011231718826](https://cdn.jsdelivr.net/gh/le1024/image1/le/image-20211011231718826.png)





总结：

<strong style="color:orange">当服务不可用时，出异常或者调用超时，都会走兜底方法</strong>

