> Zuul过滤器

<font color="coral">过滤功能负责对请求过程进行额外的处理，是请求校验过滤及服务聚合的基础。</font>

过滤器生命周期：

![image-20211014113855381](https://cdn.jsdelivr.net/gh/le1024/image1/le/image-20211014113855381.png)

#### Zuul Filter

##### 过滤类型

`pre`：在请求被路由到目标服务前执行，比如权限校验，日志打印等

`routing`：在请求被路由到目标服务时执行

`post`：在请求被路由到目标服务后执行，比如给目标服务的响应添加头参数，收集统计数据等

`error`：请求在其他阶段发生错误时执行



##### 过滤顺序

设置的数字越小越先执行

```java
@Override
public int filterOrder()
{
    return 1;
}
```



##### 开启过滤

设置`true`开启

```java
@Override
public boolean shouldFilter()
{
    return true;
}
```



##### 执行逻辑

实现自己的业务逻辑





#### 实现

前置过滤器，用于在请求路由到目标服务前打印请求日志

##### 业务代码

```java
@Component
@Slf4j
public class PreLogFilter extends ZuulFilter {

    /**
     * 过滤器类型
     * @return
     */
    @Override
    public String filterType() {
        return "pre";
    }

    /**
     * 值越小越先执行
     * @return
     */
    @Override
    public int filterOrder() {
        return 1;
    }

    /**
     * true开启过滤器
     * @return
     */
    @Override
    public boolean shouldFilter() {
        return true;
    }

    /**
     * 执行逻辑，打印日志
     * @return
     * @throws ZuulException
     */
    @Override
    public Object run() throws ZuulException {
        RequestContext requestContext = RequestContext.getCurrentContext();
        HttpServletRequest request = requestContext.getRequest();
        String host = request.getRemoteHost();
        String method = request.getMethod();
        String uri = request.getRequestURI();
        log.info("=====> Remote host:{},method:{},uri:{}", host, method, uri);
        System.out.println("********"+new Date().getTime());
        return null;

    }
}
```



##### yml配置

配置文件中，true禁用

```yaml
zuul:
  #添加过滤器
  PreLogFilter:
    pre:
      disable: false
```



##### 测试

访问请求，控制台会打印日志

![image-20211014145609443](https://cdn.jsdelivr.net/gh/le1024/image1/le/image-20211014145609443.png)

