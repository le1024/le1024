> Eureka自我保护机制

概述
保护模式主要用于一组客户端和Eureka Server之间存在网络分区场景下的保护。一旦进入保护模式，
<font color="red">Eureka Server将会尝试保护其服务注册表中的信息，不再删除服务注册表中的数据，也就是不会注销任何微服务。</font>



在EurekaServer的首页看到以下这段提示，则说明Eureka进入了保护模式：

![image-20211004203435401](https://cdn.jsdelivr.net/gh/le1024/image1/le/image-20211004203435401.png)



简而言之：

<strong style="color:red">当某个时刻，某个微服务不可用了，Eureka不会立刻清理，依旧会对该微服务的信息进行保存</strong>

<strong style="color:green">属于CAP里面的AP分支</strong>

*CAP*原则又称*CAP*定理，指的是在一个分布式系统中，一致性（Consistency）、可用性（Availability）、分区容错性（Partition tolerance）。*CAP* 原则指的是，这三个要素最多只能同时实现两点，不可能三者兼顾。



#### 什么是自我保护模式？

默认情况下，如果EurekaServer在一定时间内没有接收到某个微服务实例的心跳，EurekaServer将会注销该实例（默认90秒）。但是当网络分区故障发生(延时、卡顿、拥挤)时，微服务与EurekaServer之间无法正常通信，以上行为可能变得非常危险了——因为微服务本身其实是健康的，此时本不应该注销这个微服务。Eureka通过“自我保护模式”来解决这个问题——当EurekaServer节点在短时间内丢失过多客户端时（可能发生了网络分区故障），那么这个节点就会进入自我保护模式。


在自我保护模式中，Eureka Server会保护服务注册表中的信息，不再注销任何服务实例。
它的设计哲学就是宁可保留错误的服务注册信息，也不盲目注销任何可能健康的服务实例。一句话讲解：好死不如赖活着

综上，自我保护模式是一种应对网络异常的安全保护措施。它的架构哲学是宁可同时保留所有微服务（健康的微服务和不健康的微服务都会保留）也不盲目注销任何健康的微服务。使用自我保护模式，可以让Eureka集群更加的健壮、稳定。



#### 禁止自我保护

`自我保护机制默认是开启的`

```properties
eureka.server.enable-self-preservation=true
```

设置为`false`，关闭自我保护机制

修改7001服务：

```properties
#EurekaServer配置
eureka:
  instance:
    hostname: eureka7001.com #eureka服务端的实例名称
  client:
    register-with-eureka: false #false表示不向注册中心注册自己，当前服务不需要注册，只关注业务服务就行
    fetch-registry: false #false表示自己就是注册中心，职责就是维护服务实例，并不需要去检索服务
    service-url:
      #设置与Eureka Server交互的地址查询服务和注册服务都需要依赖这个地址。
      #相互注册，相互守望，这里配置另外的EurekaSever地址，集群配置，多个服务地址用英文逗号隔开
      defaultZone: http://eureka7002.com:7002/eureka/,http://eureka7003.com:7003/eureka/
  server:
    #关闭自我保护机制，保证不可用服务被及时踢除
    enable-self-preservation: false
    #清理无效服务间隔时间，单位毫秒
    eviction-interval-timer-in-ms: 2000
```

启动7001服务：提示自我保护机制已经关闭

![image-20211004210332254](https://cdn.jsdelivr.net/gh/le1024/image1/le/image-20211004210332254.png)





修改8001服务：

```properties
#Eureka客户端向服务端发送心跳的时间间隔，单位为秒(默认是30秒)
eureka.instance.lease-renewal-interval-in-seconds=30
#Eureka服务端在收到最后一次心跳后等待时间上限，单位为秒(默认是90秒)，超时将剔除服务
eureka.instance.lease-expiration-duration-in-seconds=90
```

```properties
#eureka配置
eureka:
  client:
    #表示是否将自己注册进EurekaServer，默认true
    register-with-eureka: true
    #是否从EurekaServer抓取已有的注册信息，默认为true。单节点无所谓，集群必须设置为true才能配合ribbon使用负载均衡
    fetch-registry: true
    service-url:
      defaultZone: http://eureka7001.com:7001/eureka
      #EurekaServer地址，集群配置，多个地址以英文逗号隔开
      #defaultZone: http://eureka7001.com:7001/eureka,http://eureka7002.com:7002/eureka
  instance:
    #微服务主机名称
    instance-id: payment8001
    #访问路径可以显示IP地址
    prefer-ip-address: true
    #Eureka客户端向服务端发送心跳的时间间隔，单位为秒(默认是30秒)
    lease-renewal-interval-in-seconds: 1
    #Eureka服务端在收到最后一次心跳后等待时间上限，单位为秒(默认是90秒)，超时将剔除服务
    lease-expiration-duration-in-seconds: 2
```

再启动8001服务，先看看是否注册进了服务中心

![image-20211004210814842](https://cdn.jsdelivr.net/gh/le1024/image1/le/image-20211004210814842.png)

然后停掉8001服务，再刷新页面看看服务中心是否把8001服务剔除了

![image-20211004210916890](https://cdn.jsdelivr.net/gh/le1024/image1/le/image-20211004210916890.png)