> @DefaultProperties 全局服务降级

之前是每个业务方法对应一个兜底方法fallback，实际开发中，业务方法量庞大，会造成代码膨胀

通过`@DefaultProperties(defaultFallback = "")`实现全局配置，统一处理，只需定义`@HystrixCommand`注解即可，个别重要业务单独定义`fallbackMethod`实现兜底方法，从而实现通用和独享的分开，减少代码量，避免代码膨胀



修改`cloud-consumer-feign-hystrix-order80`代码

**controller**

添加新注解：**@DefaultProperties(defaultFallback = "payment_Global_FallbackMethod")**指定统一处理方法

**@HystrixCommand**改为单注解即可

```java
@RestController
//HystrixCommand未指定具体方法时，就调用这个统一的方法
@DefaultProperties(defaultFallback = "payment_Global_FallbackMethod")
public class OrderHystrixController {

    @Resource
    private PaymentHystrixService paymentHystrixService;

    @GetMapping("/consumer/payment/hystrix/ok/{id}")
    public String paymentInfo_OK(@PathVariable("id") Integer id) {
        return paymentHystrixService.paymentInfo_OK(id);

    }

    @GetMapping("/consumer/payment/hystrix/timeout/{id}")
//    @HystrixCommand(fallbackMethod = "paymentTimeoutHandler", commandProperties = {
//            @HystrixProperty(name="execution.isolation.thread.timeoutInMilliseconds", value="1500")
//    })
    @HystrixCommand //加了@DefaultProperties属性注解，并且没有写具体方法名字，就用统一全局的
    public String paymentInfo_Timeout(@PathVariable("id") Integer id) {

        int i = 1/0; //异常也会进行fallback
        return paymentHystrixService.paymentInfo_Timeout(id);
    }

    public String paymentTimeoutHandler(@PathVariable("id") Integer id)
    {
        return "我是消费者80,对方支付系统繁忙请10秒钟后再试或者自己运行出错请检查自己,o(╥﹏╥)o";
    }

    public String payment_Global_FallbackMethod() {
        return "Hystrix服务降级全局处理，服务异常，请稍后再试！！";
    }
}
```

![image-20211012145536010](https://cdn.jsdelivr.net/gh/le1024/image1/le/image-20211012145536010.png)



开启Eureka7001，Hystrix8001，Hystrix80服务测试

![image-20211012145615466](https://cdn.jsdelivr.net/gh/le1024/image1/le/image-20211012145615466.png)