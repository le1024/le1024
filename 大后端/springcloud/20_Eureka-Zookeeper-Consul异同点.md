> Eureka Zookeeper Consul的异同点

由于Eureka已经停止更新了，Zookeeper和Consul可以作为替代的注册中心，当然还有Nacos。



![image-20211006232811551](https://cdn.jsdelivr.net/gh/le1024/image1/le/image-20211006232811551.png)



CAP理论关注粒度是数据，而不是整体系统设计的策略

**C:Consistency（强一致性）**

**A:Availability（可用性）**

**P:Partition tolerance（分区容错性）**



##### AP(Eureka)

AP架构
当网络分区出现后，为了保证可用性，系统B可以返回旧值，保证系统的可用性。
<strong style="color:red">结论：违背了一致性C的要求，只满足可用性A和分区容错P，即AP</strong>

![image-20211006232951004](https://cdn.jsdelivr.net/gh/le1024/image1/le/image-20211006232951004.png)



##### CP(Zookeeper/Consul)

CP架构
当网络分区出现后，为了保证一致性，就必须拒接请求，否则无法保证一致性
<strong style="color:red">结论：违背了可用性A的要求，只满足一致性C和分区容错P，即CP</strong>

![image-20211006232912210](https://cdn.jsdelivr.net/gh/le1024/image1/le/image-20211006232912210.png)
