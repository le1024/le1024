> Gateway编码实现路由

在[Gateway服务配置](./47_Gateway服务配置)是通过`yml`配置方式实现的路由配置

还可以在代码中进行编码实现



通过9527网关实现访问外网的百度新闻网址：http://news.baidu.com/guonei

修改服务`cloud-gateway-gateway9527`，代码中注入RouteLocator的Bean

```java
@Configuration
public class GateWayConfig {

    /**
     * 配置一个id为route-name的路由规则，多个路由id不能重复
     * 当访问地址：http://localhost:9527/guonei时会自动转发地址
     * @param builder
     * @return
     */
    @Bean
    public RouteLocator customRouteLocator(RouteLocatorBuilder builder) {
        RouteLocatorBuilder.Builder routes = builder.routes();

        routes
                .route("path_route_hll", r -> r.path("/guonei").uri("https://news.baidu.com/guonei"))
                .route("path_route_hll2", r -> r.path("/guoji").uri("https://news.baidu.com/guoji"))
                .build();

        return routes.build();

    }
}
```



访问：http://localhost:9527/guoji，http://localhost:9527/guonei

![image-20211014223630204](https://cdn.jsdelivr.net/gh/le1024/image1/le/image-20211014223630204.png)

