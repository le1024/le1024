> Gateway服务配置



- **新建服务**`cloud-gateway-gateway9527`

- **pom**

  ```xml-dtd
  <!--不要加web的依赖，不然会报错，这是一个网关服务，用不到web-->
      <dependencies>
          <!--gateway-->
          <dependency>
              <groupId>org.springframework.cloud</groupId>
              <artifactId>spring-cloud-starter-gateway</artifactId>
          </dependency>
          <!--eureka-client-->
          <dependency>
              <groupId>org.springframework.cloud</groupId>
              <artifactId>spring-cloud-starter-netflix-eureka-client</artifactId>
          </dependency>
          <!-- 引入自己定义的api通用包，可以使用Payment支付Entity -->
          <dependency>
              <groupId>app.springcloud</groupId>
              <artifactId>cloud-api-commons</artifactId>
              <version>${project.version}</version>
          </dependency>
          <!--一般基础配置类-->
          <dependency>
              <groupId>org.springframework.boot</groupId>
              <artifactId>spring-boot-devtools</artifactId>
              <scope>runtime</scope>
              <optional>true</optional>
          </dependency>
          <dependency>
              <groupId>org.projectlombok</groupId>
              <artifactId>lombok</artifactId>
              <optional>true</optional>
          </dependency>
          <dependency>
              <groupId>org.springframework.boot</groupId>
              <artifactId>spring-boot-starter-test</artifactId>
              <scope>test</scope>
          </dependency>
      </dependencies>
  ```

  

- **yml**

  ```yaml
  server:
    port: 9527
  
  spring:
    application:
      name: cloud-gateway
    #网关配置
    cloud:
      gateway:
        routes:
          - id: payment_route          #路由id，没有固定规则但要求唯一，建议配合服务名
            uri: http://localhost:8001 #匹配后提供服务的路由地址
            predicates:
              - Path=/payment/get/**   #断言，路径相匹配的进行路由
          - id: payment_route2         #路由id，没有固定规则但要求唯一，建议配合服务名
            uri: http://localhost:8001 #匹配后提供服务的路由地址
            predicates:
              - Path=/payment/lb/**    #断言，路径相匹配的进行路由
  
  
  #网关需要注册到服务中心
  eureka:
    client:
      register-with-eureka: true
      fetch-registry: true
      service-url:
        defaultZone: http://eureka7001.com:7001/eureka/
    instance:
      hostname: cloud-gateway-service
  
  ```

  ![image-20211014222004412](https://cdn.jsdelivr.net/gh/le1024/image1/le/image-20211014222004412.png)

- **主启动类**

  ```java
  @SpringBootApplication
  @EnableEurekaClient
  public class GatewayMain9527 {
  
      public static void main(String[] args) {
          SpringApplication.run(GatewayMain9527.class, args);
      }
  }
  ```

  

- **路由映射**

  `cloud-provider-payment8001`看看controller的访问地址

  1.http://localhost:8001/payment/get/{id}

  2.http://localhost:8001/payment/lb

  对于这两个接口通过网关服务去实现访问：

  1.http://localhost:9527/payment/get/{id}

  2.http://localhost:9527/payment/lb

  

- **测试**

  启动Eureka7001，启动cloud-provider-payment8001，启动gateway9527
  
  非网关访问：http://localhost:8001/payment/get/1
  
  网关访问：http://localhost:9527/payment/get/1
