> Hystrix服务熔断

Hystrix断路器简单的来说就是家里电源的保险丝

<strong style="color:red">熔断机制的注解是@HystrixCommand。</strong>



#### <strong style="color:blue">熔断机制概述</strong>

熔断机制是应对雪崩效应的一种微服务链路保护机制。当扇出链路的某个微服务发生故障不可用或者响应时间太长时，会进行服务降级，进而熔断该节点的服务调用，快速返回错误的提示信息。

当检测到该节点微服务调用正常响应之后，恢复调用链路。

在springcloud框架里，熔断机制通过Hystrix实现。Hystrix会监控微服务间的调用状态，当失败达到一定的阈值，**缺省是5秒内20次调用失败**，就会启动熔断机制。<strong style="color:red">熔断机制的注解是@HystrixCommand。</strong>





https://martinfowler.com/bliki/CircuitBreaker.html



理解这个图，重要！！！

在熔断机制中，服务正常时，断路器一开始是`closed`的状态，同电路保险丝一样，是闭合的才能通电。

当我们的服务出现大规模的故障时，也就是在一定时间内有一定的请求失败率，这时候断路器的状态就由`closed`变为`Half Open`状态，意思就是某些错误请求响应为异常，但还有正常的请求是能正常访问的，但是当错误的请求越来越多，即请求的失败率越来越高，这时候断路器就会转为`open`状态，导致我们现在访问正常的请求也会出错，从而影响到整个服务请求

后面再随着正确的请求越来越多，服务会慢慢的恢复，最后再次回到`closed`状态，保证服务可用，也就是恢复链路

![image-20211012214612332](https://cdn.jsdelivr.net/gh/le1024/image1/le/image-20211012214612332.png)