> Config服务端配置

#### 准备工作

1.首先需要在GitHub或者Gitee上创建一个仓库，由于GitHub访问有点慢，这里用Gitee来进行开发。

2.登录自己的gitee帐号，在gitee上面创建一个配置仓库，仓库根据自己的来填写

![image-20211015144254417](https://cdn.jsdelivr.net/gh/le1024/image1/le/image-20211015144254417.png)



3.创建完成，复制仓库地址：`git@gitee.com:le1024/springcloud-config.git`

4.在本地硬盘目录上clone该仓库

![image-20211015144539046](https://cdn.jsdelivr.net/gh/le1024/image1/le/image-20211015144539046.png)

5.往仓库准备三个配置文件：`config-dev.yml`、`config-pro.yml`、`config-test.yml`，可以直接在页面仓库里面添加，也是本地添加好后提交到仓库，配置文件内容在测试时可以区分开不同文件就行

6.再创建一个`dev`分支，同样创建三个配置文件

7.最后在gitee仓库可以看到两个分支，每个分支下面都有配置文件

![image-20211015151035715](https://cdn.jsdelivr.net/gh/le1024/image1/le/image-20211015151035715.png)





#### 创建工程服务

新建Module模块`cloud-config-center-3344`，为Cloud的配置中心模块cloudConfig Center

1. **pom**

   ```xml
   <dependencies>
           <!--config server-->
           <dependency>
               <groupId>org.springframework.cloud</groupId>
               <artifactId>spring-cloud-config-server</artifactId>
           </dependency>
           <dependency>
               <groupId>org.springframework.cloud</groupId>
               <artifactId>spring-cloud-starter-netflix-eureka-client</artifactId>
           </dependency>
           <dependency>
               <groupId>org.springframework.boot</groupId>
               <artifactId>spring-boot-starter-web</artifactId>
           </dependency>
   
           <dependency>
               <groupId>org.springframework.boot</groupId>
               <artifactId>spring-boot-starter-actuator</artifactId>
           </dependency>
           <dependency>
               <groupId>org.springframework.boot</groupId>
               <artifactId>spring-boot-devtools</artifactId>
               <scope>runtime</scope>
               <optional>true</optional>
           </dependency>
           <dependency>
               <groupId>org.projectlombok</groupId>
               <artifactId>lombok</artifactId>
               <optional>true</optional>
           </dependency>
           <dependency>
               <groupId>org.springframework.boot</groupId>
               <artifactId>spring-boot-starter-test</artifactId>
               <scope>test</scope>
           </dependency>
       </dependencies>
   ```

   

2. **application.yml**

   ```yaml
   server:
     port: 3344
   
   spring:
     application:
       name: cloud-config-center #注册进Eureka服务的微服务名
     cloud:
       config:
         server:
           git:
             uri: https://gitee.com/le1024/springcloud-config.git #在Gitee或者GitHub创建的仓库
             #搜索目录
             search-paths:
               - springcloud-config
         #读取分支
         label: master
   eureka:
     client:
       fetch-registry: true
       register-with-eureka: true
       service-url:
         defaultZone: http://eureka7001.com:7001/eureka/
   
   ```

3. **启动类**

   添加新注解：`@EnableConfigServer`

   ```java
   @SpringBootApplication
   @EnableConfigServer
   public class ConfigCenterMain3344 {
   
       public static void main(String[] args) {
           SpringApplication.run(ConfigCenterMain3344.class, args);
       }
   }
   ```

4. **修改hosts文件，不改也行，用localhost访问**

   ```bash
   127.0.0.1  config3344.com
   ```



#### 测试服务

启动Eureka7001，config3344

访问：http://config3344.com:3344/master/config-dev.yml

![image-20211015160632060](https://cdn.jsdelivr.net/gh/le1024/image1/le/image-20211015160632060.png)

同样也可以访问到另外的文件：

![image-20211015160707819](https://cdn.jsdelivr.net/gh/le1024/image1/le/image-20211015160707819.png)



#### 读取规则

从访问链接：http://config3344.com:3344/master/config-dev.yml中可以看出，`master`就是仓库的分支，`config-dev.yml`是具体的文件名

所以根据读取配置规则，主要列出以下三种格式，其余的可以参考官网

<strong style="color:orange">/{label}/{application}-{profile}.yml</strong>

```http
master对应的访问链接为：
http://config3344.com:3344/master/config-prod.yml
http://config3344.com:3344/master/config-test.yml
http://config3344.com:3344/master/config-prod.yml
```

```http
dev对应的访问链接为：
http://config3344.com:3344/dev/config-prod.yml
http://config3344.com:3344/dev/config-test.yml
http://config3344.com:3344/dev/config-prod.yml
```



<strong style="color:orange">/{application}-{profile}.yml</strong>

该规则访问yml配置的label

![image-20211015161514523](https://cdn.jsdelivr.net/gh/le1024/image1/le/image-20211015161514523.png)

```http
访问如下：
http://config3344.com:3344/config-dev.yml
http://config3344.com:3344/config-test.yml
http://config3344.com:3344/config-pro.yml
```



<strong style="color:orange">/{application}/{profile}[/{label}]</strong>

```http
master对应访问如下：
http://config3344.com:3344/config/dev/master
```

```http
dev对应访问如下：
http://config3344.com:3344/config/dev/dev
```



**label：**分支

**name：**服务名

**profile：**环境(dev/test/prod)

