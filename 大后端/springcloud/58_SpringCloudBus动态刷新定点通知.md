> SpringCloud Bus动态刷新定点通知

**广播通知是通知了所有的config客户端，都刷新了配置文件数据**

如果只需要通知某个客户端而不是所有的客户端，需要在发送给服务端的POST请求时，指定特定的客户端服务实例



请求格式：

http://localhost:配置中心的端口号/actuator/bus-refresh/<font color="red">{destination}</font>

`/bus/refresh请求`不再发送到具体的服务实例上，而是发给config server并通过`destination`参数类指定需要更新配置的服务或实例

**destination**：这个参数值是Yml配置的`spring.application.name`:`server.port`



我们这里以刷新运行在3355端口上的config-client为例：

只通知config3355，不通知config3366

访问config3344：当前version=5

![image-20211018173702028](https://cdn.jsdelivr.net/gh/le1024/image1/le/image-20211018173702028.png)

现在发起定点通知请求：

```bash
curl -X POST "http://localhost:3344/actuator/bus-refresh/cloud-config-client:3355"
```

![image-20211018174131501](https://cdn.jsdelivr.net/gh/le1024/image1/le/image-20211018174131501.png)

再去刷新界面：![image-20211018174208721](https://cdn.jsdelivr.net/gh/le1024/image1/le/image-20211018174208721.png)

![image-20211018174233404](https://cdn.jsdelivr.net/gh/le1024/image1/le/image-20211018174233404.png)

