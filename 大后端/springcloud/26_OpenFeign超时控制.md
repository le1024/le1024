> OpenFeign超时控制



#### 模拟超时出错

- **服务方8001写超时程序**

  为了测试方便，只用8001一个服务就行了

  ```java
  @GetMapping("/payment/feign/timeout")
  public String paymentFeignTimeout() {
      try {
          //暂停线程3秒
          TimeUnit.SECONDS.sleep(3);
      } catch (InterruptedException e) {
          e.printStackTrace();
      }
  
      return serverPort;
  }
  ```

  

- **feign服务消费方80`PaymentFeignService`添加超时方法**

  ```java
  @GetMapping("/payment/feign/timeout")
  String paymentFeignTimeout();
  ```

  

- **feign服务消费方80`OrderFeignController`添加超时方法**

  ```java
  @GetMapping("/consumer/payment/feign/timeout")
  public String paymentFeignTimeout() {
      return paymentFeignService.paymentFeignTimeout();
  }
  ```

- **测试**

  1.先测试8001服务是正常的：3秒后返回端口

  http://localhost:8001/payment/feign/timeout

  ![image-20211008230633951](https://cdn.jsdelivr.net/gh/le1024/image1/le/image-20211008230633951.png)

  2.访问feign服务

  http://localhost/consumer/payment/feign/timeout

  **异常报错，提示超时**

  ![image-20211008230744457](https://cdn.jsdelivr.net/gh/le1024/image1/le/image-20211008230744457.png)



#### 原因及解决

<strong style="color:red">OpenFeign默认等待1秒钟，超过后报错 </strong>

默认Feign客户端只等待1秒钟，但是服务端处理可能需要超过1秒钟，导致Feign客户端不想等待了，直接返回报错。

为了避免这种情况，有时候需要在`yaml`文件中设置Feign客户端超时控制。

```properties
#设置feign客户端超时时间(OpenFeign默认支持ribbon)
ribbon:
#指的是建立连接所用的时间，适用于网络状况正常的情况下,两端连接所用的时间
  ReadTimeout: 5000
#指的是建立连接后从服务器读取到可用资源所用的时间
  ConnectTimeout: 5000
```



配置好了，重新访问，正常

![image-20211008231123503](https://cdn.jsdelivr.net/gh/le1024/image1/le/image-20211008231123503.png)