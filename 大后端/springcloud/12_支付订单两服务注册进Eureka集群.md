> 支付服务，订单服务注册进Eureka集群

将前面的工程服务，支付模块服务，订单模块服务注册进Eureka集群。这里只注册到`Eureka7001`和`Eureka7002`，开太多服务电脑会有点卡，就不往`Eureka7003`注册了。



- **将支付服务8001微服务发布到上面2台Eureka集群配置中**

  修改`yml`即可：

  ```yaml
  defaultZone: http://eureka7001.com:7001/eureka,http://eureka7002.com:7002/eureka
  ```

- **将订单服务80微服务发布到上面2台Eureka集群配置中**

  ```yaml
  defaultZone: http://eureka7001.com:7001/eureka,http://eureka7002.com:7002/eureka
  ```

  

- **开始测试**

  先启动EurekaServer，7001/7002服务

  再启动支付服务8001

  再启动订单服务80

  访问：http://eureka7001.com:7001或者http://eureka7002.com:7002，两个微服务成功注册到注册中心

  ![image-20211003221612965](https://cdn.jsdelivr.net/gh/le1024/image1/le/image-20211003221612965.png)

  再访问：http://localhost/consumer/payment/get/1

  ![image-20211003221952079](https://cdn.jsdelivr.net/gh/le1024/image1/le/image-20211003221952079.png)