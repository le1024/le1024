> Hystrix服务搭建

先搭建一个初始的Hystrix服务工程



#### 初始生产工程搭建

- **新建`cloud-provider-hystrix-payment8001`**

- **修改pom**

  ```xml
  <dependencies>
          <!--hystrix-->
          <dependency>
              <groupId>org.springframework.cloud</groupId>
              <artifactId>spring-cloud-starter-netflix-hystrix</artifactId>
          </dependency>
          <!--eureka client-->
          <dependency>
              <groupId>org.springframework.cloud</groupId>
              <artifactId>spring-cloud-starter-netflix-eureka-client</artifactId>
          </dependency>
          <!--web-->
          <dependency>
              <groupId>org.springframework.boot</groupId>
              <artifactId>spring-boot-starter-web</artifactId>
          </dependency>
          <dependency>
              <groupId>org.springframework.boot</groupId>
              <artifactId>spring-boot-starter-actuator</artifactId>
          </dependency>
          <dependency><!-- 引入自己定义的api通用包，可以使用Payment支付Entity -->
              <groupId>app.springcloud</groupId>
              <artifactId>cloud-api-commons</artifactId>
              <version>${project.version}</version>
          </dependency>
          <dependency>
              <groupId>org.springframework.boot</groupId>
              <artifactId>spring-boot-devtools</artifactId>
              <scope>runtime</scope>
              <optional>true</optional>
          </dependency>
          <dependency>
              <groupId>org.projectlombok</groupId>
              <artifactId>lombok</artifactId>
              <optional>true</optional>
          </dependency>
          <dependency>
              <groupId>org.springframework.boot</groupId>
              <artifactId>spring-boot-starter-test</artifactId>
              <scope>test</scope>
          </dependency>
      </dependencies>
  ```

  

- **配置yml**

  ```properties
  server:
    port: 8001
  
  #eureka配置
  eureka:
    client:
      register-with-eureka: true
      fetch-registry: true
      service-url:
        #只配置一个Eureka节点测试，不用集群了，服务启动多了卡
        defaultZone: http://eureka7001.com:7001/eureka
  
  ```

  

- **主启动类**

  ```java
  @SpringBootApplication
  @EnableEurekaClient //本服务启动后会自动注册进eureka服务中
  public class PaymentHystrixMain8001 {
  
      public static void main(String[] args) {
          SpringApplication.run(PaymentHystrixMain8001.class, args);
      }
  }
  ```

  

- **业务类**

  `PaymentService`

  ```java
  @Service
  public class PaymentService {
  
      /**
       * 正常访问
       * @param id
       * @return
       */
      public String paymentInfo_OK(Integer id) {
          return "线程池:"+Thread.currentThread().getName()+"paymentInfo_OK,id: "+id+"\t"+"O(∩_∩)O";
      }
  
      /**
       * 超时访问，演示服务降级
       * @param id
       * @return
       */
      public String paymentInfo_TimeOut(Integer id) {
          try {
              TimeUnit.SECONDS.sleep(3);
          } catch (InterruptedException e) {
              e.printStackTrace();
          }
          return "线程池:"+Thread.currentThread().getName()+"paymentInfo_TimeOut,id: "+id+"\t"+"O(∩_∩)O，耗费3秒";
      }
  }
  ```

  `PaymentController`

  ```java
  @RestController
  @Slf4j
  public class PaymentController {
  
      @Autowired
      private PaymentService paymentService;
  
      @GetMapping("/payment/hystrix/ok/{id}")
      public String paymentInfo_OK(@PathVariable("id") Integer id) {
          String result = paymentService.paymentInfo_OK(id);
          log.info("****result: "+result);
          return result;
      }
  
      @GetMapping("/payment/hystrix/timeout/{id}")
      public String paymentInfo_TimeOut(@PathVariable("id") Integer id) {
          String result = paymentService.paymentInfo_TimeOut(id);
          log.info("****result: "+result);
          return result;
      }
  }
  ```

  

- **进行测试**

  启动Eureka7001，启动Hystrix8001

  访问两个接口

  ok接口：http://localhost:8001/payment/hystrix/ok/1

  timeout接口：http://localhost:8001/payment/hystrix/timeout/1

  都是可以成功访问，此时注意，ok接口返回响应是特别快的，请求立马就返回了，而timeout接口需要等待3秒钟



#### 高并发测试

上述的工程在非高并发的情况下还可以满足，现在使用Jmeter进行压测

开启Jmeter，来20000个并发压死8001，20000个请求都去访问paymentInfo_TimeOut服务

新建线程组：

![image-20211011161402912](https://cdn.jsdelivr.net/gh/le1024/image1/le/image-20211011161402912.png)

![image-20211011162041053](https://cdn.jsdelivr.net/gh/le1024/image1/le/image-20211011162041053.png)

创建完成之后，启动这个jmeter测试

然后再去浏览器访问原先的服务，会看到http://localhost:8001/payment/hystrix/ok/1 这个接口请求不再是立即就返回了，而是会有一小段时间的延迟

![image-20211011162244907](https://cdn.jsdelivr.net/gh/le1024/image1/le/image-20211011162244907.png)

出现这种情况的原因就是：<strong style="color:red">tomcat的默认的工作线程数被打满 了，没有多余的线程来分解压力和处理。</strong>



#### 消费服务工程搭建

- **新建工程`cloud-consumer-feign-hystrix-order80`**

- **修改pom**

  ```xml
  <dependencies>
          <!--openfeign-->
          <dependency>
              <groupId>org.springframework.cloud</groupId>
              <artifactId>spring-cloud-starter-openfeign</artifactId>
          </dependency>
          <!--hystrix-->
          <dependency>
              <groupId>org.springframework.cloud</groupId>
              <artifactId>spring-cloud-starter-netflix-hystrix</artifactId>
          </dependency>
          <!--eureka client-->
          <dependency>
              <groupId>org.springframework.cloud</groupId>
              <artifactId>spring-cloud-starter-netflix-eureka-client</artifactId>
          </dependency>
          <!-- 引入自己定义的api通用包，可以使用Payment支付Entity -->
          <dependency>
              <groupId>app.springcloud</groupId>
              <artifactId>cloud-api-commons</artifactId>
              <version>${project.version}</version>
          </dependency>
          <!--web-->
          <dependency>
              <groupId>org.springframework.boot</groupId>
              <artifactId>spring-boot-starter-web</artifactId>
          </dependency>
          <dependency>
              <groupId>org.springframework.boot</groupId>
              <artifactId>spring-boot-starter-actuator</artifactId>
          </dependency>
          <!--一般基础通用配置-->
          <dependency>
              <groupId>org.springframework.boot</groupId>
              <artifactId>spring-boot-devtools</artifactId>
              <scope>runtime</scope>
              <optional>true</optional>
          </dependency>
          <dependency>
              <groupId>org.projectlombok</groupId>
              <artifactId>lombok</artifactId>
              <optional>true</optional>
          </dependency>
          <dependency>
              <groupId>org.springframework.boot</groupId>
              <artifactId>spring-boot-starter-test</artifactId>
              <scope>test</scope>
          </dependency>
      </dependencies>
  ```

  

- **配置Yml**

  ```properties
  server:
    port: 80
  
  eureka:
    client:
      register-with-eureka: false
      service-url:
        defaultZone: http://eureka7001.com:7001/eureka/
  ```

  

- **主启动类**

  ```java
  @SpringBootApplication
  @EnableFeignClients
  public class OrderFeignHystrixMain80 {
  
      public static void main(String[] args) {
          SpringApplication.run(OrderFeignHystrixMain80.class, args);
      }
  }
  ```

  

- **业务类**

  ```java
  @Component
  @FeignClient(value = "CLOUD-PROVIDER-HYSTRIX-PAYMENT") //这个服务名要对上，修改hystrix8001服务的sprig.application.name
  public interface PaymentHystrixService {
  
      @GetMapping("/payment/hystrix/ok/{id}")
      String paymentInfo_OK(@PathVariable("id") Integer id);
  
  
      @GetMapping("/payment/hystrix/timeout/{id}")
      String paymentInfo_Timeout(@PathVariable("id") Integer id);
  }
  ```

  ```java
  @RestController
  public class OrderHystrixController {
  
      @Resource
      private PaymentHystrixService paymentHystrixService;
  
      @GetMapping("/consumer/payment/hystrix/ok/{id}")
      public String paymentInfo_OK(@PathVariable("id") Integer id) {
          return paymentHystrixService.paymentInfo_OK(id);
  
      }
  
      @GetMapping("/consumer/payment/hystrix/timeout/{id}")
      public String paymentInfo_Timeout(@PathVariable("id") Integer id) {
          return paymentHystrixService.paymentInfo_Timeout(id);
      }
  }
  ```

  

- **测试**

  将jmeter启动，再次压测2W个请求到生产者服务上，然后请求消费者服务：http://localhost/consumer/payment/hystrix/ok/1

  同样出现了服务请求缓慢的问题

  ![image-20211011174915557](https://cdn.jsdelivr.net/gh/le1024/image1/le/image-20211011174915557.png)



#### 原因总结

8001同一层次的其它接口服务被困死，因为tomcat线程池里面的工作线程已经被挤占完毕

80此时调用8001，客户端访问响应缓慢，转圈圈

<strong style="color:red">正因为有上述故障或不佳表现，才有我们的降级/容错/限流等技术诞生</strong>

