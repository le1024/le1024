> Zookeeper服务注册

Zookeeper下载地址：http://archive.apache.org/dist/zookeeper/

zookeeper是一个分布式协调工具，可以实现注册中心功能，用zookeeper服务器取代Eureka服务器，zk作为服务注册中心。



#### 1.服务提供者注册进zookeeper

##### 1.1搭建8004服务

**新建一个`cloud-provider-payment8004`服务**

- **pom.xml**

  引入自己安装版本对应的zookeeper，不然会和springcloud自带的有冲突，启动会报错

  ```xml
  <?xml version="1.0" encoding="UTF-8"?>
  <project xmlns="http://maven.apache.org/POM/4.0.0"
           xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
           xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
      <parent>
          <artifactId>cloud</artifactId>
          <groupId>app.springcloud</groupId>
          <version>1.0-SNAPSHOT</version>
      </parent>
      <modelVersion>4.0.0</modelVersion>
  
      <artifactId>cloud-provider-payment8004</artifactId>
  
      <dependencies>
          <!-- SpringBoot整合Web组件 -->
          <dependency>
              <groupId>org.springframework.boot</groupId>
              <artifactId>spring-boot-starter-web</artifactId>
          </dependency>
          <dependency><!-- 引入自己定义的api通用包，可以使用Payment支付Entity -->
              <groupId>app.springcloud</groupId>
              <artifactId>cloud-api-commons</artifactId>
              <version>${project.version}</version>
          </dependency>
          <!-- SpringBoot整合zookeeper客户端 -->
          <dependency>
              <groupId>org.springframework.cloud</groupId>
              <artifactId>spring-cloud-starter-zookeeper-discovery</artifactId>
              <!--先排除自带的zookeeper3.5.3-->
              <exclusions>
                  <exclusion>
                      <groupId>org.apache.zookeeper</groupId>
                      <artifactId>zookeeper</artifactId>
                  </exclusion>
              </exclusions>
          </dependency>
          <!--添加zookeeper3.4.9版本，跟自己安装的zookeeper版本要对上-->
          <dependency>
              <groupId>org.apache.zookeeper</groupId>
              <artifactId>zookeeper</artifactId>
              <version>3.4.9</version>
          </dependency>
          <dependency>
              <groupId>org.springframework.boot</groupId>
              <artifactId>spring-boot-devtools</artifactId>
              <scope>runtime</scope>
              <optional>true</optional>
          </dependency>
          <dependency>
              <groupId>org.projectlombok</groupId>
              <artifactId>lombok</artifactId>
              <optional>true</optional>
          </dependency>
          <dependency>
              <groupId>org.springframework.boot</groupId>
              <artifactId>spring-boot-starter-test</artifactId>
              <scope>test</scope>
          </dependency>
      </dependencies>
  </project>
  ```

- **application.yml配置**

  ```properties
  server:
    port: 8004 #8004表示注册到zookeeper服务器的支付服务提供者端口号
  spring:
    application:
      name: cloud-provider-payment #服务别名----注册zookeeper到注册中心名称
    cloud:
      zookeeper:
        connect-string: 192.168.171.132:2181 #zookeeper服务地址
  ```

- **主启动类**

  ```java
  @SpringBootApplication
  @EnableDiscoveryClient //该注解用于向使用consul或者zookeeper作为注册中心时注册服务
  public class PaymentMain8004 {
  
      public static void main(String[] args) {
          SpringApplication.run(PaymentMain8004.class, args);
      }
  }
  ```

- **controller**

  ```java
  @RestController
  public class PaymentController {
  
      @Value("${server.port}")
      private String serverPort;
  
      @GetMapping("/payment/zk")
      public String paymentZk() {
          return "springcloud with zookeeper:" + serverPort + UUID.randomUUID().toString();
      }
  }
  ```



##### 1.2测试

启动8004服务，访问接口地址：http://localhost:8004/payment/zk

![image-20211004225604255](https://cdn.jsdelivr.net/gh/le1024/image1/le/image-20211004225604255.png)

连上zk客户端：会看到多一个`services`，在`services`里可以看到注册成功的8004服务`cloud-provider-payment`

![image-20211004225643195](https://cdn.jsdelivr.net/gh/le1024/image1/le/image-20211004225643195.png)

![image-20211004225716077](https://gitee.com/le1024/image1/raw/master/img/image-20211004225716077.png)

可以通过服务节点查看注册服务的详细信息：

```bash
ls /service/cloud-provider-payment

get /services/cloud-provider-payment/2cd7ca18-05a0-4941-9918-8034d0bd82cf
```

![image-20211004230026038](https://cdn.jsdelivr.net/gh/le1024/image1/le/image-20211004230026038.png)



#### 2.服务节点是临时节点还是持久节点

<strong style="color:red">临时节点</strong>

停掉8004服务后，过一段时间，zookeeper里面会把8004服务剔除。



#### 3.服务消费者注册进zookeeper

##### 3.1搭建zk80服务

- **pom**

  ```xml
      <dependencies>
          <!-- SpringBoot整合Web组件 -->
          <dependency>
              <groupId>org.springframework.boot</groupId>
              <artifactId>spring-boot-starter-web</artifactId>
          </dependency>
          <!-- SpringBoot整合zookeeper客户端 -->
          <dependency>
              <groupId>org.springframework.cloud</groupId>
              <artifactId>spring-cloud-starter-zookeeper-discovery</artifactId>
              <!--先排除自带的zookeeper-->
              <exclusions>
                  <exclusion>
                      <groupId>org.apache.zookeeper</groupId>
                      <artifactId>zookeeper</artifactId>
                  </exclusion>
              </exclusions>
          </dependency>
          <!--添加zookeeper3.4.9版本-->
          <dependency>
              <groupId>org.apache.zookeeper</groupId>
              <artifactId>zookeeper</artifactId>
              <version>3.4.9</version>
          </dependency>
          <dependency>
              <groupId>org.springframework.boot</groupId>
              <artifactId>spring-boot-devtools</artifactId>
              <scope>runtime</scope>
              <optional>true</optional>
          </dependency>
          <dependency>
              <groupId>org.projectlombok</groupId>
              <artifactId>lombok</artifactId>
              <optional>true</optional>
          </dependency>
          <dependency>
              <groupId>org.springframework.boot</groupId>
              <artifactId>spring-boot-starter-test</artifactId>
              <scope>test</scope>
          </dependency>
      </dependencies>
  ```

- **application.yml**

  ```properties
  server:
    port: 80
  spring:
    application:
      name: cloud-consumer-order
    cloud:
      zookeeper:
        connect-string: 192.168.171.132:2181 #zookeeper服务地址
  
  ```

- **主启动类**

  ```java
  @SpringBootApplication
  public class OrderZK80 {
  
      public static void main(String[] args) {
          SpringApplication.run(OrderZK80.class, args);
      }
  }
  ```

- **配置Bean类**

  ```java
  @Configuration
  public class ApplicationContextBean {
  
      @Bean
      @LoadBalanced
      public RestTemplate restTemplate() {
          return new RestTemplate();
      }
  }
  ```

- **controller**

  ```java
  @RestController
  public class OrderZKController {
  
      public static final String INVOKE_URL = "http://cloud-provider-payment";
  
      @Autowired
      private RestTemplate restTemplate;
  
      @GetMapping(value = "/consumer/payment/zk")
      public String paymentInfo()
      {
          String result = restTemplate.getForObject(INVOKE_URL+"/payment/zk", String.class);
          System.out.println("消费者调用支付服务(zookeeper)--->result:" + result);
          return result;
      }
  }
  ```

  

##### 3.2测试

启动zookeeper-8004服务，再启动zookeeper-80服务，成功启动后可以在zookeeper里面看到两个服务都注册成功

![image-20211005202947079](https://cdn.jsdelivr.net/gh/le1024/image1/le/image-20211005202947079.png)

浏览器访问zookeeper-80服务也是可以的

![image-20211005203036102](https://cdn.jsdelivr.net/gh/le1024/image1/le/image-20211005203036102.png)