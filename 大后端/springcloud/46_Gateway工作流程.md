> Gateway工作流程

![image-20211014171144433](https://cdn.jsdelivr.net/gh/le1024/image1/le/image-20211014171144433.png)

客户端向SpringCloud Gateway发出请求，然后在Gateway Handler Mapping中找到与请求相匹配的路由，将其发送到Gateway Web Handler

Handler再通过指定的过滤器链来将请求发送到实际的服务执行业务逻辑，然后返回

过滤器Filter之间用虚线分开可能会在发送代理请求之前("pre")或者之后("post")执行业务逻辑

Filter在"pre"类型的过滤器可以做`参数校验，权限校验，流量控制，日志输出，协议转换`等

Filter在"post"类型的过滤器可以做`响应内容，响应头的修改，日志的输出，流量监控`等



**核心逻辑**

<strong style="color:red">路由转发 + 执行过滤器链</strong>

