> SpringCloud Bus动态刷新广播通知

#### 准备工作

1. 实现需要安装RabbitMQ环境：[RabbitMQ安装](#)
2. 以config3355工程为模板再制作一个config3366工程



#### 设计思想

有两种方案：

1. 利用消息总线触发一个客户端`/bus/refresh`，从而刷新所有客户端的配置
2. 利用消息总线触发一个服务端`/bus/refresh`，从而刷新所有客户端的配置

推荐使用第2个方案：微服务本身是业务模块，更多的职责是实现业务的，而不应该承担刷新配置的职责，不然破坏了微服务各节点的对等性



#### 配置中心服务端添加消息总线支持

`cloud-config-center-3344`工程：

##### pom

添加rabbitMQ支持

```xml
<!--添加消息总线RabbitMQ支持-->
<dependency>
    <groupId>org.springframework.cloud</groupId>
    <artifactId>spring-cloud-starter-bus-amqp</artifactId>
</dependency>
```

添加rabbitMQ支持

##### yaml

```yaml
server:
  port: 3344

spring:
  application:
    name: cloud-config-center #注册进Eureka服务的微服务名
  cloud:
    config:
      server:
        git:
          uri: https://gitee.com/le1024/springcloud-config.git #在Gitee或者GitHub创建的仓库
          #搜索目录
          search-paths:
            - springcloud-config
      #读取分支
      label: master
  rabbitmq:
    host: localhost
    port: 5672
    username: guest
    password: guest
eureka:
  client:
    fetch-registry: true
    register-with-eureka: true
    service-url:
      defaultZone: http://eureka7001.com:7001/eureka/

##rabbitmq相关配置,暴露bus刷新配置的端点
management:
  endpoints: #暴露bus刷新配置的端点
    web:
      exposure:
        include: 'bus-refresh' #这个刷新请求的服务地址

```





#### 配置中心客户端添加消息总线支持

##### config3355

###### pom

```xml
<!--添加消息总线RabbitMQ支持-->
<dependency>
    <groupId>org.springframework.cloud</groupId>
    <artifactId>spring-cloud-starter-bus-amqp</artifactId>
</dependency>
```

###### yaml

```yaml
//其余配置省略。
spring:
  application:
    name: cloud-config-client
  cloud:
  #config客户端配置
    config:
      label: master #分支
      name: config  #配置文件名称
      profile: dev  #读取后缀名称 label,name,profile综合：读取master分支上config-dev.yml的配置文件被读取http://config3344.com:3344/master/config-dev.yml
      uri: http://config3344.com:3344 #config服务端地址
  #rabbitmq相关配置 15672是Web管理界面的端口；5672是MQ访问的端口
  rabbitmq:
    host: localhost
    port: 5672
    username: guest
    password: guest

```

##### config3366

###### pom

```xml
<!--添加消息总线RabbitMQ支持-->
<dependency>
    <groupId>org.springframework.cloud</groupId>
    <artifactId>spring-cloud-starter-bus-amqp</artifactId>
</dependency>
```

###### yaml

```yaml
//其余配置省略。
spring:
  application:
    name: cloud-config-client
  cloud:
  #config客户端配置
    config:
      label: master #分支
      name: config  #配置文件名称
      profile: dev  #读取后缀名称 label,name,profile综合：读取master分支上config-dev.yml的配置文件被读取http://config3344.com:3344/master/config-dev.yml
      uri: http://config3344.com:3344 #config服务端地址
  #rabbitmq相关配置 15672是Web管理界面的端口；5672是MQ访问的端口
  rabbitmq:
    host: localhost
    port: 5672
    username: guest
    password: guest

```



#### 测试

启动四个服务：eureka7001，config3344(服务端)，config3355，config3366

分别请求：

1. http://config3344.com:3344/master/config-dev.yml
2. http://localhost:3355/configInfo
3. http://localhost:3355/configInfo

页面返回的信息是一致的



更新git仓库的文件`config-dev.yml`，再次请求三个地址：

![image-20211018163716788](https://cdn.jsdelivr.net/gh/le1024/image1/le/image-20211018163716788.png)

![image-20211018163731348](https://cdn.jsdelivr.net/gh/le1024/image1/le/image-20211018163731348.png)

![image-20211018163740604](https://cdn.jsdelivr.net/gh/le1024/image1/le/image-20211018163740604.png)

**还是只有config3344服务端的是改变的**

**这个时候，还需要手动发送一次POST请求**

```bash
curl -X POST "http://localhost:3344/actuator/bus-refresh"
```

![image-20211018163918549](https://cdn.jsdelivr.net/gh/le1024/image1/le/image-20211018163918549.png)

**注意！！！！<strong style="color:red">这里是向服务端发送请求的，不是客户端</strong>**



发送完成后再次刷新客户端的两个接口：

![image-20211018164218887](https://cdn.jsdelivr.net/gh/le1024/image1/le/image-20211018164218887.png)

![image-20211018164725624](https://cdn.jsdelivr.net/gh/le1024/image1/le/image-20211018164725624.png)

<strong style="color:orange">客户端服务不需要重启或者手动向每个客户端服务发送POST请求就可以更新配置数据了</strong>



#### 查看rabbitMQ

默认会生成一个`springcloudBus`的主题，订阅此主题的客户端都可以收到消息

![image-20211018165411930](https://cdn.jsdelivr.net/gh/le1024/image1/le/image-20211018165411930.png)



<strong style="color:red">一次修改，广播通知，处处生效</strong>

