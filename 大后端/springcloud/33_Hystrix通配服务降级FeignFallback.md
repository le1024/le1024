> Hystrix通配服务降级

将业务类的逻辑处理跟服务降级的配置分开，不混在一起，实现业务的解耦

本次案例服务降级处理是在客户端Hystrix80实现完成的，与服务端Hystrix8001没有关系

只需要为Hystrix80客户端<strong style="color:red">定义的接口添加一个服务降级处理的实现类</strong>即可实现解耦



修改`cloud-consumer-feign-hystrix-order80`

- 新建类`PaymentFallbackService`

  该类实现已有的业务接口`PaymentHystrixService`，在该类中统一处理业务类所有方法的异常

  ```java
  @Component
  public class PaymentFallbackService implements PaymentHystrixService{
  
      @Override
      public String paymentInfo_OK(Integer id) {
          return "服务调用失败，来自统一异常处理：paymentInfo_OK失败";
      }
  
      @Override
      public String paymentInfo_Timeout(Integer id) {
          return "服务调用失败，来自统一异常处理：paymentInfo_Timeout失败";
      }
  }
  ```

- `PaymentHystrixService`

  将异常信息类配上

  ```java
  @Component
  @FeignClient(value = "CLOUD-PROVIDER-HYSTRIX-PAYMENT", fallback = PaymentFallbackService.class) //出异常了都会走PaymentFallbackService封装的方法
  public interface PaymentHystrixService {
  
      @GetMapping("/payment/hystrix/ok/{id}")
      String paymentInfo_OK(@PathVariable("id") Integer id);
  
  
      @GetMapping("/payment/hystrix/timeout/{id}")
      String paymentInfo_Timeout(@PathVariable("id") Integer id);
  }
  ```

- yml文件配置上Hystrix

  ```properties
  feign:
    hystrix:
      enabled: true
  ```

- 启动测试

  依次启动eureka8001，Hystrix8001，Hystrix80服务，访问：http://localhost/consumer/payment/hystrix/ok/1

  ![image-20211012164742090](https://cdn.jsdelivr.net/gh/le1024/image1/le/image-20211012164742090.png)

  可正常访问，现在将Hystrix8001服务端停止掉，模拟服务宕机，再次访问Hystrix80服务

  测试结果：由于生产端服务宕机，服务不可用了，触发了客户端的Hystrix的服务降级，这里是通过自定的异常接口实现类实现的效果

  ![image-20211012164931326](https://cdn.jsdelivr.net/gh/le1024/image1/le/image-20211012164931326.png)

  

