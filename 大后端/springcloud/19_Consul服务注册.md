> Consul-服务注册



#### 1.服务提供者注册进Consul

新建支付服务`cloud-providerconsul-payment8006`

- **pom.xml**

  ```xml
      <dependencies>
          <!--SpringCloud consul-server -->
          <dependency>
              <groupId>org.springframework.cloud</groupId>
              <artifactId>spring-cloud-starter-consul-discovery</artifactId>
          </dependency>
          <!-- SpringBoot整合Web组件 -->
          <dependency>
              <groupId>org.springframework.boot</groupId>
              <artifactId>spring-boot-starter-web</artifactId>
          </dependency>
          <dependency>
              <groupId>org.springframework.boot</groupId>
              <artifactId>spring-boot-starter-actuator</artifactId>
          </dependency>
          <!--日常通用jar包配置-->
          <dependency>
              <groupId>org.springframework.boot</groupId>
              <artifactId>spring-boot-devtools</artifactId>
              <scope>runtime</scope>
              <optional>true</optional>
          </dependency>
          <dependency>
              <groupId>org.projectlombok</groupId>
              <artifactId>lombok</artifactId>
              <optional>true</optional>
          </dependency>
          <dependency>
              <groupId>org.springframework.boot</groupId>
              <artifactId>spring-boot-starter-test</artifactId>
              <scope>test</scope>
          </dependency>
      </dependencies>
  ```

  

- **application.yml**

  ```properties
  server:
    port: 8006
  
  spring:
    application:
      name: cloud-provider-payment
    #consul配置
    cloud:
      consul:
        host: localhost
        port: 8500
        discovery:
          service-name: ${spring.application.name}
  ```

  

- **主启动类**

  ```java
  @SpringBootApplication
  @EnableDiscoveryClient
  public class PaymentMain8006 {
  
      public static void main(String[] args) {
          SpringApplication.run(PaymentMain8006.class, args);
      }
  }
  ```

  

- **业务类**

  ```java
  @RestController
  public class PaymentController {
  
      @Value("${server.port}")
      private String serverPort;
  
      @GetMapping("/payment/consul")
      public String paymentConsul() {
          return "spring cloud with consul：" + serverPort + " " + UUID.randomUUID().toString();
      }
  }
  ```

  

- **测试**

  ​	启动8006工程，验证测试：http://localhost:8006/payment/consul

  ![image-20211006220625054](https://cdn.jsdelivr.net/gh/le1024/image1/le/image-20211006220625054.png)

  同时，consul也会注册成功

  ![image-20211006220721398](https://cdn.jsdelivr.net/gh/le1024/image1/le/image-20211006220721398.png)

  ![image-20211006220747676](https://cdn.jsdelivr.net/gh/le1024/image1/le/image-20211006220747676.png)



#### 2.服务消费者注册进Consul

新建订单服务`cloud-consumerconsul-order80`

- **pom**

  ```xml
  <dependencies>
          <!--SpringCloud consul-server -->
          <dependency>
              <groupId>org.springframework.cloud</groupId>
              <artifactId>spring-cloud-starter-consul-discovery</artifactId>
          </dependency>
          <!-- SpringBoot整合Web组件 -->
          <dependency>
              <groupId>org.springframework.boot</groupId>
              <artifactId>spring-boot-starter-web</artifactId>
          </dependency>
          <dependency>
              <groupId>org.springframework.boot</groupId>
              <artifactId>spring-boot-starter-actuator</artifactId>
          </dependency>
          <!--日常通用jar包配置-->
          <dependency>
              <groupId>org.springframework.boot</groupId>
              <artifactId>spring-boot-devtools</artifactId>
              <scope>runtime</scope>
              <optional>true</optional>
          </dependency>
          <dependency>
              <groupId>org.projectlombok</groupId>
              <artifactId>lombok</artifactId>
              <optional>true</optional>
          </dependency>
          <dependency>
              <groupId>org.springframework.boot</groupId>
              <artifactId>spring-boot-starter-test</artifactId>
              <scope>test</scope>
          </dependency>
      </dependencies>
  ```

  

- **application.yml**

  ```properties
  server:
    port: 80
  
  spring:
    application:
      name: cloud-consumer-order
    cloud:
      #配置consul
      consul:
        host: localhost
        port: 8500
        discovery:
          service-name: ${spring.application.name}
  ```

  

- **主启动类**

  ```java
  @SpringBootApplication
  @EnableDiscoveryClient //该注解用于向使用consul或者zookeeper作为注册中心时注册服务
  public class OrderConsulMain80 {
  
      public static void main(String[] args) {
          SpringApplication.run(OrderConsulMain80.class, args);
      }
  }
  ```

  

- **配置Bean**

  ```java
  @Configuration
  public class ApplicationContextBean {
  
      @Bean
      @LoadBalanced
      public RestTemplate getRestTemplate() {
          return new RestTemplate();
      }
  }
  ```

  

- **业务类**

  ```java
  @RestController
  public class OrderConsulController {
  
      public static final String INVOKE_URL = "http://cloud-provider-payment"; //服务端微服务名称
  
      @Autowired
      private RestTemplate restTemplate;
  
      @GetMapping("/consumer/payment/consul")
      public String orderConsul() {
          return restTemplate.getForObject(INVOKE_URL + "/payment/consul", String.class);
      }
  }
  ```

  

- **测试**

  查看consul web控制台，消费者服务成功注册

  ![image-20211006222444125](https://cdn.jsdelivr.net/gh/le1024/image1/le/image-20211006222444125.png)

  访问接口：http://localhost/consumer/payment/consul

  ![image-20211006222523590](https://cdn.jsdelivr.net/gh/le1024/image1/le/image-20211006222523590.png)