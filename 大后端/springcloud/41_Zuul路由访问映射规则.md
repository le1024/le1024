> Zuul路由访问映射规则

#### 1. 修改代理名称

```properties
zuul:
  #路由映射配置
  routes:
    mypayment.path: /mypayment/** #浏览器地址栏输入的路径
    mypayment.serviceId: cloud-payment-service #注册进Eureka服务的地址
```

重启zuul服务

之前访问地址：http://myzuul.com:9527/<font color="blue">cloud-payment-service</font>/payment/get/1 

现在通过配置的路由地址也可以访问：http://myzuul.com:9527/<font color="blue">mypayment</font>/payment/get/1

![image-20211013225919217](https://cdn.jsdelivr.net/gh/le1024/image1/le/image-20211013225919217.png)



#### 2. 忽略真实服务名

```properties
zuul:
  ignored-services: cloud-payment-service #忽略服务名，不允许通过服务名进行访问
```

通过微服务名是不能正常访问了

![image-20211014084757090](https://cdn.jsdelivr.net/gh/le1024/image1/le/image-20211014084757090.png)

<font color="green">忽略单个微服务用具体的服务名，多个用*，ignored-service: *</font>



#### 3. 路由转发和负载均衡功能

**服务提供者SMS短信模块**

- **创建**`cloud-provider-sms8008`

- **pom**

  ```xml
      <dependencies>
          <dependency>
              <groupId>org.springframework.boot</groupId>
              <artifactId>spring-boot-starter-web</artifactId>
          </dependency>
          <dependency>
              <groupId>org.springframework.cloud</groupId>
              <artifactId>spring-cloud-starter-netflix-eureka-client</artifactId>
          </dependency>
          <dependency>
              <groupId>org.springframework.boot</groupId>
              <artifactId>spring-boot-starter-actuator</artifactId>
          </dependency>
  
          <dependency>
              <groupId>org.springframework.boot</groupId>
              <artifactId>spring-boot-devtools</artifactId>
              <scope>runtime</scope>
              <optional>true</optional>
          </dependency>
          <dependency>
              <groupId>org.projectlombok</groupId>
              <artifactId>lombok</artifactId>
              <optional>true</optional>
          </dependency>
          <dependency>
              <groupId>org.springframework.boot</groupId>
              <artifactId>spring-boot-starter-test</artifactId>
              <scope>test</scope>
          </dependency>
      </dependencies>
  ```

- **yml**

  ```yaml
  server:
    port: 8008
  spring:
    application:
      name: cloud-provider-sms
  eureka:
    client:
      register-with-eureka: true
      fetch-registry: true
      service-url:
        defaultZone: http://eureka7001.com:7001/eureka/
    instance:
      instance-id: sms8008
  ```

- **业务类**

  ```java
  @RestController
  public class SmsController {
  
      @Value("${server.port}")
      private String severPort;
  
      @GetMapping("/sms")
      public String sms() {
          return "sms provider service: " + severPort;
      }
  }
  ```

- **启动类**

  ```java
  @SpringBootApplication
  @EnableEurekaClient
  public class SmsMain8008 {
  
      public static void main(String[] args) {
          SpringApplication.run(SmsMain8008.class, args);
      }
  }
  ```

  

**Zuul9527模块**

- **yml**

  修改yml，提现路由转发和负载均衡

  ```yaml
  zuul:
    #路由映射配置
    routes:
      mypayment.path: /mypayment/** #浏览器地址栏输入的路径
      mypayment.serviceId: cloud-payment-service #注册进Eureka服务的地址
      mysms.path: /mysms/**
      mysms.serviceId: cloud-provider-sms
    ignored-services: cloud-payment-service #忽略服务名，不允许通过服务名进行访问
  ```



**测试**

<strong style="color:red">由于Zuul自动集成了Ribbon和Hystrix，所以Zuul天生就有负载均衡和服务容错能力</strong>

启动`Eureka7001`，启动`payment8001/8002`，启动`sms8008`，启动`Zuul9527`



测试负载均衡：

http://myzuul.com:9527/mypayment/payment/get/1

![image-20211014103126472](https://cdn.jsdelivr.net/gh/le1024/image1/le/image-20211014103126472.png)

![image-20211014103146240](https://cdn.jsdelivr.net/gh/le1024/image1/le/image-20211014103146240.png)



测试路由转发：

http://myzuul.com:9527/mysms/sms

![image-20211014103229936](https://cdn.jsdelivr.net/gh/le1024/image1/le/image-20211014103229936.png)



#### 4. 设置统一公共前缀

为所有的微服务请求加一个统一的前缀

```yaml
zuul:
  prefix: /app #给服务请求加一个前缀
```

需要加上`/app`才可以访问

![image-20211014112527113](https://cdn.jsdelivr.net/gh/le1024/image1/le/image-20211014112527113.png)
