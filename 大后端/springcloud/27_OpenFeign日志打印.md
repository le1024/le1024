> OpenFeign日志打印



#### OpenFeign日志打印介绍

Feign提供了日志打印功能，我们可以通过配置来调整日志级别，从而了解Feign中http请求的细节。

说白了就是<strong style="color:red">对Feign接口的调用情况进行监控和输出</strong>



#### OpenFeign日志级别

**NONE**：默认的，不显示任何日志

**BASIC**：仅记录请求方法、URL、响应状态码及执行时间

**HEADERS**：除了 BASIC 中定义的信息之外，还有请求和响应的头信息

**FULL**：除了 HEADERS 中定义的信息之外，还有请求和响应的正文及元数据



#### OpenFeign日志打印功能开启

- 配置一个Bean类

  ```java
  @Configuration
  public class FeignConfig {
  
      /**
       * 注意引用的是feign.Logger
       */
      @Bean
      Logger.Level feignLoggerLevel() {
          return Logger.Level.FULL;
      }
  }
  ```

- YML文件里需要开启日志的Feign客户端

  ```properties
  logging:
    level:
      # feign日志以什么级别监控哪个接口
      com.springcloud.service.PaymentFeignService: debug
  ```

- 启动服务，查看控制台

  ![image-20211008232527403](https://cdn.jsdelivr.net/gh/le1024/image1/le/image-20211008232527403.png)

