> Ribbon 负载均衡算法-RoundRobinRule

#### RoundRobinRule原理

<strong style="color:red">负载均衡算法：rest接口第几次请求数 % 服务器集群总数量 = 实际调用服务器位置下标  ，每次服务重启动后rest接口计数从1开始。</strong>

`List<ServiceInstance> instances = discoveryClient.getInstances("CLOUD-PAYMENT-SERVICE");`

如：   List [0] instances = 127.0.0.1:8002
　　   List [1] instances = 127.0.0.1:8001

8001+ 8002 组合成为集群，它们共计2台机器，集群总数为2， 按照轮询算法原理：

当总请求数为1时： 1 % 2 =1 对应下标位置为1 ，则获得服务地址为127.0.0.1:8001
当总请求数位2时： 2 % 2 =0 对应下标位置为0 ，则获得服务地址为127.0.0.1:8002
当总请求数位3时： 3 % 2 =1 对应下标位置为1 ，则获得服务地址为127.0.0.1:8001
当总请求数位4时： 4 % 2 =0 对应下标位置为0 ，则获得服务地址为127.0.0.1:8002
如此类推......

服务重启，重置



##### RoundRobinRule源码

点击`IRule`，是一个接口类

![image-20211007221008182](https://cdn.jsdelivr.net/gh/le1024/image1/le/image-20211007221008182.png)

在`choose`选择实现类`RoundRobbinRule`

![image-20211007221045602](https://cdn.jsdelivr.net/gh/le1024/image1/le/image-20211007221045602.png)

里面有一个`choose`方法的具体实现

![image-20211007221534218](https://cdn.jsdelivr.net/gh/le1024/image1/le/image-20211007221534218.png)

![image-20211007221607562](https://cdn.jsdelivr.net/gh/le1024/image1/le/image-20211007221607562.png)

通过自旋锁的方式，获取到可用服务的下标



#### 自定义实现负载均衡算法

- 8001/8002服务改造，Controller添加新方法请求

  ```java
  //简单点，访问当前服务接口即可
  @GetMapping("/payment/lb")
  public String getPaymentLB() {
      return serverPort;
  }
  ```

- 80服务，注释掉`ApplicationContextConfig`类中的注解：`@LoadBalanced`

- 创建package`com.springcloud.lb`，自定义一个LoadBalancer服务

  ```java
  public interface LoadBalancer {
  
      /**
       * 从生产者服务集群实例中获取可用的服务返回
       * @param serviceInstances 生产者服务集群实例
       * @return
       */
      ServiceInstance instances(List<ServiceInstance> serviceInstances);
  }
  ```

- LoadBalancer实现类

  ```java
  @Component
  public class MyLoadBalancer implements LoadBalancer{
  
      AtomicInteger atomicInteger = new AtomicInteger(0);
  
      public final int getAndIncrement() {
          int current;
          int next; //第几次请求
  
          do {
              current = atomicInteger.get();
              /**
               * 为了程序的严谨性，判断当前请求次数是否超过2147483647，超过重置，从0计数
               */
              next = current >= 2147483647 ? 0 : current + 1;
          } while (!atomicInteger.compareAndSet(current, next));
  
          System.out.println("第几次请求，次数next:" + next);
          return next;
      }
  
      //负载均衡算法：rest接口第几次请求数 % 服务器集群总数量 = 实际调用服务器位置下标
      //每次重启服务之后，rest接口计数从1开始
      @Override
      public ServiceInstance instances(List<ServiceInstance> serviceInstances) {
          int index = getAndIncrement() % serviceInstances.size();
          return serviceInstances.get(index);
      }
  }
  ```

- 80服务添加请求

  ```java
  @Resource
  private LoadBalancer loadBalancer;
  @Resource
  private DiscoveryClient discoveryClient;
  
  @GetMapping("/consumer/payment/lb")
  public String getPaymentLB() {
      //获取集群中的实例
      List<ServiceInstance> instances = discoveryClient.getInstances("CLOUD-PAYMENT-SERVICE");
      if (instances == null || instances.size() == 0) {
          return null;
      }
  
      ServiceInstance instance = loadBalancer.instances(instances);
      //获取到可用服务的uri
      URI uri = instance.getUri();
      return restTemplate.getForObject(uri + "/payment/lb", String.class);
  }
  ```

- 测试：http://localhost/consumer/payment/lb

  ![image-20211008212307350](https://cdn.jsdelivr.net/gh/le1024/image1/le/image-20211008212307350.png)

  ![image-20211008212413998](https://cdn.jsdelivr.net/gh/le1024/image1/le/image-20211008212413998.png)

