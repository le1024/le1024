> SpringCloud Stream消息驱动分组消息与持久化

准备工作：

依照`8802服务`clone一份`8803服务`

启动服务：

- 消息中间件：rabbitMQ
- 服务注册中心：eureka7001
- 服务生产者：stream8801
- 服务消费者：stream8802、stream8803

请求接口：http://localhost:8801/sendMessage

![image-20211019210955956](https://cdn.jsdelivr.net/gh/le1024/image1/le/image-20211019210955956.png)

![image-20211019211027446](https://cdn.jsdelivr.net/gh/le1024/image1/le/image-20211019211027446.png)

![image-20211019211109374](https://cdn.jsdelivr.net/gh/le1024/image1/le/image-20211019211109374.png)



#### 服务分组，防重复消费

从上面的结果可以看出，默认情况下，做集群部署时，多个消费者服务是会重复消费生产者生产的消费，如果这是一个订单的业务，那么就会生成多个订单，这个情况是不允许的。

为了避免这个问题，需要使用<strong style="color:red">Stream中的消息分组group来解决</strong>

在rabbitMQ控制台，可以看出来，当启动多个消费者，默认的bind分组是不一样的，由于分组的不同，造成了消息的重复消费

![image-20211019212819615](https://cdn.jsdelivr.net/gh/le1024/image1/le/image-20211019212819615.png)

在stream中处于同一个group中的多个消费者存在竞争关系，能够保证消息只被其中一个消费者消费一次。

<strong style="color:red">不同组可以全面消费(重复消费)</strong>

<strong style="color:red">同组发生竞争关系，只有一个可以消费</strong>





在`yml`配置`group`即可

1. 先配置`stream8802`、`stream8803`再不同的group

**8002服务**

````yaml
spring:
  application:
    name: cloud-stream-consumer
  cloud:
    stream:
      binders: # 在此处配置上rabbitMQ的服务信息
        defaultRabbit: #表示定义名称，与bindings.input.binder对应
          type: rabbit #消息组件类型
          environment:
            spring:
              rabbitmq: #rabbitMQ环境配置
                host: localhost
                port: 5672
                username: guest
                password: guest
      bindings: #服务的整合处理
        input:
          destination: studyExchange #使用的Exchange名称
          content-type: application/json #消息类型
          binder: defaultRabbit #设置要绑定的消息服务的具体设置
          group: groupA #配置分组相同,防止重复消费，如果需要重复消费将每个消费者服务设置不同组
```
**8003服务**

````yaml

···
bindings: #服务的整合处理
        input:
          destination: studyExchange #使用的Exchange名称
          content-type: application/json #消息类型
          binder: defaultRabbit #设置要绑定的消息服务的具体设置
          group: groupB #配置分组相同,防止重复消费，如果需要重复消费将每个消费者服务设置不同组
```

![image-20211019213057626](https://cdn.jsdelivr.net/gh/le1024/image1/le/image-20211019213057626.png)

rabbitMQ控制台可以看出配置已生效

测试请求地址：http://localhost:8801/sendMessage

![image-20211019213150734](https://cdn.jsdelivr.net/gh/le1024/image1/le/image-20211019213150734.png)

![image-20211019213200933](https://cdn.jsdelivr.net/gh/le1024/image1/le/image-20211019213200933.png)

<strong style="color:red">不同分组，重复消费</strong>



2.配置`stream8802`、`stream8803`同一个group

**更改8003为groupA**

```yaml
···
bindings: #服务的整合处理
        input:
          destination: studyExchange #使用的Exchange名称
          content-type: application/json #消息类型
          binder: defaultRabbit #设置要绑定的消息服务的具体设置
          group: groupA #配置分组相同,防止重复消费，如果需要重复消费将每个消费者服务设置不同组
```

重启8003服务，接着查看rabbitMQ控制台

![image-20211019213720760](https://cdn.jsdelivr.net/gh/le1024/image1/le/image-20211019213720760.png)

![image-20211019213750521](https://cdn.jsdelivr.net/gh/le1024/image1/le/image-20211019213750521.png)

![image-20211019213820028](https://cdn.jsdelivr.net/gh/le1024/image1/le/image-20211019213820028.png)

测试请求地址：http://localhost:8801/sendMessage

查看控制台可以看出：

8802/8803实现了轮询分组，每次只有一个消费者
8801模块的发的消息只能被8802或8803其中一个接收到，这样避免了重复消费。

![image-20211019213931577](https://cdn.jsdelivr.net/gh/le1024/image1/le/image-20211019213931577.png)

![image-20211019213954538](https://cdn.jsdelivr.net/gh/le1024/image1/le/image-20211019213954538.png)

![image-20211019214015067](https://cdn.jsdelivr.net/gh/le1024/image1/le/image-20211019214015067.png)



#### 持久化

配置了`group`，就已经实现了持久化

验证：

停止8802/8803并去除掉8802的分组`group: groupA`

通过8801生成消息：请求地址：http://localhost:8801/sendMessage

![image-20211019220015236](https://cdn.jsdelivr.net/gh/le1024/image1/le/image-20211019220015236.png)

生成消息完成后，先启动8802服务，该服务无分组属性配置：

![image-20211019220221785](https://cdn.jsdelivr.net/gh/le1024/image1/le/image-20211019220221785.png)

再启动8803服务，该服务有分组配置：

![image-20211019220410765](https://cdn.jsdelivr.net/gh/le1024/image1/le/image-20211019220410765.png)