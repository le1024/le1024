canal-数据同步

canal介绍：

canal是阿里巴巴旗下的一款开源项目，纯Java开发。基于数据库增量日志解析，提供增量数据订阅&消费，目前主要支持了MySQL。

https://github.com/alibaba/canal

快速入门教程：

https://github.com/alibaba/canal/wiki/QuickStart



### 准备

<hr>

- 查看mysql是否开启Binlog，如果`log_bin`为`OFF`，需要在`my.cnf`文件中修改

  ```sql
  show variables like 'log_bin';
  ```

  ![image-20210922151726116](https://cdn.jsdelivr.net/gh/le1024/image1/le/image-20210922151726116.png)

  ```bash
  vim /etc/my.cnf
  ```

  添加如下配置：

  ```bash
  [mysqld]
  log-bin=mysql-bin # 开启 binlog
  binlog-format=ROW # 选择 ROW 模式
  server_id=1 # 配置 MySQL replaction 需要定义，不要和 canal 的 slaveId 重复
  ```

  重启数据库后查看`Binlog`状态：

  ![image-20210922152352254](https://cdn.jsdelivr.net/gh/le1024/image1/le/image-20210922152352254.png)



- 创建`canal`授权帐号

  ```mysql
  CREATE USER canal IDENTIFIED WITH mysql_native_password BY 'canal';  
  GRANT SELECT, REPLICATION SLAVE, REPLICATION CLIENT ON *.* TO 'canal'@'%';
  -- GRANT ALL PRIVILEGES ON *.* TO 'canal'@'%' ;
  FLUSH PRIVILEGES;
  ```

  

### 使用

<hr>

- 下载canal

  当前教程里面用的是`canal.deployer-1.0.24.tar.gz`

  ```http
  https://github.com/alibaba/canal/releases
  ```

- 解压缩

  ```bash
  mkdir canal
  
  tar -zxvf canal.deployer-1.0.24.tar.gz -C /home/canal/
  ```

- 修改配置

  ```bash
  vim conf/example/instance.properties
  ```

  ```bash
  canal.instance.mysql.slaveId=1234
  #数据库地址
  canal.instance.master.address=127.0.0.1:3306
  
  #数据库帐号密码及监听的数据库名称
  canal.instance.dbUsername=canal
  canal.instance.dbPassword=canal
  canal.instance.connectionCharset = UTF-8
  canal.instance.defaultDatabaseName =canalDB
  
  #监控canalDB下所有的表，这个是自己建立的库
  canal.instance.filter.regex=canalDB\\..*
  ```

- 启动

  ```bash
  sh bin/startup.sh
  ```

  **服务器内存不够的话，启动报错，编辑下startup.sh文件修改下运行内存**

- 查看server日志

  ```bash
  tail -200f logs/canal/canal.log
  ```

- 查看instance日志

  ```bash
  tail -200f logs/example/example.log
  ```

- 关闭

  ```bash
  sh bin/stop.sh
  ```

  

### 代码实现

**1.先创建一个测试表：canal_test，随便建一个就行**

**2.代码配置**

**`pom.xml`**

```xml
<dependencies>
    <dependency>
        <groupId>com.alibaba.otter</groupId>
        <artifactId>canal.client</artifactId>
        <version>1.1.0</version>
    </dependency>
    <!--mysql-->
    <dependency>
        <groupId>mysql</groupId>
        <artifactId>mysql-connector-java</artifactId>
        <version>5.1.6</version>
    </dependency>
    <dependency>
        <groupId>commons-dbutils</groupId>
        <artifactId>commons-dbutils</artifactId>
        <version>1.7</version>
    </dependency>
    <dependency>
        <groupId>org.springframework.boot</groupId>
        <artifactId>spring-boot-starter-jdbc</artifactId>
    </dependency>
</dependencies>
```

**`application.yml`**

```yaml
server:
  port: 8080
spring:
  datasource:
    url: jdbc:mysql://127.0.0.1:3306/canal?serverTimezone=GMT%2B8
    username: canal
    password: canal
    driver-class-name: com.mysql.jdbc.Driver

```





**`CanalServer`**

```java
@Component
public class CanalServer {

    //sql队列
    private Queue<String> SQL_QUEUE = new ConcurrentLinkedQueue<>();
    @Resource
    private DataSource dataSource;

    private final static int BATCH_SIZE = 1000;

    public void run() {
        CanalConnector canalConnector = CanalConnectors.newSingleConnector(
                //canal.properties配置的默认端口
                new InetSocketAddress("127.0.0.1", 11111), "example", "", "");

        try {
            canalConnector.connect();
            canalConnector.subscribe();
            canalConnector.rollback();

            try {
                while (true) {
                    //尝试拿batchSize条记录，有多少取多少，不会阻塞等待
                    Message message = canalConnector.getWithoutAck(BATCH_SIZE);
                    long batchId = message.getId();
                    int size = message.getEntries().size();

                    if (batchId == -1 || size == 0) {
                        Thread.sleep(1000);
                    } else {
                        dataHandle(message.getEntries());
                    }
                    //进行 batch id 的确认
                    canalConnector.ack(batchId);
                    //当队列里面堆积的sql大于一定数值的时候就模拟执行
                    if (SQL_QUEUE.size() >= 1) {
                        executeQueueSql();
                    }
                }
            } catch (InterruptedException | InvalidProtocolBufferException var1) {
                var1.printStackTrace();
            }
        } finally {
            canalConnector.disconnect();
        }
    }

    /**
     * 模拟执行队列里面的sql语句
     */
    public void executeQueueSql() {
        int size = SQL_QUEUE.size();
        for (int i = 0; i < size; i++) {
            String sql = SQL_QUEUE.poll();
            System.out.println("[sql]--->" + sql);
        }
    }

    /**
     * 数据处理
     * @param entries
     * @throws InvalidProtocolBufferException
     */
    private void dataHandle(List<CanalEntry.Entry> entries) throws InvalidProtocolBufferException {
        for (CanalEntry.Entry entry : entries) {
            if (CanalEntry.EntryType.ROWDATA == entry.getEntryType()) {
                RowChange rowChange = RowChange.parseFrom(entry.getStoreValue());
                CanalEntry.EventType eventType = rowChange.getEventType();
                switch (eventType) {
                    case INSERT:
                        saveInsertSql(entry);
                        break;
                    case UPDATE:
                        saveUpdateSql(entry);
                        break;
                    case DELETE:
                        saveDeleteSql(entry);
                        break;
                    default:
                        break;
                }
//                if (eventType == CanalEntry.EventType.DELETE) {
//                    saveDeleteSql(entry);
//                } else if (eventType == CanalEntry.EventType.UPDATE) {
//                    saveUpdateSql(entry);
//                } else if (eventType == CanalEntry.EventType.INSERT) {
//                    saveInsertSql(entry);
//                }

            }
        }
    }

    /**
     * 入库
     * @param sql
     */
    public void execute(String sql) {
        Connection conn = null;
        try {
            if (null == sql) {
                return;
            }

            conn = dataSource.getConnection();
            QueryRunner queryRunner = new QueryRunner();
            int row = queryRunner.execute(conn, sql);
            System.out.println("update:" +row);
        } catch (SQLException var1) {
            var1.printStackTrace();
        } finally {
            DbUtils.closeQuietly(conn);
        }
    }

    /**
     * 插入sql
     * @param entry
     */
    public void saveInsertSql(CanalEntry.Entry entry) {
        try {
            RowChange rowChange = RowChange.parseFrom(entry.getStoreValue());
            List<CanalEntry.RowData> rowDataList = rowChange.getRowDatasList();
            for (CanalEntry.RowData rowData : rowDataList) {
                List<CanalEntry.Column> columnList = rowData.getAfterColumnsList();
                StringBuffer sql = new StringBuffer();
                sql.append("insert into ").append(entry.getHeader().getTableName()).append("(");

                for (int i=0; i<columnList.size(); i++) {
                    sql.append(columnList.get(i).getName());
                    if (i != columnList.size() - 1) {
                        sql.append(",");
                    }
                }

                sql.append(") VALUES (");
                for (int i = 0; i < columnList.size(); i++) {
                    sql.append("'").append(columnList.get(i).getValue()).append("'");
                    if (i != columnList.size() - 1) {
                        sql.append(",");
                    }
                }

                sql.append(")");
                SQL_QUEUE.add(sql.toString());
            }
        } catch (InvalidProtocolBufferException var1) {
            var1.printStackTrace();
        }
    }

    /**
     * 更新sql
     * @param entry
     */
    private void saveUpdateSql(CanalEntry.Entry entry) {
        try {
            RowChange rowChange = RowChange.parseFrom(entry.getStoreValue());
            List<CanalEntry.RowData> rowDatasList = rowChange.getRowDatasList();
            for (CanalEntry.RowData rowData : rowDatasList) {
                List<CanalEntry.Column> newColumnList = rowData.getAfterColumnsList();
                StringBuffer sql = new StringBuffer();
                sql.append("update ").append(entry.getHeader().getTableName()).append(" set ");
                for (int i = 0; i < newColumnList.size(); i++) {
                    sql.append(" " + newColumnList.get(i).getName()
                            + " = '" + newColumnList.get(i).getValue() + "'");
                    if (i != newColumnList.size() - 1) {
                        sql.append(",");
                    }
                }
                sql.append(" where ");
                List<CanalEntry.Column> oldColumnList = rowData.getBeforeColumnsList();
                for (CanalEntry.Column column : oldColumnList) {
                    if (column.getIsKey()) {
                        sql.append(column.getName()).append("=").append(column.getValue());
                        break;
                    }
                }
                SQL_QUEUE.add(sql.toString());
            }
        } catch (InvalidProtocolBufferException e) {
            e.printStackTrace();
        }
    }

    /**
     * 删除sql
     * @param entry
     */
    private void saveDeleteSql(CanalEntry.Entry entry) {
        try {
            RowChange rowChange = RowChange.parseFrom(entry.getStoreValue());
            List<CanalEntry.RowData> rowDatasList = rowChange.getRowDatasList();
            for (CanalEntry.RowData rowData : rowDatasList) {
                List<CanalEntry.Column> columnList = rowData.getBeforeColumnsList();
                StringBuffer sql = new StringBuffer();
                sql.append("delete from ").append(entry.getHeader().getTableName()).append(" where ");
                for (CanalEntry.Column column : columnList) {
                    if (column.getIsKey()) {
                        sql.append(column.getName()).append("=").append(column.getValue());
                        break;
                    }
                }
                SQL_QUEUE.add(sql.toString());
            }
        } catch (InvalidProtocolBufferException e) {
            e.printStackTrace();
        }
    }
}
```

**`启动类`**

```java
@SpringBootApplication
public class AppDataCanalApplication implements CommandLineRunner {

    @Resource
    private CanalServer canalServer;

    public static void main(String[] args) {
        SpringApplication.run(AppDataCanalApplication.class, args);
    }


    @Override
    public void run(String... args) throws Exception {
        canalServer.run();
    }
}
```



启动测试，往数据库执行操作，可以直接在navicat里面操作，操作成功后，控制台会打印对应的sql

![image-20210927111729548](https://cdn.jsdelivr.net/gh/le1024/image1/le/image-20210927111729548.png)

注：这里的sql格式打印在代码里面写死的，可以根据实际需求获取数据，然后拼接成需要的sql或者其他数据对象存到别的数据库
