# lambda

> ##### List转成Map集合

```java
/**
 * uapOrganizations是从数据库查询出来的对象集合
 * getCode, getName 为转换后的map的key,value
 * k1, k2 是为了去除转换重复的数据
 */
List<UapOrganization> uapOrganizations = uapOrganizationService.selectCodeAndName();
Map<String, String> maps = uapOrganizations.stream().collect(Collectors.toMap(UapOrganization::getCode, UapOrganization::getName, (k1, k2) -> k1));
```



>  ##### List对象转成`List<String>`

获取List中某个属性的所有值,生成新的List

```java
//初始化用户数据，需要将User的id单独获取出来，全部存到List<String>中
List<User> users = initData(); 

List<String> ids = users.stream().map(user -> user.getId()).collect(Collectors.toList());
```



> ##### List对象重复属性，生成新`List<String>`

```java
List<String> repeatMenuName = list.stream().collect(Collectors.toMap(e -> e.getMenuName(), e -> 1, (a, b) -> a + b))
        .entrySet().stream()
        .filter(entry -> entry.getValue() > 1)
        .map(entry -> entry.getKey())
        .collect(Collectors.toList());
```



> ##### List排序

list集合对象根据某个属性排序

```java
升序
list.sort(Comparator.comparing(VO::getTime));

降序
list.sort((m1, m2) -> m2.getTime().compareTo(m1.getTime()));
```



> ##### List对某字段求和

```java
/** 
 * int 类型
 */
List<Integer> list = Arrays.asList(1 ,2 ,3 , 4, 5);
int total = list.stream().mapToInt(Integer::intValue).sum();
System.out.println(total);
int total2 = list.stream().reduce(0, Integer::sum);
System.out.println(total2);

/** 
 * long 类型
 */
List<Long> longList = Arrays.asList(1L, 2L, 3L, 4L, 5L);
Long longTotal = longList.stream().mapToLong(Long::longValue).sum();
Long longTotal2 = longList.stream().reduce(0L, Long::sum);
System.out.println(longTotal);
System.out.println(longTotal2);

/** 
 * BigDecimal 类型
 */
List<TestVO> decimalList = Arrays.asList(new TestVO(BigDecimal.valueOf(1)),
                                         new TestVO(BigDecimal.valueOf(2)),
                                         new TestVO(BigDecimal.valueOf(3)));
int decimalAddTotal = decimalList.stream().map(TestVO::getTotal).reduce(BigDecimal.ZERO, BigDecimal::add).intValue();
int decimalSubTotal = decimalList.stream().map(TestVO::getTotal).reduce(BigDecimal.ZERO, BigDecimal::subtract).intValue();
System.out.println(decimalAddTotal); //总和
System.out.println(decimalSubTotal); //总减
```



> ##### List对象转成一对多，`Map<String, List<Object>>`形式

```java
Map<String, List<HandleGivebackInfo>> givebackInfosMap = givebackInfos.stream().collect(Collectors.groupingBy(HandleGivebackInfo::getAuthId));
```



> ##### 多条件去重

```
List<GivebackExtendInfo> distList = itemInfoVo.getExtendInfoList().stream().collect(Collectors.collectingAndThen(
                   Collectors.toCollection(() -> new TreeSet<>(Comparator
                           .comparing(GivebackExtendInfo::getAuthId)
                           .thenComparing(GivebackExtendInfo::getBagId)
                           .thenComparing(GivebackExtendInfo::getRfidNum))
                   ), ArrayList<GivebackExtendInfo>::new));
```

