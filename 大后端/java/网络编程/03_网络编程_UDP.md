**网络编程——UDP**

****

- 类`DatagramSocket`和`DatagramPakcet`实现了基于UDP协议网络程序
- UDP数据报通过数据报套接字`DatagramSocket`发送和接收，系统不保证UDP数据报一定能够安全送达目的地，也不能确定什么时间送达
- `DatagramPacket`对象封装了UDP数据报，在数据报中包含了发送端的IP地址和端口号以及接收端的IP地址和端口号
- UDP协议中每个数据报都给出了完整的地址信息，因此无须建立发送方和接收方的连接。如同发快递包裹一样



##### UDP方式发送接收数据

```java
/**
     * 发送方
     */
    @Test
    public void sender() throws IOException {
        //创建DatagramSocket，通过DatagramSocket发送数据
        DatagramSocket socket = new DatagramSocket();

        String msg = "我是通过UDP方式发送的数据";
        byte[] data = msg.getBytes();
        //接收方的ip地址
        InetAddress inet = InetAddress.getByName("127.0.0.1");
        //创建UDP数据报,有发送的数据、接收方的ip和端口
        DatagramPacket packet = new DatagramPacket(data, 0, data.length, inet, 8888);

        socket.send(packet);

        socket.close();
    }
```

```java
/**
     * 接收方
     */
    @Test
    public void receiver() throws IOException {
        //创建DatagramSocket并指定端口号，通过DatagramSocket接收数据
        DatagramSocket socket = new DatagramSocket(8888);

        byte[] data = new byte[1024];
        DatagramPacket packet = new DatagramPacket(data, 0, data.length);

        //接收数据
        socket.receive(packet);
        System.out.println(new String(packet.getData(), 0, packet.getLength()));

        socket.close();
    }
```

![image-20220714220000396](img.assets/image-20220714220000396.png)