**网络编程——TCP**

****



##### 1.客户端给服务端发信息，服务端打印信息到控制台

```java
/**
     * 客户端
     */
    @Test
    public void client() {
        Socket socket = null;
        OutputStream os = null;

        try {
            //指定服务端ip
            InetAddress inet = InetAddress.getByName("127.0.0.1");
            //通过Socket传输数据，并指定服务端端口,端口需要同服务端端口一致
            socket = new Socket(inet, 8888);

            //获取输出流，向服务端写数据
            os = socket.getOutputStream();
            os.write("我是客户端~😊".getBytes());
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            //资源关闭
            if (os != null) {
                try {
                    os.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            if (socket != null) {
                try {
                    socket.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

    }
```

```java
/**
     * 服务端
     */
    @Test
    public void server() {
        ServerSocket ss = null;
        Socket socket = null;
        InputStream is = null;
        ByteArrayOutputStream baos = null;

        try {
            //定义服务端的socket，并指定端口，客户端需要向该端口发数据
            ss = new ServerSocket(8888);

            //通过accept()方法接收来自客户端的socket
            socket = ss.accept();
			//获取客户端socket的输入流，从中读取数据
            is = socket.getInputStream();

            //不建议直接用byte这样接收数据，可能会导致数据出现，byte定义的数组一次接收的数据是有限的
            //当某个中文字节正好没有接收完整就会出现乱码问题
//            byte[] buffer = new byte[1024];
//            int len;
//            while ((len = is.read(buffer)) != -1) {
//                String msg = new String(buffer, 0, len);
//                System.out.println(msg);
//            }

            // 使用ByteArrayOutputStream接收，ByteArrayOutputStream相当于一个缓存池，
            // 把数据都写入进去后，最后再一起取出来
            baos = new ByteArrayOutputStream();
            byte[] buffer = new byte[10];
            int len;
            while ((len = is.read(buffer)) != -1) {
                baos.write(buffer, 0 ,len);
            }
            System.out.println(baos.toString());


        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (baos != null) {
                try {
                    baos.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            if (is != null) {
                try {
                    is.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            if (socket != null) {
                try {
                    socket.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            if (ss != null) {
                try {
                    ss.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }
```

先启动服务端，然后再启动客户端，观察服务端控制台

![image-20220714204705102](img.assets/image-20220714204705102.png)





##### 2.客户端发送文件给服务端，服务端将文件保存到本地

```java
/**
     * 客户端
     */
    @Test
    public void client() throws IOException {
        //创建连接服务端的socket，ip和端口都是服务端的
        Socket socket = new Socket(InetAddress.getByName("127.0.0.1"), 8888);
        //创建一个输出流，用于输出数据
        OutputStream os = socket.getOutputStream();
        //创建文件输入流，需要发送文件流，文件需准备好
        FileInputStream fis = new FileInputStream(new File("32156101.png"));

        byte[] buffer = new byte[1024];
        int len;
        while ((len = fis.read(buffer)) != -1) {
            os.write(buffer, 0, len);
        }

        //实际需要在finally中关闭
        fis.close();
        os.close();
        socket.close();
    }
```

```java
/**
     * 服务端
     */
    @Test
    public void server() throws IOException {
        //创建服务端socket,指定端口
        ServerSocket ss = new ServerSocket(8888);
        //获取客户端的socket
        Socket socket = ss.accept();
        //从客户端的socket中获取输入流，
        InputStream is = socket.getInputStream();
        //创建一个文件输出流，将客户端socket中读取到流的数据输出
        FileOutputStream fos = new FileOutputStream(new File("new.png"));

        byte[] buffer = new byte[1024];
        int len;
        while ((len = is.read(buffer)) != -1) {
            fos.write(buffer, 0, len);
        }

        System.out.println("文件传输完毕~");

        //实际需要在finally中关闭
        fos.close();
        is.close();
        socket.close();
        ss.close();
    }
```

先启动服务端，再启动客户端，成功输出新的文件

![image-20220714210434339](img.assets/image-20220714210434339.png)

![image-20220714210450706](img.assets/image-20220714210450706.png)



##### 3.客户端给服务端发信息，服务端反馈信息给客户端

```java
@Test
    public void client() throws IOException {
        //客户端通过socket连接服务端，所以ip和端口肯定是服务端的
        Socket socket = new Socket(InetAddress.getByName("127.0.0.1"), 8090);

        OutputStream os = socket.getOutputStream();

        String msg = "来自客户端的问候~";
        os.write(msg.getBytes());

        //发送完毕后，需要执行shutdownOutput，不然服务端那边while的read处理会一直等待
        socket.shutdownOutput();

        //接收到来自服务端的反馈
        InputStream is = socket.getInputStream();
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        byte[] buffer = new byte[10];
        int len;
        while ((len = is.read(buffer)) != -1) {
            baos.write(buffer, 0, len);
        }
        System.out.println(baos.toString());

        //需要在finally中关闭
        os.close();
        socket.close();
    }
```

```java
@Test
    public void server() throws IOException {
        ServerSocket ss = new ServerSocket(8090);

        Socket socket = ss.accept();

        InputStream is = socket.getInputStream();

        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        byte[] buffer = new byte[10];
        int len;
        while((len = is.read(buffer)) != -1) {
            baos.write(buffer, 0, len);
        }
        System.out.println(baos);

        //给客户端反馈
        OutputStream os = socket.getOutputStream();
        os.write("您好，服务端收到您的问候，感谢！".getBytes());

        //需要在finally中关闭
        baos.close();
        is.close();
        socket.close();
        ss.close();
    }
```

先启动服务端，再启动客户端

![image-20220714213537900](img.assets/image-20220714213537900.png)

![image-20220714213546035](img.assets/image-20220714213546035.png)



##### 客户端和服务端实现简易聊天

