#### 1.HashMap底层

> HashMap的底层

JDK7底层实现：数组 + 链表

JDK8底层实现：<strong style="color:red">数组 + 链表 + 红黑树</strong>



#### 2.HashMap存储过程

> HashMap存储过程

在JDK8之前，创建HashMap集合对象的时候，在构造方法中会创建一个长度为16的Entry[] table 数组对象来存储键值对数据；在JDK8之后(包括8)，是在第一个put元素的时候创建长度为16的Entry[]数组对象来存储键值对数据



简单的理解下：

1.假设向哈希表中存储(k-v)张三-1，根据key张三的hashCode方法计算出hash值

（计算规则：<strong style="color:red">return (key == null) ? 0 : (h = key.hashCode()) ^ (h >>> 16);</strong>），

然后结合数组长度计算出索引，（计算规则：<strong style="color:red">(n - 1) & hash，n为数组长度</strong>）

如果数组对应的索引位置上没有数据，就会把张三-1存储出该位置上。假设索引为3

![image-20211111223359597](https://cdn.jsdelivr.net/gh/le1024/image1/le/image-20211111223359597.png)

2.向哈希表存储李四-2，同样先计算出hash值，然后结合数组长度算出索引，再存到数组中。假设索引为4

![image-20211111223438659](https://cdn.jsdelivr.net/gh/le1024/image1/le/image-20211111223438659.png)

3.向哈希表存储王五-3，假如key王五计算出的索引也是3，此时，数组在3的位置上已经存储了张三，所以需要去比较张三和王五的hash值是否一致，如果不一致，则在数组索引为3的位置以链表的方式存储王五->3

![image-20211111223547982](https://cdn.jsdelivr.net/gh/le1024/image1/le/image-20211111223547982.png)

4.向哈希表存储张三-4，此时根据hashCode计算出的索引肯定是3，然后再去比较hash值，如果hash值也相等了，此时发生<strong style="color:red">哈希碰撞</strong>，需要再次去equals两个key值是否相等。(举个例子，实际中不同的数据计算的hash和索引是可能相等的)

如果相等：将覆盖之前已存储的数据并返回旧数据

如果不相等：以链表的形式继续存储数据

![image-20211111224701938](https://cdn.jsdelivr.net/gh/le1024/image1/le/image-20211111224701938.png)





##### <strong style="color:black">面试题1：哈希表底层采用何种算法计算hash值？还有哪种些算法可以计算hash值？</strong>

```java
采用的是key的hashCode方法结合数组长度进行无符号右移(>>>)、按位异或(^)、按位与(&)计算出索引

还可以采用：取余、平方取中法、伪随机数法；但这些效率都不高
```

<strong style="color:black">面试题2：两个对象的hashCode相等时会怎样？</strong>

```java
会产生哈希碰撞，若key相同替换旧值，key不同存放链表，链表长度超过阈值8转为红黑树存储
```

<strong style="color:black">面试题3：如何解决哈希碰撞</strong>

```java
jdk8之前使用链表，jdk8后使用链表 + 红黑树
```

<strong style="color:black">面试题4：为什么使用红黑树</strong>

```java
提高效率。由于链表的查询慢，长度太长时间复杂度O(n)，效率很低；通过引用红黑树来提高效率，时间复杂度O(logn)

```

<strong style="color:red">当链表长度(阈值)超过 8 时且当前数组的长度 > 64时，将链表转换为红黑树</strong>



#### 3.HashMap继承关系

> HashMap继承关系

![image-20211112232040637](https://cdn.jsdelivr.net/gh/le1024/image1/le/image-20211112232040637.png)

继承抽象类：`AbstractMap`

实现接口：

- Cloneable：可克隆。创建并返回HashMap对象的副本
- Serializable：HashMap对象可被序列化和反序列化





#### 4.HashMap成员变量

> HashMap成员变量

1.`DEFAULT_INITIAL_CAPACITY`：集合的初始容量（必须是2的n次方）

```java
//默认初始容量是16，1 << 4相当于2的4次方，16
static final int DEFAULT_INITIAL_CAPACITY = 1 << 4; // aka 16
```

##### 问题：<strong style="color:black">为什么必须是2的n次方？如果不是2的n次方会怎么样？</strong>

```java
在向hashmap集合中存放元素的时候，会根据key的hashCode值得出hash值后结合数组长度计算出索引值，为了存取高效，
减少哈希碰撞，使得元素均匀存储，hashmap使用了取模的算法来实现，hash%length，而取余的效率不如位运算，从而使
用hash&(length - 1)，而hash%length等于hash&(length - 1)的前提是length是2的n次方。
```

为什么这样就可以减少hash碰撞？

举例：

说明：按位&运算，相同的二进制数位上，都是1的时候，结果为1，否则为0

```html
hash & (length - 1)
1.数组长度为2的n次幂
假设hash：3，数组长度length：8
3 & (8 - 1)
00000011 3
00000111 7
----------
00000011 3 索引3

假设hash：2，数组长度length：8
2 & (8 - 1)
00000010 2
00000111 7
----------
00000010 2 索引2

两次计算索引不一样

2.数组长度为2的n次幂
假设hash：3，数组长度length：9
3 & (9 -1)
00000011 3
00001000 8
----------
00000000 0 索引0

假设hash：2，数组长度length：9
2 & (9 - 1)
00000010 2
00001000 8
----------
00000000 0 索引0

两次计算索引都是0，这样计算极容易发生hash碰撞，导致数组空间没有合理的运用，造成资源浪费


```



2.`MAXIMUM_CAPACITY`：集合最大容量

```java
//2的30次方
static final int MAXIMUM_CAPACITY = 1 << 30;
```



3.`DEFAULT_LOAD_FACTOR`：加载因子

```java
static final float DEFAULT_LOAD_FACTOR = 0.75f;
```

集合的数组扩容时，不是当数组满时才会进行扩容，而是**满足length * 0.75就进行扩容，扩容是非常耗性能的，官方给了一个0.75的临界值以达到最佳的扩容效率**，另外扩容后原hashmap容量的2倍



4.`TREEIFY_THRESHOLD`：链表长度超过8转为红黑树(jdk8)（还需要数组长度超过64）

```java
static final int TREEIFY_THRESHOLD = 8;
```

之所以为8，是为了空间和时间的权衡。树节点占用空间是普通节点的两倍，当只有足够多的节点时才会转为树，而是否足够多节点是由TREEIFY_THRESHOLD决定的。链表长度达到8转为红黑树，长度降为6转为链表。

选择8是因为符合泊松分布公式：这里的0-8为存储元素的索引，当为8时，概率已经特别小了

```java
     * 0:    0.60653066
     * 1:    0.30326533
     * 2:    0.07581633
     * 3:    0.01263606
     * 4:    0.00157952
     * 5:    0.00015795
     * 6:    0.00001316
     * 7:    0.00000094
     * 8:    0.00000006
     * more: less than 1 in ten million
```



5.`UNTREEIFY_THRESHOLD`

```java
//红黑树长度为6转为链表
static final int UNTREEIFY_THRESHOLD = 6;
```



6.`MIN_TREEIFY_CAPACITY`

```java
//转为红黑树需要满足的另一个条件，数组的最小长度为64
static final int MIN_TREEIFY_CAPACITY = 64;
```



7.其他变量

```java
/* ---------------- Fields -------------- */

/**
     * The table, initialized on first use, and resized as
     * necessary. When allocated, length is always a power of two.
     * (We also tolerate length zero in some operations to allow
     * bootstrapping mechanics that are currently not needed.)
     */
	transient Node<K,V>[] table;

/**
     * Holds cached entrySet(). Note that AbstractMap fields are used
     * for keySet() and values().
     */
	transient Set<Map.Entry<K,V>> entrySet;

/**
     * The number of key-value mappings contained in this map.
     */
	transient int size; //hashmap实际存储的元素数量

/**
     * The number of times this HashMap has been structurally modified
     * Structural modifications are those that change the number of mappings in
     * the HashMap or otherwise modify its internal structure (e.g.,
     * rehash).  This field is used to make iterators on Collection-views of
     * the HashMap fail-fast.  (See ConcurrentModificationException).
     */
	transient int modCount; //hashmap操作的次数

/**
     * The next size value at which to resize (capacity * load factor).
     * 
     * @serial
     */
    // (The javadoc description is true upon serialization.
    // Additionally, if the table array has not been allocated, this
    // field holds the initial array capacity, or zero signifying
    // DEFAULT_INITIAL_CAPACITY.)
    int threshold; //等于capacity(数组长度默认16) * load factor(加载因子)，当该值超过size，进行扩容

/**
     * The load factor for the hash table.
     * 
     * @serial
     */
	final float loadFactor; //加载因子 0.75
```



#### 5.HashMap构造方法

![image-20211118112310158](https://cdn.jsdelivr.net/gh/le1024/image1/le/image-20211118112310158.png)



5.1 默认的空参构造方法，默认初始容量（16）和默认加载因子（0.75）

```java
public HashMap() {
    //加载因子默认为0.75
    this.loadFactor = DEFAULT_LOAD_FACTOR; // all other fields defaulted
}
```

5.2 指定初始容量构造方法，推荐使用这个

```java
// 传入初始容量值，加载因子默认0.75
public HashMap(int initialCapacity) {
    this(initialCapacity, DEFAULT_LOAD_FACTOR);
}
```

5.3 指定初始容量和加载因子构造方法

```java
//不建议修改默认加载因子的值
public HashMap(int initialCapacity, float loadFactor) {
    if (initialCapacity < 0)
        throw new IllegalArgumentException("Illegal initial capacity: " +
                                           initialCapacity);
    if (initialCapacity > MAXIMUM_CAPACITY)
        initialCapacity = MAXIMUM_CAPACITY;
    if (loadFactor <= 0 || Float.isNaN(loadFactor))
        throw new IllegalArgumentException("Illegal load factor: " +
                                           loadFactor);
    this.loadFactor = loadFactor;//依旧使用默认的加载因子0.75
    this.threshold = tableSizeFor(initialCapacity);//对传入的默认容量值进行计算得出正常的容量值(2的n次幂)
    //这里有个点注意下：threshold等于capacity(数组长度默认16) * load factor(加载因子)，而在下面tableSizeFor方法体的实现却不是这样的，和之前定义有矛盾。是这样的，在jdk8以后的构造方法中，并没有对table这个成员变量进行初始化，而是在put方法中进行初始化，threshold且进行了重新计算
}


// 计算hashmap的容量，前面说过，该容量值必须是2的n次幂，默认为16，当传参该值时不是2的n次幂，tableSizeFor方法会计算得出大于传参值最近的一个2的n次幂值作为hashmap容量值
//比如传的9，则返回16，传的20，返回32
static final int tableSizeFor(int cap) {
    int n = cap - 1;
    n |= n >>> 1;
    n |= n >>> 2;
    n |= n >>> 4;
    n |= n >>> 8;
    n |= n >>> 16;
    return (n < 0) ? 1 : (n >= MAXIMUM_CAPACITY) ? MAXIMUM_CAPACITY : n + 1;
}
```

5.4 传入map对象

```java
    public HashMap(Map<? extends K, ? extends V> m) {
        this.loadFactor = DEFAULT_LOAD_FACTOR;
        putMapEntries(m, false);
    }

    /**
     * Implements Map.putAll and Map constructor.
     *
     * @param m the map
     * @param evict false when initially constructing this map, else
     * true (relayed to method afterNodeInsertion).
     */
    final void putMapEntries(Map<? extends K, ? extends V> m, boolean evict) {
        int s = m.size(); //已有map集合size
        if (s > 0) {
            if (table == null) { // pre-size
                // 加1是为了向上取整，尽可能保证更大的容量，减少扩容的次数,扩容耗性能
                float ft = ((float)s / loadFactor) + 1.0F;
                int t = ((ft < (float)MAXIMUM_CAPACITY) ?
                         (int)ft : MAXIMUM_CAPACITY);
                if (t > threshold) //threshold默认0
                    threshold = tableSizeFor(t); //在t值的基础上计算出离该值最近的2的n次幂值
            }
            else if (s > threshold)
                resize();
            for (Map.Entry<? extends K, ? extends V> e : m.entrySet()) {
                K key = e.getKey();
                V value = e.getValue();
                putVal(hash(key), key, value, false, evict);
            }
        }
    }
```



#### 6.HashMap成员方法

##### 6.1 put方法

```java
public V put(K key, V value) {
    return putVal(hash(key), key, value, false, true);
}
```

```java
/**
 * 先调用hash方法，hashmap的key可以为null
 * 拿key的hashCode进行异或运算得到key的hash值
 */
static final int hash(Object key) {
    int h;
    return (key == null) ? 0 : (h = key.hashCode()) ^ (h >>> 16);
}
```

```java
final V putVal(int hash, K key, V value, boolean onlyIfAbsent,
                   boolean evict) {
        Node<K,V>[] tab; Node<K,V> p; int n, i;
        if ((tab = table) == null || (n = tab.length) == 0)
            /*
             * n=数组长度
             * 第一次put的时候，调用resize()方法扩容
             */
            n = (tab = resize()).length;
    	// (n-1) & hash 数组长度-1后与上面得到的hash值进行&运算得到索引，等价于%运算，但&运算的效率高
    	// 计算出的索引在tab中没有数据，新增数据，存在数组
        if ((p = tab[i = (n - 1) & hash]) == null)
            tab[i] = newNode(hash, key, value, null);
        else {
            Node<K,V> e; K k;
            // 计算出的索引对应位置有数据
            /**
             * p.hash == hash, 判断已有数据的hash值和put对象的hash是否相等
             * 不相等，跳过此判断，代码继续往下判断走
             * 相等，判断已有数据的key和put对象的key是否相等，如果相等，put了已有的key
             * 将对象赋值给临时对象e，后面用
             */
            if (p.hash == hash &&
                ((k = p.key) == key || (key != null && key.equals(k))))
                e = p;
            //判断是否是树，存到树中
            else if (p instanceof TreeNode)
                e = ((TreeNode<K,V>)p).putTreeVal(this, tab, hash, key, value);
            //此判断为链表，上面分别判断了数组，树
            else {
                for (int binCount = 0; ; ++binCount) {
                    // 判断已有元素下是否有节点元素，到这里已经是一个链表了，通过next寻找对象
                    if ((e = p.next) == null) {
                      //已有元素下没有节点元素，将已有元素指向put对象，然后链表新增一个新对象(新put的)
                        p.next = newNode(hash, key, value, null);
                        // 链表长度大于等于8，转为红黑树
                        if (binCount >= TREEIFY_THRESHOLD - 1) // -1 for 1st
                            treeifyBin(tab, hash);
                        break;
                    }
                    //如果p.next下已有元素，判断p.next跟put对象是否是相等，判断hash和key
                    //如果相等，说明已存在put对象的key，跳出循环
                    if (e.hash == hash &&
                        ((k = e.key) == key || (key != null && key.equals(k))))
                        break;
                    //上面条件都不满足，可以这样理解：链表的第一个对象没有匹配成功
                    //p=e, 将p赋值为链表的下一个对象，上面的e=p.next，然后再次循环判断
                    p = e;
                }
            }
            //key已经存在，替换旧值，并且返回旧值
            if (e != null) { // existing mapping for key
                V oldValue = e.value;
                if (!onlyIfAbsent || oldValue == null)
                    e.value = value;
                afterNodeAccess(e);
                return oldValue;
            }
        }
    	//map操作次数+1
        ++modCount;
        //size为存储的实际数据容量，每次put操作后+1,size值超过threshold后进行扩容
        // threshold是数组长度*负载因子0.75得到的结果
        if (++size > threshold)
            resize();
        afterNodeInsertion(evict);
        return null;
    }
```



##### 6.2 treeifyBin方法

链表转为红黑树

```java
// 链表的长度超过8(包含8),进行转换红黑树操作
if (binCount >= TREEIFY_THRESHOLD - 1) // -1 for 1st
    treeifyBin(tab, hash);
```

```java
final void treeifyBin(Node<K,V>[] tab, int hash) {
    int n, index; Node<K,V> e;
    // MIN_TREEIFY_CAPACITY值为64，加上上面的条件，只有链表长度>=8且数组的长度大于64才会进行转换红黑树
    if (tab == null || (n = tab.length) < MIN_TREEIFY_CAPACITY)
        //数组长度小于64，只进行扩容，这样扩容后hash值会重新计算，链表的长度可能会变短，相对于转成红黑树来说效率要高点
        resize();
    else if ((e = tab[index = (n - 1) & hash]) != null) {
        //hd红黑树的头节点 tl红黑树的尾节点
        TreeNode<K,V> hd = null, tl = null;
        do {
            //将当前链表节点e转换为树节点
            TreeNode<K,V> p = replacementTreeNode(e, null);
            if (tl == null)
                //将新建的节点赋值给红黑树的头节点，确定树的头节点
                hd = p;
            else {
                //树节点变成双向链表
                p.prev = tl;
                tl.next = p;
            }
            //确定树的尾节点
            tl = p;
        } while ((e = e.next) != null);
        //双向链表转化为红黑树
        if ((tab[index] = hd) != null)
            hd.treeify(tab);
    }
}
```

```java
final void treeify(Node<K,V>[] tab) {
            TreeNode<K,V> root = null;
            for (TreeNode<K,V> x = this, next; x != null; x = next) {
                next = (TreeNode<K,V>)x.next;
                x.left = x.right = null;
                if (root == null) {
                    x.parent = null;
                    x.red = false;
                    root = x;
                }
                else {
                    K k = x.key;
                    int h = x.hash;
                    Class<?> kc = null;
                    for (TreeNode<K,V> p = root;;) {
                        int dir, ph;
                        K pk = p.key;
                        if ((ph = p.hash) > h)
                            dir = -1;
                        else if (ph < h)
                            dir = 1;
                        else if ((kc == null &&
                                  (kc = comparableClassFor(k)) == null) ||
                                 (dir = compareComparables(kc, k, pk)) == 0)
                            dir = tieBreakOrder(k, pk);

                        TreeNode<K,V> xp = p;
                        if ((p = (dir <= 0) ? p.left : p.right) == null) {
                            x.parent = xp;
                            if (dir <= 0)
                                xp.left = x;
                            else
                                xp.right = x;
                            root = balanceInsertion(root, x);
                            break;
                        }
                    }
                }
            }
            moveRootToFront(tab, root);
        }
```



##### 6.3 resize方法

当HashMap中的元素个数超过数组大小(数组长度)*loadFactor(负载因子)时，就会进行数组扩容，loadFactor的默认值(DEFAULT_LOAD_FACTOR)是0.75,这是一个折中的取值。

<strong style="color:red">每次扩容都是翻倍的，扩容后，节点位置会被分配到"原位置"或"原位置 + 旧容量"的位置</strong>

因此，我们在扩充HashMap的时候，不需要重新计算hash，只需要看看原来的hash值新增的那个bit是1还是0就可以了，是0的话索引没变，是1的话索引变成“原索引+oldCap(**原位置+旧容量**)”。

扩容方法

```java
final Node<K,V>[] resize() {
    //得到当前数组
    Node<K,V>[] oldTab = table;
    //当前数组长度
    int oldCap = (oldTab == null) ? 0 : oldTab.length;
    //当前阈值点 默认是12(16*0.75)
    int oldThr = threshold;
    int newCap, newThr = 0;
    //开始扩容
    if (oldCap > 0) {
        //超过最大值不进行扩容
        if (oldCap >= MAXIMUM_CAPACITY) {
            threshold = Integer.MAX_VALUE;
            return oldTab;
        }
        //新数组长度扩为原来的两倍
        else if ((newCap = oldCap << 1) < MAXIMUM_CAPACITY &&
                 oldCap >= DEFAULT_INITIAL_CAPACITY)
            //阈值扩大一倍，newThr = oldThr * 2
            newThr = oldThr << 1; // double threshold
    }
    else if (oldThr > 0) // initial capacity was placed in threshold
        newCap = oldThr;
    else {               // zero initial threshold signifies using defaults
        newCap = DEFAULT_INITIAL_CAPACITY;
        newThr = (int)(DEFAULT_LOAD_FACTOR * DEFAULT_INITIAL_CAPACITY);
    }
    if (newThr == 0) {
        float ft = (float)newCap * loadFactor;
        newThr = (newCap < MAXIMUM_CAPACITY && ft < (float)MAXIMUM_CAPACITY ?
                  (int)ft : Integer.MAX_VALUE);
    }
    threshold = newThr;
    @SuppressWarnings({"rawtypes","unchecked"})
    Node<K,V>[] newTab = (Node<K,V>[])new Node[newCap];
    table = newTab;
    if (oldTab != null) {
        for (int j = 0; j < oldCap; ++j) {
            Node<K,V> e;
            if ((e = oldTab[j]) != null) {
                oldTab[j] = null;
                if (e.next == null)
                    newTab[e.hash & (newCap - 1)] = e;
                else if (e instanceof TreeNode)
                    ((TreeNode<K,V>)e).split(this, newTab, j, oldCap);
                else { // preserve order
                    Node<K,V> loHead = null, loTail = null;
                    Node<K,V> hiHead = null, hiTail = null;
                    Node<K,V> next;
                    do {
                        next = e.next;
                        if ((e.hash & oldCap) == 0) {
                            if (loTail == null)
                                loHead = e;
                            else
                                loTail.next = e;
                            loTail = e;
                        }
                        else {
                            if (hiTail == null)
                                hiHead = e;
                            else
                                hiTail.next = e;
                            hiTail = e;
                        }
                    } while ((e = next) != null);
                    if (loTail != null) {
                        loTail.next = null;
                        newTab[j] = loHead;
                    }
                    if (hiTail != null) {
                        hiTail.next = null;
                        newTab[j + oldCap] = hiHead;
                    }
                }
            }
        }
    }
    return newTab;
}
```



#### 7.HashMap容量初始化问题

根据《阿里巴巴Java开发手册》的建议：

![image-20211123100325477](https://cdn.jsdelivr.net/gh/le1024/image1/le/image-20211123100325477.png)

正确合理的初始化容量，防止因初始容量太小，而元素太多造成的多次扩容影响性能。

**initialCapacity=需存储元素个数 /  0.75 + 1**，之所以 + 1，是因为假如：需存储元素为6，6/0.75=8，计算出的初始容量为8，但存6个元素后，已经达到扩容的条件了，就会再次扩容一次；而加上1之后，6/0.75 + 1 = 9，得到的数组容量会是16，存6个元素是不会进行扩容的
