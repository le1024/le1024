> 介绍

CountDownLatch是一个辅助工具类，**用来同步一个或者多个任务，强制他们等待由其他任务执行一组操作完成，在完成之前将会一直等待**

CountDownLatch对象设置一个初始计数值，任何在这个对象上调用await()的方法都将阻塞，直到计数为0

CountDownLatch只能触发一次，计数值不会重置


示例1：
```java
package com.hll.dome1;

import java.util.Collections;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * @author helele
 * @Date 2021/5/27 21:19
 **/
public class CountDownLatchDemo {


    static final int SIZE = 10;

    /**
     * 用例1：主线程等待其他线程完成任务后才继续执行
     * @param args
     * @throws InterruptedException
     */
    public static void main(String[] args) throws InterruptedException {
        ExecutorService exec = Executors.newCachedThreadPool();

        CountDownLatch latch = new CountDownLatch(SIZE);
        for (int i = 0; i < 10; i++) {
            exec.execute(new WaitingTask(latch));
        }

        latch.await();
        System.out.println("所有任务执行完毕");
        exec.shutdown();
    }
}

class WaitingTask implements Runnable {
    private CountDownLatch latch;

    WaitingTask(CountDownLatch latch) {
        this.latch = latch;
    }

    @Override
    public void run() {
        try {
            Thread.sleep(1000);
            System.out.println(Thread.currentThread().getName() + "已完成任务！");
            latch.countDown();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
```
运行结果：
```java
pool-1-thread-9已完成任务！
pool-1-thread-6已完成任务！
pool-1-thread-4已完成任务！
pool-1-thread-7已完成任务！
pool-1-thread-5已完成任务！
pool-1-thread-10已完成任务！
pool-1-thread-8已完成任务！
pool-1-thread-2已完成任务！
pool-1-thread-3已完成任务！
pool-1-thread-1已完成任务！
所有任务执行完毕

Process finished with exit code 0

```


示例2：
```java
package com.hll.dome1;

import sun.awt.windows.ThemeReader;

import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * @author helele
 * @Date 2021/5/28 17:03
 **/
public class CountDownLatchDemo2 {


    /**
     * 用例2：多个线程一起等待，直到某个时候一起执行
     * 类似于整点抢购，只有到点了才能抢购，没到点就需要等待
     * @param args
     */
    public static void main(String[] args) throws InterruptedException {
        ExecutorService exec = Executors.newCachedThreadPool();

        CountDownLatch latch = new CountDownLatch(1); // 必须是1

        for (int i = 0; i < 10; i++) {
            exec.execute(new BuyTask(latch));
        }

        Thread.sleep(3000); //模拟到点
        System.out.println("开始抢购");
        latch.countDown();
    }
}

class BuyTask implements Runnable {
    private CountDownLatch latch;

    BuyTask(CountDownLatch latch) {
        this.latch = latch;
    }

    @Override
    public void run() {
        try {
            System.out.println(Thread.currentThread().getName() + "正在等待");
            latch.await();
            System.out.println(Thread.currentThread().getName() + "开始抢购");
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}

```
运行结果：
```java

pool-1-thread-2正在等待
pool-1-thread-5正在等待
pool-1-thread-6正在等待
pool-1-thread-4正在等待
pool-1-thread-1正在等待
pool-1-thread-3正在等待
pool-1-thread-8正在等待
pool-1-thread-10正在等待
pool-1-thread-7正在等待
pool-1-thread-9正在等待
开始抢购
pool-1-thread-2开始抢购
pool-1-thread-4开始抢购
pool-1-thread-6开始抢购
pool-1-thread-5开始抢购
pool-1-thread-10开始抢购
pool-1-thread-9开始抢购
pool-1-thread-7开始抢购
pool-1-thread-3开始抢购
pool-1-thread-8开始抢购
pool-1-thread-1开始抢购

```