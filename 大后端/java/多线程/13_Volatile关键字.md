#### Volatile关键字

------

一个共享变量(类的成员变量、类的静态成员变量)被volatile修饰之后：

- 保证了不同线程对这个变量进行操作时的可见性，即一个线程修改了这个变量的值，这个新值对其他线程是立即可见的。(**不保证原子性**)
- 禁止进行指令重排序。(**保证变量所在行的有序性**)
  - 当程序执行到volatile变量的读操作或者写操作时，在其前面的操作的更改肯定全部已经进行，且结果已经对后面的操作可见；在其后面的操作肯定还未进行
  - 在进行指令优化的时候，不能将在对volatile变量访问的语句放在其后面执行，也不能把volatile变量后面的语句放到其前面执行



##### 使用方式

基于volatile的作用，使用volatile必须满足以下两个条件：

- 对变量的写操作不能依赖当前值
- 该变量没有包含在具有其他变量的不变式中

```java
public class VolatileDemo {

    private volatile static boolean ready;

    private static int number;

    private static class PrintThread extends Thread {
        @Override
        public void run() {
            System.out.println("子线程运行中....");
            while (!ready) ; //不设置volatile，将会无限循环
            System.out.println("number:" + number);
        }
    }

    public static void main(String[] args) throws InterruptedException {
        new PrintThread().start();
        TimeUnit.SECONDS.sleep(1);

        number = 66;
        ready = true;
        TimeUnit.SECONDS.sleep(5);

        System.out.println("服务结束");
    }
}
```



##### volatile为什么不保证原子性

java中只有对基本类型变量的赋值和读取是原子操作，如i=1的赋值操作，但是像j=i、i++之类的操作不是原子操作。

所以一个变量被volatile修饰了，那么肯定可以保证每次读取到的这个变量值是最新的值，但是一旦对这个变量进行自增这样的非原子操作，就不能保证这个变量的原子性了。