#### 并发容器

------

因为同步容器几乎都是通过`synchronized`进行同步，这样虽然保证了线程的安全性，但代价就是严重降低了并发性能，当多个线程竞争时，吞吐量严重降低。



Java5.0开始针对多线程并发访问量重新设计，提供了并发性能较好的并发容器，引入了`java.util.concurrent`包。

##### <font color="#FF1493">ConcurrentHashMap</font>

- 对应非并发容器：HashMap
- 替代HashTable、Collections.synchronizedMap
- Java6采用加锁机制"Segment"分段锁，Java8采用CAS无锁算法

##### <font color="#FF1493">CopyOnWriteArrayList</font>

- 对应非并发容器：ArrayList
- 替代Vector、Collections.synchronizedList
- 利用高并发往往是读多写少的特性，对读操作不加锁，对写操作，先复制一份新的集合，在新的集合上进行修改，然后将新集合赋值给旧的引用，通过`volatile`保证其可见性，当然写操作还是需要加锁的

##### <font color="#FF1493">CopyOnWriteSet</font>

- 对应非并发容器：HashSet
- 替代Collections.synchronizedSet
- 基于`CopyOnWriteArrayList`实现，不同的是在add操作时调用的是`CopyOnWriteArrayList`的`addIfAbsent`方法，遍历当前Object数组，如Object数组中已有了当前元素，则直接返回，如果没有则放入Object数组的尾部并返回

##### <font color="#FF1493">ConcurrentSkipListMap</font>

- 对应非并发容器：TreeMap
- 替代Collections.synchronizedSortedMap
- Skip List（跳表），是一种可以代替平衡树的数据结构，默认是按照key值升序的。Skip List让已排序的数据分布在多层链表中，以0-1随机数决定一个数据的向上攀升与否，通过"空间换取时间"的一个算法。`ConcurrentSkipListMap`提供了一种线程安全的并发访问的排序映射表。内部是SkipList结构实现，理论上能够在*O(log(N))*时间内完成查找、插入、删除操作

##### <font color="#FF1493">ConcurrentSkipListSet</font>

- 对应非并发容器：TreeSet
- 替代Collections.synchronizedSortedSet
- 基于`ConcurrentSkipListMap`实现

##### <font color="#FF1493">ConcurrentLinkedQueue</font>

- 对应非并发容器：Queue
- 不会阻塞队列
- 基于链表实现的FIFO队列(LinkedList的并发版本)

##### <font color="#FF1493">LinkedBlockingQueue、ArrayBlockingQueue、PriorityBlockingQueue</font>

- 对应非并发容器：BlockingQueue
- 扩展了Queue，增加了可阻塞的插入和获取等操作，通过`ReentrantLock`实现了线程安全，通过`Condition`实现阻塞和唤醒
- LinkedBlockingQueue：基于链表的可阻塞的FIFO队列
- ArrayBlockingQueue：基于数组的可阻塞的FIFO队列
- PriorityBlockingQueue：按优先级排序的队列