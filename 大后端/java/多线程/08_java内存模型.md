> java内存模型

#### java程序执行流程

首先java`源代码文件(.java)`会被java编译器编译成`字节码文件(.class)`，然后会由JVM中的`类加载器`加载各个类的字节码文件，加载完毕之后，交由`JVM执行引擎`执行。

**java内存模型指的就是`Runtime Data Area(运行时数据区)`**，即程序执行期间用到的数据和相关信息保存区。

![image-20220412213438084](https://cdn.jsdelivr.net/gh/le1024/image1/le/RwheiCdmFtOanIH.png)



#### java内存模型

根据JVM规范，JVM内存共分为`虚拟机栈`、`堆`、`方法区`、`程序计数器`、`本地方法栈`五个部分。

![image-20220412220619129](https://s2.loli.net/2022/04/12/MTICD5dLle89wKJ.png)



##### 程序计数器

- 程序计数器记录线程正在执行的内存地址，以便被中断线程恢复执行时再次按照中断时的指令地址继续执行
- 每个线程对应一个程序计数器
- 各线程的程序计数器是线程私有的，互不影响，且线程安全

##### java栈(虚拟机栈 JVM Stack)

- 每个线程对应一个java栈
- 每个java栈由若干栈帧组成
- 每个方法对应一个栈帧
- 栈帧在方法运行时，创建并入栈；方法执行完，该栈帧弹出栈帧中的元素作为该方法返回值，该栈帧被清除
- 栈顶的栈帧叫做活动栈，表示当前执行的方法，可以被CPU执行
- 线程请求的栈深渡大于虚拟机所允许的深度，将抛出`StackOverflowError`异常
- 栈扩展时无法申请到足够的内存，就会抛出`OutOfMemoryError`异常



##### 方法区 MethodArea

- 方法区是java堆的永久区
- 方法区存放了要加载的类信息、类中的静态常量、类中定义的final类型的常量、类中属性信息、类中方法信息
- 方法区是被线程共享的
- 方法区要使用的内存超过其允许的大小时，会抛出`OutOfMemoryError:PremGen space`的错误信息



##### 常量池 ConstantPool

- 常量池是方法区的一部分
- 常量池存储两类数据：字面量和引用量
  - 字面量：字符串、final变量等
  - 引用量：类/接口、方法和字段的名称和描述符
- 常量池在编译期间就被确定，保存在已编译的.class文件中



##### 本地方法栈 Native Method Stack

- 本地方法栈和java栈的作用相似，区别是java栈为JVM执行java方法服务，而本地方法栈为JVM执行native方法服务
- 本地方法栈也会抛出`StackOverflowError`和`OutOfMemoryError`异常



#### java内存模型工作流程

1. 首先类加载器将java方法加载到方法区
2. 然后java执行引擎从方法区找到main方法
3. 为方法创建栈帧，并放入方法区，同时创建该栈帧的程序计数器
4. 执行引擎请求CPU执行该方法
5. CPU将方法栈数据加载到工作内存(寄存器和高速缓存)，执行该方法
6. CPU执行完之后将执行结果从工作内存同步到主内存

![image-20220412230058944](https://s2.loli.net/2022/04/12/Xl7i9aDKq5mZ6Ns.png)

记下：

```java
Object obj = new Object();

new Object这个对象是存在java堆中的，obj这个引用存在java栈中
```



