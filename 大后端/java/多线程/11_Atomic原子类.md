#### Atomic原子类

------

java.util.concurrent.atomic提供了很多的原子操作类，分为以下四类：

- 原子更新基本类型：AtomicInteger、AtomicBoolean、AtomicLong
- 原子更新数组：AtomicIntegerArray、AtomicLongArray
- 原子更新引用：AtomicReference、AtomicStampedReference
- 原子更新属性：AtomicIntegerFieldUpdater、AtomicLongFieldUpdater

提供原子类的目的就是为了解决基本类型操作的非原子性在多线程并发情况下引发的问题。



#### 问题演示

i++的非原子性操作问题

```java
public class AtomicDemo {

    static int n = 0;
    public static void main(String[] args) throws InterruptedException {
        int j = 0;
        while(j<100){
            n = 0;
            Thread t1 = new Thread(){
                public void run(){
                    for(int i=0; i<1000; i++){
                        n++;
                    }
                }
            };
            Thread t2 = new Thread(){
                public void run(){
                    for(int i=0; i<1000; i++){
                        n++;
                    }
                }
            };
            t1.start();
            t2.start();
            t1.join();
            t2.join();
            System.out.println("n的最终值是："+n);
            j++;
        }

    }

}
```

![image-20220413225454876](https://s2.loli.net/2022/04/13/pEkg8IU3MjWXwoY.png)

最终结果都是2000才是正确的结果，这里使用的多线程下的i++导致结果异常



**使用原子类操作解决问题**

```java
public class AtomicDemo {

    //static int n = 0;
    static AtomicInteger n;
    public static void main(String[] args) throws InterruptedException {
        int j = 0;
        while(j<100){
            //n = 0;
            n = new AtomicInteger(0);
            Thread t1 = new Thread(){
                public void run(){
                    for(int i=0; i<1000; i++){
                        //n++;
                        n.getAndIncrement();
                    }
                }
            };
            Thread t2 = new Thread(){
                public void run(){
                    for(int i=0; i<1000; i++){
                        //n++;
                        n.getAndIncrement();
                    }
                }
            };
            t1.start();
            t2.start();
            t1.join();
            t2.join();
            System.out.println("n的最终值是："+n.get());
            j++;
        }

    }

}
```



#### getAndIncrement原理

```java

n.getAndIncrement();

//AtomicInteger的getAndIncrement
public final int getAndIncrement() {
    	//this指的是当前AtomicInteger对象
    	//valueOffset 内存地址偏移量
        return unsafe.getAndAddInt(this, valueOffset, 1);
    }

//Unsafe
//var1当前AtomicInteger对象
//var2内存地址偏移量
//var4 1
public final int getAndAddInt(Object var1, long var2, int var4) {
        int var5;
        do { 
            //var1 var2计算出var5，var5是主内存的值，读进线程工作内存
            var5 = this.getIntVolatile(var1, var2);
            
            /**
             * var1 var2计算主内存的值，同var5比较，如果ok，var5+var4即var5+1,返回true，取反，while循环结束，返回var5。
             如果var1 var2计算的值与var5不一样，那么while就会进去死循环，但value是volatile的，当从主内存再去拿值时对比线程工作内存的值。
             无限自旋，比较和交换。
             */
        } while(!this.compareAndSwapInt(var1, var2, var5, var5 + var4));

        return var5;
    }

//
public final native boolean compareAndSwapInt(Object var1, long var2, int var4, int var5);
```



#### CAS问题