> Semaphore

```java
package com.hll.demo3;

import java.util.Random;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Semaphore;

/**
 * Semaphore 信号量，可以用来控制同时访问特定资源的线程数量，通过协调各个线程，以保证合理的使用资源
 * @author helele
 * @Date 2021/5/31 14:44
 **/
public class SemaphoreDemo1 {


    /**
     * 示例：停车场，车位数量有限，车位满了只能等待别的车离开之后才可以驶入
     * @param args
     */
    public static void main(String[] args) {
        ExecutorService exec = Executors.newCachedThreadPool();

        //模拟有3个停车位
        Semaphore semaphore = new Semaphore(3);

        // 模拟有6辆车停车
        for (int i = 1; i <= 6; i++) {
            exec.execute(new ParkTask(i, semaphore));
        }

        exec.shutdown();
    }
}

class ParkTask implements Runnable {
    Semaphore semaphore;
    int num;

    ParkTask(int num, Semaphore semaphore) {
        this.num = num;
        this.semaphore = semaphore;
    }

    @Override
    public void run() {

        try {
            /**
             * 获取令牌，尝试进入停车场
             */
            semaphore.acquire();
            System.out.println(num + "号车成功进入停车场");
            //模拟停车时间
            Thread.sleep(2000);
            System.out.println(num + "号车离开停车场");

        } catch (InterruptedException e) {
            e.printStackTrace();
        } finally {
            /**
             * 释放令牌，空闲出停车位
             */
            semaphore.release();
        }
    }
}

```

运行结果：
```java
2号车成功进入停车场
1号车成功进入停车场
3号车成功进入停车场
2号车离开停车场
1号车离开停车场
3号车离开停车场
4号车成功进入停车场
6号车成功进入停车场
5号车成功进入停车场
6号车离开停车场
4号车离开停车场
5号车离开停车场

Process finished with exit code 0
```