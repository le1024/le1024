> 介绍

CyclicBarrier 允许一组线程全部等待彼此达到共同屏障点的同步辅助


```java
package com.hll.demo2;

import java.util.concurrent.BrokenBarrierException;
import java.util.concurrent.CyclicBarrier;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * CyclicBarrier 允许一组线程全部等待彼此达到共同屏障点的同步辅助
 *
 * 加法计数
 * @author helele
 * @Date 2021/5/31 9:47
 **/
public class CyclicBarrierDemo1 {

    /**
     * @param args
     */
    public static void main(String[] args) {
        ExecutorService exec = Executors.newCachedThreadPool();

        CyclicBarrier cyclicBarrier = new CyclicBarrier(7, new Runnable() {
            @Override
            public void run() {
                System.out.println("七颗龙珠已集齐,开始召唤神龙！！");
            }
        });

        for (int i = 1; i <= 7; i++) {
            exec.execute(new DragonTask(i, cyclicBarrier));
        }

        exec.shutdown();
    }
}

class DragonTask implements Runnable {

    int num = 0;

    private CyclicBarrier cyclicBarrier;

    DragonTask(int num, CyclicBarrier cyclicBarrier) {
        this.num = num;
        this.cyclicBarrier = cyclicBarrier;
    }

    @Override
    public void run() {
        System.out.println("当前收集到" + ++num + "星球.");
        try {
            cyclicBarrier.await(); //等待
        } catch (InterruptedException | BrokenBarrierException e) {
            e.printStackTrace();
        }
    }
}

```

运行结果：
```java
当前收集到3星球.
当前收集到2星球.
当前收集到6星球.
当前收集到4星球.
当前收集到5星球.
当前收集到7星球.
当前收集到8星球.
七颗龙珠已集齐,开始召唤神龙！！
```