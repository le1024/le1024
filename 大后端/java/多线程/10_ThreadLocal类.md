**ThreadLocal**

------



#### 作用

ThreadLocal提供线程局部变量，即为使用相同变量的每一个线程维护一个该变量的副本。

当某些数据是以线程为作用域并且不同线程具有不同的数据副本的时候，就可以考虑采用ThreadLocal，比如数据库的连接connection，每个请求处理线程都需要，但又互不影响。



#### 示例

```java
public class ThreadLocalDemo {

    /**
     * 两个线程分别转账
     */

    //设置TheadLocal变量
    ThreadLocal<Integer> threadLocal = new ThreadLocal<Integer>() {
        @Override
        protected Integer initialValue() {
            return 0;
        }
    };

    //获取值
    public Integer get() {
        return threadLocal.get();
    }

    //设置值
    public void set() {
        threadLocal.set(threadLocal.get() + 10);
    }


    public static void main(String[] args) {
        ThreadLocalDemo threadLocalDemo = new ThreadLocalDemo();
        Transfer transfer = new Transfer(threadLocalDemo);

        Thread t1 = new Thread(transfer, "线程1");
        Thread t2 = new Thread(transfer, "线程2");

        t1.start();
        t2.start();
    }
}

class Transfer implements Runnable {
    ThreadLocalDemo bank;

    public Transfer(ThreadLocalDemo bank) {
        this.bank = bank;
    }

    @Override
    public void run() {
        //模拟转账10次
        for (int i = 0; i < 10; i++) {
            bank.set(); //进行转账
            System.out.println(Thread.currentThread().getName() + "账户余额：" + bank.get());
        }
    }
}
```

执行代码，可看到结果，两个线程操作的同一个变量是互不影响的

![image-20220413220407031](https://s2.loli.net/2022/04/13/LJQvKaRWTi7BMwq.png)



#### 分析

![image-20220413222654295](https://s2.loli.net/2022/04/13/PNKmpWdXB38EMJa.png)

