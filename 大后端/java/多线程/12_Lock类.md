#### Lock类

------



##### Lock接口关系图



`Lock`和`ReadWriteLock`是两大锁的根接口。

`Lock`接口支持重入、公平等的锁规则：实现类`ReentrantLock`、`ReadLock`、`WriteLock`。

`ReadWriteLock`接口定义读操作共享而写操作独占的锁，实现类：`ReentrantReadWriteLock`。



##### 重入锁

**不可重入锁**：线程请求已经拥有的锁时会阻塞。

**可重入锁**：线程可以再次进入已经拥有的锁的同步代码块。

```java
/**
     * 可重入锁，可以多次加锁
     */
    public static void main(String[] args) {
        ReentrantLock reentrantLock = new ReentrantLock();

        for (int i = 0; i < 10; i++) {
            reentrantLock.lock();
            System.out.println("加锁次数:" + (i+1));
        }

        for (int i = 0; i < 10; i++) {
            try {
                System.out.println("解锁次数:" + (i+1));
            } finally {
                reentrantLock.unlock();
            }
        }
    }
```



##### 读写锁

即可以同时读，不可以同时写；读的时候不能写，写的时候不能读。

```java
public class ReadWriteLockDemo {

    private Map<String, Object> map = new HashMap<>();

    //创建一个读写锁
    private ReadWriteLock readWriteLock = new ReentrantReadWriteLock();
    //创建一个读锁
    private Lock readLock = readWriteLock.readLock();
    //创建一个写锁
    private Lock writeLock = readWriteLock.writeLock();

    /**
     * 读操作
     * @param key
     * @return
     */
    public Object get(String key) {
        readLock.lock();
        System.out.println(Thread.currentThread().getName() + "正在执行读操作...");

        try {
            Thread.sleep(3000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        try {
            return map.get(key);
        } finally {
            readLock.unlock();
            System.out.println(Thread.currentThread().getName() + "读操作执行完成");
        }
    }

    /**
     * 写操作
     * @param key
     * @param value
     */
    public void put(String key, Object value) {
        writeLock.lock();
        System.out.println(Thread.currentThread().getName() + "正在执行写操作...");

        try {
            Thread.sleep(3000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        try {
            map.put(key, value);
        } finally {
            writeLock.unlock();
            System.out.println(Thread.currentThread().getName() + "写操作执行完成");
        }
    }

    public static void main(String[] args) {
        final ReadWriteLockDemo readWriteLockDemo = new ReadWriteLockDemo();
        readWriteLockDemo.put("k1", "v1");

        for (int i = 0; i < 5; i++) {
            new Thread(() -> readWriteLockDemo.get("k1")).start();
        }
    }
}
```

```tiki wiki
main正在执行写操作...
main写操作执行完成
Thread-0正在执行读操作...
Thread-2正在执行读操作...
Thread-1正在执行读操作...
Thread-3正在执行读操作...
Thread-4正在执行读操作...
Thread-3读操作执行完成
Thread-0读操作执行完成
Thread-1读操作执行完成
Thread-4读操作执行完成
Thread-2读操作执行完成
```

