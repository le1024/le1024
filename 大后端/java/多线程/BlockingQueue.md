> BlockingQueue

**阻塞队列**

| 方式         | 抛出异常  | 不抛异常 | 阻塞等待 | 超时等待                                |
| ------------ | --------- | -------- | -------- | --------------------------------------- |
| 添加元素     | add()     | offer()  | put()    | offer(E e, long timeout, TimeUnit unit) |
| 移除元素     | remove()  | poll()   | take()   | poll(long timeout, TimeUnit unit)       |
| 检测队首元素 | element() | peek()   | -        | -                                       |



**抛出异常**

```java
/**
     * 抛出异常
     */
public static void test1() {
    // 参数：capacity，队列容量
    ArrayBlockingQueue blockingQueue = new ArrayBlockingQueue<>(3);

    /**
         * 添加元素:返回Boolean值
         */
    System.out.println(blockingQueue.add("a"));
    System.out.println(blockingQueue.add("b"));
    System.out.println(blockingQueue.add("c"));

    /**
         * 获取队首元素
         */
        System.out.println(blockingQueue.element());

    /**
         * 会报异常，队列已满。
         * java.lang.IllegalStateException: Queue full
         */
    //System.out.println(blockingQueue.add("d"));

    /**
         * 移除元素:返回移除的元素
         */
    System.out.println(blockingQueue.remove());
    System.out.println(blockingQueue.remove());
    System.out.println(blockingQueue.remove());
    /**
         * 会报异常
         * java.util.NoSuchElementException
         */
    //System.out.println(blockingQueue.remove());
}
```



**返回值，不抛异常**

```java
/**
     * 返回值，不报异常
     */
    public static void test2() {
        // 参数：capacity，队列容量
        BlockingQueue blockingQueue = new ArrayBlockingQueue<>(3);

        /**
         * 添加元素:返回boolean
         */
        System.out.println(blockingQueue.offer("a"));
        System.out.println(blockingQueue.offer("b"));
        System.out.println(blockingQueue.offer("c"));

        /**
         * 获取队首元素，队列为空返回null
         */
        System.out.println(blockingQueue.peek());

        /**
         * 不报异常，返回boolean
         */
        System.out.println(blockingQueue.offer("d"));

        System.out.println("================================");

        /**
         * 移除元素: 存在可移除元素返回元素值，否则返回null,不报异常
         */
        System.out.println(blockingQueue.poll());
        System.out.println(blockingQueue.poll());
        System.out.println(blockingQueue.poll());
        System.out.println(blockingQueue.poll());
    }
```



**阻塞等待**

```java
    /**
     * 阻塞等待
     */
    public static void test3() {
        BlockingQueue blockingQueue = new ArrayBlockingQueue<>(3);

        try {
            /**
             * 添加元素
             */
            blockingQueue.put("a");
            blockingQueue.put("b");
            blockingQueue.put("c");

            /**
             * 将会一直阻塞
             */
            //blockingQueue.put("d");

            System.out.println("=======================");

            /**
             * 移除元素：返回元素值
             */
            System.out.println(blockingQueue.take());
            System.out.println(blockingQueue.take());
            System.out.println(blockingQueue.take());
            /**
             * 将会一直阻塞
             */
            System.out.println(blockingQueue.take());
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
```



**超时等待**

```java
    /**
     * 超时等待
     */
    public static void test4() {
        BlockingQueue blockingQueue = new ArrayBlockingQueue<>(3);

        try {
            /**
             * 添加元素:返回Boolean
             * offer(E e, long timeout, TimeUnit unit)
             */
            System.out.println(blockingQueue.offer("a", 2, TimeUnit.SECONDS));
            System.out.println(blockingQueue.offer("b", 2, TimeUnit.SECONDS));
            System.out.println(blockingQueue.offer("c", 2, TimeUnit.SECONDS));
            /**
             * 队列已满，阻塞两秒后结束，插入失败
             */
            System.out.println(blockingQueue.offer("d", 2, TimeUnit.SECONDS));

            System.out.println("=============================");

            /**
             * 移除元素：返回元素
             * poll(long timeout, TimeUnit unit)
             */
            System.out.println(blockingQueue.poll(2, TimeUnit.SECONDS));
            System.out.println(blockingQueue.poll(2, TimeUnit.SECONDS));
            System.out.println(blockingQueue.poll(2, TimeUnit.SECONDS));
            /**
             * 移除失败阻塞两秒后结束
             */
            System.out.println(blockingQueue.poll(2, TimeUnit.SECONDS));
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
```