> SynchronousQueue

同步队列。没有容量

写入一个元素，必须等待取出之后才可继续写入元素

Put之后必须take，才能继续put



```java
public class Demo1 {

    public static void main(String[] args) {
        SynchronousQueue<String> synchronousQueue = new SynchronousQueue<String>();

        ExecutorService exec = Executors.newCachedThreadPool();

        exec.execute(new PutTask(synchronousQueue));
        exec.execute(new TakeTask(synchronousQueue));
    }
}

class PutTask implements Runnable {
    SynchronousQueue synchronousQueue;

    PutTask(SynchronousQueue synchronousQueue) {
        this.synchronousQueue = synchronousQueue;
    }

    @Override
    public void run() {
        try {
            System.out.println(Thread.currentThread().getName() + " put 1");
            synchronousQueue.put("1");

            System.out.println(Thread.currentThread().getName() + " put 2");
            synchronousQueue.put("2");

            System.out.println(Thread.currentThread().getName() + " put 3");
            synchronousQueue.put("3");
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}

class TakeTask implements Runnable {
    SynchronousQueue synchronousQueue;

    TakeTask(SynchronousQueue synchronousQueue) {
        this.synchronousQueue = synchronousQueue;
    }

    @Override
    public void run() {
        try {
            System.out.println(Thread.currentThread().getName() + " take " + synchronousQueue.take());
            System.out.println(Thread.currentThread().getName() + " take " + synchronousQueue.take());
            System.out.println(Thread.currentThread().getName() + " take " + synchronousQueue.take());
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
```