> Centos7安装Oracle 11g





#### 一、准备工作

虚拟机安装的话，将虚拟机的硬盘内存设置40G以上

##### 1.系统版本

```bash
uname -m
```

<img src="https://cdn.jsdelivr.net/gh/le1024/image1/le/image-20210903141331900.png" alt="image-20210903141331900" style="float: left; zoom: 200%;" />



```bash
cat /etc/redhat-release
```



<img src="https://cdn.jsdelivr.net/gh/le1024/image1/le/image-20210903141541981.png" alt="image-20210903141541981" style="float: left; zoom: 200%;" />





#####  2.修改主机名

```bash
sed -i "s/HOSTNAME=localhost.localdomain/HOSTNAME=oracledb/" /etc/sysconfig/network

hostname oracledb
```

![image-20210903141739898](https://cdn.jsdelivr.net/gh/le1024/image1/le/image-20210903141739898.png)



##### 3.添加主机与ip对应记录

```bash
# 安装vim,已安装请忽略
yum -y install vim

# 编辑hosts文件
vim /etc/hosts
```

在打开的文件末尾添加一条hosts记录，格式：IP 主机名

添加完成保存退出

![image-20210903142223875](https://cdn.jsdelivr.net/gh/le1024/image1/le/image-20210903142223875.png)



##### 4.关闭Selinux

```bash
sed -i "s/SELINUX=enforcing/SELINUX=disabled/" /etc/selinux/config

setenforce 0
```

![image-20210903142404730](https://cdn.jsdelivr.net/gh/le1024/image1/le/image-20210903142404730.png)



##### 5.创建用户和组

创建Oracle安装组`oinstall`，数据库管理员组`dba`，及`oracle`用户

```bash
# 添加组oinstall
groupadd -g 200 oinstall

# 添加组dba
groupadd -g 201 dba

# 添加用户oracle
useradd -u 440 -g oinstall -G dba oracle

# 设置用户oracle密码
passwd oracle
```

![image-20210903144113397](https://cdn.jsdelivr.net/gh/le1024/image1/le/image-20210903144113397.png)



##### 6.修改内核参数

```bash
# 编辑sysctl.conf文件，末尾添加下面的参数
vim /etc/sysctl.conf
```

```bash
net.ipv4.ip_local_port_range= 9000 65500

fs.file-max = 6815744

kernel.shmall = 10523004

kernel.shmmax = 6465333657

kernel.shmmni = 4096

kernel.sem = 250 32000 100128

net.core.rmem_default=262144

net.core.wmem_default=262144

net.core.rmem_max=4194304

net.core.wmem_max=1048576

fs.aio-max-nr = 1048576
```

![image-20210903144335500](https://cdn.jsdelivr.net/gh/le1024/image1/le/image-20210903144335500.png)

保存并退出

```bash
#使配置生效，sysctl.conf保存退出后执行这个命令
sysctl -p
```

![image-20210903144529783](https://cdn.jsdelivr.net/gh/le1024/image1/le/image-20210903144529783.png)



##### 7.修改系统限制资源

```bash
# 编辑limits.conf，末尾添加下面的参数
vim /etc/security/limits.conf
```

![image-20210903145028760](https://cdn.jsdelivr.net/gh/le1024/image1/le/image-20210903145028760.png)

保存并退出



##### 8.修改用户验证选项

```bash
vim /etc/pam.d/login
```

找到这一行：`session required pam_namespace.so`，在改行下面加上一条`pam_limits.so`

```bash
session    required     pam_limits.so
```

![image-20210903145355342](https://cdn.jsdelivr.net/gh/le1024/image1/le/image-20210903145355342.png)

保存并退出



##### 9.创建安装目录并分配权限

```bash
mkdir -p /opt/app/oracle/

chmod 755 /opt/app/oracle/

chown oracle.oinstall -R /opt/app/oracle/

```

![image-20210903145729807](https://cdn.jsdelivr.net/gh/le1024/image1/le/image-20210903145729807.png)



##### 10.设置Oracle环境变量

可以用xshell再开一个窗口，登录oracle用户

```bash
su - oracle
```

```bash
# 切换oracle用户后，编辑.bash_profile文件
vim ~/.bash_profile
```

注释掉最后两行，在末尾添加如下参数：

```bash
export ORACLE_BASE=/opt/app/oracle

export ORACLE_HOME=$ORACLE_BASE/product/11.2.0/db_1

export PATH=$PATH:$HOME/bin:$ORACLE_HOME/bin

export ORACLE_SID=orcl

export ORACLE_PID=ora11g

export NLS_LANG=AMERICAN_AMERICA.AL32UTF8

export LD_LIBRARY_PATH=$ORACLE_HOME/lib:/usr/lib
```

![image-20210903150256999](https://cdn.jsdelivr.net/gh/le1024/image1/le/image-20210903150256999.png)

保存并退出

`使配置生效`

```bash
source ~/.bash_profile
```

`查看环境变量配置是否完成`

```bash
env | grep ORA
```

![image-20210903150947699](https://cdn.jsdelivr.net/gh/le1024/image1/le/image-20210903150947699.png)



#### 二、安装及配置

##### 1.安装需要依赖：

以下是需要安装的依赖：

```bash
binutils-2.23.52.0.1-12.el7.x86_64

compat-libcap1-1.10-3.el7.x86_64

compat-libstdc++-33-3.2.3-71.el7.i686

compat-libstdc++-33-3.2.3-71.el7.x86_64

gcc-4.8.2-3.el7.x86_64

gcc-c++-4.8.2-3.el7.x86_64

glibc-2.17-36.el7.i686

glibc-2.17-36.el7.x86_64

glibc-devel-2.17-36.el7.i686

glibc-devel-2.17-36.el7.x86_64

ksh

libaio-0.3.109-9.el7.i686

libaio-0.3.109-9.el7.x86_64

libaio-devel-0.3.109-9.el7.i686

libaio-devel-0.3.109-9.el7.x86_64

libgcc-4.8.2-3.el7.i686

libgcc-4.8.2-3.el7.x86_64

libstdc++-4.8.2-3.el7.i686

libstdc++-4.8.2-3.el7.x86_64

libstdc++-devel-4.8.2-3.el7.i686

libstdc++-devel-4.8.2-3.el7.x86_64

libXi-1.7.2-1.el7.i686

libXi-1.7.2-1.el7.x86_64

libXtst-1.2.2-1.el7.i686

libXtst-1.2.2-1.el7.x86_64

make-3.82-19.el7.x86_64

sysstat-10.1.5-1.el7.x86_64

unixODBC-2.3.1-6.el7.x86_64 or later

unixODBC-2.3.1-6.el7.i686 or later

unixODBC-devel-2.3.1-6.el7.x86_64 or later

unixODBC-devel-2.3.1-6.el7.i686 or later
```

**切换到 root 用户，使用yum -y install 进行安装**

```bash
yum -y install binutils compat-libcap1 compat-libstdc++-33 compat-libstdc++-33*i686 compat-libstdc++-33*.devel compat-libstdc++-33 compat-libstdc++-33*.devel gcc gcc-c++ glibc glibc*.i686 glibc-devel glibc-devel*.i686 ksh libaio libaio*.i686 libaio-devel libaio-devel*.devel libgcc libgcc*.i686 libstdc++ libstdc++*.i686 libstdc++-devel libstdc++-devel*.devel libXi libXi*.i686 libXtst libXtst*.i686 make sysstat unixODBC unixODBC*.i686 unixODBC-devel unixODBC-devel*.i686
```

**检查是否全部已经安装，没安装的单独进行安装**

```bash
rpm -q binutils compat-libcap1 compat-libstdc++-33 gcc gcc-c++ glibc glibc-devel ksh libaio libaio-devel libgcc libstdc++ libstdc++-devel libXi libXtst make sysstat unixODBC unixODBC-devel
```

**如果提示有未安装上的，需要手动下载安装**

下载rpm，手动rpm -ivh安装，可以在`https://pkgs.org/`下载需要的包

```bash
1.compat-libcap1-1.10-3.el7.x86_64
http://rpmfind.net/linux/rpm2html/search.php?query=compat-libcap1

2.compat-libstdc++-33
http://mirror.centos.org/centos/7/os/x86_64/Packages/compat-libstdc++-33-3.2.3-72.el7.x86_64.rpm

3.libaio-devel
http://mirror.centos.org/centos/7/os/x86_64/Packages/libaio-devel-0.3.109-13.el7.x86_64.rpm

4.unixODBC-devel
http://mirror.centos.org/centos/7/os/x86_64/Packages/unixODBC-devel-2.3.1-14.el7.x86_64.rpm
```



##### 2.安装Oracle

安装unzip，如果已安装请忽略

```bash
yum -y install unzip
```

将下载好的Oracle包上传到/opt目录下，并在opt目录下进行解压

![image-20210903152703676](https://cdn.jsdelivr.net/gh/le1024/image1/le/image-20210903152703676.png)

```bash
ls *.zip | xargs -n1 unzip -o

或者分开解压

unzip linux.x64_11gR2_database_1of2.zip

unzip linux.x64_11gR2_database_2of2.zip
```

解压会自动创建一个`database`文件目录，该目录下有三个`.rsp`文件，用来作为静默安装时的应答文件的模板

三个文件作用：

`db_install.rsp`：安装应答

`dbca.rsp`：创建数据库应答

`netca.rsp`：建立监听、本地服务名等网络设置应答

![image-20210903153421363](https://cdn.jsdelivr.net/gh/le1024/image1/le/image-20210903153421363.png)



修改静默安装的配置文件

```bash
cd /opt/database/response

# 备份一份db_install.rsp，以免修改出错
cp db_install.rsp db_install_copy.rsp

# 编辑db_install.rsp
vim db_install.rsp
```

按照下面的参数进行修改，其他的默认：

```bash
oracle.install.option=INSTALL_DB_SWONLY

ORACLE_HOSTNAME=oracledb

UNIX_GROUP_NAME=oinstall

INVENTORY_LOCATION=/opt/app/oracle/oraInventory

SELECTED_LANGUAGES=en,zh_CN

ORACLE_HOME=/opt/app/oracle/product/11.2.0/db_1

ORACLE_BASE=/opt/app/oracle

oracle.install.db.InstallEdition=EE

oracle.install.db.DBA_GROUP=dba

oracle.install.db.OPER_GROUP=oinstall

oracle.install.db.config.starterdb.type=GENERAL_PURPOSE

oracle.install.db.config.starterdb.globalDBName=ora11g

oracle.install.db.config.starterdb.SID=ora11g

oracle.install.db.config.starterdb.memoryLimit=1500

oracle.install.db.config.starterdb.password.ALL=oracle

oracle.install.db.config.starterdb.storageType=FILE_SYSTEM_STORAGE

oracle.install.db.config.starterdb.fileSystemStorage.dataLocation=/data/oracle/oradata

oracle.install.db.config.starterdb.fileSystemStorage.recoveryLocation=/data/oracle/fast_recovery_area

DECLINE_SECURITY_UPDATES=true
```

保存退出

查看配置：

```bash
less /opt/database/response/db_install.rsp |grep -v "#"|grep -v "^$"
```



##### 3.开始静默安装

切换到`oracle`用户，为了方便打开另xshell窗口操作：

```bash
su - oracle

# 进入/opt/database目录
cd /opt/database

#进行安装
./runInstaller -silent -force -responseFile /opt/database/response/db_install.rsp
```

![image-20210903155436827](https://cdn.jsdelivr.net/gh/le1024/image1/le/image-20210903155436827.png)

安装过程会需要点时间，卡在当前界面，**如果没有[FATAL]或者其他报错，请不要关掉当前窗口或者Ctrl+C，[WARNING]可以忽略**

可以另开一个窗口查看安装进度：

```bash
# 日志文件在安装提示信息里面的可以看到，注意不要复制下面的，这个是我本次安装的日志
tail -f /tmp/OraInstall2021-09-03_12-50-59AM/installActions2021-09-03_12-50-59AM.log
```



当出现 Successfully Setup Software. 证明已经安装成功，然后根据提示以 root 用户执行脚本

![image-20210903160003741](https://cdn.jsdelivr.net/gh/le1024/image1/le/image-20210903160003741.png)

![image-20210903160311037](https://cdn.jsdelivr.net/gh/le1024/image1/le/image-20210903160311037.png)



#### 三、配置监听程序

**监听命令**：

启动监听：lsnrctl start

停止监听：lsnrctl stop

重启监听：lsnrctl reload

查看监听：lsnctl status

##### 1.配置监听(oracle用户)

```bash
su - oracle

# 配置监听
$ORACLE_HOME/bin/netca /silent /responseFile /opt/database/response/netca.rsp
```

![image-20210903160606967](https://cdn.jsdelivr.net/gh/le1024/image1/le/image-20210903160606967.png)



##### 2.查看监听状态(oracle用户)

```bash
## 监听状态
lsnrctl status
```

![image-20210903160728181](https://cdn.jsdelivr.net/gh/le1024/image1/le/image-20210903160728181.png)



##### 3.静默dbca建库(root用户)

```bash
su - root

vim /opt/database/response/dbca.rsp
```

修改如下内容：

在非编辑模式下，可通过输入`:n`跳转指定行

```javascript
输入 :n 
即跳转到第n行， :10 ，回车之后就跳转到第10行
```

```bash
GDBNAME = "orcl" # 78 行

SID="orcl" # 149行

CHARACTERSET="AL32UTF8" # 415行

NATIONALCHARACTERSET="UTF8" # 425行
```

保存退出



静默dbca建库：

```bash
# 切换oracle用户
su - oracle

$ORACLE_HOME/bin/dbca -silent -responseFile /opt/database/response/dbca.rsp
```

**`执行完后会先清屏，清屏之后没有提示，直接输入oracle用户的密码，回车，再输入密码确认一次，再回车。`**

**这个密码也可以写别的，但是需要记住，这个就是完成成功后System用户的登录密码**

![image-20210903161637622](https://cdn.jsdelivr.net/gh/le1024/image1/le/image-20210903161637622.png)

稍等一会，会开始自动创建：

![image-20210903162029507](https://cdn.jsdelivr.net/gh/le1024/image1/le/image-20210903162029507.png)



##### 4.启动数据库(oracle用户)

```bash
sqlplus / as sysdba
```

![image-20210903162150561](https://cdn.jsdelivr.net/gh/le1024/image1/le/image-20210903162150561.png)

这一步oracle数据库应该是启动过了，如果没启动，执行`startup`

```sql
SQL> startup
```

启动后，可以使用 show parameter；或者 select table_name from dba_tables 看看是否正常



##### 5.配置开机自动启动监听、启动Oracle(root用户)

```bash
su - root

vim /etc/oratab
```

按照配置来设置，成功安装后，`$ORACLE_SID`和`$ORACLE_HOME`应该自动设置好了，只需把最后的`N`改成`Y`。

`$ORACLE_SID`：是orcl，上面的步骤里在`/opt/database/response/dbca.rsp`配置过

`$ORACLE_HOME`：`db_install.rsp`文件配置过

![image-20210903163630286](https://cdn.jsdelivr.net/gh/le1024/image1/le/image-20210903163630286.png)

保存退出



```bash
# 编辑rc.local
vim /etc/rc.local
```

在末尾添加：

```bash
su - oracle -c 'dbstart'
su - oracle -c 'lsnrctl start'
```

![image-20210903164314644](https://gitee.com/le1024/image1/raw/master/img/image-20210903164314644.png)

保存退出



```bash
#配置文件权限
chmod +x /etc/rc.local
```

配置完成



#### 四、开放远程访问

开放1512端口

```bash
firewall-cmd --permanent --zone=public --add-port=1521/tcp
```

重启防火墙

```bash
systemctl restart firewalld.service
```

查看端口是否开放

```bash
firewall-cmd --query-port=1521/tcp
```



或者直接关闭防火墙



DataGrip连接：

![image-20210903173535263](https://cdn.jsdelivr.net/gh/le1024/image1/le/image-20210903173535263.png)



#### 五、密码

还是记下吧

Oracle服务启动之后，可以通过DB工具去连接

默认用户：

`sys`：超级管理员，登录时用户名应该输入：sys as sysdba

`system`：管理员，用户名还是system

密码统一是：**静默dbca建库**时配置的密码



#### 六、创建表和用户

1、建表空间

```bash
CREATE TABLESPACE TEST DATAFILE '/db/oracle/oradata/orcl/test.dbf' SIZE 200M AUTOEXTEND ON; 
```


表空间大小200M，自动扩展，AUTOEXTEND是否自动扩展

2、创建用户

```bash
CREATE USER TEST IDENTIFIED BY TEST123 DEFAULT TABLESPACE TEST;
```


3、赋权限

```bash
GRANT DBA TO TEST;
```



4、设置oracle登录不区分大小写

```bash 
alter system set sec_case_sensitive_logon=false;
```

 

![image-20210903172853843](https://cdn.jsdelivr.net/gh/le1024/image1/le/image-20210903172853843.png)



#### 七、相关问题

##### 1.bash: sqlplus: command not found...

解决1：如果是用`su oracle`切换用户的是不能正常使用sqlplus的，需要`su - oracle`切换才可以。

```html
带-切换用户是会读取目标用户的环境配置文件
```



#### ~~八、导出导入~~

~~只记录了简单的备份，其他参数百度~~

~~导出：~~

~~`exp username/password@orcl file=/opt/backupdata/oracle/test1.dmp tablespaces=test`~~

~~username：用户名~~

~~password：密码~~

~~orcl：你的实例~~

~~tablespaces：导出的表空间~~

```sql
exp test/test123@orcl file=/opt/backupdata/oracle/test1.dmp tablespaces=test
```



~~导入：~~

~~`ignore=y`：表已存在时，会导入失败，设置ignore=y忽略建表~~

```sql
imp test/test123@orcl file=/opt/backupdata/oracle/test1.dmp fromuser=test touser=test ignore=y
```

~~windows下导入报错，把@去掉：~~

```sql
imp test/test123orcl file=/opt/backupdata/oracle/test1.dmp fromuser=test touser=test ignore=y

# 不写fromuser touser的话，用full=y代替
```





------



结束了~~

