> #### 表中有字段，查询报错ORA-00904: "xxx": 标识符无效

```sql
--确认不是字段未建立的问题的话，可以执行下语句，把表字段全部更新为大写
begin
for c in (select COLUMN_NAME cn from all_tab_columns where table_name='表名') loop
begin
execute immediate 'alter table 表名 rename column "'||c.cn||'" to '||c.cn;
exception
when others then
dbms_output.put_line('表名'||'.'||c.cn||'已经存在');
end;
end loop;
end;
```



> #### Oracle ORA-01033: ORACLE initialization or shutdown in progress

任务管理器关闭数据库之后，plsql重新连接数据库报错或者sqlplus里面执行`alter database open`报错

```sql
sqlplus /nolog

conn sys/123456 as sysdba

shutdown normal

startup mount

#最后开启数据库
alter database open
```

或者在任务管理器，把oracle相关的服务都重启下



> #### ORA-01157: 无法标识/锁定数据文件 7 - 请参阅 DBWR 跟踪文件

直接在文件夹中删除了表空间，重连数据库会报这个错

解决方法：

```sql
# genggai表空间
alter database datafile 'd:\test.dbf' offline drop;

#重新打开数据库
alter database open;

#再删除表空间
drop tablespace test including contents;
```



> #### imp导入数据库脚本，提示ORA-12899: 列xxxx 值太大 (实际值: 21, 最大值: 20)

```bash
修改数据库字符集为：ZHS16GBK


在oracle目录下 打开应用程序开发--> SQL Plus，然后：
查看服务器端字符集SQL > select * from V$NLS_PARAMETERS 
修改：
$sqlplus /nolog 
SQL>conn / as sysdba
若此时数据库服务器已启动，则先执行 SHUTDOWN IMMEDIATE 命令关闭数据库服务器， 
然后执行以下命令: 
SQL>shutdown immediate; 
SQL>STARTUP MOUNT; 
SQL>ALTER SYSTEM ENABLE RESTRICTED SESSION; 
SQL>ALTER SYSTEM SET JOB_QUEUE_PROCESSES=0; 
SQL>ALTER SYSTEM SET AQ_TM_PROCESSES=0; 
SQL>ALTER DATABASE OPEN; 
SQL>ALTER DATABASE CHARACTER SET ZHS16GBK;

接下来可能出现两种情况：
ERROR at line 1  RA-12721: operation cannot execute when other sessions are active 
1、若出现上面的错误，使用下面的办法进行修改，使用INTERNAL_USE可以跳过超集的检查： 
SQL>ALTER DATABASE CHARACTER SET INTERNAL_USE ZHS16GBK; 

ORA-12712: new character set must be a superset of old character set
RROR at line 1: 
2、结果报错，提示新字符集必须是老字符集的超集。 
于是强制转换 
>ALTER DATABASE character set INTERNAL_USE ZHS16GBK; 

最后步骤： 
>shutdown immediate; 
>STARTUP; 
```

