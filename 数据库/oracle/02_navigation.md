#### 1.新建表和用户

```sql
--创建临时表空间
CREATE TEMPORARY TABLESPACE TEST_TEMP TEMPFILE '/home/oracle/tablespace/TEST_TEMP.DBF' SIZE 32M AUTOEXTEND ON NEXT 32M MAXSIZE UNLIMITED EXTENT MANAGEMENT LOCAL;


--创建表
CREATE TABLESPACE TEST LOGGING DATAFILE '/home/oracle/tablespace/TEST.DBF' SIZE 32M AUTOEXTEND ON NEXT 32M MAXSIZE UNLIMITED EXTENT MANAGEMENT LOCAL;

--创建用户
CREATE USER test IDENTIFIED BY test12345 ACCOUNT UNLOCK DEFAULT TABLESPACE TEST TEMPORARY TABLESPACE TEST_TEMP;

--赋予用户权限
GRANT CONNECT,RESOURCE TO test;
GRANT DBA TO test;（可选，DBA为数据库管理员权限）
```



#### 2.删除用户及权限

`cascade`可以把用户连带的数据也一并删除

```sql
drop user user_name cascade;
```



#### 3.导入导出(expdp impdp方式)

**为防止出现其他意外情况：expdp和impdp目录都为英文目录，否则会实现失败的情况**

**导出expdp**

`sqlplus /nolog`进入到oracle，登录sys用户

先创建逻辑目录，磁盘对应位置要创建物理目录

dump_dir：可以自定义其他名字

```sql
create directory dump_dir as 'C:\app\dump_dir';
grant read,write on directory dump_dir TO SCOTT;
```

注：directory=dba_dir 必须在账号密码的后面，如果放在其他位置，会有以下报错

```sql
ORA-39002: 操作无效
ORA-39070: 无法打开日志文件。
ORA-39087: 目录名 DUMP_DIR无效
```



如果忘记之前创建的`dump_dir`，可以连接数据库通过命令查询:

```sql
#运行这个命令后，找到之前创建的目录
SQL>select * from dba_directories;
```



打开cmd，执行导出，不用进入到数据库

```sql
#按用户导出
expdp SCOTT/SCOTT directory=dump_dir schemas=SCOTT dumpfile=scott_20181125.dmp
#按表导出
expdp username/password@你的实例 directory=dump_dir tables=emp,dept dumpfile=expdp.dmp
#按查询条件导出
expdp username/password@你的实例 directory=dump_dir dumpfile=expdp.dmp tables=empquery='where deptno=20'									
#按表空间导出
expdp XXXXX/XXXXX@xxxx directory=dump_dir dumpfile=tablespace.dmp tablespaces=temp,example
#导出整个数据库
expdp XXXXX/XXXXX@xxxx directory=dump_dir dumpfile=full.dmp full=y

expdp: 导出使用命令。
SCOTT/SCOTT: 指定导出数据时用来登录数据库的用户名和密码。
directory=dump_dir: 指定使用的目录
schemas=SCOTT: 指定导出SCOTT用户表
tablespaces=TEMP：按空间导出
FULL=Y：用这个条件可直接导出整个数据库
dumpfile=scott_20181125.dmp: 指定导出dmp文件名称
REUSE_DUMPFILES=y ：覆盖已存在的文件
```

导出失败的话，去掉expdp命令的@实例试试



**导入impdp**

打开cmd，执行导入，不用进入到数据库

```sql
impdp SCOTT/SCOTT directory=db_bak dumpfile=scott_20181125.dmp schemas=SCOTT

如果导入的表空间不存在，需要事先创建好

1)导入用户（例：从用户scott导入到用户scott）   ※注：文件名区分大小写								
impdp scott/XXXXX@xxxx directory=dump_dir dumpfile=expdp.dmp schemas=scott;																								
2)导入表（从scott用户中把表dept和emp导入到system用户中）										
impdp system/XXXXX@xxxx directory=dump_dir dumpfile=expdp.dmptables=scott.dept,scott.emp remap_schema=scott:system;												
												
3)导入表空间												
impdp system/XXXXX@xxxx directory=dump_dir dumpfile=tablespace.dmp tablespaces=example;																							
4)导入数据库												
impdp system/XXXXX@xxxx directory=dump_dir dumpfile=full.dmp full=y;											
```



#### 4.修改用户密码

```shell
sqlplus /nolog

conn sys/123456 as sysdba;

alter user system identified by "Zhcgdongzhi@2022!";

alter user zhcgdongzhi identified by "Zhcgdongzhi@2022!";

alter user szzf_dongzhi identified by "Szzfdongzhi@2022!";
```

