### <font color="#8a2be2">centos更改mysql数据库目录</font>

------

**本教程测试通过MySQL版本：**

- centos7
- mysql5.7.32和mysql5.7.27
- 其他数据库版本应该一样，暂未测试



#### 1.停掉mysql服务

```bash
systemctl stop msyql
```

**提示：centos的selinux需要设置为disabled，设置完需要重启服务器**

```bash
vim /etc/selinux/config
```

![image-20220428115804962](https://fastly.jsdelivr.net/gh/le1024/image1/le/image-20220428115804962.png)

**或者执行`setenforce 0`关闭selinux，临时生效**





#### 2.创建新的mysql数据库目录

```bash
#具体目录位置可以看自己需求创建
mkdir /opt/mysqldata
```

后续就是把默认安装的mysql移动到新创建的`mysqldata`目录下



#### 3.移动mysql到新创建的目录

由于是通过rpm的方式安装的mysql，mysql默认会存放在`/var/lib/mysql`目录

把`/var/lib/msyql`整个移动到`/opt/mysqldata/`

```bash
mv /var/lib/mysql /opt/mysqldata/
```



#### 4.设置`mysqldata`目录的权限

更改`mysqldata`目录的用户名和用户组

```bash
chown -R mysql:mysql /opt/mysqldata/mysql
```

修改`mysqldata`目录的访问权限

```bash
chmod 755 /opt/mysqldata/mysql
#直接设置最外层的,只设置一个mysql的可能不够
chmod 755 /opt
```



#### 5.修改`/etc/my.cnf`

需要修改下`my.cnf`文件里面的路径指向为新的目录

```bash
#修改datadir为新的目录地址
#datadir=/var/lib/mysql
datadir=/opt/mysql/mysql

#修改socket为新的目录地址
#socket=/var/lib/mysql/mysql.sock
socket=/opt/mysql/mysql/mysql.sock

# Disabling symbolic-links is recommended to prevent assorted security risks
symbolic-links=0

#通过rpm安装的，会没有client模块，需要加上，不然mysql -u -p命令无法登录到mysql
[client]
socket=/opt/mysql/mysql/mysql.sock

#通过rpm安装的，会没有[mysql_safe]，需要加上
[mysqld_safe]
log-error=/var/log/mysqld.log
pid-file=/var/run/mysqld/mysqld.pid
```

<strong style="color:red">红框的内容就是在原文件基础上添加修改的</strong>

![image-20220428115107056](https://fastly.jsdelivr.net/gh/le1024/image1/le/image-20220428115107056.png)

这样改完之后就可以重启了

```bash
systemctl start mysqld
```



#### 6.验证结果

登录mysql

```bash
mysql -u root -p
```

查看数据库地址

```mysql
select @@datadir;
```

![image-20220428115407551](https://fastly.jsdelivr.net/gh/le1024/image1/le/image-20220428115407551.png)



