**<font size="5px" color="green">mysql数据保存乱码</font>**

------



#### 问题

可能有两种情况，一是前端传给接口的时候就已经乱码了，断点查看；二是入库的时候乱码。

这里只处理数据库的

首先查看数据库的编码：

```mysql
show variables like 'character_set_database';
```

![image-20220428150413040](https://fastly.jsdelivr.net/gh/le1024/image1/le/image-20220428150413040.png)

`latin1`是ISO-8859-1的别名

如果数据库出现`???`之类的乱码，可以将数据库的编码设置成`utf-8`或者`gbk`



#### 设置数据库编码

需要编辑`my.cnf`文件

```bash
vim /etc/my.cnf
```

添加一行代码：

```bash
character-set-server=utf8
```

重启mysql，之后

```bash
systemctl restart mysqld
```



![image-20220428151155813](https://fastly.jsdelivr.net/gh/le1024/image1/le/image-20220428151155813.png)