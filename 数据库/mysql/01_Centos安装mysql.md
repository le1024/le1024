> Linux版本：centos 7.6
> Mysql版本：mysql 8.0.17

#### 一. 下载MySQL安装包文件
下载地址：[MySQL Community Downloads](https://dev.mysql.com/downloads/mysql/) 
![在这里插入图片描述](https://img-blog.csdnimg.cn/20190924103614781.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3NpbmF0XzMzMTUxMjEz,size_16,color_FFFFFF,t_70)
![在这里插入图片描述](https://img-blog.csdnimg.cn/20190924103628374.png)

#### 二. 安装MySQL
**1. 上传安装包到服务器**
/usr/local 目录下新建mysql文件夹，将安装包上传到这个文件夹中

**2. 删除mariadb**
 查询是否有安装mariadb，命令：

 ```java
 rpm -qa|grep mariadb
 ```
 如果有的话，需要删除，命令：
 ```java
 #mariadb是上一步查询出来的mariadb文件，需要写完整的名字，如果有多个，需依次执行命令删除
 rpm -e --nodeps mariadb
 ```

**3.解压MySQL安装包**
进入到MySQL安装包目录：
```java
cd /usr/local/software/mysql

tar -xvf mysql-8.0.17-1.el7.x86_64.rpm-bundle.tar
```
解压的文件：
![在这里插入图片描述](https://img-blog.csdnimg.cn/20190924105935907.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3NpbmF0XzMzMTUxMjEz,size_16,color_FFFFFF,t_70)

**4.安装MySQL rpm包**
【必须安装】
```java
rpm -ivh mysql-community-common-8.0.17-1.el7.x86_64.rpm
rpm -ivh mysql-community-libs-8.0.17-1.el7.x86_64.rpm
rpm -ivh mysql-community-client-8.0.17-1.el7.x86_64.rpm
rpm -ivh mysql-community-server-8.0.17-1.el7.x86_64.rpm
```
![在这里插入图片描述](https://img-blog.csdnimg.cn/20190924110319466.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3NpbmF0XzMzMTUxMjEz,size_16,color_FFFFFF,t_70)

【非必须安装】
```java
rpm -ivh mysql-community-embedded-compat-8.0.17-1.el7.x86_64.rpm
rpm -ivh mysql-community-libs-compat-8.0.17-1.el7.x86_64.rpm
rpm -ivh mysql-community-devel-8.0.17-1.el7.x86_64.rpm
rpm -ivh mysql-community-test-8.0.17-1.el7.x86_64.rpm
```

<br>

#### 三.启用MySQL服务
**1.初始化mysql**
```java
mysqld --initialize --console
```

**2.mysql目录授权**
```java
 chown -R mysql:mysql /var/lib/mysql/
```

**3.启动mysq**l
```java
systemctl start mysqld.service
systemctl status mysqld.service
```
![在这里插入图片描述](https://img-blog.csdnimg.cn/2019092411114039.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3NpbmF0XzMzMTUxMjEz,size_16,color_FFFFFF,t_70)

#### 四.数据库配置
安装MySQL之后，登录密码是随机生成的，需要更改自己配置的密码。

1.查看随机密码
```java
cat /var/log/mysqld.log
```
![在这里插入图片描述](https://img-blog.csdnimg.cn/20190924111432190.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3NpbmF0XzMzMTUxMjEz,size_16,color_FFFFFF,t_70)

**2.登录数据库**
```java
#密码输入上一步查看到的随机密码
mysql -u root -p
```
![在这里插入图片描述](https://img-blog.csdnimg.cn/20190924111539736.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3NpbmF0XzMzMTUxMjEz,size_16,color_FFFFFF,t_70)

**3.重置MySQL密码**
```java
#identified by 后面的是新秘密
alter user 'root'@'localhost' identified by 'root';
```
表示重置成功
![在这里插入图片描述](https://img-blog.csdnimg.cn/20190924111756925.png)

**4.远程连接MySQL**
执行以下命令：
```java
show databases;

use mysql;

select host, user, authentication_string, plugin from user;
```
![在这里插入图片描述](https://img-blog.csdnimg.cn/20190924112033467.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3NpbmF0XzMzMTUxMjEz,size_16,color_FFFFFF,t_70)
执行以下命令：

```java
update user set host = '%' where user = 'root';	

select host, user, authentication_string, plugin from user;

flush privileges;
```
![在这里插入图片描述](https://img-blog.csdnimg.cn/20190924112224857.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3NpbmF0XzMzMTUxMjEz,size_16,color_FFFFFF,t_70)

#### 五.连接MySQL
![在这里插入图片描述](https://img-blog.csdnimg.cn/20190924112457821.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3NpbmF0XzMzMTUxMjEz,size_16,color_FFFFFF,t_70)
