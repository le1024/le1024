#### 1. 创建新用户并分配数据库权限

```mysql
#1.创建用户xxljob, localhost为本地访问，设置%可支持外网访问，identified by 设置密码
create user 'xxljob'@'localhost' IDENTIFIED by 'xxljob';
#2.刷新权限
flush PRIVILEGES;
#3.创建数据库xxljob
create database xxljob default charset utf8 collate utf8_general_ci;
#4.给用户xxljob分配对数据库xxljob.权限，如需外网将localhost改成%
grant all privileges on xxljob.* to 'xxljob'@'localhost' identified by 'xxljob';
#5.刷新权限
flush PRIVILEGES;
```

