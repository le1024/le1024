**<font size="5px" color="green">mysql数据误删恢复</font>**

------



#### 开启binlog

需要确定mysql的`binlog`已经开启

```mysql
show variables like '%log_bin%';
```

![image-20220513111841568](https://fastly.jsdelivr.net/gh/le1024/image1/le/image-20220513111841568.png)

`log_bin`值为ON是开启，OFF是未开启

**开启binlog的方式：**

```bash
vim /etc/my.cnf
```

[mysqld]添加：

```bash
log-bin=/opt/mysql/mysql/binlog
server-id=1
```

`log-bin` ：binlog的位置，可任意位置，但需要确保有路径权限

`server-id`：mysql实例id

![image-20220513112300034](https://fastly.jsdelivr.net/gh/le1024/image1/le/image-20220513112300034.png)

配置完成后，重启mysql

![image-20220513112448688](https://fastly.jsdelivr.net/gh/le1024/image1/le/image-20220513112448688.png)

`log_bin`值为ON，开启binlog成功



> `binlog`开启后，会影响MySQL的性能，所以对于存放重要的业务数据库开启`binlog`。其他的可做备份



#### 查看binlog文件

文件路径在你的数据库`datadir`路径：

![image-20220513151857242](https://fastly.jsdelivr.net/gh/le1024/image1/le/image-20220513151857242.png)



#### mysqlbinlog工具恢复数据

我的`mysqlbinlog`在`/usr/bin`目录下



