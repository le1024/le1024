**ElasticSearch**

------



The Elastic Stack，包括ElasticSearch、Kibana、Beats和Logstash，也称为ELK。能够安全可靠地获取任何来源、任何格式的数据，然后实时的对数据进行搜索、分析和可视化。

ElasticSearch简称为ES，是一个开源的搞扩展的`分布式全文搜索引擎`，是整个Elastic Stack的核心。它可以近乎实时的存储、检索数据；本身扩展性很好，可以扩展到上百台服务器，处理PB级别的数据。

