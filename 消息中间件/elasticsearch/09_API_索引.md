**elasticsearch—API索引**

------

API服务启动成功后，可以通过API对象对ES索引进行操作



#### 创建索引

```java
/**
     * 创建索引
     */
    @Test
    public void createIndex() throws IOException {
        //创建es客户端
        RestHighLevelClient esClient = new RestHighLevelClient(
                RestClient.builder(new HttpHost("localhost", 9200, "http"))
        );

        //创建索引-请求对象 org.elasticsearch.client.indices.CreateIndexRequest;
        CreateIndexRequest request = new CreateIndexRequest("phone");//phone是索引名
        //发送请求，获取响应 org.elasticsearch.client.indices.CreateIndexResponse;
        CreateIndexResponse response = esClient.indices().create(request, RequestOptions.DEFAULT);
        //可从response中拿到响应结果
        System.out.println("操作结果：" + response.isAcknowledged());
        //关闭es客户端
        esClient.close();
    }
```





#### 查询索引

```java
/**
     * 查看索引
     */
    @Test
    public void getIndex() throws IOException {
        //创建es客户端
        RestHighLevelClient esClient = new RestHighLevelClient(
                RestClient.builder(new HttpHost("localhost", 9200, "http"))
        );

        //查询索引-请求对象 org.elasticsearch.client.indices.GetIndexRequest;
        GetIndexRequest request = new GetIndexRequest("phone");//phone是索引名
        //发送请求-获取响应 org.elasticsearch.client.indices.GetIndexResponse;
        GetIndexResponse response = esClient.indices().get(request, RequestOptions.DEFAULT);
        //可从response中拿到响应结果
        System.out.println(response.getAliases());
        System.out.println(response.getMappings());
        System.out.println(response.getSettings());

        //关闭es客户端
        esClient.close();
    }
```

```java
{phone=[]}
{phone=org.elasticsearch.cluster.metadata.MappingMetaData@b56ee9df}
{phone={"index.creation_date":"1652885969956","index.number_of_replicas":"1","index.number_of_shards":"1","index.provided_name":"phone","index.uuid":"EdqPK2sbQf2kwnPWnoYTIQ","index.version.created":"7080099"}}
2022-05-18 23:11:49.248  INFO 1576 --- [extShutdownHook] o.s.s.concurrent.ThreadPoolTaskExecutor  : Shutting down ExecutorService 'applicationTaskExecutor'
```



#### 删除索引

```java
/**
     * 索引删除
     */
    @Test
    public void deleteIndex() throws IOException {
        //创建es客户端
        RestHighLevelClient esClient = new RestHighLevelClient(
                RestClient.builder(new HttpHost("localhost", 9200, "http"))
        );

        //删除索引-请求对象
        DeleteIndexRequest request = new DeleteIndexRequest("phone"); //phone是索引名
        //发送请求-获取响应
        AcknowledgedResponse response = esClient.indices().delete(request, RequestOptions.DEFAULT);
        //可从response中拿到响应结果
        System.out.println(response.isAcknowledged());

        //关闭es客户端
        esClient.close();
    }
```

