**elasticsearch映射**

------

概念：

**自动或者手动为index中的_doc建立的一种数据结构和相关配置，简称为mapping映射。mapping决定了index中field的特征。**

`索引-index`相当于mysql中的数据库，而`映射-mapping`类似于mysql中的表结构，记录了各种字段的类型



#### 创建映射

需要先创建索引：http://localhost:9200/user

> 在apipost中，向ES服务器发<strong style="color:red">PUT</strong>请求：http://localhost:9200/user/_mapping

```json
{
    "properties": {
        "nickname": {		//字段名,任意填写,类似于mysql表的字段
            "type": "text", //数据类型
            "index": true   //是否被索引，即该字段是否可以被用来搜索
        },
        "realname": {
            "type": "keyword",
            "index": true
        },
        "phone": {
            "type": "keyword",
            "index": false
        }
    }
}
```

<strong style="color:green">映射数据说明：</strong>

- **字段名**：任意填写，根据业务设计属性，例如：username、sex、age等

- **type**：数据类型

  - String：字符串类型
    - `text`：可分词
    - `keyword`：不可分词，数据会作为完整的字段进行匹配
  - Numerical：数值类型
    - 基本数据类型：long、integer、short、byte、double、float、half_float
    - 浮点数高精度类型：scaled_float
  - Date：日期类型
  - Array：数组类型
  - Object：对象

- **index**：是否索引，默认为true所有字段都可以被索引

  - true：字段会被索引，可以用来进行搜索
  - false：字段不会被索引，不可以用来搜索

- **store**：是否将数据进行独立存储，默认为false

  原始的数据文本会存储在`_source`里面，默认情况下其他提取出来的字段都不是独立存储的，是从`_source`里面提取的。设置`"store:true"`即可独立存储某个字段，同时获取独立存储的字段比`_source`解析要快的多，但也会占用更多的空间，需要根据实际业务来设置。

- **analyzer**：分词器

![image-20220517221029493](https://fastly.jsdelivr.net/gh/le1024/image1/le/image-20220517221029493.png)

请求后，ES服务器响应：

![image-20220517221050689](https://fastly.jsdelivr.net/gh/le1024/image1/le/image-20220517221050689.png)



#### 查询映射

> 在apipost中，向ES服务器发<strong style="color:red">GET</strong>请求：http://localhost:9200/user/_mapping

![image-20220517221316431](https://fastly.jsdelivr.net/gh/le1024/image1/le/image-20220517221316431.png)



#### 映射属性测试

添加文档数据

> 在apipost中，向ES服务器发<strong style="color:red">POST</strong>请求：http://localhost:9200/user/_doc/1

请求体：属性为创建映射设置的

```json
{
    "nickname": "小三",
    "realname": "张三",
    "phone": "10086"
}
```

添加完成后，

**测试1：**

> 在apipost中，向ES服务器发<strong style="color:red">GET</strong>请求：http://localhost:9200/user/_search

请求体：

```json
{
    "query": {
        "match": {
            //nickname为小、三、小三都是可以查询到数据的，因为映射的时候配置的type是text且index是true，是可以进行分词查询
            "nickname": "小"
        }
    }
}
```

![image-20220517222212976](https://fastly.jsdelivr.net/gh/le1024/image1/le/image-20220517222212976.png)

请求后，ES服务器响应：

![image-20220517222234673](https://fastly.jsdelivr.net/gh/le1024/image1/le/image-20220517222234673.png)



**测试2：**

> 在apipost中，向ES服务器发<strong style="color:red">GET</strong>请求：http://localhost:9200/user/_search

请求体：

```json
{
    "query": {
        "match": {
            //只有张三才可以查询数据，因为映射配置的type是keyword，无法分词查询
            "realname": "张三"
        }
    }
}
```

![image-20220517222605980](https://fastly.jsdelivr.net/gh/le1024/image1/le/image-20220517222605980.png)

请求后，ES服务器响应：

![image-20220517222540296](https://fastly.jsdelivr.net/gh/le1024/image1/le/image-20220517222540296.png)



**测试3：**

> 在apipost中，向ES服务器发<strong style="color:red">GET</strong>请求：http://localhost:9200/user/_search

请求体：

```json
{
    "query": {
        "match": {
            //查询报错，因为phone字段映射配置的index为false，不允许进行搜索
            "phone": "10086"
        }
    }
}
```

![image-20220517222720499](https://fastly.jsdelivr.net/gh/le1024/image1/le/image-20220517222720499.png)



#### 索引映射关联

> 在apipost中，向ES服务器发<strong style="color:red">PUT</strong>请求：http://localhost:9200/shopping

```json
{
    "settings": {},
    "mappings": {
        "properties": {
            "title": {
                "type": "keyword",
                "index": true
            }
        }
    }
}
```

![image-20220517231713005](https://fastly.jsdelivr.net/gh/le1024/image1/le/image-20220517231713005.png)

