**elasticsearch集成springboot**

****



maven引入：

```xml
<!--springboot服务直接引入es的starter就行了-->
<dependency>
    <groupId>org.springframework.boot</groupId>
    <artifactId>spring-boot-starter-data-elasticsearch</artifactId>
</dependency>
```



写个配置类：

```java
@Configuration
public class EsRestClientConfig  extends AbstractElasticsearchConfiguration {

    @Override
    @Bean
    public RestHighLevelClient elasticsearchClient() {
        final ClientConfiguration clientConfiguration = ClientConfiguration.builder()
                .connectedTo("localhost:9200")
                .build();
        return RestClients.create(clientConfiguration).rest();
    }
}
```



测试类：

```java
//注入RestHighLevelClient
@Autowired
private RestHighLevelClient restHighLevelClient;

/**
     * 查询全部文档数据
     */
@Test
public void queryAllIndex() throws IOException {
    
    //构建查询的请求体
    SearchSourceBuilder sourceBuilder = new SearchSourceBuilder();
    //查询所有数据
    sourceBuilder.query(QueryBuilders.matchAllQuery());

    //创建搜索请求对象
    SearchRequest request = new SearchRequest();
    request.indices("phone");
    request.source(sourceBuilder);

    //发送请求，获取响应结果
    SearchResponse response = restHighLevelClient.search(request, RequestOptions.DEFAULT);

    //处理查询结果
    SearchHits hits = response.getHits();
    System.out.println("took:" + response.getTook());
    System.out.println("timeout:" + response.isTimedOut());
    System.out.println("total:" + hits.getTotalHits());
    System.out.println("maxScore:" + hits.getMaxScore());
    System.out.println("查询记录：》》》》》》》》》");
    hits.forEach(hit -> {
        System.out.println(hit.getSourceAsString());
    });

    //关闭es客户端
    restHighLevelClient.close();
}
```

