**ElasticSearch下载安装**

------



[官方下载地址]:https://www.elastic.co/cn/downloads/past-releases#elasticsearch
[国内镜像下载地址]:https://mirrors.huaweicloud.com/elasticsearch/



#### windows安装

下载完成解压：

![image-20220425235129871](https://s2.loli.net/2022/06/17/F6tysgO3wC5n1D7.png)

![image-20220425235152579](https://s2.loli.net/2022/06/17/SCysDQJATIhZ2Wr.png)

双击`elasticsearch.bat`脚本即可启动es

**注意：ES的环境是依赖于JDK的，如果本地安装了JDK就会使用你本地安装的JDK，如果没有就会使用默认自带的，JDK版本建议8以上**

**注意：如果启动窗口闪退，可能是内存不足问题，修改config/jvm.options配置文件的 -Xms1g -Xmx1g，或者更小一点**



![image-20220425235728103](https://s2.loli.net/2022/06/17/OvgU7aX6y3lu5PG.png)

**注意：`9300`端口为ElasticSearch集群间组件的通信端口，`9200`端口为浏览器访问的http协议RESTful端口**



打开浏览器输入：**localhost:9200**

![image-20220425235950165](https://s2.loli.net/2022/06/17/HA9rFKYnXcwpsMy.png)



#### linux安装

> 下载地址：https://artifacts.elastic.co/downloads/elasticsearch/elasticsearch-7.8.0-linux-x86_64.tar.gz



##### 1）解压软件

先将下载好的es软件上传到服务器，然后解压

```shell
tar -zxvf elasticsearch-7.8.0-linux-x86_64.tar.gz
```

##### 2）创建用户

<strong style="color:green">因为安全问题，elasticsearch不允许root用户直接运行，需要单独创建新用户运行</strong>

```shell
#使用root用户创建es新用户
useradd myes #新增es用户
passwd myes #设置用户密码
```

![image-20220606222247786](https://s2.loli.net/2022/06/06/HTB28Pe1fZkKuxG.png)

如果想要删除用户，可以

```shell
userdel -r myes
```

创建用户完成后，进行授权，路径就是解压的es文件路径

```shell
chown -R myes:myes /opt/elasticsearch-7.8.0
```



##### 3）修改配置文件

修改`/opt/elasticsearch-7.8.0/config/elasticsearch.yml`文件：

```yaml
#加入如下配置
cluster.name: elasticsearch
node.name: node-1
network.host: 0.0.0.0
http.port: 9200
cluster.initial_master_nodes: ["nodes-1"]
```

修改`/etc/security/limits.conf`

```yaml
#在文件末尾增加下面内容
#每个进程可以打开的文件数的限制
myes soft nofile 65536
myes hard nofile 65536
```

修改`/etc/security/limits.d/20-nproc.conf`

```yaml
#在文件末尾增加下面内容
#每个进程可以打开的文件数的限制
myes soft nofile 65536
myes hard nofile 65536
# 操作系统级别对每个用户创建的进程数的限制
* hard nproc 4096
# 注：* 代表 Linux 所有用户名称
```

修改`etc/sysctl.conf`

```yaml
# 在文件中增加下面内容
# 一个进程可以拥有的 VMA(虚拟内存区域)的数量,默认值为 65536
vm.max_map_count=655360
```

修改完成后，重新加载下配置文件

```shell
sysctl -p
```



##### 4）启动es

需要从root用户切换到`myes`用户，也就是你第二步创建的新用户

```shell
#切换用户
su myes
#进入到es目录
cd /opt/elasticsearch-7.8.0/
#启动es
bin/elasticsearch
```

以后台的形式启动es：

```shell
bin/elasticsearch -d
```

启动成功后访问：

![image-20220606224625619](https://fastly.jsdelivr.net/gh/le1024/image1/le/image-20220606224625619.png)

##### 5）问题

1.如果服务器内存不够的话，将es的内存调小一点

```shell
vim /opt/elasticsearch-7.8.0/config/jvm.options
```

可设置512，256....

![image-20220606224832376](https://fastly.jsdelivr.net/gh/le1024/image1/le/image-20220606224832376.png)



2.es的启动运行日志查看，有什么错误的话可以查看日志

```shell
tail -200f logs/elasticsearch.log
```





#### elasticsearch集群

##### 1）准备工作

三台linux服务器，将下载好的es压缩包分别上传到服务器，然后解压

```shell
tar -zxvf elasticsearch-7.8.0-linux-x86_64.tar.gz
```

##### 2）创建用户

<strong style="color:green">因为安全问题，elasticsearch不允许root用户直接运行，需要单独创建新用户运行</strong>

```shell
#使用root用户创建es新用户
useradd myes #新增es用户
passwd myes #设置用户密码
```

如果想要删除用户，可以

```shell
userdel -r myes
```

创建用户完成后，进行授权，路径就是解压的es文件路径

```shell
# 记得授权，不然myes用户启动会提示权限不足
chown -R myes:myes /opt/elasticsearch-7.8.0
```

![image-20220607143530213](https://fastly.jsdelivr.net/gh/le1024/image1/le/image-20220607143530213.png)



##### 3）修改配置文件

**3.1** 修改`/opt/elasticsearch-7.8.0/config/elasticsearch.yml`文件

```properties
# 集群名称
cluster.name: my-es
# 节点名称，每个节点名称不能重复
node.name: node-1
# ip地址，每个节点的ip地址不能重复，也可以填当前服务器的hostname
network.host: hll1
# 是不是有资格主节点
node.master: true
node.data: true
# es端口
http.port: 9200
# head插件需要打开这两个配置
http.cors.allow-origin: "*"
http.cors.enabled: true
http.max_content_length: 200mb
# es7.x后新增的配置，初始化一个新的集群时需要此配置来选举master
cluster.initial_master_nodes: ["node-1"]
# es7.x后新增的配置，节点发现
discovery.seed_hosts: ["hll1:9300", "hll2:9300", "hll3:9300"]
gateway.recover_after_nodes: 2
network.tcp.keep_alive: true
network.tcp.no_delay: true
transport.tcp.compress: true
# 集群内同时启动的数据任务个数，默认2个
cluster.routing.allocation.cluster_concurrent_rebalance: 16
# 添加或删除节点及负载均衡时并发恢复的线程个数，默认4个
cluster.routing.allocation.node_concurrent_recoveries: 16
```

配置完成后，将`elasticsearch.yml`文件复制到另外两台服务器，替换原先的旧文件

<font size="2px" color="red">注：不能重复的配置项需要根据服务器修改，比如节点名，ip地址等</font>

```bash
# 复制hll1机器的配置文件到hll2和hll3,elasticsearch.yml文件的路径不要替换错了
scp elasticsearch-7.8.0/config/elasticsearch.yml root@hll2:/opt/elasticsearch-7.8.0/config/
scp elasticsearch-7.8.0/config/elasticsearch.yml root@hll3:/opt/elasticsearch-7.8.0/config/
```

![image-20220607145922610](https://fastly.jsdelivr.net/gh/le1024/image1/le/image-20220607145922610.png)

复制到另外两台服务器之后，记得修改配置！！！

> elasticsearch.yml文件要写规范点，不然会启动服务时会提示：expected block end, but found block mapping start
>
> 配置项开头不要用tab缩近，也不要空格





**3.2** 修改`/etc/security/limits.conf`

```shell
# 在文件末尾添加下面内容,myes是新创建的用户名
myes soft nofile 65536
myes hard nofile 65536
```

**3.3 **修改`/etc/security/limits.d/20-nproc.conf`

```shell
# 在文件末尾中增加下面内容,myes是新创建的用户名,* 代表 Linux 所有用户名称
myes soft nofile 65536
myes hard nofile 65536
* hard nproc 4096
```

**3.4 **修改`/etc/sysctl.conf`

```shell
# 在文件中增加下面内容
vm.max_map_count=655360
```

重新加载

```shell
sysctl -p
```

<font size="2px" color="red">注：上面修改的文件都需要在另外的服务器进行同样的配置修改，可手动修改也可scp直接复制到另外的服务器</font>

```shell
# hll2
scp /etc/security/limits.conf root@hll2:/etc/security/
scp /etc/security/limits.d/20-nproc.conf root@hll2:/etc/security/limits.d/
scp /etc/sysctl.conf root@hll2:/etc/
sysctl -p

# hll3
scp /etc/security/limits.conf root@hll3:/etc/security/
scp /etc/security/limits.d/20-nproc.conf root@hll3:/etc/security/limits.d/
scp /etc/sysctl.conf root@hll3:/etc/
sysctl -p
```

![image-20220607161059142](https://fastly.jsdelivr.net/gh/le1024/image1/le/image-20220607161059142.png)

拷贝完成后另外两台服务器需要执行`sysctl -p`，不要忘记了



##### 4）启动es

分别启动不同节点的es，<strong style="color:red">使用你新创建的用户启动</strong>

```shell
su myes

cd /opt/elasticsearch-7.8.0/

# 启动es
bin/elasticsearch
# 后台方式启动
bin/elasticsearch -d
```

如果服务器内存不够的话，可以将es启动内存设置小一点

```shell
vim /opt/elasticsearch-7.8.0/config/jvm.options
```



![image-20220607165115277](https://fastly.jsdelivr.net/gh/le1024/image1/le/image-20220607165115277.png)

#### elasticsearch-head插件

**Head是elasticsearch的集群管理工具,可以用于数据的浏览和查询**