**elasticsearch—API文档**

------



#### 创建文档

```java
/**
     * 创建文档
     */
    @Test
    public void createDoc() throws IOException {
        //创建es客户端
        RestHighLevelClient esClient = new RestHighLevelClient(
                RestClient.builder(new HttpHost("localhost", 9200, "http"))
        );

        //创建文档-请求对象
        IndexRequest request = new IndexRequest();
        //设置索引及文档主键
        request.index("phone").id("1");
        //创建数据对象
        Phone phone = new Phone("iPhone", "iPhone 11", "5999");
        //添加文档数据，数据格式为JSON格式
        request.source(JSONObject.toJSONString(phone), XContentType.JSON);
        //客户端发送请求，获取响应结果
        IndexResponse response = esClient.index(request, RequestOptions.DEFAULT);
        //打印结果
        System.out.println("index:" + response.getIndex());
        System.out.println("id:" + response.getId());
        System.out.println("result:" + response.getResult());

        //关闭es客户端
        esClient.close();
    }
```



#### 查询文档

```java
/**
     * 查询文档
     */
    @Test
    public void getDoc() throws IOException {
        //创建es客户端
        RestHighLevelClient esClient = new RestHighLevelClient(
                RestClient.builder(new HttpHost("localhost", 9200, "http"))
        );

        //创建查询文档请求对象
        GetRequest request = new GetRequest();
        //设置索引，根据文档id查询
        request.index("phone").id("1");
        //客户端发送请求，获取响应对象
        GetResponse response = esClient.get(request, RequestOptions.DEFAULT);
        //打印结果
        System.out.println("index:" + response.getIndex());
        System.out.println("type:" + response.getType());
        System.out.println("id:" + response.getId());
        System.out.println("source:" + response.getSourceAsString());

        //关闭es
        esClient.close();
    }
```

```java
index:phone
type:_doc
id:1
source:{"brand":"iPhone","model":"iPhone 11","price":"5999"}
```



#### 修改文档

```java
/**
     * 修改文档
     */
    @Test
    public void updateDoc() throws IOException {
        //创建es客户端对象
        RestHighLevelClient esClient = new RestHighLevelClient(
                RestClient.builder(new HttpHost("localhost", 9200, "http"))
        );

        //修改文档-请求对象
        UpdateRequest request = new UpdateRequest();
        //设置索引及文档主键
        request.index("phone").id("1");
        //设置请求体，对文档进行修改
        request.doc(XContentType.JSON, "model", "iPhone 11 Pro", "price", 8999);
        //客户端发送请求，获取响应结果
        UpdateResponse response = esClient.update(request, RequestOptions.DEFAULT);
        //打印响应结果
        System.out.println("index:" + response.getIndex());
        System.out.println("id:" + response.getId());
        System.out.println("source:" + response.getResult());

        //关闭es
        esClient.close();
    }
```

```java
index:phone
id:1
source:UPDATED
```

再次查询文档：

```java
index:phone
type:_doc
id:1
source:{"brand":"iPhone","model":"iPhone 11 Pro","price":8999}
```



#### 删除文档

```java
/**
     * 删除文档
     */
    @SneakyThrows
    @Test
    public void deleteDoc() {
        //创建es客户端对象
        RestHighLevelClient esClient = new RestHighLevelClient(
                RestClient.builder(new HttpHost("localhost", 9200, "http"))
        );
        //创建删除文档请求对象
        DeleteRequest request = new DeleteRequest();
        //设置索引及文档主键
        request.index("phone").id("1");
        //客户端发送请求，获取响应结果
        DeleteResponse response = esClient.delete(request, RequestOptions.DEFAULT);
        //打印响应信息
        System.out.println(response.getResult());

        //关闭es
        esClient.close();
    }
```



#### 批量修改文档

```java
/**
     * 批量新增文档
     */
    @Test
    public void bulkCreateDoc() throws IOException {
        //创建es客户端对象
        RestHighLevelClient esClient = new RestHighLevelClient(
                RestClient.builder(new HttpHost("localhost", 9200, "http"))
        );

        //创建批量新增请求对象
        BulkRequest request = new BulkRequest();

        //文档数据1
        IndexRequest request1 = new IndexRequest("phone").id("2");
        Phone phone1 = new Phone("iPhone", "iPhone 13", 5999);
        request1.source(JSONObject.toJSONString(phone1), XContentType.JSON);
        request.add(request1);

        //文档数据2
        IndexRequest request2 = new IndexRequest("phone").id("3");
        Phone phone2 = new Phone("iPhone", "iPhone 13 Pro", 9999);
        request2.source(JSONObject.toJSONString(phone2), XContentType.JSON);
        request.add(request2);

        //文档数据3
        IndexRequest request3 = new IndexRequest("phone").id("3");
        Phone phone3 = new Phone("华为", "HUAWEI P50", 4488);
        request3.source(JSONObject.toJSONString(phone3), XContentType.JSON);
        request.add(request3);

        //发送请求，获取响应结果
        BulkResponse response = esClient.bulk(request, RequestOptions.DEFAULT);
        //打印结果
        System.out.println("took:" + response.getTook()); //took是批量执行花费的时间
        System.out.println("items:" + Arrays.toString(response.getItems()));

        //关闭es
        esClient.close();
    }
```



#### 批量删除文档

```java
/**
     * 批量删除文档
     */
    @SneakyThrows
    @Test
    public void bulkDeleteDoc() {
        //创建es客户端对象
        RestHighLevelClient esClient = new RestHighLevelClient(
                RestClient.builder(new HttpHost("localhost", 9200, "http"))
        );

        //创建批量删除请求对象
        BulkRequest request = new BulkRequest();
        DeleteRequest request1 = new DeleteRequest("phone").id("1");
        DeleteRequest request2 = new DeleteRequest("phone").id("2");
        DeleteRequest request3 = new DeleteRequest("phone").id("3");

        request.add(request1);
        request.add(request2);
        request.add(request3);

        //客户端发送请求，获取响应结果
        BulkResponse response = esClient.bulk(request, RequestOptions.DEFAULT);
        //打印响应结果
        System.out.println("took:" + response.getTook()); //took是批量执行花费的时间
        System.out.println("items:" + response.getItems());
        
        //关闭es
        esClient.close();
    }
```

