**elasticsearch分布式集群**

------



请先搭建好集群环境：[elasticsearch安装](../02_elasticsearch安装)

#### 节点集群

先启动集群es服务，然后创建一个新的索引`users`，并给该索引分配三个主分片和一份副本（一份副本是三个主分片都拥有一个自己的副本分片）

设置分片和索引的请求体：

```json
{
    "settings": {
        //3个主分片
        "number_of_shards": 3,
        //一份副本
        "number_of_replicas": 1
    }
}
```

![image-20220612185339579](https://fastly.jsdelivr.net/gh/le1024/image1/le/image-20220612185339579.png)

为了观察到es集群的实时状态，可以安装elasticsearch-head插件

![image-20220612213257538](https://fastly.jsdelivr.net/gh/le1024/image1/le/image-20220612213257538.png)



#### 水平扩容

主分片的数量在创建索引的时候就确定下来了，`number_of_shards`的值，后期是无法进行更新的。

但是读操作的搜索和数据返回是可以同时被分片和副本所处理，所以当拥有越多的副本分片时，也会拥有越高的吞吐量，虽然主分片的数量无法更新，但是副本是可以动态更新的，可以通过动态调整副本的数目，按需伸缩集群。

副本数从1增加到2

```json
{
    "number_of_replicas": 2
}
```

![image-20220612194728088](https://fastly.jsdelivr.net/gh/le1024/image1/le/image-20220612194728088.png)

刷新elasticsearch-head插件：

![image-20220612194904060](https://fastly.jsdelivr.net/gh/le1024/image1/le/image-20220612194904060.png)

users 索引现在拥有 9 个分片：3 个主分片和 6 个副本分片。 这意味着我们可以将集群 扩容到 9 个节点，每个节点上一个分片。相比原来 3 个节点时，集群搜索性能可以提升 3 倍。



#### 应对故障

集群必须拥有一个主节点来保证工作，当主节点发生故障后就需要选举一个新的主节点，模拟故障异常，关闭主节点`node-1`，然后访问node-2节点服务器上面的elasticsearch-head服务：

![image-20220612221709958](https://fastly.jsdelivr.net/gh/le1024/image1/le/image-20220612221709958.png)

当前集群服务还有两个节点，集群依旧是正常提供服务的。

如果我们重新启动 Node 1 ，集群可以将缺失的副本分片再次进行分配，那么集群的状 态也将恢复成之前的状态。 如果 Node 1 依然拥有着之前的分片，它将尝试去重用它们， 同时仅从主分片复制发生了修改的数据文件。和之前的集群相比，只是 Master 节点切换了。

![image-20220612220900042](https://fastly.jsdelivr.net/gh/le1024/image1/le/image-20220612220900042.png)







