**elasticsearch索引**

------



es的索引就相当于mysql的数据库。

如果我们需要用mysql存储使用数据，首先肯定需要一个database，一个库，所以在es中，我们操作数据时同样也需要一个库的概念的，就是Index--索引。



前期的学习，是直接用api工具进行elasticsearch的接口请求的，这里[推荐使用ApiPost进行elasticsearch的api调试](https://www.apipost.cn/)



#### 创建索引

对比关系型数据库，创建索引就是创建数据库，创建一个`shopping`索引

> 在apipost中，向ES服务器发送<strong style="color:red">PUT</strong>请求：http://localhost:9200/shopping

![image-20220430225223758](https://fastly.jsdelivr.net/gh/le1024/image1/le/image-20220430225223758.png)

请求后，es服务器响应：

![image-20220430225301441](https://s2.loli.net/2022/06/17/TxFwHYvIXktmSn8.png)

```json
{
    "acknowledged": true, //响应结果
    "shards_acknowledged": true, //分片结果
    "index": "shopping" //索引名称
}
# 注意：创建索引库的分片数默认 1 片，在 7.0.0 之前的 Elasticsearch 版本中，默认 5 片
```

<strong style="color:red">只能使用PUT请求，不可使用POST，且PUT具有幂等性所以同一个请求只能发送一次</strong>

![image-20220430225709665](https://s2.loli.net/2022/06/17/DbZmQfMXYV84Kwu.png)

![image-20220430225750869](https://s2.loli.net/2022/06/17/hTNgDLMl2JzSiQd.png)



#### 查看索引

##### 查看单个索引

**查看索引与创建索引的请求路径是一致的。但是HTTP方法不一致。**

> 在Apipost中，向ES服务器发送<strong style="color:red">GET</strong>请求：http://localhost:9200/shopping

![image-20220430230411600](https://s2.loli.net/2022/06/17/sALqDn4ZEWfz1bC.png)

请求后，es服务器响应：

![image-20220430230619158](https://s2.loli.net/2022/06/17/sdznS3GC4oXlvIm.png)

```json
{
	"shopping": { //索引名称
		"aliases": {}, //别名
		"mappings": {}, //映射
		"settings": { //索引设置信息
			"index": { //索引信息
				"creation_date": "1651330308104", //索引-创建时间
				"number_of_shards": "1", //索引-主分片数量
				"number_of_replicas": "1", //索引-副分片数量
				"uuid": "SgZOd1OSQbiwvdNpM9b3HA", //索引-唯一标识
				"version": { //索引-版本
					"created": "7080099"
				},
				"provided_name": "shopping" //索引-名称
			}
		}
	}
}
```



##### 查看所有索引

> 在apipost中，向ES服务器发送<strong style="color:red">GET</strong>请求：http://localhost:9200/_cat/indices?v

![image-20220430232131372](https://s2.loli.net/2022/06/17/uovr8gn9bmaIyMP.png)

- _cat：表示查看的意思
- indices表示索引

整体就相当于mysql的show tables

请求后，es服务器响应：

![image-20220430232119274](https://s2.loli.net/2022/06/17/w3AGJQMU7nNLE1S.png)

| 表头           | 含义                                                         |
| -------------- | ------------------------------------------------------------ |
| health         | 当前服务器健康状态，<font color="green">green</font>(集群完整)，<font color="yellow">yellow</font>(单点正常，集群不完整)，<font color="red">red</font>(单点不正常) |
| status         | 索引状态：打开\|关闭                                         |
| index          | 索引名                                                       |
| uuid           | 索引唯一标识                                                 |
| pri            | 主分片数量                                                   |
| rep            | 副本数量                                                     |
| docs.count     | 可用文档数量                                                 |
| docs.deleted   | 文档删除状态（逻辑删除）                                     |
| store.size     | 主分片和副分片整体占空间大小                                 |
| pri.store.size | 主分片占空间大小                                             |



#### 删除索引

> 在apipost中，向ES服务器发送<strong style="color:red">DELETED</strong>请求：http://localhost:9200/shopping

![image-20220430232939679](https://s2.loli.net/2022/06/17/B38R6pa1Di4m7Zn.png)

请求后，ES服务器响应：

![image-20220430233009821](https://s2.loli.net/2022/06/17/zhKP87n3duI1CTw.png)

再次请求查看索引，提示索引不存在了

![image-20220430233039072](https://s2.loli.net/2022/06/17/EVQFRSoqcGPz8mM.png)