**elasticsearch—API文档**

------



#### 创建映射

[照着官方文档干就完了](https://www.elastic.co/guide/en/elasticsearch/client/java-rest/7.9/java-rest-high-put-mapping.html)

```java
/**
     * 创建映射
     * <p>https://www.elastic.co/guide/en/elasticsearch/client/java-rest/7.9/java-rest-high-put-mapping.html</p>
     */
    @Test
    public void createMapping() throws IOException {
        //创建es客户端对象
        RestHighLevelClient client = new RestHighLevelClient(
                RestClient.builder(new HttpHost("localhost", 9200, "http"))
        );

        PutMappingRequest request = new PutMappingRequest("phone");
        request.source(
                "{\n" +
                        "    \"properties\": {\n" +
                        "        \"brand\": {\n" +
                        "            \"type\": \"text\",\n" +
                        "            \"index\": true,\n" +
                        "            \"fielddata\": true\n" +
                        "        },\n" +
                        "        \"model\": {\n" +
                        "            \"type\": \"text\",\n" +
                        "            \"index\": true,\n" +
                        "            \"fielddata\": true\n" +
                        "        },\n" +
                        "        \"price\": {\n" +
                        "            \"type\": \"integer\",\n" +  //注意类型哦，不然聚合查询会出错
                        "            \"index\": true\n" +
                        "        }\n" +
                        "    }\n" +
                        "}",
        XContentType.JSON);

        AcknowledgedResponse response = client.indices().putMapping(request, RequestOptions.DEFAULT);
        System.out.println(response.isAcknowledged());
    }
```

