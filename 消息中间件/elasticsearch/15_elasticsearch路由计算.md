**elasticsearch路由计算**

------



#### 路由计算

当索引中添加一个文档的时候，文档会被存储到一个主分片中。elasticsearch并不是随机分配存储的，是根据下面的公式来分配：

```json
shard = hash(routing) % number_of_primary_shards
```

routing：是一个可变值，默认是文档的_id，也可以自定义

routing通过hash函数生成一个数字，然后这个数字再除以`number_of_primary_shards`（主分片数量）后得到的余数。这个分布在0到`number_of_primary_shards - 1`之间的余数，就是所寻求的文档所在分片的位置。

这就解释了，**为什么创建索引的时候就要确定好主分片的数量并且不允许更改：因为如果主分片数量变化了，那么之前路由的值都会失效，文档也就查询不到了。**

所有文档的API（get、index、delete、bulk、update、mget）都接收routing路由参数，通过这个参数可以自定义文档到分片的映射。一个自定义路由参数可以用来确保所有相关的文档--例如同一用户文档都存到同一个分片中。