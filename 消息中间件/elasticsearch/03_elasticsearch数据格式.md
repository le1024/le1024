**<font color="red">elasticsearch数据格式</font>**

------

#### 数据格式

Elasticsearch是面向文档型数据库，一条数据就是一个文档。将ES与MySQL的存储做一个类比：

![image-20220430222810937](https://s2.loli.net/2022/06/17/JQ5B1hkOnqSWyAH.png)

ES中的Index可以看做一个数据库，Types相当于表，Documents相当于表的行，Fields相当于表的列。

注意：Elasticsearch 7.X中，Type的概念已经被删除了。