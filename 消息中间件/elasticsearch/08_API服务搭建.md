**elasticsearch—API服务搭建**

------

通过Java API的方式对Elasticsearch服务进行访问



#### 创建maven项目

`pom.xml`

```xml
<!--springboot服务直接引入es的starter就行了-->
<dependency>
    <groupId>org.springframework.boot</groupId>
    <artifactId>spring-boot-starter-data-elasticsearch</artifactId>
</dependency>
```



#### 创建ES客户端对象

```java
/**
     * 创建客户端对象
     */
    @Test
    public void esClient() throws IOException {

        //创建客户端对象RestHighLevelClient
        RestHighLevelClient esclient = new RestHighLevelClient(
                RestClient.builder(new HttpHost("localhost", 9200, "http"))
        );

        //关闭客户端
        esclient.close();
        System.out.println("es closed!");
    }
```

执行程序，如果没有连接异常或者其他异常错误，程序正常执行结束，说明创建ES客户端对象完成。

