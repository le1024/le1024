> activemq的传输协议

activemq支持的传输协议有：TCP,NIO,UDP,SSL,HTTP(S),VM



在`activemq.xml`文件可以看到具体配置信息。

<img src="https://cdn.jsdelivr.net/gh/le1024/image1/le/image-20210815160135171.png" alt="image-20210815160135171" style="zoom:200%;float:left;" />

activemq的默认消息协议是`openwire`.



### 1.tcp协议

1.默认的broker配置，tcp的client监听端口是`61616`.

2.在网络传输数据前，必须序列化数据，消息是通过一个叫`wire protocol`来序列化成字节流.

默认情况下，activemq把`wire protocol`叫做`openwire`,他的目的是促使网络上的效率和数据快速交互.

3.tcp的连接形式如：`tcp://hostname:port?key=value&key=value`，后面参数可选.

4.tcp传输的优点：

​	4.1 tcp协议传输可靠性高，稳定性强

​	4.2 高效性：字节流方式传递，效率更高

​	4.3 有效性、可用性：应用广泛，支持任何平台

5.tcp协议的可配置参数可以参考官网：`http://activemq.apache.org/tcp-transport-reference`.



### 2.nio协议

1.NIO协议和TCP协议类型，但NIO更侧重于底层的访问操作。它允许开发人员对同一资源可有更多的client调用和服务端有更多的负载。

2.适合使用NIO的场景：

​	2.1 可能有大量的client去连接到Broker上，一般情况下，大量的client去连接broker是被操作系统的线程所限制的。因此，NIO的实现比TCP需要更少的线程去运行，所以建议使用NIO协议.

​	2.2 可能对于broker有一个很迟钝的网络传输，NIO比TCP提供更好的性能.

3.nio连接形式如：`nio://hostname:port?key=value`

4.nio协议的可配置参数可以参考官网：`http://activemq.apache.org/nio-transport-reference`.

```xml
<broker>
  ...
  <transportConnectors>
    <transportConnector name="nio" uri="nio://0.0.0.0:61616"/>  
  </<transportConnectors>
  ...
</broker>
```



### 3.amqp协议，stomp协议，ssl协议，mqtt协议，ws协议 扩展学习

`http://activemq.apache.org/configuring-version-5-transports.html`



activemq支持的网络协议：

| 协议    | 描述                                                         |
| :------ | :----------------------------------------------------------- |
| **TCP** | **默认协议，性能相对可以**                                   |
| **NIO** | **基于tcp协议，进行了扩展和优化，具有更好的扩展性**          |
| UDP     | 性能比tcp更好，但是不具有可靠性                              |
| SSL     | 安全链接                                                     |
| HTTP(s) | 基于http或者HTTPS                                            |
| VM      | VM本身不是协议，当客户端和代理在同一个java虚拟机（vm）中运行时，他们之间需要通信，但是不想占用网络通道，而是直接通信，可以使用该方式 |

