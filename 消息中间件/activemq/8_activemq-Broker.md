> 什么是Broker

Broker是实现ActiveMQ功能的一个实例。以代码的形式去创建启动一个**Broker**，就可以当做一个微型的mq服务器传递消息。



> 通过conf启动不同的activemq实例

```bash
# 复制一份activemq.xml
cp activemq.xml activemq02.xml

# 通过activemq02.xml启动mq
./activemq start xbean:file:/home/activemq/conf/activemq02.xml

//多个实例同理
```

<img src="https://cdn.jsdelivr.net/gh/le1024/image1/le/image-20210814230720737.png" alt="image-20210814230720737" style="float:left;" />



> 实现broker

```java
public static void main(String[] args) throws Exception {
        /**
         * 启动这个broker之后，demo1 demo2中的broker_url就可以改成tcp://localhost:61616
         * 用这个broker启动的微型的activemq服务来传递消息
         */
        BrokerService brokerService = new BrokerService();
        brokerService.setUseJmx(true);
        brokerService.addConnector("tcp://localhost:61616");
        brokerService.start();
    }
```

