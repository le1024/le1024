![image-20210818125934139](https://cdn.jsdelivr.net/gh/le1024/image1/le/image-20210818125934139.png)



对于activemq长期的持久性，在使用JDBC时建议也使用高性能的journal

Activemq  Journal，使用了高速缓存写入技术，大大提高了性能，由于JDBC每次将消息去写库和读库会耗费时间，消息数据量多的话，就会造成频繁的写库。

journal是会把生产这者生产的消息存到journal文件中，在消费者消费很快的情况下，journal文件中的消息未同步到DB之前，消费者已经消费了90%的消息，这时，只需要同步剩下的10%的消息到DB。如果消费者消费速度很慢，journal文件可以以批量的方式写到DB。



> journal配置

在`activemq.xml`中修改配置

之前jdbc的配置：

```xml
 <persistenceAdapter>
     <jdbcPersistenceAdapter dataDirectory="activemq-data" useDatabaseLock="false"  dataSource="#mysql-ds"  createTablesOnStartup="true"  />
</persistenceAdapter>
```

改成：添加journal配置

```xml
 <persistenceFactory>
     <journalPersistenceAdapterFactory 
                                       journalLogFiles="4"
                                       journalLogFileSize="32768"
                                       useJournal="true"
                                       useQuickJournal="true"
                                       dataDirectory="activemq-data" 
                                       useDatabaseLock="false"  
                                       dataSource="#mysql-ds"  
                                       createTablesOnStartup="true"  />
</persistenceFactory>
```



> 测试

启动生产者生产消息，再去查看数据库，`ACTIVEMMQ_MSGS`不会立即就会有消息写入，由于`journal`的存在，需要等待一会