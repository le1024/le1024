[TOC]



# springboot整合activemq

## 1.配置文件

>  pom.xml

```xml
<dependency>
    <groupId>org.springframework.boot</groupId>
    <artifactId>spring-boot-starter-activemq</artifactId>
</dependency>
```

> application.yml

```yaml
# activemq配置
spring:
  activemq:
    broker-url: tcp://106.13.187.36:61616 #activemq服务地址
    user: admin # 用户名
    password: hll123 #密码
  jms:
    pub-sub-domain: false # false = Queue, true = Topic
 my-queue: springboot-myqueue
```

`pub-sub-domain`不写默认为false

<img src="https://cdn.jsdelivr.net/gh/le1024/image1/le/image-20210815140739954.png" alt="image-20210815140739954" style="zoom:80%;float:left;" />



## 2.队列-queue

> config

```java
@Component
@EnableJms
public class ActivemqConfig {

    @Value("${my-queue}")
    private String myQueue;

    @Bean
    public Queue queue() {
        return new ActiveMQQueue(myQueue);
    }
}
```



> 生产者代码

```java
@Component
public class QueueProduce {
    @Autowired
    private JmsMessagingTemplate jmsMessagingTemplate;

    @Autowired
    private Queue queue;

    public void produceMessage() {
        jmsMessagingTemplate.convertAndSend(queue, "生产一条消息");
        System.out.println("生产mq消息完成");
    }

}
```

> 单元测试--生产消息

```java
@Resource
private QueueProduce queueProduce;


@Test
public void testSend() {
    queueProduce.produceMessage();
}
```



> 消费者代码

启动main方法测试即可

```java
@Component
public class QueueConsumer {

    @JmsListener(destination = "${my-queue}")
    public void receive(TextMessage textMessage) throws JMSException {
        System.out.println("消费者收到消息：" + textMessage.getText());
    }
}
```



## 3.主题-topic

配置文件需要更改一下`pub-sub-domain: true`

```yaml
spring:
	jms:
		pub-sub-domain: true
my-topic: springboot-mytopic
```

`ActivemqConfig`

```java
/**
     * 主题
     */
    @Value("${my-topic}")
    private String myTopic;

    @Bean
    public Topic topic() {
        return new ActiveMQTopic(myTopic);
    }
```

> 生产者

```java
@Component
public class TopicProduce {
    @Autowired
    private JmsMessagingTemplate jmsMessagingTemplate;

    @Autowired
    private Topic topic;

    public void produceMessage() {
        jmsMessagingTemplate.convertAndSend(topic, "生产一条主题消息");
        System.out.println("生产mq消息完成");
    }
}
```

> 消费者

```java
@Component
public class TopicConsumer {

    @JmsListener(destination = "${my-topic}")
    public void receive(TextMessage textMessage) {
        try {
            System.out.println("消费者收到主题消息：" + textMessage.getText());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
```

> 单元测试

需要先启动消费者订阅，再启动生产者发送消息

```java
@Test
    public void testSendTopic() {
        topicProduce.produceMessage();
    }
```

> 多消费者测试

配置`application.yml`文件多个端口，然后启动项目，就会有多个消费者服务，之后再配置成生产者服务启动。

生产者服务可以配置成定时生产消息，不通过`单元测试`的方式来测试。

一个生产者服务，两个消费者服务，在控制台会看到两个消费者消费生产的消息
