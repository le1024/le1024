> queue

`生产者`

```java
public class JmsProduce {

    public static final String ACTIVEMQ_URL = "tcp://127.0.0.1:61616";

    public static final String USERNAME = "admin";

    public static final String PASSWORD = "hll123";

    public static final String QUEUE_NAME = "queue01";

    public static void main(String[] args) throws Exception {
        //1.按照给定的url创建连接工厂
        ActiveMQConnectionFactory activeMQConnectionFactory = new ActiveMQConnectionFactory(USERNAME, PASSWORD,ACTIVEMQ_URL);
        // 2.通过工厂连接connection 和启动
        Connection connection = activeMQConnectionFactory.createConnection();
        // 3.启动
        connection.start();
        // 4.创建会话session
        //两个参数，第一个事务，第二个签收
        Session session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
        // 5.创建目的地，队列、主题，这里用队列
        Queue queue = session.createQueue(QUEUE_NAME);
        // 6.创建消息的生产者
        MessageProducer messageProducer = session.createProducer(queue);
        // 7.通过MessageProducer生产3条消息发送到消息队列中
        for (int i = 1; i <= 6; i++) {
            //8.创建消息
            TextMessage textMessage = session.createTextMessage("msg:" + LocalDateTime.now());
            //9.发送消息
            messageProducer.send(textMessage);
        }

        // 10.关闭资源
        messageProducer.close();
        session.close();
        connection.close();

        System.out.println(" **** 消息发送到MQ完成 **** ");
    }
}
```

`消费者`

```java
public class JmsConsumer {
    public static final String ACTIVEMQ_URL = "tcp://127.0.0.1:61616";

    public static final String USERNAME = "admin";

    public static final String PASSWORD = "hll123";

    public static final String QUEUE_NAME = "queue01";

    public static void main(String[] args) throws Exception {
        //创建连接工厂
        ActiveMQConnectionFactory activeMQConnectionFactory = new ActiveMQConnectionFactory(USERNAME, PASSWORD, ACTIVEMQ_URL);
        // 创建连接connection
        Connection connection = activeMQConnectionFactory.createConnection();
        //开启连接
        connection.start();
        //创建会话session
        Session session = connection.createSession(false,  Session.AUTO_ACKNOWLEDGE);
        //创建队列，同生产者一致
        Queue queue = session.createQueue(QUEUE_NAME);
        //创建消息消费者
        MessageConsumer messageConsumer = session.createConsumer(queue);

        /**
         * 方法1：同步阻塞方式 receive
         * receive在接收到消息之前或者超时之前将会一直阻塞
         */
//        while(true) {
//            TextMessage textMessage = (TextMessage) messageConsumer.receive(5000L);
//            if (null != textMessage) {
//                System.out.println("**** 消费者接收到消息 ****：" + textMessage.getText());
//            } else {
//                break;
//            }
//
//        }
//        messageConsumer.close();
//        session.close();
//        connection.close();
//        System.out.println("**** 消费者消费消息完成 ****");
        /**end**/

        /**
         * 方法2：通过监听器的方式
         */
        messageConsumer.setMessageListener(new MessageListener() {
            @Override
            public void onMessage(Message message) {
                if (null != message && message instanceof TextMessage) {
                    TextMessage textMessage = (TextMessage) message;
                    try {
                        System.out.println("**** 消费者接收到消息 ****：" + textMessage.getText());
                    } catch (JMSException e) {
                        e.printStackTrace();
                    }
                }
            }
        });
        System.in.read(); // 必须加行代码，不然程序会直接往下执行结束了
        messageConsumer.close();
        session.close();
        connection.close();
        System.out.println("**** 消费者消费消息完成 ****");


    }
}
```





> topic

`生产者`

```java
public class JmsProduceTopic {

    public static final String BROKER_URL = "tcp://106.13.187.36:61616";

    public static final String USERNAME = "admin";

    public static final String PASSWORD = "hll123";

    public static final String TOPIC_NAME = "topic01";

    public static void main(String[] args) throws Exception {
        ActiveMQConnectionFactory activeMQConnectionFactory = new ActiveMQConnectionFactory(USERNAME, PASSWORD, BROKER_URL);

        Connection connection = activeMQConnectionFactory.createConnection();
        connection.start();

        Session session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
        Topic topic = session.createTopic(TOPIC_NAME);

        MessageProducer messageProducer = session.createProducer(topic);

        for (int i = 1; i <= 3; i++) {
            TextMessage textMessage = session.createTextMessage("topic msg:" + Instant.now().atZone(ZoneId.systemDefault()));
            messageProducer.send(textMessage);
        }

        messageProducer.close();
        session.close();
        connection.close();
        System.out.println(" **** 主题消息发送到MQ完成 **** ");
    }
}
```

`消费者`

```java

public class JmsConsumerTopic {
    public static final String BROKER_URL = "tcp://106.13.187.36:61616";

    public static final String USERNAME = "admin";

    public static final String PASSWORD = "hll123";

    public static final String TOPIC_NAME = "topic01";

    public static void main(String[] args) throws Exception {
        ActiveMQConnectionFactory activeMQConnectionFactory = new ActiveMQConnectionFactory(USERNAME, PASSWORD, BROKER_URL);

        Connection connection = activeMQConnectionFactory.createConnection();
        connection.start();

        Session session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
        Topic topic = session.createTopic(TOPIC_NAME);

        MessageConsumer messageConsumer = session.createConsumer(topic);
        messageConsumer.setMessageListener(message -> {
            if (null != message && message instanceof TextMessage) {
                TextMessage textMessage = (TextMessage) message;
                try {
                    System.out.println("**** 消费者接收到主题消息 ****：" + textMessage.getText());
                } catch (JMSException e) {
                    e.printStackTrace();
                }
            }
        });

        System.in.read();
        messageConsumer.close();
        session.close();
        connection.close();
        System.out.println("**** 主题消息消费完成 ****");
    }
}
```



> 总结

> 两种消费方式

**1.同步阻塞方式(receive)**

通过MessageConsumer的receive方法接收消息，receive接收到消息前或者超时前将会一直阻塞

`receive()`  一直阻塞

`receive(long time)` 超时



**2.异步非阻塞方式(监听器)**

通过MessageConsumer的setMessageListener注册一个消息监听器，消息到达之后会自动调用监听器MessageListener的onMessage方法



> 队列如何消费消息

```html
1.先生产消息，只启动一个消费者。
消费者正常消费消息。

2.先生产消息，先启动消费者1，再启动消费者2
消费者1正常消费所有消息，消费者2无消息可消费。

3.先启动两个消费者，再生产6条消息
两个消费者会平均消费消息，各自消费3条，轮询

```

> 主题如何消费消息

```
多个消费者订阅同一个主题，主题发布消息后，都将会收到发布的消息。
```





> 点对点消费，队列（queue）

1.每个消息只有一个消费者，1对1。

2.消息的生产者和消费者没有时间上的相关性。消息被生产之后，无论是生产者是否处于运行状态还是消费者处于运行状态，等到有消费者建立时就会消费消息。

3.消费被消费之后不会存在队列中，所以消费者不会消费已经消费的消息。



> 一对多消费，主题（topic）

1.消息可发送给多个消费者，1对多。

2.消息生产者与消费者有时间上的相关性，消费者订阅主题之后的时间发布的消息可以被消息，订阅之前的不能。

3.生产者发布消息后不保存消息，是无状态的，如果没有订阅，就是一条废消息。所以应该先启动消费者订阅后再启动生产者发布消息。



> queue topic比较

| 目的地     | Topic                                                        | Queue                                                        |
| ---------- | ------------------------------------------------------------ | ------------------------------------------------------------ |
| 工作模式   | “订阅-发布”模式，如果当前没有订阅者，消息会被丢弃。如果有多个订阅者，那么每个订阅者都会收到消息。 | “负载均衡”模式，如果当前没有消费者，消息不会丢弃；如果有多个消费者，每个消息也只会发给一个消费者，并且是ack消费。 |
| 有无状态   | 无状态                                                       | 默认存储在activemq安装路径下的==data/kahadb/db.data==里面。也可以配置成DB存储。 |
| 传递完整性 | 没有订阅者，消息会被丢弃                                     | 消息不会丢弃                                                 |
| 处理效率   | 消息是按照订阅者的数量进行复制，所以处理速度会随着订阅者的增加而明显降低，当然也需要结合不同消息协议自身的性能差异 | 一条消息只会发送给一个消费者，所以性能不会明显降低，同样也需要结合不同消息协议自身的性能差异 |

