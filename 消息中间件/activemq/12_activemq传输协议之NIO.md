如果不特别指定activemq的网络监听端口，那么这些端口将默认使用BIO。

为了提高单节点的网络吞吐性能，需要明确指定activemq的网络IO模型。

如：配置URI格式头以`nio`开头，表示这个端口使用以tcp为基础的NIO网络IO模型。

> 修改activemq.xml

在`transportConnectors`节点加上nio配置

```xml
<transportConnector name="nio" uri="nio://0.0.0.0:61618?trace=true"/>
```

保存`activemq.xml`，重新启动activemq，访问控制台，可以看到新添加的NIO

<img src="https://cdn.jsdelivr.net/gh/le1024/image1/le/image-20210815165002193.png" alt="image-20210815165002193" style="zoom:80%;float:left;" />



> 代码中使用

将原先的uri中的`tcp`改成`nio`即可



> 配置端口支持nio且支持多协议

通过`auto`关键词配置。

activemq 5.13.0版本之后支持OpenWire, STOMP, AMQP, and MQTT可以被自动的侦测到，允许4种类型的客户端共享一种传输。

官网：`http://activemq.apache.org/auto`

[activemq官网]: http://activemq.apache.org/auto



<img src="https://cdn.jsdelivr.net/gh/le1024/image1/le/image-20210815223438937.png" alt="image-20210815223438937" style="zoom:80%;float:left;" />

在`transportConnectors`节点加上`auto+nio`配置

```xml
<transportConnector name="auto+nio" 
                    uri="nio://0.0.0.0:61618?maximumConnections=1000&amp;wireFormat.maxFrameSize=104857600&amp;org.apache.activemq.transport.nio.SelectorManager.corePoolSize=20&amp;org.apache.activemq.transport.nio.SelectorManager.maximumPoolSize=50" 
                    />
```

<img src="https://cdn.jsdelivr.net/gh/le1024/image1/le/image-20210815223849933.png" alt="image-20210815223849933" style="zoom:80%;float:left" />

