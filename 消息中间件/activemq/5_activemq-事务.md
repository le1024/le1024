> 生产者事务



```java
//两个参数，第一个事务，第二个签收
Session session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
```

**false**

只要执行send，就进入到队列中。

关闭事务，第2个签收参数的设置需要有效。

**true**

先执行send再执行commit，消息才会被真正的提交到队列中。

手动commit可以方便事务进行回滚

```java
try {
    // ... 业务完成后commit
    session.commit();
} catch (Exception e) {
    // 出错
    session.rollback();
} finally {
	if (session != null) {
        session.close();
    }
}
```



> 消费者事务

```java
//两个参数，第一个事务，第二个签收
Session session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);

//....
session.commit();
//....
```

配置同生产者事务，如果设置true后未commit，会造成重复消费。