> 非事务签收

**自动签收**(默认)

```java
//两个参数，第一个事务，第二个签收
//Session.AUTO_ACKNOWLEDGE
Session session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
```

**手动签收**

```java
//两个参数，第一个事务，第二个签收
//Session.CLIENT_ACKNOWLEDGE
Session session = connection.createSession(false, Session.CLIENT_ACKNOWLEDGE);

message.acknowledge();
```



**允许重复消息**(不常用)

```java
//两个参数，第一个事务，第二个签收
//Session.DUPS_OK_ACKNOWLEDGE
Session session = connection.createSession(false, Session.DUPS_OK_ACKNOWLEDGE);
```





> 事务签收

开启事务后，必须commit。之后会自动签收。

至于签收属性的设置无所谓。**事务大于签收。**



> 签收和事务关系

```java
1.在事务性的会话当中，当一个事务被成功提交则消息被自动签收。
如果事务回滚，则消息会被再次传送
2.非事务性的会话当中，消息何时被签收取决于创建会话时的应答模式：acknowledge
```

