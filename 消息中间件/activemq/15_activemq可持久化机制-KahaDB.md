> KahaDB

KahaDB是一个基于文件的持久性数据库，从5.4版本开始默认使用，类似于redis的`AOF`

具有更好的快速持久性

[KahaDB]: https://activemq.apache.org/kahadb



> 配置

查看 `activemq.xml`

<img src="https://cdn.jsdelivr.net/gh/le1024/image1/le/image-20210816215101133.png" alt="image-20210816215101133" style="zoom:50%;float:left;" />



在activemq安装路径下，activemq/data/hahadb，可以看到db文件

<img src="https://cdn.jsdelivr.net/gh/le1024/image1/le/image-20210816215220208.png" alt="image-20210816215220208" style="zoom:67%;float:left;" />



> KahaDB存储原理

KahaDB目前是默认的存储方式，可用于任何场景，提高了性能和恢复能力.

消息存储使用一个`事务日志`和一个`索引`来存储他所有的地址.



KahaDB数据保存目录有4类文件加1个lock文件。

**db-1.log**，**db.data**，**db.free**，**db.redo**，**lock**



- `db-<number>.log存消息数据`，number表示存储消息达到预定的大小时，文件的命令方式。当文件满时，会创建一个新的文件，number值是随之递增的，db-1.log,db-2.log,db-3.log..... 当不再有消息存到数据文件中时，文件会被删除或存档

  `存的是具体的消息数据`

- `db.data包含了持久化的B-Tree索引`，索引了消息数据记录中（db-<number>.log）的消息，他是消息的索引文件，本质上是B-Tree，`使用B-Tree作为索引指向db-<number>.log里面的存储的数据`

  `索引文件`

- `db-free 记录db.data文件里面的空闲页面`，存的是空闲页的id，方便建立索引时，先从空闲的开始建立保证索引的连续性，没有碎片

- `db.redo 进行消息恢复`，如果kahaDB消息存储强制退出后启动，用于恢复B-Tree索引

- lock文件锁，表示当前获取kahaDB读写权限的broker，类似mysql的悲观锁

