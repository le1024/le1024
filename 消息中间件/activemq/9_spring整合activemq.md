目录

[TOC]



# spring整合activemq

## spring整合activemq-队列

> applicationContext.xml

```xml
<?xml version="1.0" encoding="UTF-8"?>
<beans xmlns="http://www.springframework.org/schema/beans"
       xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
       xmlns:context="http://www.springframework.org/schema/context"
       xsi:schemaLocation="http://www.springframework.org/schema/beans http://www.springframework.org/schema/beans/spring-beans.xsd http://www.springframework.org/schema/context http://www.springframework.org/schema/context/spring-context.xsd">

    <!--开启包自动扫描-->
    <context:component-scan base-package="app.activemq" />

    <!--配置生产者-->
    <!--获取connection工厂-->
    <bean id="jmsFactory" class="org.apache.activemq.pool.PooledConnectionFactory" destroy-method="stop">
        <property name="connectionFactory">
            <bean class="org.apache.activemq.ActiveMQConnectionFactory">
                <property name="brokerURL" value="tcp://106.13.187.36:61616" />
                <property name="userName" value="admin" />
                <property name="password" value="hll123" />
            </bean>
        </property>
        <property name="maxConnections" value="100" />
    </bean>

    <!--配置队列目的地-->
    <bean id="destinationQueue" class="org.apache.activemq.command.ActiveMQQueue">
        <!-- spring-active-queue:队列名称 -->
        <constructor-arg index="0" value="spring-active-queue" />
    </bean>

    <!--spring提供的jms工具类，用来消息发送接收，类似jdbcTemplate,redisTemplate-->
    <bean id="jmsTemplate" class="org.springframework.jms.core.JmsTemplate">
        <property name="connectionFactory" ref="jmsFactory" />
        <property name="defaultDestination" ref="destinationQueue" />
        <property name="messageConverter">
            <bean class="org.springframework.jms.support.converter.SimpleMessageConverter" />
        </property>
    </bean>
</beans>
```

> 生产者

```java
@Service
public class SpringMQ_Produce {

    @Autowired
    private JmsTemplate jmsTemplate;

    public static void main(String[] args) {
        ApplicationContext ctx = new ClassPathXmlApplicationContext("applicationContext.xml");
        SpringMQ_Produce produce = (SpringMQ_Produce) ctx.getBean("springMQ_Produce");

        produce.jmsTemplate.send((session) -> {
            return session.createTextMessage("spring整合activemq");
        });
        System.out.println("****发送消息到MQ****");
    }
}
```

> 消费者

```java
@Service
public class SpringMQ_Consumer {

    @Autowired
    private JmsTemplate jmsTemplate;

    public static void main(String[] args) {
        ApplicationContext ctx = new ClassPathXmlApplicationContext("applicationContext.xml");

        SpringMQ_Consumer consumer = (SpringMQ_Consumer) ctx.getBean("springMQ_Consumer");

        String msg = (String) consumer.jmsTemplate.receiveAndConvert();
        System.out.println("消费者收到的消息:" + msg);
    }
}
```



## spring整合activemq-主题

> applicationContext.xml

```xml
<?xml version="1.0" encoding="UTF-8"?>
<beans xmlns="http://www.springframework.org/schema/beans"
       xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
       xmlns:context="http://www.springframework.org/schema/context"
       xsi:schemaLocation="http://www.springframework.org/schema/beans http://www.springframework.org/schema/beans/spring-beans.xsd http://www.springframework.org/schema/context http://www.springframework.org/schema/context/spring-context.xsd">

    <!--开启包自动扫描-->
    <context:component-scan base-package="app.activemq" />

    <!--配置生产者-->
    <!--获取connection工厂-->
    <bean id="jmsFactory" class="org.apache.activemq.pool.PooledConnectionFactory" destroy-method="stop">
        <property name="connectionFactory">
            <bean class="org.apache.activemq.ActiveMQConnectionFactory">
                <property name="brokerURL" value="tcp://106.13.187.36:61616" />
                <property name="userName" value="admin" />
                <property name="password" value="hll123" />
            </bean>
        </property>
        <property name="maxConnections" value="100" />
    </bean>

    <!--配置队列目的地-->
    <bean id="destinationTopic" class="org.apache.activemq.command.ActiveMQTopic">
        <!-- spring-active-queue:队列名称 -->
        <constructor-arg index="0" value="spring-active-topic" />
    </bean>

    <!--spring提供的jms工具类，用来消息发送接收，类似jdbcTemplate,redisTemplate-->
    <bean id="jmsTemplate" class="org.springframework.jms.core.JmsTemplate">
        <property name="connectionFactory" ref="jmsFactory" />
        <property name="defaultDestination" ref="estinationTopic" />
        <property name="messageConverter">
            <bean class="org.springframework.jms.support.converter.SimpleMessageConverter" />
        </property>
    </bean>
</beans>
```

> 生产者

```java
@Service
public class SpringMQ_Produce {

    @Autowired
    private JmsTemplate jmsTemplate;

    public static void main(String[] args) {
        ApplicationContext ctx = new ClassPathXmlApplicationContext("applicationContext.xml");
        SpringMQ_Produce produce = (SpringMQ_Produce) ctx.getBean("springMQ_Produce");

        produce.jmsTemplate.send((session) -> {
            return session.createTextMessage("spring整合activemq");
        });
        System.out.println("****发送消息到MQ****");
    }
}
```

> 消费者

```java
@Service
public class SpringMQ_Consumer {

    @Autowired
    private JmsTemplate jmsTemplate;

    public static void main(String[] args) {
        ApplicationContext ctx = new ClassPathXmlApplicationContext("applicationContext.xml");

        SpringMQ_Consumer consumer = (SpringMQ_Consumer) ctx.getBean("springMQ_Consumer");

        String msg = (String) consumer.jmsTemplate.receiveAndConvert();
        System.out.println("消费者收到的消息:" + msg);
    }
}
```



## 监听方式的实现

```xml
<!--配置监听程序-->
    <bean id="jmsContainer" class="org.springframework.jms.listener.DefaultMessageListenerContainer">
        <property name="connectionFactory" ref="jmsFactory" />
        <property name="destination" ref="destinationQueue" />
        <property name="messageListener" ref="myMessageListener" />
    </bean>
```



```java
@Component
public class MyMessageListener implements MessageListener {

    @Override
    public void onMessage(Message message) {
        if (null != message && message instanceof TextMessage) {
            TextMessage textMessage = (TextMessage) message;
            try {
                System.out.println(textMessage.getText());
            } catch (JMSException e) {
                e.printStackTrace();
            }
        }
    }
}
```

