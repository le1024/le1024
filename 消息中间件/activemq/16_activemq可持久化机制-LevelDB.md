> LevelDB（了解）

[LevelDB]: https://activemq.apache.org/leveldb-store

LevelDB是在activemq 5.8版本之后引入的，和KahaDB类似，也是基于文件的本地数据库存储形式，但比KahaDB更快的持久性。没有使用定制的B-Tree实现索引来写日志，而是使用基于LevelDB的索引，由于“append only”文件访问模式，具有更好的属性：

- 快速更新(不需要进行随机磁盘更新)
- 并发读取
- 使用硬链接的快速索引快照

注意：

```tiki wiki
Warning

The LevelDB store has been deprecated and is no longer supported or recommended for use. The recommended store is KahaDB
```

但是，ActiveMQ V5.14.2版本开始弃用了LevelDB，并在V5.17.0版本删除了LevelDB，官方推荐还是使用`KahaDB`.



默认配置如下，activemq.xml文件：

```xml
<persistenceAdapter>
    <levelDB directory="activemq-data" />
</persistenceAdapter>
```



