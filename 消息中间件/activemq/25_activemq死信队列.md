> 死信队列

死信队列：Dead Letter Queue

一条消息被重复发送多次之后（默认重发6次，redeliveryCounter=6），将会被移入到死信队列。开发人员可以通过这个queue查看出错的消息，进行人工干预。



![image-20210824205735458](https://cdn.jsdelivr.net/gh/le1024/image1/le/image-20210824205735458.png)





一般生产环境中使用mq的时候设计两个队列：

`1.核心业务队列` ：处理正常的业务逻辑

`2.死信队列`：处理异常信息



> 配置

##### `SharedDeadLetterStrategy`：共享死信队列策略

将所有的DeadLetter保存在一个共享的队列中，这是activemq的默认策略。共享队列默认为“ActiveMQ.DLQ”，可以通过“deadLetterQueue”属性来设定。

```xml
<deadLetterStrategy>
    <sharedDeadLetterStrategy deadLetterQueue="DLQ-QUEUE" />
</deadLetterStrategy>
```



#####  `IndividualDeadLetterStrategy`：独立的死信队列策略

把DeadLetter放入到各自的死信队列中。

对于queue而言：死信通道的前缀默认为“ActiveMQ.DLQ.Queue”

对于topic而言：死信通道的前缀默认为“ActiveMQ.DLQ.Topic”



比如队列order，那么它对应的死信通道为“ActiveMQ.DLQ.Queue.Order”

我们使用"queuePrefix"，"topicPrefix"来指定上述前缀

默认情况下，queue和topic，broker都将使用queue来保存DeadLetter，即死信通道通常为Queue，也可以指定为Topic：

```xml
<policyEntry queue="order">
    <deadLetterStrategy>
    	<individualDeadLetterStrategy queuePrefix="DLQ." useQueueForQueueMessages="false" />
    </deadLetterStrategy>
</policyEntry>
```

将队列Order中出现的DeadLetter保存在DLQ.Order中，不过此时的DLQ.Order为topic.

`useQueueForQueueMessages`：表示是否将topic的DeadLetter保存在Queue中.默认`true`



##### 自动删除过期消息

有时需要删除过期的消息而不需要发送到死信队列中。"processExpired"表示是否将过期的消息放到死信队列，默认`true`

```xml
<policyEntry queue=">">
    <deadLetterStrategy>
    	<sharedDeadLetterStrategy processExpired="false" />
    </deadLetterStrategy>
</policyEntry>
```



##### 存放非持久的消息到死信队列

默认情况下，mq不会把非持久的有毒消息放到死信队列中。

"processNonPersistent"表示是否将非持久的消息放到死信队列，默认`false`，不放

```xml
<!-- queue=">" 代表全部有效 -->
<policyEntry queue=">">
    <deadLetterStrategy>
    	<sharedDeadLetterStrategy processNonPersistent="true" />
    </deadLetterStrategy>
</policyEntry>
```

