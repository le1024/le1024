> Message消息之消息头



**JMSDestination**

消息发送的目的地，分为queue和topic。可以针对某个特殊的消息进行设置。

```java
// 设置目的地
textMessage.setJMSDestination(topic);
```



**JMSDeliveryMode**

持久化模式

对于一条持久化消息：应该被传送“一次仅仅一次”，这就意味着如果JMS提供者出现故障，该消息不会丢失，它会在服务器恢复之后再次传递

对于一条非持久化的消息：最多会传送一次，意味着服务器出现故障，该消息会丢失。

```java
// 设置持久化 
// DeliveryMode.NON_PERSISTENT 非持久化
// DeliveryMode.PERSISTENT 持久化
textMessage.setJMSDeliveryMode(DeliveryMode.PERSISTENT);
```



**JMSExpiration**

消息过期时间

可以设置消息在一定时间后过期，==默认永不过期==

如果消息在过期时间达到之后还没发送到目的地，该消息就会被清除。

```java
// 设置过期时间
textMessage.setJMSExpiration(1000L);

// 也可以用send方法设置
send方法的timeToLive值为0表示消息用不过期
    
```



**JMSPriority**

消息的优先级

从0-9十个级别，0-4普通消息，5-9加急消息，默认是4级。

JMS不要求mq严格按照这10个优先级发送消息，但必须保证加急消息要优于普通消息到达。

```java
// 设置优先级
textMessage.setJMSPriority(4);
```



**JMSMessageID**

MQ对每个消息产生的唯一标识的ID。

```java
textMessage.setJMSMessageID(UUID.randomUUID().toString());
```



一些消息头数据可以直接在==send方法==里面设置

![](https://cdn.jsdelivr.net/gh/le1024/image1/le/image-20210728160708568.png)



> Message消息之消息体

JMS消息体：用来封装具体的消息数据。



消息体类型有五种，常用的是**TextMessage**和**MapMessage**

==发送方和接收方的消息体类型必须一致。==



| 消息类型      | 说明                                           |
| ------------- | ---------------------------------------------- |
| TextMessage   | 普通字符串，包含一个String                     |
| MapMessage    | Map类型，Key为String字符串，值为java的基本类型 |
| ByteMessage   | 二进制数组消息，包含一个byte[]                 |
| StreamMessage | java数据量消息，用标准流操作来顺序的填充和读取 |
| ObjectMessage | 对象消息，包含一个可序列化的java对象           |



> Message消息之消息属性



以属性名和属性值为键值对关系的形式指定的。

可以将属性视为消息头的扩展，属性指定一些消息头没有包括的扩展信息。消息的属性就是给一条消息附加一条消息头，可以添加一些不透明的数据在属性里。



```java
// 为某条数据设置属性，可以区分数据
TextMessage textMessage = session.createTextMessage();
textMessage.setStringProperty("user_01", "vip");

// 获取消息属性
textMessage.getStringProperty("user_01");
```

