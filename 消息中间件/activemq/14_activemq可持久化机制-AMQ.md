> AMQ （了解）

AMQ是一种文件存储 的形式，它具有写入速度快和易恢复的特点。消息存储在一个文件中，文件默认大小为`32M`，当一个文件里面的消息都被消费后，这个文件就会被标志为可删除，在下一个清除阶段，这个文件删除。

AMQ适用于activemq 5.3之前的版本

[amq文档]: https://activemq.apache.org/amq-message-store

```xml
<broker brokerName="broker" persistent="true" useShutdownHook="false">
    <persistenceAdapter>
      <amqPersistenceAdapter directory="${activemq.base}/activemq-data" maxFileLength="32mb"/>
    </persistenceAdapter>
    <transportConnectors>
      <transportConnector uri="tcp://localhost:61616"/>
    </transportConnectors>
  </broker>
```

### AMQ Store Properties

| property name                 | default value | Comments                                 |
| ----------------------------- | ------------- | ---------------------------------------- |
| `directory`                   | activemq-data | 存储数据路径                             |
| `useNIO`                      | true          | 使用nio将消息写入数据日志                |
| `syncOnWrite`                 | false         | 同步写入                                 |
| `maxFileLength`               | 32mb          | 消息数据日志最大值                       |
| `persistentIndex`             | true          | 使用持久化索引，false表示使用内存结构    |
| `maxCheckpointMessageAddSize` | 4kb           | 自动提交之前保持事务中最大的消息数       |
| `cleanupInterval`             | 30000         | 检测丢弃或者移动消息数据之前的时间(ms)   |
| `indexBinSize`                | 1024          | 索引默认容器数，容器越大索引性能相对越好 |
| `indexKeySize`                | 96            | 索引键的大小，key默认是消息id            |
| `indexPageSize`               | 16kb          | 索引页的大小，页面越大，索引写入性能越好 |
| `directoryArchive`            | archive       | 存储丢弃的数据日志路径                   |
| `archiveDataLogs`             | false         | 真实的数据移动到存档目录，而不是删除     |