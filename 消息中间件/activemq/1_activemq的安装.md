> 官网下载activemq

https://activemq.apache.org/components/classic/download/



##### 1.安装到服务器

下载的文件上传到服务器的指定路径，==并解压==

```bash
# 解压
tar -zxvf apache-activemq-5.16.2-bin.tar.gz

# 重命名,可以忽略这一步
mv apache-activemq-5.16.2-bin.tar.gz activemq
```



#####  2.启动activemq

进入到 `activemq/bin`目录

```bash
./activemq start
```

停止mq可以**kill进程**或者**./activemq stop**



##### 3.访问activemq

**ip:8161**进行访问，默认用户名密码：admin  admin

![image-20210727103835533](https://cdn.jsdelivr.net/gh/le1024/image1/le/image-20210727103835533.png)

##### 4.访问配置

###### 4.1用户名和密码

更新用户名和密码

编辑`jetty-realm.properties`文件

```bash
#用户名: 密码, 角色
admin: admin123, admin
```

###### 4.2远程访问

防火墙和端口，以及安全组都正常开放的情况下，还是无法通过ip:8161访问到activemq的控制台的话，打开配置文件**`jetty.xml`**，如图所示，注释掉

![远程访问](https://cdn.jsdelivr.net/gh/le1024/image1/le/6007a3eb428d43a6b4531c1bd90d0cb4.png)