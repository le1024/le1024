> 使用activemq如何保证高可用性

zookeeper+replicated leveldb的主从集群

基于zookeeper和LevelDB搭建activemq集群。集群仅提供主备方式的高可用集群功能，避免单点故障



![image-20210818221559535](https://cdn.jsdelivr.net/gh/le1024/image1/le/image-20210818221559535.png)

原理说明：

使用zookeeper集群注册的所有的activemq broker中有且只有一个broker可以提供服务，视为**master**，

其他的broker处于待机状态被视为**slave**。

如果master因故障不能提供服务，zookeeper将会从slave中选举一个broker当做master。

slave连接master会同步他们的存储状态，slave是不接受客户端连接的。连接上master的salve会复制到master上所有的存储操作。故障节点在恢复后会重新加入到集群中并连接master进入到slave模式。

所有需要同步的消息操作都将等待存储状态被复制得到其他法定节点的操作完成才能完成。

所以，如果配置了replicas=3，那么法定节点数量是(3/2)+1=2。master将会存储并更新然后等待(2-1)=1个slave存储和更新完成，才汇报success。

有一个节点要作为观察者存在，当一个新的master被选中，至少需要保障一个法定节点在线以能够找到拥有最新状态的节点。这个节点才可以成为新的master。

推荐运行至少3个replica nodes以防止一个Node失败后服务中断. **一主二从**



