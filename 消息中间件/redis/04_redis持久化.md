> redis-持久化

redis提供了两种持久化的方式，`RDB`持久化和`AOF`持久化



#### RDB

##### 1.什么是RDB

将数据库的快照（snapshot）以二进制的方式保存到磁盘中



##### 2.配置方式

`redis.conf`中有默认的配置，例：<font color="red">save 900 1</font> 在900秒之后，如果至少有一个key发生变化，redis就会自动触发`BGSAVE`命令创建快照



##### 3.触发机制

- 触发`redis.conf`配置里面的save机制
- 执行flushall命令
- 退出redis



##### 4.恢复数据

dump.rdb文件放在redis启动目录，redis启动时会自动检测并恢复数据，

<strong style="color:rgb(196, 0, 7)">**注意！！dump.rdb文件不能随意删除！！并做好备份！！**</strong>



##### 5.优缺点

优点：

- <strong style="color:rgb(60,179,113)">适合大规模数据恢复，适合冷备，全量恢复</strong>

- <strong style="color:rgb(60,179,113)">恢复数据相对AOF更快</strong>

  <strong style="color:rgb(60,179,113)">原因：rdb是一份数据文件，直接加载到内存中就完了，而aof需要回放和执行所有的指令日志</strong>

- <strong style="color:rgb(60,179,113)">性能消耗小</strong>

  <strong style="color:rgb(60,179,113)">原因：主进程fork一个子进程来进行rdb的持久化操作，只有在触发机制的时候才把数据存盘</strong>

缺点：

- <strong style="color:rgb(255,165,0)">无法实现实时或者秒级持久化</strong>
- <strong style="color:rgb(255,165,0)">fork进程的时候，占用一定的内存空间</strong>



#### AOF

##### 1.什么是AOF

将所有<strong style="color:rgb(196, 0, 7)">**写入**</strong>到redis的命令及其参数记录到aof文件中，以达到记录数据库状态的目的



##### 2.配置方式

`redis.conf`文件中，找到`appendonly`，配置为yes即可！



##### 3.触发机制

配置完成，重启redis生效

**AOF文件修复机制**：aof损坏，redis无法正常启动，在redis安装目录下通过`redis-check-aof --fix appendonly.aof`命令修复



##### 4.恢复数据

检测完apf文件正常后，重启redis会自动恢复

测试：

```bash
127.0.0.1:6379> set k1 v1
OK
127.0.0.1:6379> set k2 v2
OK
127.0.0.1:6379> flushall
OK
127.0.0.1:6379> get k1
(nil)
```

执行完上述命令，`appendonly.aof`文件中会有图中的数据生成，`flushall`命令也会存在，恢复数据`flushall`需要删除

![image-20210930160558222](https://cdn.jsdelivr.net/gh/le1024/image1/le/image-20210930160558222.png)

```bash
vim appendonly.aof
```

删除最后一行`flushall`命令，不然恢复的时候，会执行该命令，数据还是会被删除

关闭redis，执行`redis-check-aof --fix appendonly.aof`，检测完成后重启redis，可以正常获取到数据了

![image-20210930160836190](https://cdn.jsdelivr.net/gh/le1024/image1/le/image-20210930160836190.png)



##### 5.优缺点

优点：

- <strong style="color:rgb(60,179,113)">更好的保护数据，数据更完整</strong>
- <strong style="color:rgb(60,179,113)">写入性能更高，aof文件过大也不会影响客户端的读写</strong>
- <strong style="color:rgb(60,179,113)">适合灾难性的误删紧急恢复</strong>

缺点：

- <strong style="color:rgb(255,165,0)">aof文件过大，不影响客户端读写，但是恢复速度会慢</strong>
- <strong style="color:rgb(255,165,0)">性能开销大，aof一般设置成每秒同步一次日志文件</strong>





#### 如何选择

1.rdb和aof同时开启，优先通过aof来恢复数据，因为aof的数据要更加完整