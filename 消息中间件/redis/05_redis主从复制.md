> redis-主从复制



#### 1.概念

一台redis服务器复制其他redis服务器数据，前者为**master**-写为主，后者为**slave**-读为主

<strong style="color:rgb(255,20,147)">**数据复制为单向的，只能主节点到子节点**</strong>



#### 2.作用

- [x] 数据热备份

- [x] 故障恢复：主节点出现问题，子节点提供服务
- [x] 负载均衡：主节点写，子节点读，分担服务器负载
- [x] 哨兵模式和集群搭建的基础



#### 3.环境搭建

<strong style="color:#33ccff">redis本身就是一个master</strong>，所以只需要再配置slave即可



`info replication`：查看当前redis信息

```bash
127.0.0.1:6379> info replication #查看当前库的信息
# Replication 
role:master # 角色 master 主机
connected_slaves:0 # 0没有从机
master_failover_state:no-failover
master_replid:27ccc52906f9c01782b0960bc04eb78fc074603a
master_replid2:0000000000000000000000000000000000000000
master_repl_offset:0
second_repl_offset:-1
repl_backlog_active:0
repl_backlog_size:1048576
repl_backlog_first_byte_offset:0
repl_backlog_histlen:0
```

![image-20210930163008581](https://cdn.jsdelivr.net/gh/le1024/image1/le/image-20210930163008581.png)



##### 3.1 一主二从服务搭建

需要三个服务，`redis.conf`文件复制三份，`redis79.conf`，`redis80.conf`，`redis81.conf`

```bash
#复制出三个redis.conf 分别对应6379 6380 6381三个端口
[root@MiWiFi-R1CM-srv src]# cd /usr/local/bin
[root@MiWiFi-R1CM-srv bin]# ls
lconfig  redis-benchmark  redis-check-aof  redis-check-rdb  redis-cli  redis-sentinel  redis-server
[root@MiWiFi-R1CM-srv bin]# cd lconfig/
[root@MiWiFi-R1CM-srv lconfig]# ls
redis79.conf  redis80.conf  redis81.conf  redis.conf
```

并且，修改文件配置：

- port：改成对应的端口，6379，6380，6381
- pidfile：可按端口命名
- logfile：可按端口命名
- dbfilename：可按端口命名

```bash
port 6379/6380/6381
pidfile  /var/run/redis_6379.pid
log "6379.log"
dbfilename dump6379.rdb
```





##### 3.2 配置从机

通过`redis-server redis.conf`方式连接两个从机，执行命令：<strong style="color:rgb(30,144,255)">**SLAVEOF HOST PORT**</strong>

```bash
redis-server lconfig/redis79.conf
redis-server lconfig/redis80.conf
redis-server lconfig/redis81.conf

[root@MiWiFi-R1CM-srv bin]# ps -ef|grep redis
root      47780      1  0 23:50 ?        00:00:00 redis-server 127.0.0.1:6379
root      49921      1  0 23:50 ?        00:00:00 redis-server 127.0.0.1:6380
root      50706      1  0 23:50 ?        00:00:00 redis-server 127.0.0.1:6381
root      51440  38217  0 23:51 pts/2    00:00:00 grep --color=auto redis
```

例：`slaveof 127.0.0.1 6379`，配置为6379redis服务器的从机

```bash
127.0.0.1:6380> slaveof 127.0.0.1 6379
OK
127.0.0.1:6380> info replication #查看信息
# Replication
role:slave # 变成了slave了
master_host:127.0.0.1 # 主机信息
master_port:6379 # 主机端口
master_link_status:up
master_last_io_seconds_ago:8
master_sync_in_progress:0
slave_repl_offset:84
slave_priority:100
slave_read_only:1
replica_announced:1
connected_slaves:0
master_failover_state:no-failover
master_replid:1dfed2c24e7e86ad6a9e615e55c4b9b5c9005f05
master_replid2:0000000000000000000000000000000000000000
master_repl_offset:84
second_repl_offset:-1
repl_backlog_active:1
repl_backlog_size:1048576
repl_backlog_first_byte_offset:1
repl_backlog_histlen:84
```

同理，可配置另一台redis从机

查看主机配置

```bash
127.0.0.1:6379> info replication
# Replication
role:master
connected_slaves:2 # 有两个从机
slave0:ip=127.0.0.1,port=6380,state=online,offset=476,lag=0 #从机1信息
slave1:ip=127.0.0.1,port=6381,state=online,offset=476,lag=1 #从机2信息
master_failover_state:no-failover
master_replid:1dfed2c24e7e86ad6a9e615e55c4b9b5c9005f05
master_replid2:0000000000000000000000000000000000000000
master_repl_offset:476
second_repl_offset:-1
repl_backlog_active:1
repl_backlog_size:1048576
repl_backlog_first_byte_offset:1
repl_backlog_histlen:476
```



#### 4.相关说明

- <strong style="color:rgb(30,144,255)">通过命令的方式开启主从复制只是暂时的，需要在redis.conf里面配上 SLAVEOF IP PORT</strong> 

- 从机只能进行读操作，无法进行写操作

  ![img](https://cdn.jsdelivr.net/gh/le1024/image1/le/2021070722335290.png)

- 如果主机断开，从机依旧连接主机，此时redis集群没有写操作。当主机重连之后，从机依旧可以读取到主机写入的数据
- 如果从机断开，重连后从机会从slave变成master（如果是通过命令行的形式配置的），从机是无法读取到主机的写入数据的，需要重新slaveof才可以

