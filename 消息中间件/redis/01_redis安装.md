> redis-安装

[官网下载安装包](https://redis.io/)

<hr>

#### 1.安装redis

1.上传安装包到服务器，并解压

```bash
tar -zxvf redis-6.0.10.tar.gz
```



2.安装环境，已有`gcc-c++`环境可忽略

```bash
#查看是否安装
g++ -v

yum install gcc-c++
```



3.编译安装redis

```bash
cd redis-6.0.10/src

make && make install
```

make如果报错：`You specified a maxmemory value that is less than 1MB`，升级下c++版本

```bash
yum -y install centos-release-scl
yum -y install devtoolset-9-gcc devtoolset-9-gcc-c++ devtoolset-9-binutils 
scl enable devtoolset-9 bash

echo "source /opt/rh/devtoolset-9/enable" >>/etc/profile
```



4.安装成功后，redis在`/usr/local/bin`目录下

如果需要自己定义个目录，在make这一步骤时指定目录

```bash
make PREFIX=/usr/local/redis install
make install
```



#### 2.配置redis

```bash
#进入到redis开始解压的目录
cd /opt/redis-6.0.10/

#需要备份redis的redis.conf文件,复制一份到自己定义的目录里
#leredis.conf是自定义的配置文件名称
cp redis.conf /usr/local/bin/leredis.conf

# 后面就使用leredis.conf文件操作

#修改leredis.conf
vim /usr/local/bin/leredis.conf
```

##### 2.1开启后台运行

找到`daemonize`配置，更改为`yes`

```bash
# 开启后台运行,daemonize 默认是no，改成yes
daemonize yes
```

##### 2.2开启远程连接

```
bind 0.0.0.0
```

##### 2.3配置认证密码

```bash
requirepass 123456
```





#### 3.启动redis

通过自定的配置文件`leredis.conf`启动redis

```bash
cd /usr/local/bin

redis-server leredis.conf
```

![image-20210930145036717](https://cdn.jsdelivr.net/gh/le1024/image1/le/image-20210930145036717.png)



#### 4.进入redis

端口默认`6379`，可省略

##### 4.1无密码登录

```bash
redis-cli
```

![image-20210930145218459](https://cdn.jsdelivr.net/gh/le1024/image1/le/image-20210930145218459.png)

##### 4.2有密码登录

方式1：

```bash
redis-cli

redis 127.0.0.1:6379> auth 123456:
```

![image-20210930150759137](https://cdn.jsdelivr.net/gh/le1024/image1/le/image-20210930150759137.png)

方式2：

```bash
redis-cli -p 6379 -a 123456
```





> redis离线安装

[redis离线安装](https://www.cnblogs.com/yy3b2007com/p/10513752.html)