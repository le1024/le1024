> redis-订阅发布

Redis 发布订阅 (pub/sub) 是一种消息通信模式：发送者 (pub) 发送消息，订阅者 (sub) 接收消息。

![img](https://cdn.jsdelivr.net/gh/le1024/image1/le/20210630213327599.png)



### 1. 发布订阅测试

#### 1.1 创建订阅频道

```bash
127.0.0.1:6379> SUBSCRIBE channel_1
Reading messages... (press Ctrl-C to quit)
1) "subscribe"
2) "channel_1"
3) (integer) 1
1) "message"
2) "channel_1"
3) "send message 1"
1) "message"
2) "channel_1"
3) "send message 2"
```

#### 1.2 发布消息

```bash
127.0.0.1:6379> PUBLISH channel_1 "send message 1"
(integer) 1
127.0.0.1:6379> PUBLISH channel_1 "send message 2"
(integer) 1
```

向已创建好的订阅频道channel_1发布消息时，已订阅channel_1的客户端就会收到发布的消息。



### 2.java实现发布订阅

