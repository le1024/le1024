**Mycat**

****

#### Mycat是什么

Mycat是数据库中间件，用于连接java程序与数据库



#### 为什么用Mycat

- java与数据库紧耦合
- 高访问量高并发对数据库的压力
- 读写数据请求不一致



#### Mycat可以做什么

##### 1.读写分离

![image-20220619231756319](img.assets/image-20220619231756319.png)

##### 2.数据分片

垂直分片（分库）、水平分片（分表）、垂直+水平拆分（分库分表）

![image-20220619233043824](https://fastly.jsdelivr.net/gh/le1024/image1/le/image-20220619233043824.png)

##### 3.多数据源整合

![image-20220619233917257](https://fastly.jsdelivr.net/gh/le1024/image1/le/image-20220619233917257.png)





#### Mycat原理

Mycat的原理中最重要的一个动词是"拦截"，它拦截了用户发送过来的SQL语句，首先对SQL语句做了一些特定的分析：如分片分析、路由分析、读写分离分析、缓存分析等，然后将此SQL发往后端的真实数据库，并将返回的结果做适当的处理，最终再返回给用户。

![image-20220620000053427](https://fastly.jsdelivr.net/gh/le1024/image1/le/image-20220620000053427.png)

这种方式把数据库的分布式从代码中解耦出来，程序员察觉不出来后台使用了Mycat还是MySQL