**Mycat配置文件**

****



##### 1.服务（server）

服务相关配置，在`conf`路径下，默认配置即可

![image-20220621232353983](https://fastly.jsdelivr.net/gh/le1024/image1/le/image-20220621232353983.png)



##### 2.用户（user）

配置用户相关信息，在`conf/users`路径下

命令方式：{用户名}.user.json

![image-20220621232532787](https://fastly.jsdelivr.net/gh/le1024/image1/le/image-20220621232532787.png)

```bash
vim root.user.json

{
    "ip":null,
    "password":"123456",
    "transactionType":"xa",
    "username":"root",
    "isolation":3
}

# 字段含义
#ip：客户端访问ip，建议为空，填写后会对客户端的ip进行限制
#username：用户名
#password：密码

#isolation：初始化的事务隔离级别
READ_UNCOMMITTED:1
READ_COMMITTED:2
REPEATED_READ:3,默认
SERIALIZABLE:4

#transactionType：事务类型
proxy 本地事务,在涉及大于 1 个数据库的事务,commit 阶段失败会导致不一致,但是兼容性最好
xa 事务,需要确认存储节点集群类型是否支持 XA
```



##### 3.数据源（datasource）

配置mycat连接的数据源信息，在`conf/datasources`路径下

命令方式：{数据源名称}.datasource.json

![image-20220621233711238](https://fastly.jsdelivr.net/gh/le1024/image1/le/image-20220621233711238.png)

```bash
vim protorypeDs.datasource.json

{
    "dbType": "mysql",
    "idleTimeout": 60000,
    "initSqls": [],
    "initSqlsGetConnection": true,
    "instanceType": "READ_WRITE",
    "maxCon": 1000,
    "maxConnectTimeout": 3000,
    "maxRetryCount": 5,
    "minCon": 1,
    "name": "prototype",
    "password": "123456",
    "type": "JDBC",
    "url":
    "jdbc:mysql://127.0.0.1:3306/mysql?useUnicode=true&serverTimezone=UTC",
    "user": "root",
    "weight": 0,
    "queryTimeout":30,//mills
}

#字段含义
# dbType：数据库类型，mysql
# name：用户名
# password：密码
# type：数据源类型，默认JDBC
# url：数据库访问地址
# idleTimeout：空闲连接超时时间
# initSqls：初始化SQL
# initSqlsGetConnection：对于jdbc每次获取连接是否都执行initSqls
# instanceType：配置实例只读还是读写，READ_WRITE,READ,WRITE
# weight：权重

# 连接相关配置
"maxCon": 1000,
"maxConnectTimeout": 3000,
"maxRetryCount": 5,
"minCon": 1

```



##### 4.集群（cluster）

配置集群信息，在`conf/clusters`路径下

在mycat中，创建一个集群，就会对应生成一个cluster文件

命令方式：{集群名称}.cluster.json

![image-20220621234244189](https://fastly.jsdelivr.net/gh/le1024/image1/le/image-20220621234244189.png)

```bash
vim prototype.cluster.json

{
    "clusterType":"MASTER_SLAVE",
    "heartbeat":{
        "heartbeatTimeout":1000,
        "maxRetryCount":3,//2021-6-4前是maxRetry，后更正为maxRetryCount
        "minSwitchTimeInterval":300,
        "slaveThreshold":0
    },
    "masters":[ //配置多个主节点,在主挂的时候会选一个检测存活的数据源作为主节点
    	"prototypeDs"
    ],
    "replicas":[//配置多个从节点
    	"xxxx"
    ],
    "maxCon":200,
    "name":"prototype",
    "readBalanceType":"BALANCE_ALL",
    "switchType":"SWITCH",
    "timer":{ //MySQL集群心跳周期,配置则开启集群心跳,Mycat主动检测主从延迟以及高可用主从切换
        "initialDelay": 30,
        "period":5,
        "timeUnit":"SECONDS"
    },
    readBalanceName:"BALANCE_ALL",
    writeBalanceName:"BALANCE_ALL",
}

#字段含义
# clusterType：集群类型，可选值SINGLE_NODE:单一节点，MASTER_SLAVE:普通主从，GARELA_CLUSTER:garela cluster/PXC集群，MHA：MHA 集群，MGR：MGR 集群

# readBalanceType：查询负载均衡策略
可选值:
BALANCE_ALL(默认值)
获取集群中所有数据源
BALANCE_ALL_READ
获取集群中允许读的数据源
BALANCE_READ_WRITE
获取集群中允许读写的数据源,但允许读的数据源优先
BALANCE_NONE
获取集群中允许写数据源,即主节点中选择

# switchType：切换类型
可选值:
NOT_SWITCH:不进行主从切换
SWITCH:进行主从切换


```

官网：https://www.yuque.com/books/share/6606b3b6-3365-4187-94c4-e51116894695/6fcb04592f6c08423f0041efb188963c



##### 5.逻辑库表（schema）

配置逻辑库表，实现分库分表，在`conf/schemas`路径

在mycat中创建一个逻辑库，就会对应生成一份schema文件

命令方式：{库名}.schema.json

![image-20220621235021673](https://fastly.jsdelivr.net/gh/le1024/image1/le/image-20220621235021673.png)

```bash
vim mysql.schema.json

# 库配置
{
    "schemaName": "mydb",
	"targetName": "prototype"
}
# schemaName：逻辑库名
# targetName：目的数据源或集群,targetName自动从prototype目标加载test库下的物理表或者视图作为单表,prototype必须是mysql服务器

#单表配置
{
    "schemaName": "mysql-test",
    "normalTables": {
    "role_edges": {
    "createTableSQL":null,//可选
    "locality": {
        "schemaName": "mysql",//物理库,可选
        "tableName": "role_edges",//物理表,可选
        "targetName": "prototype"//指向集群,或者数据源
	}
}
......
#详细配置见分库分表

```

