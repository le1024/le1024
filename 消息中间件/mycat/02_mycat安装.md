**Mycat安装**

****



#### 安装mycat



**1.下载安装包**

需要下载zip安装包以及jar包

zip包：http://dl.mycat.org.cn/2.0/install-template/mycat2-install-template-1.20.zip

jar包：http://dl.mycat.org.cn/2.0/1.21-release/



zip包下载完成后解压，把另一个下载的jar包放到zip解压的文件夹里面，文件夹路径是`mycat2-install-template-1.20\mycat\lib`

![image-20220620230946397](https://fastly.jsdelivr.net/gh/le1024/image1/le/image-20220620230946397.png)



**2.上传安装包**

然后将整个zip的文件夹上传到linux

![image-20220620232223616](https://fastly.jsdelivr.net/gh/le1024/image1/le/image-20220620232223616.png)



**3.修改文件夹及文件权限**

需要修改下面的文件权限为777，不然启动的时候会提示权限不足导致报错

![image-20220620232409529](https://fastly.jsdelivr.net/gh/le1024/image1/le/image-20220620232409529.png)

```bash
chmod 777 wrapper-linux-ppc-64 wrapper-linux-x86-32 wrapper-linux-x86-64 mycat
```



#### 配置mycat

**1.先自行安装好mysql及创建新的数据库和用户，或者直接用root也可以**

<strong style="color:red">Mycat 作为数据库中间件要和MySQL不在同一台服务器上面，且确保安装Mycat和MySQL的服务器可互相ping通，MyCat也是基于mysql操作的，所以Mycat的服务器也需要安装一个MySQL</strong>，嫌麻烦，用一台也行

```mysql
CREATE USER 'mycat'@'%' IDENTIFIED BY '123456';
--必须要赋的权限mysql8才有的，不是mysql8跳过
GRANT XA_RECOVER_ADMIN ON *.* TO 'root'@'%';
---视情况赋权限
GRANT ALL PRIVILEGES ON *.* TO 'mycat'@'%' ;
flush privileges
```



**2.修改mycat的prototype配置**

启动mycat之前需要配置`prototype`数据源所对应的mysql数据库配置，修改对应的用户，密码，连接url

就是配置上一步创建的用户，或者直接用root

```shell
vim conf/datasources/prototypeDs.datasource.json
```

![image-20220620233742897](https://fastly.jsdelivr.net/gh/le1024/image1/le/image-20220620233742897.png)

以自己实际环境为准，修改成自己的信息



**3.修改mycat的users配置**

在`../conf/users/`目录会有mycat用户的信息

![image-20220628091719921](https://fastly.jsdelivr.net/gh/le1024/image1/le/image-20220628091719921.png)

注意：这里面的用户信息是mycat登录的用户信息，跟上面创建的mysql用户不一样

![image-20220628091806006](https://fastly.jsdelivr.net/gh/le1024/image1/le/image-20220628091806006.png)

查看`root.user.json`，里面的用户名和密码就是登录时需要的



#### 启动mycat

```shell
cd bin/
./mycat start #启动mycat
```

其他操作命令：

```bash
./mycat stop #停止mycat
./mycat status #查看启动状态
./mycat console #前台运行
./mycat restart #重启服务
./mycat pause #暂停
./mycat install 添加到系统自动启动（暂未实现）
./mycat remove 取消随系统自动启动（暂未实现）
```

<strong style="color:Red">可能启动不了的问题：服务器内存不足，mycat默认配置的Xmx Xms很大，根据自己服务器改小点就行</strong>

```bash
vim ../conf/wrapper.conf
```

![image-20220626221251316](https://fastly.jsdelivr.net/gh/le1024/image1/le/image-20220626221251316.png)



**1.登录后台管理窗口**

此登录方式用于管理维护mycat

```shell
 #9066是mycat端口，端口记得开放或者关闭防火墙
 #用户名和密码是mycat的user.json里面的
 mysql -uroot -p123456 -P 9066
```

![image-20220620234600040](https://fastly.jsdelivr.net/gh/le1024/image1/le/image-20220620234600040.png)

```shell
help;
```

![image-20220620234655768](https://fastly.jsdelivr.net/gh/le1024/image1/le/image-20220620234655768.png)



**2.登录数据窗口**

此登录方式用于通过mycat查询数据，开发时选择这种方式访问mycat：

```shell
#8066是mycat端口，端口记得开放或者关闭防火墙
#用户名和密码是mycat的user.json里面的，且需要加ip，不然进入的mysql，不是mycat
mysql -uroot -p -P8066 -h192.168.171.142
```

![image-20220628212713984](https://fastly.jsdelivr.net/gh/le1024/image1/le/image-20220628212713984.png)



**窗口是和mysql一样的，但是他不是真正的mysql**

可以直接用navicat访问：

![image-20220628092203566](https://fastly.jsdelivr.net/gh/le1024/image1/le/image-20220628092203566.png)