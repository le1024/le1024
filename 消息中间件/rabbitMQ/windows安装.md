#### 1.安装erlang

下载地址：http://erlang.org/download/，打开一直往下拉，数据比较多，页面加载可能比较慢，新版的下载在最后面

一直下一步安装即可。

erlang版本和rabbitMQ版本也对应上



#### 2.安装rabbitMQ

下载地址：https://github.com/rabbitmq/rabbitmq-server/releases/download/v3.9.7/rabbitmq-server-3.9.7.exe

一直下一步安装即可。



#### 3.可视化插件

进入到安装的`sbin`目录，在此目录下进入到cmd执行：

```bash
rabbitmq-plugins enable rabbitmq_management
```

![image-20211016225312814](https://cdn.jsdelivr.net/gh/le1024/image1/le/image-20211016225312814.png)

<img src="https://cdn.jsdelivr.net/gh/le1024/image1/le/image-20211016225349505.png" alt="image-20211016225349505" style="zoom:50%;float:left;" />



#### 4.访问

启动服务

菜单快捷方式无法成功启动的话，用命令启动

```bash
rabbitmq-server start
```

![image-20211017194925681](https://cdn.jsdelivr.net/gh/le1024/image1/le/image-20211017194925681.png)

访问地址：http://localhost:15672/

默认账号：guest/guest