> 消费者分区分配策略

#### 分区分配策略

一个consumer group中有个多个topic，一个topic有多个partition，所以必然会涉及到partition的分配问题，即确定哪个partition由哪个消费者进行消费。



kafka有两种分配策略，`RoundRobin`和`Range`

#### RoundRobin策略

**按消费者组来分配**

多个分区会以轮询的方式，分配给多个消费者，因此消费者之间相差的最大分区数为1。

![image-20220125152840452](https://s2.loli.net/2022/04/20/lmvwk8MDH6nXQzq.png)



如果是消费者组订阅了多个主题，首先kafka将多个主题中的分区当做一个整体，然后根据`TopicAndPartition`方法将主题中的分区对象通过hash值的方式重新排个序，然后生成一个新的整体出来，再轮询到消费者组中消费者对象中

![image-20220125154930763](https://s2.loli.net/2022/04/20/c1dAMpxBV4DXKhk.png)

<strong style="color:green">需要保证消费者组中的消费者订阅的主题是一样，不然会出现消费者消费了不该属于他消费的数据，因为同一个消费者组中的订阅的主题，kafka使用RoundRbin是把消费者组订阅的主题当做一个整体重新排序后再进行轮询分配的</strong>

比如下面这两种情况：

![image-20220125160836561](https://s2.loli.net/2022/04/20/7xGbLCaV8BMZhzW.png)

A B两个消费者都有单独订阅主题，由于RoundRobin分区分配策略，Topic1的分区也会分配给B，Topic的分区也会分配给A



#### Range策略

**Range策略是默认的分区分配策略**

**针对主题来进行划分**，按范围来给消费者

![image-20220125162440197](https://s2.loli.net/2022/04/20/AYjTtmlSfzgDi7N.png)

然而range分配的弊端是：

![image-20220125204122348](https://s2.loli.net/2022/04/20/WxdNLHQaeujkcT5.png)



<strong style="color:red">当消费者中的个数发生变化，增多或者减少都会触发分区分配策略</strong>

