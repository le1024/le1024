> kafka工作流程



#### 工作流程

kafka中消息是以`topic`进行分类的，生产者生产消息，消费者消费消息，都是面向`topic`的。

`topic`是逻辑上的概念，而`partition`是物理上的概念，每一个`partition`对应一个`log`文件，该`log`文件中存储的是producer生产的数据。producer生产的消息会被不断的追加到该`log`文件末端，且每条数据都有自己的`offset`。消费者组中的每个消费者，都会实时记录自己消费到了哪个`offset`，以便出错恢复时，可以从上次的位置继续消费。

![image-20220106213035585](https://cdn.jsdelivr.net/gh/le1024/image1/le/image-20220106213035585.png)

进入到kafka的数据目录，再进入到对应的分区中就可以看到数据文件了：

![image-20220106213307978](https://cdn.jsdelivr.net/gh/le1024/image1/le/image-20220106213307978.png)





#### 文件存储

![image-20220106213608766](https://cdn.jsdelivr.net/gh/le1024/image1/le/image-20220106213608766.png)

由于生产者生产的消息会不断的被追加到log文件末尾，为防止log文件过大导致的数据定位效率低下，kafka采用了`分片`和`索引`机制，将每个partition分为多个`segment`。每个`segment`对应两个文件——`.log`文件和`.index`文件，这些文件位于同一个文件夹（topic名称+分区序号）下。比如创建topic：first，共3个分区，则会在kafka配置的数据存储目录创建：first-0，first-1，first-2三个文件夹，且每个文件夹下都会有如图所示的文件数据：

![image-20220106213307978](https://s2.loli.net/2022/04/20/m1NkJZvnIWAxyXG.png)

<strong style="color:red">index和log文件是以当前segment的第一条消息的offset命名的。</strong>

<strong style="color:red">".index"文件存储大量的索引信息，".log"文件存储大量的数据</strong>，索引文件中的元数据指向对应数据中message的物理偏移地址。



```java
//todo 画个图 index和log详解，画的可能不太对
```

![image-20220117123727102](https://cdn.jsdelivr.net/gh/le1024/image1/le/image-20220117123727102.png)

