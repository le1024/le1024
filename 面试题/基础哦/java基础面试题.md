#### 01_面向对象

对比面向过程，是两种不同的处理角度

面向过程更注重事情的每一个步骤及顺序，面向对象更注重事情有哪些参与者及各自需要做什么



#### 02_==和equals区别

==比较的是栈中的值，基本数量类型是变量值，引用类型是堆中内存对象的地址

equals：object中默认采用的是==比较，通常会重写



Object的默认equals：

```java
public boolean equals(Object obj) {
    return (this == obj);
}
```

String重写的equals：比较的是字符串的内容了

```java
public boolean equals(Object anObject) {
    if (this == anObject) {
        return true;
    }
    if (anObject instanceof String) {
        String anotherString = (String)anObject;
        int n = value.length;
        if (n == anotherString.value.length) {
            char v1[] = value;
            char v2[] = anotherString.value;
            int i = 0;
            while (n-- != 0) {
                if (v1[i] != v2[i])
                    return false;
                i++;
            }
            return true;
        }
    }
    return false;
}
```



#### 03_final对象



- 修饰类：表示类不可被继承
- 修饰方法：表示方法不可被子类覆盖，可以重载
- 修饰变量：**基本类型变量，不可变。引用类型变量，地址不可变，内容可变。**



##### 内部类引用的变量为什么是final

首先：内部类和外部类是处于同一级别的，内部类不会因为定义在方法中就会随着方法的执行完毕而销毁。

产生问题：当外部类的方式结束时，局部变量也就随着被销毁了，但是内部类对象还存在。这时候内部类对象会访问一个不存在的变量。

解决方式：将局部变量复制一份作为内部类的成员变量，当局部变量销毁后，内部类仍然可以访问它，实际上访问的就是局部变量的"copy"，**为了保证局部变量和内部类成员变量一样，所以就需要将局部变量设置为final**。



#### 04_ArrayList和LinkedList区别

**ArrayList**：基于动态数组，连续的内存存储，适合下标访问（随机访问），查询快。

扩容机制，因为数组长度固定，超出数组长度时就需要新建数组，将原来老数组的数据拷贝到新创建的数组中，如果不是尾插法就会涉及到元素的移动，如果使用尾插法并指定初始容量则会提升性能。

![image-20220417225526903](https://cdn.jsdelivr.net/gh/le1024/image1/le/WDFd2CzXrb7ugVH.png)



**LinkedList**：基于链表，可以存储在分散的内存中，适合数据插入和删除操作，不适合查询：需要逐一遍历。

遍历LinkedList必须使用iterator不能使用for循环，因为每次for循环体内通过get(i)取得某一元素时都需要对list重新遍历，性能消耗大。

也不要尝试通过indexOf等返回元素索引，并利用其进行遍历，因为使用indexOf会对list进行遍历，当结果为空时，会遍历整个list。



#### 05_hashCode和equals

hashCode的作用是获取哈希码，确定对象在哈希表中的索引位置。

##### 为什么要用hashCode

以hashset来说，hashset会计算对象的hashCode值判断对象是否存在，如果没有，hashset会确定对象没有重复出现。如果有，会调用equals方法判断两个对象是否相同，如果相同，就不会存到hashset中。



##### 为什么重写equals，还是重写hashCode

**提高效率。**采取重写hashCode方法，先进行hashCode的比较，如果不同，那么就没必要进行equals的比较了，大大的减少了equals的比较次数

**保证是同一个对象。**保证在equals相同的情况下hascode必定相同。



equals相等的两个对象，hashCode一定相等。

hascode不相等的，equals一定不相等。

hashCode相等，equals可能相等，可能不相等。



#### <font color="green">06_hashMap扩容机制</font>