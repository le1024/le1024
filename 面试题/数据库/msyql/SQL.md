> 各种SQL编写

#### SQL1：查找最晚入职的员工所有信息

有一个员工employees表简况如下:

![](https://cdn.jsdelivr.net/gh/le1024/image1/le/0BFB4D140D9C3E92AF681D9F9CB92D55)

请你查找employees里最晚入职员工的所有信息，以上例子输出如下:

![](https://cdn.jsdelivr.net/gh/le1024/image1/le/D2ABA1E2F5834850B16146F168AC5476)

**思路一：**

- 知识点
  - ORDER BY 根据指定的列对结果集进行排序，默认按照升序，降序 ORDER BY DESC
  - LIMIT(m, n) 从第 m + 1 行开始取 n 条记录
- 最晚员工自然是 hire_data，最晚可以用排序 ORDER BY DESC 降序来得到，然后是获取第一条记录，这样理论上是有 bug 的，因为 hire_data 可能有多个相同的记录

```sql
SELECT * FROM employees order by hire_date desc limit 0,1
```

**思路二：**

- 知识点
  - MAX 取最大值
- 先获取所有最晚的 hire_date，然后把与其相等的记录取出来，这个可以取多条，因此必定是正确的

```mysql
SELECT * FROM employees WHERE hire_date == (SELECT MAX(hire_date) FROM employees)
```



#### SQL2：查找入职员工时间排名倒数第三的员工所有信息

