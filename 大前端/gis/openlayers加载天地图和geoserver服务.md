> Openlayers加载天地图和geoserver服务

#### 前提

- [x] 下载openlayers js和css文件

- [x] 已申请天地图key
- [x] 已通过geoserver发布服务



#### 开始

```javascript
<!DOCTYPE>
<html lang="en">
	<head>
	    <meta charset="UTF-8">
	    <meta name="viewport" content="width=device-width, initial-scale=1.0">
	    <title>hefei</title>
	    <link rel="stylesheet" href="./css/ol.css" />
	    <script src="./js/ol.js"></script>
	</head>
	
	<body>
		<div id="map"></div>
		<script>
			var layer1 = new ol.layer.Tile({
						source: new ol.source.XYZ({
							attributions: '<button style="height:20px; width:100px;" onclick="viewRoads()">显示/隐藏路网</button>',
							url: 'https://t0.tianditu.gov.cn/DataServer?T=vec_w&x={x}&y={y}&l={z}&tk=你的天地图key',
						})
					});
			var layer2 = new ol.layer.Tile({
						source: new ol.source.XYZ({
							url: 'https://t0.tianditu.gov.cn/DataServer?T=cva_w&x={x}&y={y}&l={z}&tk=你的天地图key',
						})
					});
			var layer3 = new ol.layer.Tile({
						source: new ol.source.TileWMS({
							url: 'http://localhost:8084/geoserver/hefei/wms',
							params: {
								'LAYERS': 'hefei:hefei2_roads',
								'TILED': false
							},
							serverType: 'geoserver'
						})
					});
			
			var map = new ol.Map({
				target: 'map',
				layers: [
					
				],
				view: new ol.View({
					center: ol.proj.transform([117.233423,31.826481], "EPSG:4326", "EPSG:3857"),
					zoom: 11,
					//1.设置缩放级别为整数 
					constrainResolution: true, 
					//2.关闭无级缩放地图
					smoothResolutionConstraint: false
				})
			});
			
			map.addLayer(layer1);
			map.addLayer(layer2);
			map.addLayer(layer3);
			
			var flag = true;
			
			function viewRoads() {
				if (flag) {
					this.map.removeLayer(layer3);	
					flag = false;
				} else {
					this.map.addLayer(layer3);
					flag = true;
				}
			}
		</script>
	</body>
</html>
```



![image-20220622090115169](https://fastly.jsdelivr.net/gh/le1024/image1/le/image-20220622090115169.png)

![image-20220622090150298](https://fastly.jsdelivr.net/gh/le1024/image1/le/image-20220622090150298.png)

