就简单记下流程，具体操作查看别的文档或者再百度

1.安装postgresql 

2.安装postgis 

3.安装geoserver 

4.网格数据导入postgis  (可以使用qgis，或者postgis shapefile)

5.geoserver发布postgis数据

6.学习openlayers

7.openlayers增删改查geoserver发布的图层（geoserver的服务要开启相关权限）

8.修改geoserver发布的图层样式（可以使用qgis或者udig）