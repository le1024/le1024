**geoserver发布自定义样式**

****

geoserver服务有内置的一些样式布局，但是都不太好看，可以通过`uDig`软件来编辑样式后再发布到geoserver中



> udig不知道咋回事，图层不显示修改样式没法预览，可以先用udig试试，如果可以显示的话，就按本教程操作，如果不可以显示的话就使用qgis软件编辑样式



**前提：**

- [x] 已有地图数据发布到geoserver或者postgis数据库有数据或者本地有shapefile文件
- [x] [udig软件下载](http://udig.refractions.net/download/)



**udig使用**

#### 1.添加地图文件

File > New > New Layer

![image-20220527170229665](img.assets/image-20220527170229665.png)



#### 2.选择数据源

如果数据发布到了geoserver可以选择`WebFeature server`添加数据

如果是本地数据，选择`Files`添加数据，本地文件最好是英文名

如果数据在POSTGIS中，选择`PostGIS`添加数据，这里使用`PostGIS`方式

<img src="img.assets/image-20220527170532520.png" alt="image-20220527170532520" style="zoom:50%; float:left" />![image-20220527170959857](img.assets/image-20220527170959857-1657767063664.png)

填写自己的postgis数据库连接信息，完成连接即可

<img src="img.assets/image-20220527170959857.png" alt="image-20220527170959857" style="zoom:50%; float:left;" />

获取到数据

<img src="img.assets/image-20220527171145511.png" alt="image-20220527171145511" style="zoom:50%; float:left" />



#### 3.图层预览

勾选上某个图层，即可完成预览

![image-20220527171307352](img.assets/image-20220527171307352.png)



#### 4.图层编辑

右键图层，选择Change Style...

![image-20220527171343616](img.assets/image-20220527171343616.png)

![image-20220527171859983](img.assets/image-20220527171859983.png)

下面的话，网上复制的

> https://blog.csdn.net/qq_30665009/article/details/121434563
>
> 在Style Editor中，右边目录树是针对显示操作。Lines、Points、Polygons是根据图层具体属性点线面来进行操作。Simple feature是简单设置，Filter是过滤显示图层的哪些图形。Theme是图层默认主题，可修改设置，默认就行。Xml是前面进行修改操作其自动生成的。在geoserver中可以复制粘贴制作style。注：但要将编码格式改为GB2312。



**Lines、Points、Polygons是根据图层本身具体的属性来进行操作，这里选择的监督网格是Polygons属性图层，所以在Polygons选项中操作**

`Border`设置边框的样式，宽度、透明度、颜色等

![image-20220527173211657](img.assets/image-20220527173211657.png)

`Fill`设置填充的样式

![image-20220527173306249](img.assets/image-20220527173306249.png)

`Label`设置标注文字

![image-20220527173331830](img.assets/image-20220527173331830.png)

**显示文字是中文时一定要将font中set font字体选择为中文字体，如宋体等，并将“脚本(R)”修改为中文GB2312。**

![image-20220527173517492](img.assets/image-20220527173517492.png)

最后呈现的效果

![image-20220527173549451](img.assets/image-20220527173549451.png)



#### 5.geoserver添加样式

刚刚设置的样式完成后，就可以添加到geoserver中使用了

如果样式加载不出来，这个编码GBK GB2312 UTF-8都试试

![image-20220527173801682](img.assets/image-20220527173801682.png)

复制样式的XML数据，记得将UTF-8改为**GB2312**

打开geoserver，添加新样式：

![image-20220527173946879](img.assets/image-20220527173946879.png)

样式名称自己设置，工作区按自己的选择

![image-20220527174030881](img.assets/image-20220527174030881.png)



#### 6.图层设置样式

在图层选项中，找到需要设置的图层，点击图层进行修改

![image-20220527174210390](img.assets/image-20220527174210390.png)

选择发布：

![image-20220527174317094](img.assets/image-20220527174317094.png)

![image-20220527174415798](img.assets/image-20220527174415798.png)

保存！



#### 7.验证

![image-20220527174517609](img.assets/image-20220527174517609.png)

编码的问题，自己排查，有的直接使用UTF-8是可以的，不行的话，用GB2312



#### 8.问题

> 发布后，图层无法正常显示出来

不知道具体原因，反正本地环境发布是可以的，但是换个环境就不行了，可能是geoserver版本不一样吧

暂时解决方式：

使用udig自定义样式，不设置`Labels`，或者用qgis编辑

<img src="img.assets/image-20220530104511299.png" alt="image-20220530104511299" style="zoom:75%; float:left;" />

