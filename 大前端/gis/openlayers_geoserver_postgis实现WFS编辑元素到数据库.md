**openlayers geoserver postgis 实现WFS编辑元素到数据库**

****

geoserver、postgis的安装这里不介绍了，自行安装好



#### 准备工作



##### 创建表

首先postgis肯定需要有对应的数据库及数据表来存储openlayers绘制的元素数据，建表的具体字段根据实际业务来，这里简单的创建一下，数据库的创建这里不创建了，参考postgis安装

主键和其他字段都可以随意创建，根据自己需要，`geometry`该字段不要随意更改，且类型是`"public"."geometry"`

```sql
/*绘制线元素存储表*/
CREATE TABLE "public"."hefei_line" (
  "id_0" SERIAL PRIMARY KEY,
  "geometry" "public"."geometry",
  "id" varchar(50) COLLATE "pg_catalog"."default",
  "type" varchar(50) COLLATE "pg_catalog"."default"
);
/*绘制面元素存储表*/
CREATE TABLE "public"."hefei_polygon" (
  "id_0" SERIAL PRIMARY KEY,
  "geometry" "public"."geometry",
  "id" varchar(50) COLLATE "pg_catalog"."default",
  "type" varchar(50) COLLATE "pg_catalog"."default"
);

//或者其他业务表，根据自己需要创建
```



##### geoserver发布数据

刚新建数据表成功后，需要通过geoserver给他发布一下，geoserver发布postgis参考：xxxxxxx

![image-20220614185800107](img.assets/image-20220614185800107.png)



#### 业务代码

